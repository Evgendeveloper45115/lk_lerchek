<?php
/**
 * OrderNumber gm40_w1_pc0_1475143772
 * gm40 GroupMember->id
 * w1 week 1
 * pc0 personal_curator
 * 1475143772 Unique
 */

namespace app\controllers;


use app\helpers\My;
use app\models\GroupMember;
use app\models\PaymentLer;
use app\models\Ref;
use app\models\Retail;
use app\models\User;
use app\services\payment\IPaymentGateway;
use app\services\payment\PaymentData;
use app\services\payment\PaymentFactory;
use app\services\payment\Yandex;
use RetailCrm\ApiClient;
use RetailCrm\Exception\CurlException;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PaymentController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
//                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionCheck()
    {

        My::log('new-check', 'hello');
        My::log('request-check', json_encode(Yii::$app->request));
        if (!Yii::$app->request->isPost) {
            throw new NotFoundHttpException();
        }
        try {
            My::log('new-check2', 'hello');
            $paymentGateway = PaymentFactory::createPaymentGateway($this->getPaymentType());
            $paymentData = $paymentGateway->getPaymentData(Yii::$app->request);
            My::log('test-check', $this->isValidOrder($paymentData->amount));
            My::log('render', $paymentGateway->renderSuccessCheckView());

            if ($this->isValidOrder($paymentData->amount) && $paymentGateway->isValidPayment(Yii::$app->request)) {
                $this->setDataBaseVal($paymentData);
                return $paymentGateway->renderSuccessCheckView();
            } else {
                return $paymentGateway->renderFailedCheckView();
            }
        } catch (\Exception $e) {
            My::log('exception_check', $e->getMessage());
        }
    }

    public function setDataBaseVal($paymentData)
    {
        /**
         * @var $user User
         * @var $paymentData PaymentData
         * @var $member GroupMember
         */
        $user = User::find()->where(['email' => $paymentData->email])->one();
        $types = My::getType($paymentData->manWoman);
        if (!is_array($types) && $user->getGroupMemberGuest($types)->one()) {
            $member = $user->getGroupMemberGuest($types)->one();
            $member->invoiceId = $paymentData->invoiceId;
            $member->save(false);
        } else {
            if ($paymentData->email_man !== null) {
                My::log('couple', $types);
                My::log('couple', $types[0]);
                $member = $user->getGroupMemberGuest($types[0])->one();
                $member->invoiceId = $paymentData->invoiceId;
                $member->save(false);
                $user = User::find()->where(['email' => $paymentData->email_man])->one();
                My::log('member', json_encode($member));
                $member = $user->getGroupMemberGuest($types[1])->one();
                $member->invoiceId = $paymentData->invoiceId;
                $member->save(false);
                My::log('user_man', json_encode($user));
            }
        }
    }

    public function actionAviso()
    {
        /**
         * @var $member GroupMember
         * @var $user User
         * @var $payment PaymentLer
         * @var $retail Retail
         */
        if (!Yii::$app->request->isPost) {
            throw new Exception('Данные должны быть в виде POST');
        }
        My::log('new-aviso', 'try hello');
        $paymentGateway = PaymentFactory::createPaymentGateway($this->getPaymentType());
        $paymentData = $paymentGateway->getPaymentData(Yii::$app->request);

        try {
            if ($this->isValidOrder($paymentData->amount) && $paymentGateway->isValidPayment(Yii::$app->request)) {
                $members = GroupMember::find()->where(['invoiceId' => $paymentData->invoiceId])->all();
                My::log('member', json_encode($members));
                if (!empty($members)) {
                    foreach ($members as $key => $member) {
                        $mail = 'generate-link-' . My::$type_program[$member->type_program];
                        $subject = Yii::$app->params['generate-link-' . My::$type_program[$member->type_program]];
                        $payment = $member->payment;
                        if ($payment === null) {
                            $payment = new PaymentLer();
                        }
                        $payment->date_payment = date('Y-m-d H:i:s');
                        $payment->member_id = $member->id;
                        $payment->continue_program = 0;
                        $payment->cost = (int)$paymentData->amount;
                        $payment->doplata = $paymentData->continue;
                        $payment->shop_id = $paymentData->shop_id;

                        My::log('ref', $paymentData->ref);
                        My::log('ref', $member->user->email);
                        My::log('ref', $payment->isNewRecord);
                        My::log('ref', count($member->user->groupMembers));
                        if ($paymentData->ref != null && $payment->isNewRecord && $key == 0 && count($member->user->groupMembers) == 1) {
                            $ref_user = User::findOne(['email' => $paymentData->ref]);
                            $ref = new Ref();
                            $ref->user_id = $ref_user->id;
                            $ref->unique_id = md5(mktime());
                            $ref->sum = '+' . $payment->cost * 0.10;
                            $ref->status = Ref::NEW_PAYMENT;
                            $ref->status_export = Ref::NO_EXP;
                            $ref->date = date('Y-m-d');
                            $ref->member_id = $member->id;
                            if ($ref->save(false)) {
                                My::log('ref2', $payment->isNewRecord);
                            } else {
                                My::log('ref11', $ref->getErrors());
                            }
                        }
                        if (!$payment->save()) {
                            My::log('payment-error', 'Пусто клиент ' . json_encode($payment->getErrors()));
                        } else {
                            if ($paymentData->ga_param) {
                                $paymentData->ga_param = preg_replace('/^\d+?\.\d+?\.(.*)/', '$1', $paymentData->ga_param);
                            } else {
                                $paymentData->ga_param = 777777;
                            }
                            if (!$paymentData->continue) {
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, 'https://www.google-analytics.com/collect?v=1&t=transaction&tid=UA-61339947-13&cid=' . $paymentData->ga_param . '&ti=' . ($paymentData->retail_id ? $paymentData->retail_id : $paymentData->orderNumber) . '&ta=Lerchek%20marafon&tr=' . (int)$paymentData->amount . '&cu=RUB');
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                $result = curl_exec($ch);
                                curl_close($ch);
                                My::log('analytics', $result);
                                My::log('ga_param', $paymentData->ga_param);
                            }

                            /* $oauthToken = 'dsERGE4564GBFDG34t3GDEREBbrgbdfbg4564DG3'; // OAuth-токен
                             $counterId = 68513506; // идентификатор счетчика
                             $client_id_type = 'CLIENT_ID'; // или USER_ID

                             $metrikaOffline = new \Meiji\YandexMetrikaOffline\Conversion($oauthToken);
                             $metrikaConversionUpload = $metrikaOffline->upload($counterId, $client_id_type);
                             $metrikaConversionUpload->comment(My::$type_program_rus[$member->type_program]); // Опционально
                             $time = explode('.', $paymentData->ga_param);
                             $metrikaConversionUpload->addConversion($paymentData->cid_param, 'purchase', $time[1], $payment->cost, 'RUB'); // Добавяем ещё конверсию

                             $uploadResult = $metrikaConversionUpload->send();*/


                        }
                        if ($paymentData->continue) {
                            $mail = 'continue-program';
                            $subject = 'Спасибо за покупку продления марафона!';
                        }
                        if ($member->phone && !$paymentData->continue) {
                            $this->sendSms($member);
                        }
                        My::sendEmail($member, $mail, $subject);
                        if ($paymentData->retail_id && My::isGirl($member->type_program)) {
                            $this->retailSave($paymentData, $member->user, $member->type_program);
                        }
                    }
                } else {
                    My::log('empty-client', 'Пусто клиент ' . $paymentData->invoiceId);
                }
                return $paymentGateway->renderSuccessView();
            } else {
                My::log('error_payment_aviso', $this->isValidOrder($paymentData->amount));
                My::log('error_payment_aviso', $paymentGateway->isValidPayment(Yii::$app->request));
                My::log('error_payment_aviso', $member->user->email);
                return $paymentGateway->renderFailedView();
            }
        } catch (\Exception $e) {
            My::log('error_payment_all', $e->getMessage());
            return $paymentGateway->renderFailedView();
        }
    }

    /**
     * @param GroupMember $member
     */
    private function sendSms(GroupMember $member)
    {
        $text = 'Спасибо за покупку марафона! Женская анкета: https://lk.lerchek.ru/marafon/signup?email=' . $member->user->email . '&type=' . $member->type_program;
        if (!My::isGirl($member->type_program)) {
            $text = 'Спасибо за покупку марафона! Мужская анкета: https://lk.lerchek.ru/marafon/signup?email=' . $member->user->email . '&type=' . $member->type_program;
        }
        $smsaero_api = new \app\helpers\SmsaeroApiV2('admin@lerchek.ru', '2r0dxVxGZCuxg1SliJwk3fBmcfz7', 'Lerchek'); // api_key из личного кабинета
        $smsaero_api->send($member->phone, $text, 'DIRECT');
    }

    private function retailSave(PaymentData $paymentData, User $user, $type_program)
    {
        /**
         * @var $member GroupMember
         */
        $client = new ApiClient(
            'https://letique.retailcrm.ru',
            'xgd1broIFkagoUk5c13Y9CLFEFShmarb',
            ApiClient::V5
        );
        $member = $user->getGroupMemberGuest($type_program)->one();
        try {
            $response = $client->request->ordersPaymentCreate(
                [
                    'order' => [
                        'id' => $paymentData->retail_id
                    ],
                    'amount' => $paymentData->payment_amount,
                    'paidAt' => date('Y-m-d H:i:s'),
                    'status' => 'paid',
                    'type' => 'bank-card',
                ],
                'lerchek-ru'
            );
            if ($response->isSuccessful()) {
                $member->retail->date_payment = date('Y-m-d H:i:s');
                $member->retail->payment_status = 1;
                $member->retail->save(false);
                return $response->id;
            } else {
                $member->retail->status = false;
                $member->retail->date_payment = date('Y-m-d H:i:s');
                $member->retail->error_text = $response->getResponseBody();
                $member->retail->save(false);
                My::log('retail-errors', $response->getResponseBody());
            }
        } catch (CurlException $e) {
            if ($member && $member->retail) {
                $member->retail->status = false;
                $member->retail->date_payment = date('Y-m-d H:i:s');
                $member->retail->error_text = json_encode($e->getMessage());
                $member->retail->save(false);
                My::log('retail-errors', $e->getMessage());
            }
        } catch (\Exception $e) {
            $member->retail->status = false;
            $member->retail->date_payment = date('Y-m-d H:i:s');
            $member->retail->error_text = $e->getMessage();
            $member->retail->save(false);
            My::log('retail-errors', $e->getMessage());
        }
    }

    private function getPaymentType()
    {
        return PaymentFactory::getPaymentTypeFromRequest(Yii::$app->request);
    }

    /**
     * @param $sum
     * @return bool
     */
    private function isValidOrder($sum)
    {
        if ($sum >= 1900) {
            return true;
        }
        return false;
    }


    public function actionSuccess()
    {
        $this->layout = false;
        if (!Yii::$app->user->isGuest) {
            return $this->render('success-user', ['user' => Yii::$app->user->identity]);
        }

        $user_id = Yii::$app->request->get('user_id');
        $user = User::findOne($user_id);
        return $this->render('success', ['user' => $user]);
    }

    public function actionFail()
    {
        $this->layout = 'payment';
        return $this->render('fail');
    }

    public function actionTinkoff()
    {
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'tinkoff.log';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        $str = json_decode(file_get_contents("php://input"));
        file_put_contents($file, file_get_contents("php://input"), FILE_APPEND);
        file_put_contents($file, 1, FILE_APPEND);

        file_put_contents($file, json_encode($_POST), FILE_APPEND);
        file_put_contents($file, 2, FILE_APPEND);
        file_put_contents($file, json_encode($_GET), FILE_APPEND);
        file_put_contents($file, 3, FILE_APPEND);

    }
}
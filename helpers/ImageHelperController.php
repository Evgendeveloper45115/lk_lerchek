<?php
/**
 * /* Значение 1 соответствует картинке в формате GIF;
 * /* Значение 2 соответствует картинке в формате JPEG;
 * /* Значение 3 соответствует картинке в формате PNG;
 */

namespace app\helpers;


class ImageHelperController
{
    public static function is_image($filename)
    {
        $is = getimagesize($filename);
        if (!$is) return false;
        elseif (!in_array($is[2], array(1, 2, 3))) return false;
        else return true;
    }

    public static function imageCreateFromAny($filepath)
    {
        $type = exif_imagetype($filepath);
        $allowedTypes = array(
            1,  // [] gif
            2,  // [] jpg
            3,  // [] png
        );
        if (!in_array($type, $allowedTypes)) {
            return false;
        }
        switch ($type) {
            case 1 :
                $im = imagecreatefromgif($filepath);
                break;
            case 2 :
                $im = imagecreatefromjpeg($filepath);
                break;
            case 3 :
                $im = imagecreatefrompng($filepath);
                break;
            default:
                $im = false;
        }
        return [$im, $type];
    }
}
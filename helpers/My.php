<?php

namespace app\helpers;


use app\components\MyUrlManager;
use app\models\ContentType;
use app\models\EveryDay;
use app\models\Group;
use app\models\Popups;
use app\models\User;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Imagine\Imagick\Imagine;
use pendalf89\imageresizer\Image;
use pendalf89\imageresizer\ImageResizer;
use phpDocumentor\Reflection\Types\Self_;
use Yii;
use yii\db\Connection;
use yii\helpers\VarDumper;

class My extends ImageResizer
{
    /** @var  Connection */
    static $_db;

    const CONCURS = 1;
    const BASE = 2;
    const FINAL_VOTE = 3;
    const FOR_GIRL = 1;
    const FOR_MAN = 2;
    const PRESS_FOR_GIRL = 3;
    const PRESS_FOR_MAN = 4;
    const FOR_GIRL_CLONE = 5;
    const FOR_MAN_CLONE = 6;
    const FOR_GIRL_30_10 = 7;
    const FOR_MAN_30_10 = 8;
    const FOR_GIRL_04_01_2020 = 9;
    const FOR_MAN_04_01_2020 = 10;
    const FOR_GIRL_16_02_2021 = 11;
    const FOR_MAN_16_02_2021 = 12;
    const FOR_GIRL_22_03_2021 = 13;
    const FOR_MAN_22_03_2021 = 14;
    const COUPLE = '1-2';
    const COUPLE_PRESS = '3-4';
    const COUPLE_CLONE = '5-6';
    const COUPLE_30_10 = '7-8';
    const COUPLE_04_01_2020 = '9-10';
    const COUPLE_16_02_2021 = '11-12';
    const COUPLE_22_03_2021 = '13-14';

    public static $ratings = [
        self::CONCURS => 'Конкурс',
        self::BASE => 'База',
        self::FINAL_VOTE => "Финал",
    ];
    public static $main_program_girl = [
        self::FOR_GIRL,
        self::FOR_GIRL_CLONE,
        self::FOR_GIRL_30_10,
        self::FOR_GIRL_04_01_2020,
        self::FOR_GIRL_16_02_2021,
        self::FOR_GIRL_22_03_2021,
    ];

    public static $girl_clone = [
        self::FOR_GIRL_30_10,
        self::FOR_GIRL_04_01_2020,
        self::FOR_GIRL_16_02_2021,
        self::FOR_GIRL_22_03_2021,
        15,
        17,
        19,
        21
    ];
    public static $man_clone = [
        self::FOR_MAN_30_10,
        self::FOR_MAN_04_01_2020,
        self::FOR_MAN_16_02_2021,
        self::FOR_MAN_22_03_2021,
        16,
        18,
        20,
        22
    ];
    public static $main_program_man = [
        self::FOR_MAN,
        self::FOR_MAN_CLONE,
        self::FOR_MAN_30_10,
        self::FOR_MAN_04_01_2020,
        self::FOR_MAN_16_02_2021,
        self::FOR_MAN_22_03_2021,
    ];
    public static $type_program = [
        1 => 'for-girl',
        2 => 'for-man',
        3 => 'press-for-girl',
        4 => 'press-for-man',
        5 => 'for-girl',
        6 => 'for-man',
        7 => 'for-girl',
        8 => 'for-man',
        9 => 'for-girl',
        10 => 'for-man',
        11 => 'for-girl',
        12 => 'for-man',
        13 => 'for-girl',
        14 => 'for-man',
    ];
    public static $curators_high = [
        'nsaproshina@gmail.com',
        'Khlebova.e@gmail.com',
        'admin@lerchek.ru',
        'V_shakina@mail.ru',
    ];
    public static $curators_password = [
        'kelemen1506@gmail.com',
        'tomka3005@gmail.com',
        'olga.kudiyarova@mail.ru',
        'marryklim@yandex.ru',
        'savinairina77@gmail.com',
        'ld14safea@mail.ru',
        'nsaproshina@gmail.com',
        'Khlebova.e@gmail.com',
        'admin@lerchek.ru',
        'V_shakina@mail.ru',
    ];
    public static $type_voices = [
        1 => 'На конкурс',
        2 => 'В базу лучших участниц',
    ];
    public static $unlimaccess = [
        1 => My::FOR_GIRL,
        2 => My::FOR_MAN,
        3 => My::PRESS_FOR_GIRL,
        4 => My::PRESS_FOR_MAN,
        5 => self::FOR_GIRL_CLONE,
        6 => self::FOR_MAN_CLONE,
    ];
    public static $type_program_rus = [
        14 => 'Мужской марафон (22.03.21 - 25.04.21)',
        13 => 'Женский марафон (22.03.21 - 25.04.21)',
        12 => 'Мужской марафон (15.02.21 - 21.03.21)',
        11 => 'Женский марафон (15.02.21 - 21.03.21)',
        10 => 'Мужской марафон (11.01.21 - 15.02.21)',
        9 => 'Женский марафон (11.01.21 - 15.02.21)',
        8 => 'Мужской марафон (30.11.20 - 04.01.21)',
        7 => 'Женский марафон (30.11.20 - 04.01.21)',
        6 => 'Мужской марафон(26.10.20 - 23.11.20)',
        5 => 'Женский марафон(26.10.20 - 23.11.20)',
        4 => 'Мужской идеальный пресс (28.09.20 - 25.10.20)',
        3 => 'Женский идеальный пресс (28.09.20 - 25.10.20)',
        2 => 'Мужской марафон (28.09.20 - 25.10.20)',
        1 => 'Женский марафон (28.09.20 - 25.10.20)',
    ];
    public static $type_program_rus_moder = [
        10 => 'Мужской марафон (11.01.21 - 15.02.21)',
        9 => 'Женский марафон (11.01.21 - 15.02.21)',
    ];
    public static $type_program_rus_menu = [
        14 => '22.03.21 - 25.04.21',
        13 => '22.03.21 - 25.04.21',
        12 => '15.02.21 - 21.03.21',
        11 => '15.02.21 - 21.03.21',
        10 => '11.01.21 - 15.02.21',
        9 => '11.01.21 - 15.02.21',
        8 => '30.11.20 - 04.01.21',
        7 => '30.11.20 - 04.01.21',
        6 => '26.10.20 - 23.11.20',
        5 => '26.10.20 - 23.11.20',
        4 => '28.09.20 - 25.10.20',
        3 => '28.09.20 - 25.10.20',
        2 => '28.09.20 - 25.10.20',
        1 => '28.09.20 - 25.10.20',
    ];

    static function typeProgram()
    {

    }

    /**
     * @param $member
     * @param $view
     * @param $subject
     * @param null $password
     * @return bool|string
     */
    static function sendEmail($member, $view, $subject, $password = null)
    {
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'log-email-sender.txt';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        $post_value = [
            'api_key' => '69e8txindd4yyjiiu3h3ftu6rg4oh7y7ptogy3ya',
            'domain' => 'lerchek.ru',
            'message' => [
                'body' => [
                    'html' => Yii::$app->controller->renderPartial('@app/mail/' . $view, ['member' => $member, 'password' => $password])
                ],
                'subject' => $subject,
                'from_email' => 'info@lerchek.ru',
                'from_name' => 'Ler_chek',
                'recipients' => [
                    [
                        'email' => $member->user->email,
                    ],
                ],
            ],
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://eu1.unione.io/ru/transactional/api/v1/email/send.json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_value));
        $result = curl_exec($ch);
        curl_close($ch);
        file_put_contents($file, PHP_EOL . $result, FILE_APPEND);
        return $result;

    }

    static function sendEmailCurator($email, $view, $subject, $password = null)
    {
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'log-email-sender.txt';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        $post_value = [
            'api_key' => '69e8txindd4yyjiiu3h3ftu6rg4oh7y7ptogy3ya',
            'domain' => 'lerchek.ru',
            'message' => [
                'body' => [
                    'html' => Yii::$app->controller->renderPartial('@app/mail/' . $view, ['email' => $email, 'password' => $password])
                ],
                'subject' => $subject,
                'from_email' => 'info@lerchek.ru',
                'from_name' => 'Ler_chek',
                'recipients' => [
                    [
                        'email' => $email,
                    ],
                ],
            ],
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://eu1.unione.io/ru/transactional/api/v1/email/send.json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_value));
        $result = curl_exec($ch);
        curl_close($ch);
        file_put_contents($file, PHP_EOL . $result, FILE_APPEND);
        return $result;

    }

    static function isGirl($type = null)
    {
        if ($type === null) {
            $type = Yii::$app->getUser()->getIdentity()->session_type;
        }
        if (
            $type == self::FOR_GIRL
            || $type == self::PRESS_FOR_GIRL
            || $type == self::FOR_GIRL_30_10
            || $type == self::FOR_GIRL_04_01_2020
            || $type == self::FOR_GIRL_16_02_2021
            || $type == self::FOR_GIRL_22_03_2021
            || $type == self::COUPLE_30_10
            || $type == self::COUPLE_04_01_2020
            || $type == self::COUPLE_16_02_2021
            || $type == self::COUPLE_22_03_2021
            || $type == self::FOR_GIRL_CLONE
            || $type == self::COUPLE_PRESS
            || $type == self::COUPLE
        ) {
            return true;
        }
        return false;
    }


    static function isPressProgram($type = null)
    {
        if ($type === null) {
            $type = Yii::$app->getUser()->getIdentity()->session_type;
        }
        if ($type == self::PRESS_FOR_MAN || $type == self::PRESS_FOR_GIRL || $type == self::COUPLE_PRESS) {
            return true;
        }
        return false;
    }

    static function isCloneProgram($type = null)
    {
        if ($type === null) {
            $type = Yii::$app->getUser()->getIdentity()->session_type;
        }
        if ($type == self::FOR_MAN_CLONE || $type == self::FOR_GIRL_CLONE || $type == self::COUPLE_CLONE) {
            return true;
        }
        if ($type == self::FOR_GIRL_30_10 || $type == self::FOR_MAN_30_10 || $type == self::COUPLE_30_10) {
            return true;
        }
        if ($type == self::FOR_GIRL_04_01_2020 || $type == self::FOR_MAN_04_01_2020 || $type == self::COUPLE_04_01_2020) {
            return true;
        }
        if ($type == self::FOR_GIRL_16_02_2021 || $type == self::FOR_MAN_16_02_2021 || $type == self::COUPLE_16_02_2021) {
            return true;
        }
        if ($type == self::FOR_GIRL_22_03_2021 || $type == self::FOR_MAN_22_03_2021 || $type == self::COUPLE_22_03_2021) {
            return true;
        }
        return false;
    }

    public static function getImage($model, $img, $dir, $link)
    {
        $preview = [];
        if (!$model->isNewRecord && $img) {
            $preview = [
                'initialPreview' => [
                    '/uploads/' . $dir . '/' . $img,
                ],
                'initialPreviewAsData' => true,
                'deleteUrl' => \yii\helpers\Url::toRoute([$link, 'id' => $model->id]),
                'initialCaption' => $img,
                'initialPreviewConfig' => [
                    ['caption' => '/uploads/' . $dir . '/' . $img, 'size' => '873727'],
                ],
            ];
        }
        return $preview;
    }

    public static function createTypeProgram($class, $id)
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        $className = explode('\\', $class);
        $model = ContentType::find()->where(['type_program' => Yii::$app->user->identity->session_type, 'class_name' => $class, 'relation_id' => $id])->one();
        if ($model === null) {
            if (isset($_POST[$className[2]]['is_clone']) && $user->role == User::ROLE_ADMIN) {
                if (isset($_POST[$className[2]]['is_clone']) && $_POST[$className[2]]['is_clone'] == 1) {
                    foreach (My::$type_program as $type => $name) {
                        $model = new ContentType();
                        $model->type_program = $type;
                        $model->class_name = $class;
                        $model->relation_id = $id;
                        $model->save();
                    }
                    return true;
                }
                $model = new ContentType();
                $model->type_program = Yii::$app->user->identity->session_type;
                $model->class_name = $class;
                $model->relation_id = $id;
                return $model->save();
            }
            $model = new ContentType();
            $model->type_program = Yii::$app->user->identity->session_type;
            $model->class_name = $class;
            $model->relation_id = $id;
            return $model->save();
        }
        return true;
    }

    public static function getEveryDayTask()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->getUser()->getIdentity();
        return EveryDay::find()->getContent()->where(['day' => $user->groupMember->getCurrentDay()])->one();

    }

    /**
     * @return \DateTime|string
     * @throws \Throwable
     */
    public static function getTimeToStart()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->getUser()->getIdentity();
        if (!$user->groupMember->group_id || !$user->groupMember->group) {
            return 'Группа ожидает старта';
        }
        return new \DateTime($user->groupMember->group->datetime_start);
    }

    static function getMainText($type = null)
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->getUser()->getIdentity();
        if ($type === null) {
            $type = $user->session_type;
        }
        if (in_array($type, self::$main_program_man)) {
            $name = 'Марафон Идеальное тело с Артемом Чекалиным';
        }
        if (in_array($type, self::$main_program_girl)) {
            $name = 'Марафон Идеальное тело с Валерией Чекалиной';
        }
        if ($type == self::PRESS_FOR_GIRL) {
            $name = 'Марафон Идеальный пресс с Валерией Чекалиной';
        }
        if ($type == self::PRESS_FOR_MAN) {
            $name = 'Марафон Идеальный пресс с Артемом Чекалиным';
        }
        return $name;
    }

    /**
     * @return int|string
     * @throws \Throwable
     */
    static function getCountQuestion()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->getUser()->getIdentity();
        return \app\models\Questions::find()->where('view is null')->andWhere(['member_id' => $user->groupMember->id])->andWhere('answer is not null')->count();
    }

    static function getCoverUp()
    {
        $css = '/images/cover-up/coverup_' . self::getTextProgram() . '.jpg';
        if (Yii::$app->devicedetect->isMobile()) {
            $css = '/images/cover-up/coverup_mobile_' . self::getTextProgram() . '.jpg';
        }
        return $css;
    }

    static function getTextProgram()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->getUser()->getIdentity();
        return self::$type_program[$user->session_type];
    }

    public static function getAccess()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->getUser()->getIdentity();

        /* if (!$user->groupMember->group_id || strtotime($user->groupMember->group->datetime_start) > strtotime(date('Y-m-d H:i:s'))) {
             return false;
         }*/

        if ($user->groupMember->getAccess()) {
            return true;
        } else {
            Yii::$app->controller->view->registerJs("
            $('.access_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    ");
            echo Yii::$app->controller->renderPartial('../../popups/access_popup', ['member' => $user->groupMember]);
            return false;
        }
    }

    public static function getProgressPopup()
    {
        /**
         * @var User $user
         */

        $user = Yii::$app->getUser()->getIdentity();
        $popup = Popups::findOne(['member_id' => $user->groupMember->id, 'status' => 0, 'popup_id' => 1]);
        if ($popup) {
            Yii::$app->controller->view->registerJs("
            $('.access_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    ");
            $popup->status = 1;
            $popup->save(false);
            echo Yii::$app->controller->renderPartial('../../popups/progress_popup', ['member' => $user->groupMember]);
        }
        return false;

    }

    public static function getProgressStatus()
    {
        /**
         * @var User $user
         */

        $user = Yii::$app->getUser()->getIdentity();
        $popup = Popups::findOne(['member_id' => $user->groupMember->id, 'status' => 0, 'popup_id' => 2]);
        if ($popup) {
            Yii::$app->controller->view->registerJs("
            $('.access_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    ");
            $popup->status = 1;
            $popup->save(false);
            echo Yii::$app->controller->renderPartial('../../popups/progress_popup_status', ['member' => $user->groupMember, 'popup' => $popup]);
        }
        return false;

    }

    public static function getTYFlash()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->getUser()->getIdentity();

        if (Yii::$app->session->getFlash('question_sent')) {
            Yii::$app->controller->view->registerJs("
        $('.access_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    ");
            echo Yii::$app->controller->renderPartial('../../popups/question');
        }
    }

    /**
     * @param $week
     * @param $select_week
     * @return string|null
     */
    public static function getActiveClassWeek($week, $select_week)
    {
        return $week == $select_week ? 'active' : null;
    }

    public static function getActiveClassNumber($number, $select_number)
    {
        return $number == $select_number ? 'active' : null;
    }

    public static function getBlockImage()
    {
        $css = '/images/cover-up/coverup_' . self::$type_program[Yii::$app->getUser()->identity->session_type] . '.jpg';
        if (Yii::$app->devicedetect->isMobile()) {
            $css = '/images/cover-up/coverup_mobile_' . self::$type_program[Yii::$app->getUser()->identity->session_type] . '.jpg';
        }
        return $css;
    }

    static function getCountryCost()
    {
        return [
            'RU' => 0,
            'BY' => 0,
            'KZ' => 0,
            'AU' => '2600',
            'AT' => '2800',
            'AZ' => '2600',
            'AM' => '2050',
            'BE' => '2200',
            'BG' => '3100',
            'GB' => '2450',
            'HU' => '2650',
            'DE' => '2400',
            'GR' => '2950',
            'GE' => '3600',
            'DK' => '2800',
            'IL' => '2250',
            'IE' => '2250',
            'IS' => '2400',
            'CA' => '3900',
            'CY' => '2750',
            'CN' => '2000',
            'KG' => '2450',
            'LV' => '2800',
            'LT' => '2800',
            'LI' => '2350',
            'NL' => '2450',
            'NO' => '2150',
            'NZ' => '2650',
            'AE' => '2300',
            'PL' => '2800',
            'PT' => '2800',
            'RO' => '2650',
            'SK' => '2750',
            'SI' => '2750',
            'US' => '2750',
            'TJ' => '3250',
            'TM' => '2550',
            'UZ' => '2650',
            'UA' => '2650',
            'FI' => '3000',
            //'FR' => '2650',
            'HR' => '3700',
            //  'ME' => '2200',
            'CZ' => '2700',
            'CH' => '2000',
            'SE' => '2750',
            'EE' => '2700',
            'KR' => '1750',
            'JP' => '2250',
        ];
    }

    static function getCountryNames()
    {
        return [
            'RU' => 'Россия',
            'BY' => 'Беларусь',
            'KZ' => 'Казахстан',
            'AU' => 'Австралия',
            'AT' => 'Австрия',
            'AZ' => 'Азербайджан',
            'AM' => 'Армения',
            'BE' => 'Бельгия',
            'BG' => 'Болгария',
            'GB' => 'Великобритания',
            'HU' => 'Венгрия',
            'DE' => 'Германия',
            'GR' => 'Греция',
            'GE' => 'Грузия',
            'DK' => 'Дания',
            'IL' => 'Израиль',
            'IE' => 'Ирландия',
            'IS' => 'Исландия',
            'CA' => 'Канада',
            'CY' => 'Кипр',
            'CN' => 'Китай',
            'KG' => 'Кыргызстан',
            'LV' => 'Латвия',
            'LT' => 'Литва',
            'LI' => 'Лихтенштейн',
            'NL' => 'Нидерланды',
            'NO' => 'Норвегия',
            'NZ' => 'Новая Зеландия',
            'AE' => 'ОАЭ',
            'PL' => 'Польша',
            'PT' => 'Португалия',
            'RO' => 'Румыния',
            'SK' => 'Словакия',
            'SI' => 'Словения',
            'US' => 'США',
            'TJ' => 'Таджикистан',
            'TM' => 'Туркмения',
            'UZ' => 'Узбекистан',
            'UA' => 'Украина',
            'FI' => 'Финляндия',
            //  'FR' => 'Франция',
            'HR' => 'Хорватия',
            // 'ME' => 'Черногория',
            'CZ' => 'Чехия',
            'CH' => 'Швейцария',
            'SE' => 'Швеция',
            'EE' => 'Эстония',
            'KR' => 'Южная Корея',
            'JP' => 'Япония',
        ];
    }

    static function getComplexes()
    {
        return [
            '4406_4404' => 'Холодный антицеллюлитный Комплекс',
            '4403_4405' => 'Горячий антицеллюлитный комплекс',
            '77_79' => 'Холодный антицеллюлитный Комплекс',
            '76_78' => 'Горячий антицеллюлитный комплекс',
            '76_166' => 'Горячий антицеллюлитный комплекс',
            '128_78' => 'Горячий антицеллюлитный комплекс',
            '128_79' => 'Холодный антицеллюлитный Комплекс',
            '128_166' => 'Горячий антицеллюлитный комплекс',
            '173_167' => 'Скраб вишня + обертывание жар-холод',
            '171_168' => 'Скраб франжипани-монои + обертывание лифтинг-эффект',
            '171_169' => 'Скраб Франжипани-монои + обертывание Уменьшение объемов',
            '172_166' => 'Скраб лайм-имбирь + обертывание интенсивное жиросжигание',
            '172_167' => 'Скраб лайм-имбирь + Обертывание жар-холод',
            '174_169' => 'Скраб шоколад-миндаль + обертывание уменьшение объемов',
            '171_79' => 'Холодный антицеллюлитный комплекс',
            '170' => 'крем-корсет',
        ];
    }

    static function countSet($ids)
    {
        if (count($ids) == 2) {
            return [
                [
                    'quantity' => 1,
                    'offer' => [
                        'id' => $ids[0],
                    ],
                ],
                [
                    'quantity' => 1,
                    'offer' => [
                        'id' => $ids[1],
                    ],
                ],
            ];
        }
        return [
            [
                'quantity' => 1,
                'offer' => [
                    'id' => $ids[0],
                ],
            ],
        ];
    }

    /**
     * @param null $type
     * @return array|null
     */
    static function getType($type = null)
    {
        if ($type === null) {
            $type = $_GET['type'];
        }

        if (stristr($type, '-')) {
            $type = explode('-', $type);
        }
        return $type;
    }

    /**
     * @param $name
     * @param $data
     */
    static function log($name, $data)
    {
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '-' . $name . '.log';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        file_put_contents($file, PHP_EOL . date('Y-m-d H:i:s'), FILE_APPEND);
        file_put_contents($file, PHP_EOL . json_encode($data), FILE_APPEND);
    }

    public static function setResize($tmp, $img)
    {
        /**
         * @var $resizer ImageResizer
         */
        $ret = ImageHelperController::imageCreateFromAny($tmp);
        $image = $ret[0];
        if ($ret[1] == 2) {
            $exif = @exif_read_data($tmp);
            //     VarDumper::dump($exif,11,1);
            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    // Поворот на 180 градусов
                    case 3:
                    {
                        $image = imagerotate($image, 180, 0);
                        break;
                    }
                    // Поворот вправо на 90 градусов
                    case 6:
                    {
                        $image = imagerotate($image, -90, 0);
                        break;
                    }
                    // Поворот влево на 90 градусов
                    case 8:
                    {
                        $image = imagerotate($image, 90, 0);
                        break;
                    }
                }
                imagejpeg($image, $tmp, 90);
            }
        }

        move_uploaded_file($tmp, $img);
        $sourceImg = Image::getImagine()->open($img);
        if ($sourceImg->getSize()->getWidth() > 600) {
            $resizer = Yii::$app->imageResizer;
            $resizer->resize($img);
            unlink($img);
            $ext = pathinfo($img, PATHINFO_EXTENSION);
            $file = basename($img, "." . $ext);
            $dir = Yii::getAlias('@webroot/uploads/progress/');
            rename($dir . $file . '-' . '.' . $ext, $img);
        } else {
            Image::getImagine()->open($img)->thumbnail(new Box($sourceImg->getSize()->getWidth(), $sourceImg->getSize()->getHeight()), ManipulatorInterface::THUMBNAIL_OUTBOUND)->save($img);
        }
    }

    public static function getTelegramLinks($type)
    {
        if (is_array($type)) {
            return '<a target="_blank" href="https://tele.click/joinchat/AAAAAFfqFxWjMmMwVRI5gQ" class="button__quest link__lk start-program__link"> Женский телеграм канал</a>
                    <a target="_blank" href="https://tele.click/joinchat/AAAAAEuJ1nkv7DOhJuMMwg" class="button__quest link__lk start-program__link">Мужской телеграм канал</a>';
        }
        if ($type == My::FOR_GIRL) {
            return '<a target="_blank" href="https://tele.click/joinchat/AAAAAFfqFxWjMmMwVRI5gQ" class="button__quest link__lk start-program__link">Телеграм канал</a>';
        }
        return '<a target="_blank" href="https://tele.click/joinchat/AAAAAEuJ1nkv7DOhJuMMwg" class="button__quest link__lk start-program__link">Телеграм канал</a>';
    }

    public static function getTelegramLinksCover($type)
    {
        if ($type == My::FOR_GIRL) {
            return '<a target="_blank" href="https://tele.click/joinchat/AAAAAFfqFxWjMmMwVRI5gQ" class="start-program__link">Присоединяйтесь к Telegram-каналу</a>';
        }
        return '<a target="_blank" href="https://tele.click/joinchat/AAAAAEuJ1nkv7DOhJuMMwg" class="start-program__link">Присоединяйтесь к Telegram-каналу</a>';
    }

    public static function getTypeContentClone($class = null)
    {
        $type = Yii::$app->getUser()->getIdentity()->session_type;
        if (in_array($type, self::$girl_clone)) {
            $type = My::FOR_GIRL_CLONE;
        }
        if (in_array($type, self::$man_clone)) {
            $type = My::FOR_MAN_CLONE;
        }

        if (Yii::$app->controller->id == 'vote' || Yii::$app->controller->id == 'group' || Yii::$app->controller->id == 'user' || stristr($class::className(), 'Group') && Yii::$app->user->identity->role == User::ROLE_ADMIN || stristr($class::className(), 'Group') && Yii::$app->user->identity->role == User::ROLE_MODERATOR) {
            $type = Yii::$app->getUser()->getIdentity()->session_type;
        }
        if (Yii::$app->getUser()->isGuest && isset($_GET['type']) && Yii::$app->controller->action->id == 'signup') {
            return $_GET['type'];
        }
        return $type;
    }

    public static function getDelivery($city, $country)
    {
        if (stristr($city, 'Москва') || stristr($city, 'Санкт-Петербург')) {
            $date = new \DateTime('+1 days');
            return ['129/526' => $date->format('Y-m-d')];
        } elseif ($country == 'RU' /*|| $country == 'BY' || $country == 'KZ'*/) {
            return '1';
        } else {
            return 'POSTAL_PARCEL_EXPORT';
        }
    }
}
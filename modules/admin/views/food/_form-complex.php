<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\RationComplex */
/* @var $model_groups array */
/* @var $rationComplexIngredients \app\models\Ingredient[] */
/* @var $ration_type int */
/* @var $form yii\widgets\ActiveForm */
$this->registerAssetBundle(\zxbodya\yii2\tinymce\TinyMceAsset::className());
$this->registerJsFile(Yii::getAlias('@web/js/ration-form.js'), [
    'depends' => [\app\assets\AppAsset::className()]
]);
$this->registerJsFile(Yii::getAlias('@web/js/tinyMCERation.js'), [
    'depends' => [\app\assets\AppAsset::className()]
]);
$exist_weeks = array_keys($model_groups);

?>
<div class="ration-form">
    <?php
    $week_items = [];

    foreach ($model->getWeekRation() as $week_type => $week_label) {
        $day_items = [];
        $exist_days = [''];
        if (!empty($model_groups) && isset($model_groups[$week_type])) {
            foreach (array_keys($model_groups[$week_type]) as $dk) {
                $exist_days[$dk] = $model->getDaysRation($dk);
            }
        }

        foreach ($model->getDaysRation() as $day_type => $day_label) {
            $day_part_items = [];
            foreach ($model->getDayParts() as $day_part_type => $day_part_label) {
                $m = $model_groups[$week_type][$day_type][$day_part_type];

                $day_part_items[] = [
                    'label' => $day_part_label,
                    'headerOptions' => [
                        'class' => 'last_child_tab'
                    ],
                    'content' => $this->render('_form-complex_part', [
                        'model' => $m ?: new \app\models\RationComplex(),
                        'rationComplexIngredients' => $m && isset($rationComplexIngredients[$m->id])
                            ? $rationComplexIngredients[$m->id]
                            : [],
                        'ration_type' => $ration_type,
                        'week_type' => $week_type,
                        'day_type' => $day_type,
                        'day_part_type' => $day_part_type,
                        'exist_weeks' => $exist_weeks,
                        'exist_days' => $exist_days,
                    ]),
                    'active' => $model->id && $model->day_part == $day_part_type || !$model->id && $day_part_type == 1,
                ];
            }

            $day_items[] = [
                'label' => $day_label,
                'headerOptions' => [
                    'class' => 'middle_child_tab'
                ],
                'content' => Tabs::widget([
                    'items' => $day_part_items
                ]),
                'active' => $model->id && $model->day == $day_type || !$model->id && $day_type == 1,
            ];
        }


        $week_items[] = [
            'label' => $week_label,
            'headerOptions' => [
                'class' => 'first_tab'
            ],
            'content' => Tabs::widget([
                'items' => $day_items
            ]),
            'active' => $model->id && $model->week == $week_type || !$model->id && $week_type == 1
        ];
    }
    echo Tabs::widget([
        'items' => $week_items
    ]);
    ?>
</div>

<?php

use app\models\Ration;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\RationComplex */
/* @var $rationComplexIngredients \app\models\Ingredient[] */
/* @var $ration_type integer */
/* @var $week_type integer */
/* @var $day_type integer */
/* @var $day_part_type integer */
/* @var $exist_weeks array */
/* @var $exist_days array */

$model->ration_type = $ration_type;
$model->week = $week_type;
$model->day = $day_type;
$model->day_part = $day_part_type;

if (empty($rationComplexIngredients)) {
    $rationComplexIngredients = [new \app\models\Ingredient()];
}
$exists = [''];
foreach ($exist_weeks as $v) {
    $exists[$v] = $model->getWeekRation($v);
}
?>
<div class="ration-form">

    <?php $form = ActiveForm::begin([
        'enableClientScript' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'options' => [
                'class' => 'use-bootstrap form_group clearfix',
                'data-week-key' => $model->week,
            ],
            //'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
        ],
    ]); ?>

    <?= Html::activeHiddenInput($model, "ration_type") ?>
    <?= Html::activeHiddenInput($model, "week") ?>
    <?= Html::activeHiddenInput($model, "day") ?>
    <?= Html::activeHiddenInput($model, "day_part") ?>
    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'is_clone')->radioList([1 => 'Да'], ['style' => 'display: grid'])->label('Такой же контент?');
    }

    ?>
    <br/>
    <div class="row">
        <div class="col-md-4">
            <div class="use-bootstrap form_group clearfix">
                <label class="control-label" for="clone_week-name">Скопировать неделю из</label>
                <?= Html::dropDownList('week_key', null, $model->getWeekRation(), [
                    'class' => 'form-control week-drop-down-input',
                ]) ?>
            </div>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('<span class="glyphicon glyphicon-book"></span>', [
                'name' => 'week_clone_exec',
                'class' => 'btn custom-icon-button',
                'style' => 'margin-top: 6px;',
            ]) ?>
        </div>
        <div class="col-md-4">
            <div class="use-bootstrap form_group clearfix">
                <label class="control-label" for="clone_week-name">Скопировать день из</label>
                <?= Html::dropDownList('day_key', null, $model->getDaysRation(), [
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton('<span class="glyphicon glyphicon-book"></span>', [
                'class' => 'clone-day-button btn custom-icon-button',
                'style' => 'margin-top: 6px;',
                'name' => 'day_clone_exec',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'portion')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'calories')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'protein')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'grease')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'carbohydrate')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($model->image) {
                $dir = Yii::getAlias('@web/uploads/food/');
                echo Html::img($dir . $model->image, [
                    'style' => 'max-width: 150px;',
                ]);
            }
            ?>
            <?= $form->field($model, 'img')->fileInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['id' => 'qwe' . $model->day_part, 'rows' => 15]); ?>
        </div>
    </div>

    <div class="ingredient-items-block">
        <div class="row">
            <div class="ingredient-items-container col-md-12">
                <?php foreach ($rationComplexIngredients as $rci) { ?>
                    <div class="ingredient-item row">
                        <div class="col-md-4">
                            <?= $form->field($rci, "name[]")->textInput([
                                'value' => $rci->name
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($rci, "value[]")->textInput([
                                'value' => $rci->value
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($rci, "count_symbol[]")->textInput([
                                'value' => $rci->count_symbol
                            ]) ?>
                            <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', '#', [
                                'class' => 'remove-button',
                                'title' => 'Редактировать'
                            ]) ?>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div style="width: 100%; text-align: center">
                    <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', '#', [
                        'class' => 'add-new-button',
                        'title' => 'Редактировать'
                    ]) ?>
                </div>
                <hr/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать рацион' : 'Изменить рацион', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Сохранить с удалением картинки', ['name' => 'delete-img', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <div class="form-group">
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<style>
    a.custom-icon-button {
        width: 40px;
        height: 34px;
        font-size: 15px !important;;
        line-height: 2.7 !important;;
        margin-top: 24px !important;;
    }
</style>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RationComplex */
/* @var $model_groups array */
/* @var $rationComplexIngredients \app\models\Ingredient[] */
/* @var $ration_type integer */

$this->title = 'Создание нового рациона';
$active_menu = 'admin-ration-new-custom';
?>
<div class="food-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-complex', [
        'model' => $model,
        'model_groups' => $model_groups,
        'rationComplexIngredients' => $rationComplexIngredients,
        'ration_type' => $ration_type,
    ]) ?>

</div>
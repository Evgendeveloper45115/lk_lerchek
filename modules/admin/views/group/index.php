<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы, всего клиентов: ' . $count;
$this->params['breadcrumbs'][] = $this->title;
?>
<ul>
    <li>
        Дорогой куратор, здесь будет краткий manual по работе фильтров.
    </li>
    <li>
        1)<strong>"нажми на иконку "глазик" что бы перейти в просмотр списка клиентов группы"</strong>
    </li>
    <li>
        2)<strong>"Начало"</strong> фильтр клиентов, которые загрузили или отредактировали фото "Начало", к этому
        фильтру доступны в связке любые другие фильтры ниже из списка(не голубые кнопки)
    </li>
    <li>
        3)<strong>"Коллаж"</strong> фильтр клиентов, которые загрузили или отредактировали фото на
        4ой неделе программы + начало + есть коллаж + ссылка на видео
    </li>
    <li>
        4)<strong>"Все"</strong> Выводит всех клиентов
    </li>
    <li>
        5)<strong>"Для удобства используйте разные фильтры в связках"</strong>
    </li>
    <li>
        6)<strong style="color:red;">"Если хочешь посмотреть новых клиетов, которые исправили фото, просто надо выбрать
            в фильтре "Статус сообшения" -> "Исправлено(ждёт ответа)" и доп фильтр (Начало)"</strong>
    </li>
    <li>
        7)<strong>"Email"</strong> Этот фильтр ищет всех клиентов данного потока !!! Не работает в связке с другими
        фильтрами
    </li>
</ul>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'num',
            [
                'attribute' => '',
                'label' => 'Участники',
                'value' => function (\app\models\Group $model) {
                    return $model->membersCount;
                }
            ],
            'datetime_create',
            'datetime_start',
            'date_continue',
            'link_main',
            'link_second',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' =>
                    [
                        'delete' => Yii::$app->user->identity->email == 'admin@lerchek.ru' ? true : false,
                        'change' => Yii::$app->user->identity->email == 'admin@lerchek.ru' ? true : false,
                        'update' => Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN && Yii::$app->user->identity->email == 'admin@lerchek.ru' ? true : false
                    ],

                'template' => '<span style="text-wrap: none">{view} {update} {delete} {change}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['view-users', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },

                    'change' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-time"></span>', \yii\helpers\Url::to(['change-group', 'id' => $model->id]), [
                            'title' => 'Перенос'
                        ]);
                    },

                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['create']) . '" class="btn btn-default" title="Создать новую группу">Создать новую группу</a>',
                'options' => ['class' => 'btn-group mr-2']
            ],
            [
                'content' => in_array(Yii::$app->user->identity->email, \app\helpers\My::$curators_high) ? '<a href="' . \yii\helpers\Url::to(['delete-group-completed']) . '" class="btn btn-info">Удалить сообщения</a>' : null,
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],
            [
                'content' => in_array(Yii::$app->user->identity->email, \app\helpers\My::$curators_high) ? '<a href="' . \yii\helpers\Url::to(['delete-group-visible']) . '" class="btn btn-info">Снять метки</a>' : null,
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],

            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Группы',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Группа',
        'itemLabelPlural' => 'Группы'
    ]) ?>
</div>

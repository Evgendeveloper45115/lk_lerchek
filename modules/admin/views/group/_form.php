<?php

use yii\helpers\Html;
use \kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $form \kartik\form\ActiveForm */
?>

<div class="group-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form-horisonal',
        'type' => ActiveForm::TYPE_VERTICAL,
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>
    <div class="col-row">
        <div class="col-md-6">
            <?= $form->field($model, 'num')->textInput(['maxlength' => true]) ?>

            <?php
            echo '<label>Дата старта 1.0</label>';
            echo \kartik\date\DatePicker::widget([
                'model' => $model,
                'attribute' => 'datetime_start',
                'options' => ['placeholder' => 'Выбрать дату старта'],
                //  'convertFormat' => true,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
            <?= $form->field($model, 'link_main')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?php
            echo '<label>Дата окончания потока</label>';
            echo \kartik\date\DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_continue',
                'options' => ['placeholder' => 'Выбрать дату окончания потока'],
                //  'convertFormat' => true,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
            <?= $form->field($model, 'link_second')->textInput(['maxlength' => true]) ?>
            <?php
            if ($model->isNewRecord) {
                ?>
                <?= $form->field($model, 'is_clone')->checkbox() ?>
                <?php
            }
            ?>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\GroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'num') ?>

    <?= $form->field($model, 'datetime_create') ?>

    <?= $form->field($model, 'datetime_start') ?>

    <?= $form->field($model, 'datetime_end') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'training_type') ?>

    <?php // echo $form->field($model, 'date_continue') ?>

    <?php // echo $form->field($model, 'instruction_main') ?>

    <?php // echo $form->field($model, 'instruction_second') ?>

    <?php // echo $form->field($model, 'link_main') ?>

    <?php // echo $form->field($model, 'link_second') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

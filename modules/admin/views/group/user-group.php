<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $group  \app\models\Group */

$this->title = 'Клиенты группы: ' . $group->num;
$this->params['breadcrumbs'][] = $this->title;
?>
<ul>
    <li>
        Дорогой куратор, здесь будет краткий manual по работе фильтров.
    </li>
    <li>
        1)<strong>"Начало"</strong> фильтр клиентов, которые загрузили или отредактировали фото "Начало", к этому
        фильтру доступны в связке любые другие фильтры ниже из списка(не голубые кнопки)
    </li>
    <li>
        2)<strong>"Коллаж"</strong> фильтр клиентов, которые загрузили или отредактировали фото на
        4ой неделе программы + начало + есть коллаж + ссылка на видео
    </li>
    <li>
        3)<strong>"Все"</strong> Выводит всех клиентов
    </li>
    <li>
        4)<strong>"Для удобства используйте разные фильтры в связках"</strong>
    </li>
    <li>
        5)<strong style="color:red;">"Если хочешь посмотреть новых клиетов, которые исправили фото, просто надо выбрать
            в фильтре "Статус сообшения" -> "Исправлено(ждёт ответа)" и доп фильтр (Начало)"</strong>
    </li>
    <li>
        6)<strong>"Email"</strong> Этот фильтр ищет всех клиентов данного потока !!! Не работает в связке с другими
        фильтрами
    </li>

</ul>

<?php

?>
<div class="user-index">

    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'email',
                'format' => 'raw',
                'value' => function (\app\models\User $model) {
                    $css = 'black';
                    if (!$model->groupMember->payment) {
                        $css = 'red';
                    }
                    return '<div style="color: ' . $css . '">' . $model->groupMember->user->email . '</div>';
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],

            ],
            [
                'attribute' => '',
                'label' => 'password',
                'value' => function (\app\models\User $model) {
                    return in_array(Yii::$app->user->identity->email, \app\helpers\My::$curators_password) ? $model->groupMember->password : 'Доступно старшему куратору';
                }
            ],
            [
                'attribute' => 'read',
                'label' => 'Статус сообщения',
                'value' => function (\app\models\User $model) {
                    if ($model->groupMember->progressPopup) {
                        return \app\models\Popups::$statuses[$model->groupMember->progressPopup->status];
                    } else {
                        return 'Нет сообщений';
                    }
                },
                'filter' => \app\models\Popups::$statuses + ['4' => 'Нет сообщений']
            ],
            [
                'attribute' => 'visible_curator',
                'label' => 'Статус осмотра',
                'format' => 'raw',
                'value' => function (\app\models\User $model) {
                    if ($model->groupMember->visible_curator == 1) {
                        return '<span style="background-color: red;color:white">Младший куратор</span>';
                    } elseif ($model->groupMember->visible_curator == 2) {
                        return '<span style="background-color: green;color:white">Старший куратор</span>';
                    }
                    return '<span style="background-color: #3c8dbc;color:white">Не просмотрено</span>';
                },
                'filter' => \app\models\User::$visible_curators
            ],
            [
                'label' => 'Дата загрузки фото',
                'format' => 'raw',
                'value' => function (\app\models\User $model) {
                    if ($model->groupMember->progressRelLastWeek) {
                        return $model->groupMember->progressRelLastWeek->date_create;
                    }
                    if ($model->groupMember->progressRelFirstWeek) {
                        return $model->groupMember->progressRelFirstWeek->date_create;
                    }
                    return 'Фото не загружено';
                },
                'filter' => \app\models\User::$visible_curators
            ],
            [
                'attribute' => 'rating',
                'label' => 'Оценка куратора',
                'value' => function (\app\models\User $model) {
                    return \app\helpers\My::$type_voices[$model->groupMember->rating];
                },
                'filter' => \app\helpers\My::$type_voices + [4 => 4, 5 => 5]
            ],
            [
                'attribute' => '',
                'label' => 'Кто дал ответ на фото',
                'value' => function (\app\models\User $model) {
                    return $model->groupMember->progressPopup->curator->email;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' =>
                    [
                        'view' => true,
                        'avatar' => Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN ? true : false
                    ],

                'template' => '<span style="text-wrap: none">{view} {disable} {avatar}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'view' => function ($url, \app\models\User $model) {
                        if ($model->groupMember->visible_curator == 2) {
                            return Html::a('<span style="color: green" class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['/admin/user/view', 'id' => $model->groupMember->id]), [
                                'title' => 'Редактировать'
                            ]);
                        }
                        return Html::a('<span style="color: ' . ($model->groupMember->visible_curator ? "red" : "#3c8dbc") . '" class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['/admin/user/view', 'id' => $model->groupMember->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                    'disable' => function ($url, \app\models\User $model) {
                        return Html::a('<span class="glyphicon glyphicon-retweet"></span>', \yii\helpers\Url::to(['/admin/user/disable', 'id' => $model->groupMember->id]), [
                            'title' => 'Снять красный глаз'
                        ]);

                    },
                    'avatar' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', \yii\helpers\Url::to(['/marafon/index', 'avatar' => $model->id]), [
                            'title' => 'Аватар',
                            'data' => [
                                'confirm' => 'Переключить на Аватар?',
                                'method' => 'post',
                            ],
                        ]);

                    },


                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['view-users', 'id' => $_GET['id'], 'progress-first' => 1]) . '" class="btn btn-info">Начало</a>',
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],

            [
                'content' => '<a href="' . \yii\helpers\Url::to(['view-users', 'id' => $_GET['id'], 'progress-completed' => 1]) . '" class="btn btn-info">Коллаж</a>',
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['view-users', 'id' => $_GET['id']]) . '" class="btn btn-info">Все</a>',
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],
            [
                'content' => in_array(Yii::$app->user->identity->email, \app\helpers\My::$curators_high) ? '<a href="' . \yii\helpers\Url::to(['delete-group-completed', 'id' => $_GET['id']]) . '" class="btn btn-info">Удалить сообщения</a>' : null,
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],
            [
                'content' => in_array(Yii::$app->user->identity->email, \app\helpers\My::$curators_high) ? '<a href="' . \yii\helpers\Url::to(['delete-group-visible', 'id' => $_GET['id']]) . '" class="btn btn-info">Снять метки</a>' : null,
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],
            [
                'content' => in_array(Yii::$app->user->identity->email, \app\helpers\My::$curators_high) ? '<a href="' . \yii\helpers\Url::to(['user/create-couple', 'group_id' => $_GET['id']]) . '" class="btn btn-info">Сформировать пары</a>' : null,
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],

        ],

        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Список участников: ' . $group->num,
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Клиент',
        'itemLabelPlural' => 'Клиентов'
    ]) ?>
</div>

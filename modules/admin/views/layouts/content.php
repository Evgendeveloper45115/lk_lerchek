<?php

use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper <?= \app\helpers\My::$type_program[Yii::$app->getUser()->getIdentity()->session_type] ?>">
    <section
            class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            ['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>


<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab" aria-expanded="true"><i
                        class="fa fa-home"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Выбор марафона</h3>
            <ul class='control-sidebar-menu'>
                <?php
                $types = \app\helpers\My::$type_program_rus;
                if (Yii::$app->user->identity->role == \app\models\User::ROLE_MODERATOR) {
                    $types = \app\helpers\My::$type_program_rus_moder;

                }
                foreach ($types as $type => $name) {
                    ?>
                    <li>
                        <a href='<?= \yii\helpers\Url::to(['user/change-program', 'type_program' => $type]) ?>'>
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 style="<?= $type == \app\helpers\My::FOR_MAN_CLONE || $type == \app\helpers\My::FOR_GIRL_CLONE ? 'color:red' : null ?> "
                                    class="control-sidebar-subheading"><?= $name ?></h4>
                            </div>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
</aside><!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>
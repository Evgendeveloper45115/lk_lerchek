<?php

use \app\models\User;

/**
 * @var $user \app\models\User
 */
$user = Yii::$app->user->getIdentity();
$email = Yii::$app->user->getIdentity()->email;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $email ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    [
                        'label' => \app\helpers\My::$type_program_rus_menu[Yii::$app->user->identity->session_type],
                        'options' => [
                            'class' => 'header pulse ' . \app\helpers\My::$type_program[1]
                        ],
                    ],
                    [
                        'label' => 'Новые участники',
                        'url' => ['user/index'],
                        'icon' => 'users',
                        'active' => Yii::$app->controller->id == 'user' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                    ],
                    [
                        'label' => 'Группы',
                        'url' => ['group/index'],
                        'icon' => 'file',
                        'active' => Yii::$app->controller->id == 'group' ? true : false
                    ],
                    [
                        'label' => 'Тренировки',
                        'url' => ['training/index'],
                        'icon' => 'pie-chart',
                        'active' => Yii::$app->controller->id == 'training' ? true : false,
                        'visible' => $email == 'admin@lerchek.ru' || $email == 'd.smirnov@specialview.ru' ? true : false
                    ],
                    ['label' => 'Рекомендации по кардио',
                        'url' => ['recommendation/index'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->id == 'recommendation' ? true : false,
                        'visible' => $email == 'admin@lerchek.ru' ? true : false
                    ],
                    [
                        'label' => 'Питание',
                        'icon' => 'share',
                        'url' => '#',
                        'active' => Yii::$app->controller->id == 'food' || Yii::$app->controller->id == 'recipe' || Yii::$app->controller->id == 'week-products' || Yii::$app->controller->id == 'calories' || Yii::$app->controller->id == 'calories-cat' || Yii::$app->controller->id == 'potato' ? true : false,
                        'visible' => $email == 'admin@lerchek.ru' || $email == 'd.smirnov@specialview.ru' ? true : false,
                        'items' => [
                            [
                                'label' => 'Сложный рацион',
                                'icon' => 'circle-o',
                                'items' => [
                                    [
                                        'label' => 'Веганский рацион',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 1 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 1],

                                    ],
                                    [
                                        'label' => 'Особенное меню',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 2 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 2],

                                    ],
                                    [
                                        'label' => 'Меню для пар',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 3 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 3],

                                    ],
                                    [
                                        'label' => 'Базовое меню',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 5 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 5],
                                    ],
                                    [
                                        'label' => 'AntiAge',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 8 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 8],
                                    ],
                                    [
                                        'label' => 'Альтернативное',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 7 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 7],
                                    ],
                                    [
                                        'label' => 'Базовое new',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 9 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 9],
                                    ],
                                    [
                                        'label' => 'Постное',
                                        'icon' => 'share',
                                        'active' => $_GET['ration_type'] == 10 ? true : false,
                                        'url' => ['food/create', 'ration_type' => 10],
                                    ]
                                ]
                            ],
                            [
                                'label' => 'Рецепты',
                                'icon' => 'circle-o',
                                'active' => Yii::$app->controller->id == 'recipe' ? true : false,
                                'url' => ['recipe/index'],
                            ],
                            [
                                'label' => 'на неделю',
                                'icon' => 'circle-o',
                                'active' => Yii::$app->controller->id == 'week-products' && Yii::$app->controller->action->id != 'detox-day' ? true : false,
                                'url' => ['week-products/index'],
                            ],
                            [
                                'label' => 'Детокс',
                                'icon' => 'circle-o',
                                'active' => Yii::$app->controller->action->id == 'detox-day' ? true : false,
                                'url' => ['week-products/detox-day'],
                            ],
                            [
                                'label' => 'Это вам не батат',
                                'icon' => 'circle-o',
                                'active' => Yii::$app->controller->id == 'potato' ? true : false,
                                'url' => ['potato/index'],
                            ],
                            [
                                'label' => 'БЖУ',
                                'icon' => 'circle-o',
                                'active' => Yii::$app->controller->id == 'calories-cat' || Yii::$app->controller->id == 'calories' ? true : false,
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'Категории',
                                        'icon' => 'circle-o',
                                        'active' => Yii::$app->controller->id == 'calories-cat' ? true : false,
                                        'url' => ['calories-cat/index'],
                                    ],
                                    [
                                        'label' => 'Продукты',
                                        'icon' => 'circle-o',
                                        'url' => ['calories/index'],
                                        'active' => Yii::$app->controller->id == 'calories' ? true : false,
                                    ]
                                ]
                            ],
                        ],
                    ],

                    [
                        'label' => 'Faq',
                        'url' => ['faq/index'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->id == 'faq' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                    ],
                    [
                        'label' => 'Вопросы',
                        'url' => ['questions/index'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->id == 'questions' && Yii::$app->controller->action->id != 'curators' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                    ],
                    [
                        'label' => 'Кураторы',
                        'url' => ['questions/curators'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->action->id == 'curators' ? true : false,
                        'visible' => $email == 'admin@lerchek.ru' ? true : false,
                    ],
                    [
                        'label' => 'Отчеты',
                        'icon' => 'share',
                        'url' => '#',
                        'active' => Yii::$app->controller->id == 'report' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                        'items' => [
                            [
                                'label' => 'Отчеты клиентов',
                                'icon' => 'circle-o',
                                'url' => ['report/reports'],
                                'active' => Yii::$app->controller->action->id == 'reports' ? true : false,
                            ],
                            [
                                'label' => 'Создать отчет',
                                'icon' => 'circle-o',
                                'url' => ['report/index'],
                                'visible' => $email == 'admin@lerchek.ru' ? true : false,
                                'active' => Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'index' ? true : false,
                            ],
                        ],
                    ],
                    [
                        'label' => 'Задания на день',
                        'visible' => $email == 'admin@lerchek.ru' ? true : false,
                        'url' => ['every-day/index'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->id == 'every-day' ? true : false,

                    ],
                    [
                        'label' => 'Уход',
                        'visible' => $email == 'admin@lerchek.ru' ? true : false,
                        'url' => ['care/index'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->id == 'care' ? true : false,
                    ],
                    [
                        'label' => 'Упражнения',
                        'visible' => $email == 'admin@lerchek.ru' ? true : false,
                        'url' => ['express-exercise/index'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->id == 'express-exercise' ? true : false,
                    ],
                    [
                        'label' => 'Выплаты',
                        'url' => ['ref/index'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->id == 'ref' ? true : false,
                        'visible' => $email == 'admin@lerchek.ru' || $email == 'd.smirnov@specialview.ru' ? true : false
                    ],
                    [
                        'label' => 'Голосование New',
                        'url' => ['vote/new'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->action->id == 'new' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                    ],
                    [
                        'label' => 'Голосование New(Пары)',
                        'url' => ['vote/coupe'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->action->id == 'coupe' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                    ],

                    [
                        'label' => 'Инструкция(Голосование)',
                        'url' => ['vote/instruction'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->action->id == 'instruction' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                    ],
                    [
                        'label' => 'Финалисты',
                        'url' => ['vote/final'],
                        'icon' => 'file-sound-o',
                        'active' => Yii::$app->controller->action->id == 'final' ? true : false,
                        'visible' => $user->role == User::ROLE_ADMIN,
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>

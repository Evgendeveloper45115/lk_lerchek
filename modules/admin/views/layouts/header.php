<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php
$cnt_questions = \app\models\Questions::find()->where(['status' => 0])->joinWith(['groupMember' => function (\yii\db\ActiveQuery $query) {
    $query->andWhere(['type_program' => Yii::$app->getUser()->identity->session_type]);
}])->count();
$cnt_report = \app\models\ReportHasUser::getReportCount();
$user = Yii::$app->user->getIdentity();
?>
<header class="main-header">

    <?= Html::a('<span class="logo-mini">app</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
                <li class="messages-menu">
                    <a href="<?= \yii\helpers\Url::to(['questions/index']) ?>">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success"><?= $cnt_questions ?></span>
                    </a>
                </li>
                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu">
                    <a href="<?= \yii\helpers\Url::to(['report/reports']) ?>"
                    <i class="fa fa-flag-o"></i>
                    <span class="label label-danger"><?= $cnt_report ?></span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= $user->email ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                Администратор и куратор личного кабинета
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Профиль</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выход',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>

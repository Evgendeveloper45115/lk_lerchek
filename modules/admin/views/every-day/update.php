<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EveryDay */

$this->title = 'Редактирование задачи';
$this->params['breadcrumbs'][] = ['label' => 'Список Заданий', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="every-day-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

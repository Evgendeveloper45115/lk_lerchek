<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EveryDay */

$this->title = 'Создать задачу на день';
$this->params['breadcrumbs'][] = ['label' => 'Список заданий', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="every-day-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

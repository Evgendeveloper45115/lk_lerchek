<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model app\models\Questions */

/* @var $this yii\web\View */
/* @var $member \app\models\GroupMember */
/* @var $form \kartik\form\ActiveForm */
/* @var $troublesView \app\models\Questions[] */
?>
<div class="col-row row">

    <div class="questions-form">
        <?php
        \yii\widgets\Pjax::begin();
        foreach ($troublesView as $troubleView) {
            ?>
            <div class="recipe-form">
                <?php
                $form = \yii\widgets\ActiveForm::begin([
                    'fieldConfig' => [
                        'options' => ['class' => 'form_group clearfix', 'enctype' => 'multipart/form-data',],
                        'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
                    ],
                ]); ?>
                <div class="form_group clearfix" style="margin-top: 20px">
                    <div class="col_left with-back">
                        <div>
                            <b><?= !$troubleView->answer ? 'Нерешенный вопрос:' : ('ответ получен от ' . $troubleView->userCurator->email . ':') ?></b> <?= $troubleView->question ?>
                        </div>
                    </div>
                    <?php
                    if ($troubleView->image) {
                        ?>
                        <div>
                            <?= Html::img('/uploads/questions/' . $troubleView->image, ['style' => 'width:40%']) ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?= $form->field($troubleView, 'id')->hiddenInput()->label(false) ?>
                <?= $form->field($troubleView, 'answer', [
                    'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
                ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
                    'options' => ['rows' => 15, 'value' => !$troubleView->answer ? ('Здравствуйте, ' . $model->groupMember->first_name . ' ' . $model->groupMember->last_name . '! ') : $troubleView->answer],
                    'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
                    'fileManager' => [
                        'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                        'connectorRoute' => 'el-finder/connector',
                    ],
                ]); ?>


                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Ответить' : 'Ответить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php \yii\widgets\ActiveForm::end();
                ?>
            </div>
            <?php
        }
        \yii\widgets\Pjax::end();
        ?>

    </div>
</div>
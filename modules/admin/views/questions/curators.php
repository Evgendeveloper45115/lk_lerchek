<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ответы кураторов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'curator_id',
                'value' => function (\app\models\Questions $model) {
                    return $model->userCurator->email;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['role' => \app\models\User::ROLE_ADMIN])->all(), 'id', 'email')
            ],
            [
                'attribute' => 'member_id',
                'value' => function (\app\models\Questions $model) {
                    return $model->groupMember->user->email;
                },
            ],
            [
                'attribute' => 'question',
                'value' => function (\app\models\Questions $model) {
                    return \yii\helpers\StringHelper::truncate($model->question, 40);
                },
            ],
            [
                'attribute' => 'count',
                'label' => 'Отвеченных вопросов',
                'format' => 'html',
                'value' => function (\app\models\Questions $model) {
                    if ($_GET['CuratorQuestionsSearch']['date_added'] == '') {
                        return 'Отвеченных вопросов: ' . \app\models\Questions::find()->where(['curator_id' => $model->curator_id])->andWhere(['status' => 1])->count();
                    }
                    $date_explode = explode(" - ", $_GET['CuratorQuestionsSearch']['date_added']);
                    $date1 = trim($date_explode[0]);
                    $date2 = trim($date_explode[1]);
                    // VarDumper::dump(date('Y-m-d', strtotime($date1)), 11, 1);
                    return 'Отвеченных вопросов: ' . \app\models\Questions::find()->where(['curator_id' => $model->curator_id])->andWhere(['status' => 1])->andWhere(['between', 'date_added', date('Y-m-d', strtotime($date1)), date('Y-m-d', strtotime('+1 day', strtotime($date2)))])->count() . '<br> Ответ: ' . $model->answer;
                }
            ],
            [
                'attribute' => 'date_added',
                'label' => 'Дата ответа',
                'options' => [
                    'format' => 'YYYY-MM-DD',
                ],
                'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => ([
                    'model' => $model,
                    'attribute' => 'date_added',
                    'presetDropdown' => TRUE,
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'Y-m-d H:i:s',
                        'opens' => 'left'
                    ],
                    'value' => function (\app\models\Questions $model) {
                        return $model->date_added;
                    }
                ])
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update}{avatar}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['questions/answer', 'id' => $model->id]), [
                            'title' => 'Ответить'
                        ]);

                    },
                    'avatar' => function ($url, $model) {
                        /**
                         * @var $model \app\models\Questions
                         */
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', \yii\helpers\Url::to(['/marafon/index', 'avatar' => $model->groupMember->user->id]), [
                            'title' => 'Аватар',
                            'data' => [
                                'confirm' => 'Переключить на Аватар?',
                                'method' => 'post',
                            ],
                        ]);

                    },
                ]
            ]

            //'answer:ntext',
            //'view',
            //'image',
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Вопросы',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Вопрос',
        'itemLabelPlural' => 'Вопросы'
    ]) ?>
</div>

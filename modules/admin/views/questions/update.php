<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $troublesView \app\models\Questions[] */
$this->title = 'Ответ для ' . $model->groupMember->user->email;
$this->params['breadcrumbs'][] = ['label' => 'Список вопросов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->groupMember->user->email;
?>
<div class="questions-update">

    <?= $this->render('_form', [
        'troublesView' => $troublesView,
        'model' => $model
    ]) ?>

</div>

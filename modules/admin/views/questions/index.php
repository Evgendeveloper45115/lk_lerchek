<?php

use yii\helpers\Html;
use \kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'member_id',
                'value' => function (\app\models\Questions $model) {
                    return $model->groupMember->user->email;
                },
                'headerOptions' => ['style' => 'width:20%'],
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],

            ],
            [
                'attribute' => 'group_id',
                'label' => 'Группа',
                'value' => function (\app\models\Questions $model) {
                    return $model->groupMember->group->num;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Group::find()->getContent()->all(), 'id', 'num'),
                'filterType' => GridView::FILTER_SELECT2,
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Выбрать группу',
                ],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Выбрать группу'],
                    'pluginOptions' => ['multiple' => true],
                ],
                'headerOptions' => ['style' => 'width:20%'],
            ],
            [
                'attribute' => 'count',
                'label' => 'Неотвеченных вопросов',
                'headerOptions' => ['style' => 'width:20%'],
                'value' => function (\app\models\Questions $model) {
                    return 'Неотвеченных вопросов: ' . \app\models\Questions::find()->where(['member_id' => $model->member_id])->andWhere(['status' => 0])->count();
                }
            ],
            [
                'attribute' => 'date_added',
                'filterType' => \kartik\grid\GridView::FILTER_DATE,
                'headerOptions' => ['style' => 'width:20%'],
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        //  'format' => 'dd-mm-yyyy',
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
                'value' => function (\app\models\Questions $model) {
                    return $model->date_added;
                }
            ],

            [
                'attribute' => 'question',
                'headerOptions' => ['style' => 'width:20%'],
                'value' => function (\app\models\Questions $model) {
                    return \yii\helpers\StringHelper::truncate($model->question, 25);
                }
            ],
            //'answer:ntext',
            //'view',
            //'image',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update}{avatar}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['questions/answer', 'id' => $model->id]), [
                            'title' => 'Ответить'
                        ]);

                    },
                    'avatar' => function ($url, $model) {
                        /**
                         * @var $model \app\models\Questions
                         */
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', \yii\helpers\Url::to(['/marafon/index', 'avatar' => $model->groupMember->user->id]), [
                            'title' => 'Аватар',
                            'data' => [
                                'confirm' => 'Переключить на Аватар?',
                                'method' => 'post',
                            ],
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Вопросы',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Вопрос',
        'itemLabelPlural' => 'Вопросы'
    ]) ?>
</div>

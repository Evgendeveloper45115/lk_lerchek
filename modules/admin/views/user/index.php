<?php

use app\helpers\My;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Новые клиенты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'email',
                'format' => 'raw',
                'value' => function (\app\models\User $model) {
                    $css = 'black';
                    if (!$model->groupMember->payment) {
                        $css = 'red';
                    }
                    return '<div style="color: ' . $css . '">' . $model->groupMember->user->email . '</div>';
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],

            ],
            [
                'attribute' => '',
                'label' => 'password',
                'value' => function (\app\models\User $model) {
                    return in_array(Yii::$app->user->identity->email, My::$curators_password) ? $model->groupMember->password : 'Доступно старшему куратору';
                }
            ],
            [
                'attribute' => 'first_name',
                'value' => 'groupMember.first_name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Имя'
                ],

            ],
            [
                'attribute' => 'last_name',
                'value' => 'groupMember.last_name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Фамилия'
                ],

            ],
            [
                'attribute' => 'telegram',
                'value' => 'groupMember.telegram',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Телеграм'
                ],

            ],
            [
                'attribute' => 'complete',
                'label' => 'Статус анкеты',
                'value' => function (\app\models\User $model) {
                    return $model->groupMember->password && $model->groupMember->physical_activity ? "Заполнил" : 'Не заполнил';
                },
                'filter' => [2 => 'Заполнил', 1 => 'Не заполнил']

            ],

            [
                'attribute' => 'datetime_signup',
                'filterType' => \kartik\grid\GridView::FILTER_DATETIME,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        //  'format' => 'dd-mm-yyyy',
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
                'value' => function (\app\models\User $model) {
                    return $model->groupMember->datetime_signup;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' =>
                    [
                        'update' => true,
                        'delete' => Yii::$app->user->identity->email == 'admin@lerchek.ru' || Yii::$app->user->identity->email == 'V_shakina@mail.ru' ? true : false
                    ],
                'template' => '<span style="text-wrap: none">{update} {delete} {continue} {avatar}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'continue' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-circle-arrow-right"></span>', \yii\helpers\Url::to(['continue', 'id' => $model->id]), [
                            'title' => '890руб'
                        ]);

                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', \yii\helpers\Url::to(['delete', 'id' => $model->id]), [
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Вы точно хотите удалить клиента?',
                                'method' => 'post',
                            ],
                        ]);

                    },
                    'avatar' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', \yii\helpers\Url::to(['/marafon/index', 'avatar' => $model->id]), [
                            'title' => 'Аватар',
                            'data' => [
                                'confirm' => 'Переключить на Аватар?',
                                'method' => 'post',
                            ],
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<button type="button" class="btn btn-default generate_in_email" data-toggle="popover" data-placement="bottom" title="Выслать анкету участинку">Выслать анкету на участие</button>',
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Список новых участников',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Клиент',
        'itemLabelPlural' => 'Клиентов'
    ]) ?>
</div>

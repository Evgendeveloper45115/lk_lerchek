<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form-horisonal',
        'type' => ActiveForm::TYPE_VERTICAL,
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>
    <div class="col-row row">
        <div class="col-md-2">
            <?= $form->field($model->groupMember, 'last_name')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'first_name')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'patronymic')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'phone')->textInput(['autocomplete' => 'off']) ?>

        </div>
        <div class="col-md-2">
            <?= $form->field($model->groupMember, 'group_id')->dropDownList(['' => 'Выбрать группу'] + [\yii\helpers\ArrayHelper::map(\app\models\Group::find()->getContent()->all(), 'id', 'num')]) ?>
            <?= $form->field($model->groupMember, 'age')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'height')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'weight')->textInput(['autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model->groupMember, 'bust')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'waist')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'hip')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model->groupMember, 'physical_activity')->radioList($model->groupMember->getPhysicalActivities(), ['class' => 'physical_activity'])->label('Дневная активность') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model->groupMember, 'training_type')->radioList(\app\models\Training::getTrainingType()) ?>
            <?= $form->field($model->groupMember, 'veg')->radioList($model->groupMember->getVeg()); ?>
            <?= $form->field($model->groupMember, 'ration_type')->radioList($model->groupMember->getTypes()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model->groupMember, 'smoke')->radioList($model->groupMember->getSmoke()) ?>
            <?= $form->field($model->groupMember, 'alcohol')->radioList($model->groupMember->getAlcoholType()) ?>
            <?= $form->field($model->groupMember, 'food_last_3days')->textarea()->label('Какие цели ставите на ближайшие 4 недели?') ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'email')->textInput() ?>
            <?php
            if (in_array(Yii::$app->user->identity->email, \app\helpers\My::$curators_password)) {
                echo $form->field($model->groupMember, 'password')->textInput();
            }
            ?>
            <?= $form->field($model, 'role')->dropDownList(\app\models\User::$roles)->label('Роль') ?>
            <?php
            echo '<label>Дата оплаты</label>';
            echo \kartik\date\DatePicker::widget([
                'model' => $model->groupMember->payment,
                'attribute' => 'date_payment',
                'options' => ['placeholder' => 'Выбрать дату оплаты'],
                //  'convertFormat' => true,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
            </div>
            <div class="form-group">
                <?= Html::a(Yii::t('app', 'Назад'), Yii::$app->request->referrer, ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="col-md-6"></div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExpressExercise */

$this->title = 'Создание упражнения';
$this->params['breadcrumbs'][] = ['label' => 'Список упражнений', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="express-exercise-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Упражнения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'text',
                'format' => 'html',
                'value' => function (\app\models\ExpressExercise $model) {
                    return \yii\helpers\StringHelper::truncate($model->text, 25, '...', null, true);
                }
            ],
            [
                'attribute' => 'type',
                'value' => function (\app\models\ExpressExercise $model) {
                    return \app\models\ExpressExercise::$types[$model->type];
                },
                'filter' => \app\models\ExpressExercise::$types
            ],
            [
                'label' => 'Видео',
                'attribute' => 'video',
                'headerOptions' => ['style' => 'width:20%'],
                'content' => function (\app\models\ExpressExercise $data) {
                    return '<iframe width="232" height="128" src="https://www.youtube.com/embed/' . $data->video . '" frameborder="0" allowfullscreen></iframe>';
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update} {delete}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['create']) . '" class="btn btn-default" title="Создать новое упражнение">Создать</a>',
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Упражнения',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Упражнение',
        'itemLabelPlural' => 'Упражнений'
    ]) ?>
</div>

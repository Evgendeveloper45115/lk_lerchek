<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExpressExercise */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="express-exercise-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>


    <?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(\app\models\ExpressExercise::$types) ?>

    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'is_clone')->radioList([1 => 'Да'], ['style' => 'display: grid'])->label('Такой же контент?');
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

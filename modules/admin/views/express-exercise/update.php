<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExpressExercise */

$this->title = 'Редакирование упражнения: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список упражнений', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="express-exercise-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

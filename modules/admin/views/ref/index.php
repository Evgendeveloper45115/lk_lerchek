<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Ref */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Refs';
$this->params['breadcrumbs'][] = $this->title;
?>
<button class="ref_button btn btn-success" action="change-status" data-val="<?= \app\models\Ref::SUCCESS ?>"
        style="margin-bottom: 10px">Выполнено
</button>
<button class="ref_button btn btn-danger" action="change-status" data-val="<?= \app\models\Ref::DECLINED ?>"
        style="margin-bottom: 10px">Отклонить
</button>
<button class="ref_button btn btn-warning" action="change-status-exp" data-val="<?= \app\models\Ref::EXP ?>"
        style="margin-bottom: 10px">Выгрузить
</button>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => '\kartik\grid\CheckboxColumn',
                'checkboxOptions' => function ($model) {
                    if (!$model->status) {
                        return ['disabled' => true];
                    } else {
                        return [];
                    }
                },
            ],
            'phone',
            [
                'attribute' => 'user_id',
                'value' => function (\app\models\Ref $model) {
                    return $model->user->email;
                },
            ],
            'unique_id',
            'sum',
            [
                'attribute' => 'status',
                'value' => function (\app\models\Ref $model) {
                    return \app\models\Ref::$statuses[$model->status];
                },
                'filter' => \app\models\Ref::$statuses
            ],
            [
                'attribute' => 'status_export',
                'value' => function (\app\models\Ref $model) {
                    return \app\models\Ref::$statuses_export[$model->status_export];
                },
                'filter' => \app\models\Ref::$statuses_export
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' =>
                    [
                        'update' => true,
                        'delete' => Yii::$app->user->identity->email == 'admin@lerchek.ru' || Yii::$app->user->identity->email == 'V_shakina@mail.ru' ? true : false
                    ],
                'template' => '<span style="text-wrap: none">{avatar}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'avatar' => function ($url, \app\models\Ref $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', \yii\helpers\Url::to(['/marafon/index', 'avatar' => $model->user->id]), [
                            'title' => 'Аватар',
                            'data' => [
                                'confirm' => 'Переключить на Аватар?',
                                'method' => 'post',
                            ],
                        ]);

                    },
                ]
            ]
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Заявки на выплату',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Заявка',
        'itemLabelPlural' => 'заявок'
    ]) ?>
</div>
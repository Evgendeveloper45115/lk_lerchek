<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Care */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Список', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="care-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

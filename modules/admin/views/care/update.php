<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Care */

$this->title = 'Редатирование';
$this->params['breadcrumbs'][] = ['label' => 'Список', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="care-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

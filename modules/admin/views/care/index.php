<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Уход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'description',
                'format' => 'html',
                'value' => function (\app\models\Care $model) {
                    return \yii\helpers\StringHelper::truncate($model->description, 25, '...', null, true);
                }
            ],
            [
                'attribute' => 'type',
                'value' => function (\app\models\Care $model) {
                    return \app\models\Care::$types[$model->type];
                },
                'filter' => \app\models\Care::$types
            ],
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function (\app\models\Care $model) {
                    if ($model->image) {
                        return Html::img('/uploads/care/' . $model->image, ['style' => 'width:150px']);
                    }
                }
            ],
            [
                'label' => 'Видео',
                'attribute' => 'video_link',
                'headerOptions' => ['style' => 'width:20%'],
                'content' => function (\app\models\Care $data) {
                    return '<iframe width="232" height="128" src="https://www.youtube.com/embed/' . $data->video_link . '" frameborder="0" allowfullscreen></iframe>';
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update} {delete}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['create']) . '" class="btn btn-default" title="Создать новую группу">Создать</a>',
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Группы',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Уход',
        'itemLabelPlural' => 'Уходов'
    ]) ?>
</div>

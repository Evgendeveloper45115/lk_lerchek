<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\VoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Парное голосование';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vote-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            array(
                'attribute' => 'members_id',
                'label' => 'Пара',
                'format' => 'raw',
                'value' => function (\app\models\CoupleMembers $model) {
                    $members = explode('.', $model->members_id);

                    $member_one = \app\models\GroupMember::findOne($members[0]);
                    $member_two = \app\models\GroupMember::findOne($members[1]);
                    $str = $member_one->user->email . ' - ' . $member_one->coupleVoteCount . '<br>';
                    $str .= $member_two->user->email . ' - ' . $member_two->coupleVoteCount . '<br>';
                    return $str;
                },
                'filterInputOptions' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ),

            ),
            [
                'label' => 'К-тво просмотров',
                'format' => 'raw',
                'value' => function (\app\models\CoupleMembers $model) {
                    return $model->counter;
                },

            ],
            [
                'attribute' => 'group_id',
                'format' => 'raw',
                'value' => function (\app\models\CoupleMembers $model) {
                    return $model->group->num;
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Группа'
                ],

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{view}</span>',
                'header' => 'Действия',
                'buttons' => [
                    /*'view' => function ($url, $model) {
                        return Html::a('<span style="color: blue" class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['/admin/user/view', 'id' => $model->groupMemberVote->id]), [
                            'title' => 'Анкета'
                        ]);

                    }*/
                ]
            ]
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Список участников',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Клиент',
        'itemLabelPlural' => 'Клиентов'
    ]) ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vote */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="vote-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="vote-form">
        <?php $form = ActiveForm::begin(); ?>
        <label for="text">Описание</label>
        <?= $form->field($model, 'text', [
            'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
        ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
            'options' => ['rows' => 15],
            'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
            'fileManager' => [
                'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                'connectorRoute' => 'el-finder/connector',
            ],
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

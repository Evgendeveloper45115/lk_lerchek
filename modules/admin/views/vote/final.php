<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $group  \app\models\Group */

$this->title = 'Финалисты';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

?>
<div class="user-index">

    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'email',
                'format' => 'raw',
                'value' => function (\app\models\User $model) {
                    $css = 'black';

                    return '<div style="color: ' . $css . '">' . $model->groupMember->user->email . '</div>';
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],

            ],
            [
                'attribute' => 'first_name',
                'value' => 'groupMember.first_name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Имя'
                ],

            ],
            [
                'attribute' => 'phone',
                'value' => 'groupMember.phone',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Телефон'
                ],

            ],
            [
                'attribute' => 'last_name',
                'value' => 'groupMember.last_name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Фамилия'
                ],

            ],
            [
                'attribute' => 'telegram',
                'value' => 'groupMember.telegram',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Телеграм'
                ],

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' =>
                    [
                        'view' => true,
                        'avatar' => Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN ? true : false
                    ],

                'template' => '<span style="text-wrap: none">{view} {disable} {avatar}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'view' => function ($url, \app\models\User $model) {
                        if ($model->groupMember->visible_curator == 2) {
                            return Html::a('<span style="color: green" class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['/admin/user/view', 'id' => $model->groupMember->id]), [
                                'title' => 'Редактировать'
                            ]);
                        }
                        return Html::a('<span style="color: ' . ($model->groupMember->visible_curator ? "red" : "#3c8dbc") . '" class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['/admin/user/view', 'id' => $model->groupMember->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                    'disable' => function ($url, \app\models\User $model) {
                        return Html::a('<span class="glyphicon glyphicon-retweet"></span>', \yii\helpers\Url::to(['/admin/user/disable', 'id' => $model->groupMember->id]), [
                            'title' => 'Снять красный глаз'
                        ]);

                    },
                    'avatar' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', \yii\helpers\Url::to(['/marafon/index', 'avatar' => $model->id]), [
                            'title' => 'Аватар',
                            'data' => [
                                'confirm' => 'Переключить на Аватар?',
                                'method' => 'post',
                            ],
                        ]);

                    },


                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [

        ],

        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Список финалистов',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Клиент',
        'itemLabelPlural' => 'Клиентов'
    ]) ?>
</div>

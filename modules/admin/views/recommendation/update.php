<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recommendation */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recommendations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="recommendation-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recommendation */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recommendations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recommendation-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

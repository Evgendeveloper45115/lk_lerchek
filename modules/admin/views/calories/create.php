<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Calories */

$this->title = 'Create Calories';
$this->params['breadcrumbs'][] = ['label' => 'Calories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calories-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Calories */

$this->title = 'Update Calories: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Calories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="calories-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

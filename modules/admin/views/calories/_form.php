<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calories-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'protein')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grease')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carbohydrate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sugar')->textInput(['maxlength' => true]) ?>

    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img',
        'pluginOptions' => \app\helpers\My::getImage($model, $model->image, 'calories', 'delete-image')
    ]) ?>
    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'is_clone')->radioList([1 => 'Да'], ['style' => 'display: grid'])->label('Такой же контент?');
    }
    ?>

    <?= $form->field($model, 'cat_id')->dropDownList(\app\models\Calories::getTypesCalories()) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

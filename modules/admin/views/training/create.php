<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Training */

$this->title = Yii::t('app', 'Create Training');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trainings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

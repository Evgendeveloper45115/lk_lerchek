<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Training */
/* @var $group array */
/* @var $filled array */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Тренировки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создание';
?>
<div class="training-update">


    <?= $this->render('_form', [
        'model' => $model,
        'group' => $group,
        'filled' => $filled,
    ]) ?>

</div>

<?php
/* @var $this yii\web\View */

use yii\bootstrap\Html;

?>
<div class="form_group clearfix Training_number_block week-block" data-number="{number}">
    <div>
        <div class="col-md-12">
            <div class="Training-week_text">
                <label style="display: block" class="control-label Training-week_text-label" data-label-text="Описание"
                       for="Training_week_text">Описание</label>
                <textarea style="width: 100%; height: 100px" id="Training_week_text"
                          name="Training[week][{number}][text]"></textarea>
            </div>
        </div>
    </div>

    <div>
        <div class="col-md-6">
            <label class="control-label Training_number_label" data-label-text="Номер тренировки">Номер
                тренировки</label>
        </div>
        <div class="col-md-6">
            <div class="Training-Training_number">
                <?php foreach (\app\models\Training::getExerciseNumber() as $v => $label) { ?>
                    <label><?= Html::checkbox("Training[week][{number}][{$v}]", false, [
                            'value' => $v
                        ]) ?> <?= $label ?></label>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="tpl-training-sub-container">
    </div>
</div>
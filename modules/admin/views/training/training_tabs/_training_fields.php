<div class="field-Training_id-{training_number} week-training-block" data-training_number="{training_number}"
     style="margin-left: 20px;    margin-top: 1vw;">
    <div class="clearfix">
        <div class="col-md-6">
            <label class="control-label sort_index_label" data-label-text="Сортировка" for="Training-sort_index">Сортировка</label>
        </div>
        <div class="col-md-6">
            <input type="text" id="Training-sort_index" class="form-control"
                   name="Training[week][{number}][{training_number}][sort_index]" style="width:100%">
        </div>
    </div>
</div>
<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\JsExpression;
use yii\helpers\Json;
use app\models\Training;

/**
 * @var $this yii\web\View
 * @var $model app\models\Training
 * @var $group array
 * @var $filled array
 * @var $form yii\widgets\ActiveForm
 * @var $user_ex bool
 */

BootstrapPluginAsset::register($this);
$this->registerJs('window["training_group"] = ' . (new JsExpression(Json::encode($group))) . ';')
?>
<div class="Training-form">
    <script type="text/tpl" id="tpl_Training_number">
        <?= Yii::$app->controller->renderPartial('training_tabs/template', ['model' => $model]) ?>










    </script>

    <script type="text/tpl" id="tpl_fields_pair">
        <?= Yii::$app->controller->renderPartial('training_tabs/_training_fields', ['model' => $model]) ?>










    </script>
    <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        echo '<div class="alert-success">' . $message . '</div>';
    }
    ?>

    <?php
    $form = ActiveForm::begin([
        'id' => 'training_form',
        'fieldConfig' => [
            'options' => ['class' => 'form_group clearfix'],
            'template' => "<div class='col-md-3'>{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"error\">{error}</div>",
        ],
    ]);
    echo $form->field($model, 'type')->radioList(Training::getTrainingTypeAdmin(), ['style' => 'display: grid']);
    if ($model->isNewRecord) {
        echo $form->field($model, 'is_clone')->radioList([1 => 'Да'], ['style' => 'display: grid'])->label('Такой же контент?');
    }
    echo $form->field($model, 'name')->textInput(['maxlength' => 255, 'style' => 'width:100%']);
    echo $form->field($model, 'video')->textInput(['maxlength' => 255, 'style' => 'width:100%']);
    echo $form->field($model, 'body_part')->dropDownList(Training::getBodyParts());
    ?>

    <div class="form_group clearfix field-Training-week">
        <div>
            <div class="col-md-3"><label class="control-label">Неделя</label></div>
            <div class="col-md-9">
                <input type="hidden" name="Training[week]" value="">

                <div id="Training-week">
                    <?php
                    foreach ($filled as $week_n => $checked) { ?>
                        <label>
                            <?= Html::checkbox('week', $checked, [
                                'value' => $week_n,
                                'class' => 'week',
                            ]) ?>
                            <?= Training::getWeekName($week_n) ?>
                        </label>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

    <div class="tpl-training-container"></div>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать упражнение' : 'Изменить упражнение', [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
            ]) ?>
        </div>
    </div>
    <?php
    $form->end();
    ?>
</div>
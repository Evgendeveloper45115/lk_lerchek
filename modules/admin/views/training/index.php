<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тренировки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Видео',
//                        'attribute' => 'video',
                'headerOptions' => ['style' => 'width:20%'],
                'content' => function ($data) {
                    return "<div style='text-align: center; font-weight: bold;'>$data->name</div>" . '<iframe width="232" height="128" src="https://www.youtube.com/embed/' . $data->video . '" frameborder="0" allowfullscreen></iframe>';
                },
            ],
            [
                'attribute' => 'type',
                'value' => function (\app\models\Training $model) {
                    return $model::getTrainingTypeAdmin()[$model->type];
                },
                'filter' => \app\models\Training::getTrainingTypeAdmin()

            ],
            [
                'label' => 'Неделя',
                'attribute' => 'number',
                'headerOptions' => ['style' => 'width:10%'],
                'format' => 'html',
                'value' => function ($data) {
                    /* @var $data \app\models\Training */
                    return implode('я-неделя<br> ', \yii\helpers\ArrayHelper::map($data->weeks, 'id', 'number')) . ($data->weeks ? 'я-неделя' : null);
                },
                'filter' => \app\models\Training::getWeeksTraining(),

            ],
            [
                'label' => 'Номер тренировки',
                'attribute' => 'trainingNumber',
                'headerOptions' => ['style' => 'width:10%'],
                'format' => 'html',
                'value' => function ($data) {
                    /* @var $data \app\models\Training */
                    if ($data->weeks) {
                        $text = '';
                        foreach ($data->weeks as $key => $weeks) {
                            $text .= implode(',', \yii\helpers\ArrayHelper::map($weeks->weekTrainings, 'id', 'training_number')) . '<br>';
                        }
                        return $text;
                    }
                },
                'filter' => \app\models\Training::getExerciseNumber(),

            ],
            [
                'label' => 'Часть тела',
                'attribute' => 'body_part',
                'headerOptions' => ['style' => 'width:10%'],
                'value' => function ($data) {
                    return \app\models\Training::getBodyParts($data->body_part);
                },
                'filter' => \app\models\Training::getBodyParts(),
            ],
            [
                'label' => 'Индекс сортировки',
                'attribute' => 'sort_index',
                'headerOptions' => ['style' => 'width:10%'],
                'format' => 'html',
                'value' => function ($data) {
                    /* @var $data \app\models\Training */
                    if ($data->weeks) {
                        $text = '';
                        foreach ($data->weeks as $key => $weeks) {
                            $text .= implode(',', \yii\helpers\ArrayHelper::map($weeks->weekTrainings, 'id', 'sort_index')) . '<br>';
                        }
                        return $text;
                    }
                },

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update} {delete}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['create']) . '" class="btn btn-default" title="Создать новую тренировку">Создать новую тренировку</a>',
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Тренировки',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Тренировка',
        'itemLabelPlural' => 'Тренировок'
    ]) ?>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рецепты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'type',
                'value' => function (\app\models\Recipe $model) {
                    return $model->getTypes()[$model->type];
                },
                'filter' => \app\models\Recipe::getTypes()
            ],
            'name',
            [
                'attribute' => 'text',
                'format' => 'html',
                'value' => function (\app\models\Recipe $model) {
                    return \yii\helpers\StringHelper::truncate($model->text, 25, '...', null, true);
                }
            ],
            [
                'attribute' => 'text_cut',
                'format' => 'html',
                'value' => function (\app\models\Recipe $model) {
                    return \yii\helpers\StringHelper::truncate($model->text_cut, 25, '...', null, true);
                }
            ],
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function (\app\models\Recipe $model) {
                    if ($model->image) {
                        return Html::img('/uploads/recipe/' . $model->image, ['style' => 'width:150px']);
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update} {delete}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['create']) . '" class="btn btn-default" title="Создать новую группу">Создать новый рецепт</a>',
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Рецепты',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Рецепт',
        'itemLabelPlural' => 'Рецепты'
    ]) ?>
</div>

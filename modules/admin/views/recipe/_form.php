<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Recipe;

/* @var $this yii\web\View */
/* @var $model Recipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recipe-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="col-md-12">
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList(Recipe::getTypes()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'text_cut')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <label for="text">Описание</label>
    <?= $form->field($model, 'text', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img',
        'pluginOptions' => \app\helpers\My::getImage($model, $model->image, 'recipe', '/recipe/delete-image')
    ]) ?>
    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'is_clone')->radioList([1 => 'Да'], ['style' => 'display: grid'])->label('Такой же контент?');
    }

    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

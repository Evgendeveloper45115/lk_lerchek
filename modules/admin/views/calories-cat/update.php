<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaloriesCat */

$this->title = 'Редактировние категории:' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="calories-cat-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

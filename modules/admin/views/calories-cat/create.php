<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaloriesCat */

$this->title = 'Создание категории';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calories-cat-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WeekProducts */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Week Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="week-products-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use kartik\form\ActiveForm;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WeekProducts */

$this->title = 'Детокс день';
$this->params['breadcrumbs'][] = ['label' => 'Детокс день', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="week-products-create">

    <div class="week-products-form">

        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <label for="text">Описание</label>
        <?= $form->field($model, 'text', [
            'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
        ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
            'options' => ['rows' => 15],
            'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
            'fileManager' => [
                'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                'connectorRoute' => 'el-finder/connector',
            ],
        ]); ?>

        <?= \kartik\file\FileInput::widget([
            'model' => $model,
            'language' => 'ru',
            'attribute' => 'img',
            'pluginOptions' => \app\helpers\My::getImage($model, $model->image, 'detox-day', '/week-products/delete-image-detox')
        ]) ?>
        <?php
        if ($model->isNewRecord) {
            echo $form->field($model, 'is_clone')->radioList([1 => 'Да'], ['style' => 'display: grid'])->label('Такой же контент?');
        }
        ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WeekProducts */

$this->title = 'Создать продукт на неделю';
$this->params['breadcrumbs'][] = ['label' => 'Week Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="week-products-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WeekProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="week-products-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <label for="text">Описание</label>
    <?= $form->field($model, 'text', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>

    <?= $form->field($model, 'veg')->radioList(\app\models\RationComplex::$rationTypes) ?>
    <?= $form->field($model, 'week')->radioList(
        \app\models\RationComplex::getWeekRation()
    ) ?>

    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img',
        'pluginOptions' => \app\helpers\My::getImage($model, $model->image, 'week-products', '/week-products/delete-image')
    ]) ?>
    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'is_clone')->radioList([1 => 'Да'], ['style' => 'display: grid'])->label('Такой же контент?');
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

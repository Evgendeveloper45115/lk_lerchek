<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Potato */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Potatoes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="potato-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

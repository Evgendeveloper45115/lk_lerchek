<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Potato */

$this->title = 'Update Potato: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Potatoes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="potato-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $member \app\models\GroupMember */
/* @var $form \kartik\form\ActiveForm */
/* @var $reportsView \app\models\ReportHasUser[] */
?>

<div class="questions-form">
    <?php
    \yii\widgets\Pjax::begin();
    foreach ($reportsView as $reportView) {
        ?>
        <div class="recipe-form">
            <?php
            $form = \yii\widgets\ActiveForm::begin([
                'fieldConfig' => [
                    'options' => ['class' => 'form_group clearfix', 'enctype' => 'multipart/form-data',],
                    'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
                ],
            ]); ?>
            <div class="form_group clearfix" style="margin-top: 20px">
                <div class="col_left with-back">
                    <div>
                        <b><?= !$reportView->admin_text ? 'Нерешенный вопрос:' : 'ответ получен:' ?> Неделя
                            № <?= $reportView->report->week ?></b><br> <?= $reportView->user_text ?>
                    </div>
                </div>
            </div>
            <?= $form->field($reportView, 'id')->hiddenInput()->label(false) ?>
            <?= $form->field($reportView, 'admin_text', [
                'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
            ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
                'options' => ['rows' => 15, 'value' => !$reportView->admin_text ? ('Здравствуйте, ' . $member->first_name . ' ' . $member->last_name . '! ') : $reportView->admin_text],
                'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
                'fileManager' => [
                    'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
                    'connectorRoute' => 'el-finder/connector',
                ],
            ]); ?>


            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Ответить' : 'Ответить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php \yii\widgets\ActiveForm::end();
            ?>
        </div>
        <?php
    }
    \yii\widgets\Pjax::end();
    ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $member app\models\GroupMember */
/* @var $reportView \app\models\ReportHasUser[] */
$this->title = 'Ответ для ' . $model->groupMember->user->email;
$this->params['breadcrumbs'][] = ['label' => 'Список отчетов', 'url' => ['reports']];
$this->params['breadcrumbs'][] = $model->groupMember->user->email;
?>
<div class="questions-update">

    <?= $this->render('_form_answer', [
        'reportsView' => $reportView,
        'member' => $member,
    ]) ?>

</div>

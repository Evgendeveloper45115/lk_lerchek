<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчеты клиентов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'member_id',
                'value' => function (\app\models\ReportHasUser $model) {
                    return $model->groupMember->user->email;
                }
            ],
            [
                'attribute' => 'count',
                'label' => 'Неотвеченных вопросов',
                'value' => function (\app\models\ReportHasUser $model) {
                    return 'Неотвеченных вопросов: ' . \app\models\ReportHasUser::find()->where(['member_id' => $model->member_id])->andWhere(['status' => 0])->count();
                }
            ],

            'date_added',
            [
                'attribute' => 'user_text',
                'value' => function (\app\models\ReportHasUser $model) {
                    return \yii\helpers\StringHelper::truncate($model->user_text, 25);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['report/answer', 'id' => $model->id]), [
                            'title' => 'Ответить'
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Отчеты',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Отчет',
        'itemLabelPlural' => 'Отчеты'
    ]) ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Report */

$this->title = 'Создать отчет';
$this->params['breadcrumbs'][] = ['label' => 'Список отчетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

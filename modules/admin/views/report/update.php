<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Report */

$this->title = 'Редактирование: ' . $model->title_adm;
$this->params['breadcrumbs'][] = ['label' => 'Список отчетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title_adm;
?>
<div class="report-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

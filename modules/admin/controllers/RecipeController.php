<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\User;
use Yii;
use app\models\Recipe;
use app\models\search\RecipeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RecipeController implements the CRUD actions for Recipe model.
 */
class RecipeController extends MyController
{
    /**
     * @return string
     * @throws \Throwable
     */
    public function actionIndex()
    {
        $searchModel = new RecipeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recipe model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recipe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recipe();

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(Recipe::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Recipe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(Recipe::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Успешно!');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Recipe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recipe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recipe::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function saveFile($model)
    {
        $dir = Yii::getAlias('@webroot/uploads/recipe/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file = UploadedFile::getInstance($model, 'img');
        if (!empty($file)) {
            if ($model->image) {
                unlink($dir . $model->image);
            }
            $fname = uniqid("", true) . '.' . $file->extension;
            $model->image = $fname;
            return $file->saveAs($dir . $fname);
        }
        return true;
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@webroot/uploads/recipe/');
        unlink($dir . $model->image);
        $model->image = null;
        return json_encode($model->save(false));
    }

}

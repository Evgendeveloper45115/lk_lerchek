<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\CaloriesCat;
use app\models\User;
use Yii;
use app\models\Calories;
use app\models\search\CaloriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CaloriesController implements the CRUD actions for Calories model.
 */
class CaloriesController extends MyController
{

    /**
     * Lists all Calories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CaloriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Calories model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Calories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Calories();

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(Calories::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Calories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(Calories::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Успешно!');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Calories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calories::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function saveFile($model)
    {
        $dir = Yii::getAlias('@webroot/uploads/calories/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file = UploadedFile::getInstance($model, 'img');
        if (!empty($file)) {
            if ($model->image) {
                unlink($dir . $model->image);
            }
            $fname = uniqid("", true) . '.' . $file->extension;
            $model->image = $fname;
            return $file->saveAs($dir . $fname);
        }
        return true;
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@webroot/uploads/calories/');
        unlink($dir . $model->image);
        $model->image = null;
        return json_encode($model->save(false));
    }

}

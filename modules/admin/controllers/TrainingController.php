<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\User;
use app\models\Week;
use app\models\WeekTraining;
use Yii;
use app\models\Training;
use app\models\search\TrainingSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrainingController implements the CRUD actions for Training model.
 */
class TrainingController extends MyController
{

    /**
     * @return string
     * @throws \Throwable
     */
    public function actionIndex()
    {
        $searchModel = new TrainingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreate()
    {
        return $this->_edit();
    }


    /**
     * @param $id
     * @param null $group_id
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id, $group_id = null)
    {
        return $this->_edit($id);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _edit($id = null)
    {
        if (!$id || (!$model = Training::find()->getContent()->where(['training.id' => $id])->one())) {
            $model = new Training();
        }

        $models = WeekTraining::find()
            ->alias('wt')
            ->leftJoin(['w' => Week::tableName()], 'wt.week_id = w.id')
            ->where(['w.training_id' => $model->id])
            ->select([
                'wt.id',
                'wt.week_id',
                'wt.training_number',
                'wt.sort_index',

                'w.training_id',
                'w.number',
                'w.text',
            ])
            ->orderBy(['wt.sort_index' => SORT_ASC])
            ->groupBy([
                'wt.id',
            ])
            ->asArray()
            ->all();

        $group = ArrayHelper::index($models, 'training_number', ['number']);
        $filled = Training::getFilledWeeksAdmin(array_flip(array_keys($group)));

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post('Training');
            if ($model->load(Yii::$app->request->post('Training'), '')) {
                foreach ($model->weeks as $week) {
                    foreach ($week->weekTrainings as $wt) {
                        $wt->delete();
                    }
                    $week->delete();
                }
                if ($model->save() && My::createTypeProgram(Training::class, $model->id)) {
                    if (!empty($post['week']) && is_array($post['week'])) {
                        foreach ($post['week'] as $week_id => $data) {
                            $week = new Week();
                            $week->training_id = $model->id;
                            $week->number = $week_id;
                            $week->text = $data['text'];

                            if ($week->save()) {
                                foreach ($data as $week_training_number => $v) {
                                    if (!empty($v) && is_array($v)) {
                                        $weekTraining = new WeekTraining();
                                        $weekTraining->week_id = $week->id;
                                        $weekTraining->training_number = $week_training_number;
                                        $weekTraining->sort_index = $v['sort_index'];
                                        $weekTraining->save();
                                    }
                                }
                            }
                        }
                    }
                    Yii::$app->getSession()->setFlash('success', 'Сохранено', false);
                    return $this->redirect('index');
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'group' => $group,
            'filled' => $filled,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Успешно!');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Training::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

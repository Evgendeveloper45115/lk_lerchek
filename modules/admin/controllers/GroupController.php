<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\ContentType;
use app\models\GroupMember;
use app\models\Popups;
use app\models\Progress;
use app\models\User;
use app\models\UserSearch;
use Yii;
use app\models\Group;
use app\models\search\GroupSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends MyController
{
    /**
     * @return string
     * @throws \Throwable
     */
    public function actionIndex()
    {
        /** @var $user User */
        $user = Yii::$app->user->identity;
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->setPageSize(100);
        $count = GroupMember::find()->where(['type_program' => $user->session_type])->andWhere(['is not', 'group_id', null])->count();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'count' => $count,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $model = new Group();

        if ($model->load(Yii::$app->request->post())) {
            $model->datetime_create = date('Y-m-d H:i:s');
            $date_end = strtotime($model->datetime_start);
            $model->datetime_end = date('Y-m-d H:i:s', strtotime('+' . Yii::$app->params['program']['for-girl'] . ' days', $date_end));
            if ($model->save(false) && My::createTypeProgram(Group::class, $model->id)) {
                Yii::$app->session->setFlash('success', 'Успешно измененно');
                return $this->redirect(Url::to(['index']));
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка =(');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var $user User */
        $model = $this->findModel($id);
        $user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post()) && $model->save() && My::createTypeProgram(Group::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно измененно');
            if (!stristr($model->num, 'заморозка') && !stristr($model->num, 'test') && !stristr($model->num, 'продолжение')) {
                $content_types = ContentType::find()->where(['class_name' => Group::class, 'type_program' => $user->session_type])->all();
                foreach ($content_types as $content_type) {
                    $group = Group::findOne($content_type->relation_id);
                    if ($group && !stristr($group->num, 'заморозка') && !stristr($group->num, 'разморозка') && !stristr($group->num, 'test') && !stristr($group->num, 'продолжение')) {
                        $group->date_continue = $model->date_continue;
                        $group->datetime_start = $model->datetime_start;
                        $group->datetime_end = $model->datetime_end;
                        $group->save(false);
                    }
                }
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Успешно!');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionViewUsers($id)
    {
        $searchModel = new \app\models\search\UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $group = Group::findOne($id);
        return $this->render('user-group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'group' => $group,
        ]);

    }

    public function actionSetGroupCompleted($id)
    {
        /**
         * @var $members GroupMember[]
         */
        $members = GroupMember::find()->where(['group_id' => $id])->all();
        foreach ($members as $member) {
            if (!$member->progressPopup) {
                $model = new Popups();
                $model->member_id = $member->id;
                $model->popup_id = 1;
                $model->status = 0;
                $model->save(false);
            }
        }
        Yii::$app->session->setFlash('success', 'Успешно!');
        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionDeleteGroupCompleted($id = null)
    {
        /**
         * @var $members GroupMember[]
         */
        if ($id) {
            $members = GroupMember::find()->where(['group_id' => $id])->all();
            foreach ($members as $member) {
                if (!$member->payment) {
                    continue;
                }
                if ($member->progressPopup) {
                    $member->progressPopup->delete();
                }
            }
        } else {
            $members = Yii::$app->db->createCommand('SELECT id FROM group_member WHERE group_id is not null AND type_program=' . Yii::$app->user->identity->session_type)->queryAll();
            Yii::$app->db->createCommand('DELETE FROM popups WHERE member_id IN (' . implode(',', ArrayHelper::map($members, 'id', 'id')) . ') AND popup_id = 2')->execute();
        }

        Yii::$app->session->setFlash('warning', 'Успешно');
        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionDeleteGroupVisible($id = null)
    {
        /**
         * @var $members GroupMember[]
         */
        if ($id) {
            Yii::$app->db->createCommand('UPDATE group_member SET group_member.visible_curator = 0 WHERE group_id =' . $id)->execute();
        } else {
            Yii::$app->db->createCommand('UPDATE group_member SET group_member.visible_curator = 0 WHERE type_program =' . Yii::$app->user->identity->session_type)->execute();
        }
        Yii::$app->session->setFlash('warning', 'Успешно');
        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionChangeGroup($id)
    {
        /**
         * @var $group Group
         */
        $group = Group::findOne($id);
        $contentType = ContentType::findOne(['class_name' => 'app\models\Group', 'relation_id' => $group->id]);
        if ($contentType) {
            $contentType->type_program = $contentType->type_program + 2;
            $contentType->save();
            foreach ($group->groupMembers as $member) {
                $member->type_program = Yii::$app->user->identity->session_type + 2;
                $member->save(false);
                $member->user->session_type = Yii::$app->user->identity->session_type + 2;
                $member->user->save(false);
            }
        }
        Yii::$app->session->setFlash('warning', 'Успешно');
        return $this->redirect(Yii::$app->request->referrer);
    }
}

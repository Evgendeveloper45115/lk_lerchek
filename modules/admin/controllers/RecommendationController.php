<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\User;
use Yii;
use app\models\Recommendation;
use app\models\search\RecommendationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RecommendationController implements the CRUD actions for Recommendation model.
 */
class RecommendationController extends MyController
{
    /**
     * @return string
     * @throws \Throwable
     */
    public function actionIndex()
    {
        $searchModel = new RecommendationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recommendation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recommendation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recommendation();

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(Recommendation::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Recommendation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(Recommendation::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Успешно!');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Recommendation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recommendation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recommendation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $model
     * @return bool
     */
    public function saveFile($model)
    {
        $dir = Yii::getAlias('@webroot/uploads/recommendation/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file = UploadedFile::getInstance($model, 'img');
        if (!empty($file)) {
            if ($model->image) {
                unlink($dir . $model->image);
            }
            $fname = uniqid("", true) . '.' . $file->extension;
            $model->image = $fname;
            return $file->saveAs($dir . $fname);
        }
        return true;
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@webroot/uploads/recommendation/');
        unlink($dir . $model->image);
        $model->image = null;
        return json_encode($model->save(false));
    }
}

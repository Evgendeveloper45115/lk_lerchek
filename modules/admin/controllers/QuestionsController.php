<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\GroupMember;
use app\models\search\CuratorQuestionsSearch;
use app\models\User;
use Yii;
use app\models\Questions;
use app\models\search\QuestionsSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends MyController
{

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCurators()
    {
        /** @var $user User */
        $user = Yii::$app->user->identity;
        if ($user->email != 'admin@lerchek.ru') {
            Yii::$app->session->setFlash('error', 'Ошибка доступа');
            return $this->redirect('/');
        }
        $searchModel = new CuratorQuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('curators', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAnswer($id)
    {
        /**
         * @var $questions Questions
         */
        $questions = Questions::find()->where(['questions.id' => $id])->one();
        if ($questions === null) {
            return $this->redirect(['/admin/questions/index']);
        }
        $idPost = Yii::$app->request->post('Questions')['id'];
        if ($idPost) {
            $questions = Questions::find()->where(['questions.id' => $idPost])->one();
        }

        if ($questions) {
            if ($questions->load(Yii::$app->request->post())) {
                $questions->status = 1;
                $questions->curator_id = Yii::$app->user->id;
                $questions->date_added = date('Y-m-d H:i:s');
                $questions->save();
                Yii::$app->session->setFlash('success', 'Успешно!');
            }
        }
        $troublesView = Questions::find()->where(['member_id' => $questions->member_id])->orderBy('id DESC')->all();
        if (empty($troublesView)) {
            return $this->redirect(['/admin/question/index']);
        }
        return $this->render('update', ['troublesView' => $troublesView, 'member' => $questions->groupMember, 'model' => $questions]);

    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Questions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        /**
         * @var $questions Questions
         */
        $model = $this->findModel($id);
        $member_id = $model->member_id;
        $model->delete();
        $questions = Questions::find()->where(['member_id' => $member_id])->one();
        Yii::$app->session->setFlash('success', 'Успешно!');
        if ($questions) {
            return $this->redirect(['/admin/questions/answer', 'id' => $questions->id]);
        }
        return $this->redirect(['/admin/questions/index']);
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\DetoxDay;
use app\models\Training;
use app\models\User;
use Yii;
use app\models\WeekProducts;
use app\models\search\WeekProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * WeekProductsController implements the CRUD actions for WeekProducts model.
 */
class WeekProductsController extends MyController
{
    /**
     * @return string
     * @throws \Throwable
     */
    public function actionIndex()
    {
        $searchModel = new WeekProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WeekProducts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WeekProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WeekProducts();

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(WeekProducts::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WeekProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save() && My::createTypeProgram(WeekProducts::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('success', 'Успешно!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the WeekProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WeekProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WeekProducts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function saveFile($model)
    {
        $dir = Yii::getAlias('@webroot/uploads/week-products/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file = UploadedFile::getInstance($model, 'img');
        if (!empty($file)) {
            if ($model->image) {
                unlink($dir . $model->image);
            }
            $fname = uniqid("", true) . '.' . $file->extension;
            $model->image = $fname;
            return $file->saveAs($dir . $fname);
        }
        return true;
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@webroot/uploads/week-products/');
        unlink($dir . $model->image);
        $model->image = null;
        return json_encode($model->save(false));
    }

    public function saveFileDetox($model)
    {
        $dir = Yii::getAlias('@webroot/uploads/detox-day/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file = UploadedFile::getInstance($model, 'img');
        if (!empty($file)) {
            if ($model->image) {
                unlink($dir . $model->image);
            }
            $fname = uniqid("", true) . '.' . $file->extension;
            $model->image = $fname;
            return $file->saveAs($dir . $fname);
        }
        return true;
    }

    public function actionDeleteImageDetox($id)
    {
        $model = DetoxDay::find()->limit(1)->orderBy('id DESC')->where(['type_program' => Yii::$app->getUser()->getIdentity()->session_type])->all();
        if (empty($model)) {
            $model = new DetoxDay();
        } else {
            $model = $model[0];
        }
        $dir = Yii::getAlias('@webroot/uploads/detox-day/');
        unlink($dir . $model->image);
        $model->image = null;
        return json_encode($model->save(false));
    }

    public function actionDetoxDay()
    {
        $model = DetoxDay::find()->limit(1)->orderBy('id DESC')->getContent()->all();
        if (empty($model)) {
            $model = new DetoxDay();
        } else {
            $model = $model[0];
        }
        if ($model->load(Yii::$app->request->post()) && $this->saveFileDetox($model) && $model->save() && My::createTypeProgram(DetoxDay::class, $model->id)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['detox-day']);
        }

        return $this->render('detox-day', [
            'model' => $model,
        ]);

    }
}

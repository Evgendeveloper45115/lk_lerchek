<?php

namespace app\modules\admin\controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use app\models\Ref;
use app\models\search\Ref as RefSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefController implements the CRUD actions for Ref model.
 */
class RefController extends MyController
{
    /**
     * Lists all Ref models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RefSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ref model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ref model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ref();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ref model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ref model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ref model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ref the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ref::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionChangeStatus()
    {
        foreach ($_POST['ids'] as $id) {
            $ref = Ref::findOne($id);
            $ref->status = $_POST['value'];
            $ref->save(false);
        }
        return false;
    }

    public function actionChangeStatusExp()
    {
        /**
         * @var $models Ref[]
         */
        $file_name = date('Y-m-d') . '-payment-client.xlsx';
        $file = \Yii::getAlias('@webroot') . '/uploads/' . $file_name;
        $models = Ref::find()->where(['id' => $_POST['ids']])->all();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        foreach ($models as $model) {
            $sheet->setCellValue('A' . $i, $model->id);
            $sheet->setCellValue('B' . $i, $model->unique_id);
            $sheet->setCellValue('C' . $i, $model->phone);
            $sheet->setCellValue('D' . $i, preg_replace("/[^0-9]/", '', $model->sum));
            $sheet->setCellValue('E' . $i, 'RUB');
            $sheet->setCellValue('F' . $i, 'Да');
            $i++;
            $model->status_export = Ref::EXP;
            $model->save(false);
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
        return '/uploads/' . $file_name;
    }
}

<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\ContentType;
use app\models\Faq;
use app\models\Ingredient;
use app\models\User;
use Yii;
use app\models\RationComplex;
use app\models\search\RationComplex as RationComplexSearch;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FoodController implements the CRUD actions for RationComplex model.
 */
class FoodController extends MyController
{
    /**
     * Lists all RationComplex models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RationComplexSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RationComplex model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param int $ration_type
     * @return string
     * @throws ErrorException
     * @throws \yii\db\Exception
     */
    public function actionCreate($ration_type)
    {
        $model = new RationComplex();

        if ($model->load(Yii::$app->request->post())) {
            $week_key = null;
            $day_key = null;
            if (Yii::$app->request->post('week_clone_exec') !== null) {
                $week_key = Yii::$app->request->post('week_key');
                $ids = RationComplex::find()->getContent()->select('ration_complex.id')->where([
                    'ration_type' => $ration_type,
                    'week' => $model->week,
                ])->createCommand()->queryAll();
                Ingredient::deleteAll(['ration_complex_id' => $ids]);
                RationComplex::deleteAll(['id' => $ids]);
                $cloneWeek = RationComplex::find()->getContent()->where([
                    'ration_type' => $ration_type,
                    'week' => $week_key,
                ])->all();
                foreach ($cloneWeek as $to) {
                    $m = new RationComplex();
                    $m->setAttributes($to->attributes);
                    $m->week = $model->week;
                    $m->image = $this->cloneImage($to->image);
                    $m->ration_type = $ration_type;
                    $m->save(false);
                    My::createTypeProgram(RationComplex::class, $m->id);
                    foreach (Ingredient::find()->where([
                        'ration_complex_id' => $to->id,
                    ])->all() as $ing_to) {
                        $ing_new = new Ingredient();
                        $ing_new->setAttributes($ing_to->attributes);
                        $ing_new->ration_complex_id = $m->id;
                        $ing_new->save(false);
                    }
                }
            } elseif (Yii::$app->request->post('day_clone_exec') !== null) {
                $day_key = Yii::$app->request->post('day_key');
                $ids = RationComplex::find()->getContent()->select('ration_complex.id')->where([
                    'ration_type' => $ration_type,
                    'week' => $model->week,
                    'day' => $model->day,
                ])->createCommand()->queryAll();
                Ingredient::deleteAll(['ration_complex_id' => $ids]);
                RationComplex::deleteAll(['id' => $ids]);
                foreach (RationComplex::find()->getContent()->where([
                    'ration_type' => $ration_type,
                    'week' => $model->week,
                    'day' => $day_key,
                ])->all() as $to) {
                    $m = new RationComplex();
                    $m->setAttributes($to->attributes);
                    $m->day = $model->day;
                    $m->ration_type = $ration_type;
                    $m->image = $this->cloneImage($to->image);
                    $m->save(false);
                    My::createTypeProgram(RationComplex::class, $m->id);

                    foreach (Ingredient::find()->where([
                        'ration_complex_id' => $to->id,
                    ])->all() as $ing_to) {
                        $ing_new = new Ingredient();
                        $ing_new->setAttributes($ing_to->attributes);
                        $ing_new->ration_complex_id = $m->id;
                        $ing_new->save(false);
                    }
                }
            }

            if (!$week_key && !$day_key) {
                $ration = RationComplex::find()->getContent()->where([
                    'ration_type' => $ration_type,
                    'week' => $model->week,
                    'day' => $model->day,
                    'day_part' => $model->day_part,
                ])->createCommand()->queryOne();
                $image = $ration['image'];
                $dir = Yii::getAlias('@webroot/uploads/food/');
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }
                if ($file = UploadedFile::getInstance($model, 'img')) {
                    $fname = 'new_ration_' . uniqid("", true) . '.' . $file->extension;
                    if (file_exists($dir . $fname)) {
                        unlink($dir . $fname);
                    }
                    $model->image = $fname;
                    $file->saveAs($dir . $fname);
                } else {
                    $model->image = $image;
                }
                $imgDel = Yii::$app->request->post('delete-img');
                if ($imgDel === '') {
                    if (file_exists($dir . $model->image)) {
                        unlink($dir . $model->image);
                    }
                    $model->image = null;
                }
                Ingredient::deleteAll(['ration_complex_id' => $ration['id']]);
                RationComplex::deleteAll(['id' => $ration['id']]);
                ContentType::deleteAll(['relation_id' => $ration['id'], 'class_name' => RationComplex::class]);
                if ($model->save(false) && My::createTypeProgram(RationComplex::class, $model->id)) {
                    if (sizeof($post = Yii::$app->request->post('Ingredient', []))) {
                        Ingredient::deleteAll(['ration_complex_id' => $model->id]);
                        foreach (array_keys($post['name']) as $k) {
                            $ing = new Ingredient();
                            $ing->ration_complex_id = $model->id;
                            $ing->name = $post['name'][$k];
                            $ing->value = $post['value'][$k];
                            $ing->count_symbol = $post['count_symbol'][$k];
                            $ing->save(false);
                        }
                    }
                } else {
                    throw new ErrorException($model->getErrors());
                }
            }
        }
        $model_groups = ArrayHelper::index(RationComplex::find()->getContent()->where(['ration_type' => $ration_type])->all(), 'day_part', [
            'week',
            'day',
        ]);
        $rationComplexIngredients = ArrayHelper::index(Ingredient::find()->all(), 'id', 'ration_complex_id');
        return $this->render('create-complex', [
            'model' => $model,
            'model_groups' => $model_groups,
            'rationComplexIngredients' => $rationComplexIngredients,
            'ration_type' => $ration_type,
        ]);
    }

    public function cloneImage($image)
    {
        $ext = stristr('jpg', $image) ? 'jpg' : (stristr('png', $image) ? 'png' : 'jpg');
        $dir = Yii::getAlias('@webroot/uploads/food/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $fname = 'food_copy' . uniqid("", true) . '.' . $ext;
        copy($dir . $image, $dir . $fname);
        return $fname;
    }

    /**
     * Updates an existing RationComplex model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $food = $this->findModel($id);
        Ingredient::deleteAll(['ration_complex_id' => $food->id]);
        $food->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the RationComplex model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RationComplex the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RationComplex::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionBok()
    {
        /**
         * @var $rations RationComplex[]
         */
        $rations = RationComplex::find()->getContent()->where(['ration_type' => 2])->andWhere(['week' => [5, 6, 7, 8]])->all();
        foreach ($rations as $ration) {
            $week = 1;
            if ($ration->week == 6) {
                $week = 2;
            } elseif ($ration->week == 7) {
                $week = 3;
            } elseif ($ration->week == 8) {
                $week = 4;
            }
            $newRation = new RationComplex();
            $newRation->ration_type = $ration->ration_type;
            $newRation->week = $week;
            $newRation->day = $ration->day;
            $newRation->day_part = $ration->day_part;
            $newRation->name = $ration->name;
            $newRation->portion = $ration->portion;
            $newRation->calories = $ration->calories;
            $newRation->protein = $ration->protein;
            $newRation->grease = $ration->grease;
            $newRation->carbohydrate = $ration->carbohydrate;
            $newRation->image = $ration->image;
            $newRation->description = $ration->description;
            $newRation->save(false);
            foreach ($ration->ingredients as $ingredients) {
                $newIng = new Ingredient();
                $newIng->name = $ingredients->name;
                $newIng->value = $ingredients->value;
                $newIng->count_symbol = $ingredients->count_symbol;
                $newIng->ration_complex_id = $newRation->id;
                $newIng->save(false);
                $ingredients->delete();
            }
            $ration->delete();
            $model = new ContentType();
            $model->type_program = 3;
            $model->class_name = RationComplex::class;
            $model->relation_id = $newRation->id;
            $model->save(false);
        }
        exit;
    }

}

<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\Calories;
use app\models\CoupleMembers;
use app\models\forms\UserView;
use app\models\GroupMember;
use app\models\PaymentLer;
use app\models\Popups;
use app\models\Recipe;
use Yii;
use app\models\User;
use app\models\search\UserSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends MyController
{

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = GroupMember::find()->where(['id' => $id])->one();
        $model->visible_curator = in_array(Yii::$app->user->identity->email, My::$curators_high) ? 2 : 1;
        $popup = Popups::find()->where(['member_id' => $id, 'popup_id' => 2])->one();
        if ($popup === null) {
            $popup = new Popups();
        }
        if (isset($_POST['no_error'])) {
            $popup->description = '<strong>Ваши фото отсмотрены куратором, замечаний у него нет.
                    Но последняя ответственность все равно за вами лично! Куратор лишь дает свои рекомендации. </strong><br>
                    Все инструкции, как правильно сделать фото и видео, выложены в телеграмм канале и в инструкциях в разделе прогресс. 
                    В случае, если куратор не обнаружил какую-то ошибку, ответственность за правильность фото и надписи все равно лежит на вас!';
            $popup->popup_id = 2;
            $popup->status = 0;
            $popup->member_id = $id;
            $popup->curator_id = Yii::$app->user->id;
            $popup->save(false);
        }
        if (isset($_POST['yes_error'])) {
            $popup->description = $_POST['Popups']['description'];
            $popup->popup_id = 2;
            $popup->status = 0;
            $popup->member_id = $id;
            $popup->curator_id = Yii::$app->user->id;
            $popup->save(false);
        }
        if ($this->saveFile($model) && $model->save() && Yii::$app->request->isPost) {
            Yii::$app->session->setFlash('success', 'Успешно!');
        }

        return $this->render('view', [
            'model' => $model,
            'popup' => $popup,
        ]);
    }

    public function actionDisable($id)
    {
        $model = GroupMember::find()->where(['id' => $id])->one();
        $model->visible_curator = false;
        $model->save(false);
        Yii::$app->session->setFlash('success', 'Успешно!');
        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (
            $model->load(Yii::$app->request->post()) && $model->save(false)
            && $model->groupMember->load(Yii::$app->request->post()) && $model->groupMember->save(false)
            && $model->groupMember->payment->load(Yii::$app->request->post()) && $model->groupMember->payment->save(false)
        ) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->groupMember->payment) {
            $model->groupMember->payment->delete();
        }
        $model->groupMember->delete();
        if (empty($model->groupMembers)) {
            $model->delete();
        }
        Yii::$app->session->setFlash('success', 'Успешно удален!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionPaymentAdmin()
    {
        /**
         * @var $user User
         */
        $this->enableCsrfValidation = false;
        $post = Yii::$app->request->post();
        $user = User::find()->where(['email' => $post['email']])->one();
        if (!$user) {
            $user = new User();
        }
        $user->setAttributesAdmin();
        if ($user->save(false)) {
            $member = $user->groupMember;
            if ($member === null) {
                $member = new GroupMember();
            }
            $member->setAttributesAdmin($user->id);
            if ($member->save(false)) {
                $pay = $user->groupMember->payment;
                if ($pay === null) {
                    $pay = new PaymentLer();
                }
                $pay->setAttributesAdmin($member->id);
                if ($pay->save(false)) {
                    My::sendEmail($member, 'generate-link-' . My::$type_program[$user->session_type], Yii::$app->params['generate-link-' . My::$type_program[$user->session_type]]);
                    Yii::$app->session->setFlash('success', 'Успешно! ' . $user->email . ' создан!');
                    return false;
                }
            }
        }
        Yii::$app->session->setFlash('error', 'Ошибка попробуйте еще раз!');
        return $this->redirect(['index']);
    }

    public function actionContinue($id)
    {
        $user = User::findOne($id);
        if ($user->groupMember->payment->doplata) {
            $user->groupMember->payment->doplata = false;
            Yii::$app->session->setFlash('success', 'Успешно! ' . $user->email . ' Программа 1.0 вкл!');
        } else {
            $user->groupMember->payment->doplata = true;
            $user->groupMember->payment->date_payment = date('Y-m-d H:i:s');
            Yii::$app->session->setFlash('success', 'Успешно! ' . $user->email . ' Программа 2.0 вкл!');
        }
        $user->groupMember->payment->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionChangeProgram($type_program)
    {
        $user = User::findOne(Yii::$app->getUser()->getId());
        $user->session_type = $type_program;
        $user->save(false);
        Yii::$app->session->setFlash('success', 'Успешно! Вы в программе ' . My::$type_program_rus[$user->session_type]);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSetRating()
    {
        $user = User::findOne(['email' => $_POST['email']]);
        if ($user) {
            if ($_POST['rating'] == 3) {
                \app\models\Vote::deleteAll(['vote_id' => $user->groupMember->id]);
            }
            $user->groupMember->rating = $_POST['rating'];
            $user->groupMember->save(false);
        }
    }

    public function actionRemoveRating()
    {
        $user = User::findOne(['email' => $_POST['email']]);
        if ($user) {
            $user->groupMember->rating = null;
            $user->groupMember->save(false);
        }
    }

    public function actionDeleteImage($id)
    {
        $model = GroupMember::findOne($id);
        $dir = Yii::getAlias('@webroot/uploads/progress/');
        unlink($dir . $model->avatar);
        $model->avatar = null;
        return json_encode($model->save(false));
    }

    public function saveFile($model)
    {
        $dir = Yii::getAlias('@webroot/uploads/progress/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file = UploadedFile::getInstance($model, 'img');
        if (!empty($file)) {
            if ($model->avatar) {
                unlink($dir . $model->avatar);
            }
            $fname = uniqid("", true) . '.' . $file->extension;
            $model->avatar = $fname;
            return $file->saveAs($dir . $fname);
        }
        return true;
    }

    public function actionCreateCouple($group_id)
    {
        /**
         * @var $members GroupMember[]
         */
        $couples = [];
        $members = GroupMember::find()->where(['group_id' => $group_id])->andWhere(['rating' => My::CONCURS])->all();
        CoupleMembers::deleteAll(['group_id' => $group_id]);
        $new_arr_group = [];
        foreach ($members as $key => $member) {
            if ($member->vote_category) {
                $new_arr_group[$member->vote_category][] = $member->id;
            }
        }
        foreach ($new_arr_group as $group => $members) {
            foreach ($members as $key => $member) {
                $numb = $key / 2;
                if (is_int($numb)) {
                    $couples[$group . $key] = $member;
                }
                if (is_float($numb)) {
                    $couples[$group . ($key - 1)] .= '.' . $member;
                }
            }
        }
        $couples = array_values($couples);
        foreach ($couples as $key => $couple) {
            if ($couple) {
                if (!stristr($couple, '.')) {
                    $old = explode('.', $couples[$key - 1]);
                    $couple .= '.' . $old[0];
                }
            }
            $coupleMember = new CoupleMembers();
            $coupleMember->members_id = $couple;
            $coupleMember->counter = 0;
            $coupleMember->group_id = $group_id;
            $coupleMember->save(false);
            My::createTypeProgram(CoupleMembers::class, $coupleMember->id);
        }
        Yii::$app->session->setFlash('success', 'Пары успешно сформировались! ' . count($couples) . ' пар');
        return $this->redirect(Yii::$app->request->referrer);
    }
}

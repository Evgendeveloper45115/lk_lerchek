<?php

namespace app\modules\admin\controllers;

use app\helpers\My;
use app\models\Questions;
use app\models\Recommendation;
use app\models\ReportHasUser;
use app\models\search\QuestionsSearch;
use app\models\search\ReportHasUserSearch;
use app\models\User;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use app\models\Report;
use app\models\search\ReportSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportController extends MyController
{
    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Report model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Report model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Report();

        if ($model->load(Yii::$app->request->post()) && $model->save() && My::createTypeProgram(Report::class, $model->id)) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Report model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save() && My::createTypeProgram(Report::class, $model->id)) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Report::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionReports()
    {
        $searchModel = new ReportHasUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAnswer($id)
    {
        /**
         * @var $RHU ReportHasUser
         */
        $RHU = ReportHasUser::find()->getContent()->where(['report_has_user.id' => $id])->one();
        $idPost = Yii::$app->request->post('ReportHasUser')['id'];
        if ($idPost) {
            $RHU = ReportHasUser::find()->getContent()->where(['report_has_user.id' => $idPost])->one();
        }
        if ($RHU) {
            if ($RHU->load(Yii::$app->request->post())) {
                $RHU->status = 1;
                $RHU->save();
                Yii::$app->session->setFlash('success', 'Успешно!');
            }
        }
        $reportView = ReportHasUser::find()->where(['member_id' => $RHU->member_id])->orderBy('date_added DESC')->all();

        return $this->render('update-answer', ['reportView' => $reportView, 'member' => $RHU->groupMember, 'model' => $RHU]);

    }

    public function actionCsv($link)
    {
        $Data = str_getcsv(file_get_contents($link), "\n", '', ''); //parse the rows
        $new_array = [];
        $new_array2 = [];
        foreach ($Data as &$Row) {
            $Row = str_getcsv($Row, ";");
            $new_array[$Row[0]] += $Row[1];
            $new_array2[$Row[0]] += $Row[2];
        }
        $objPHPExcel = new Spreadsheet();
        $row = 1;
        foreach ($new_array as $name => $val) {
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->
            setCellValue('A' . $row, $name)
                ->setCellValue('B' . $row, $val)
                ->setCellValue('C' . $row, $new_array2[$name]);
            $row++;

        }

        $writer = new Xlsx($objPHPExcel);
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '.xlsx';
        $writer->save($file);
        Yii::$app->response->sendFile($file);
    }


}

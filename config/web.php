<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => 'Марафон Лерчек',
    'timeZone' => 'Europe/Moscow',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'defaultRoute' => 'user/index',
            'layout' => 'main'
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1']
        ],
    ],

    'components' => [
        'imageResizer' => [
            'class' => 'app\helpers\My',
            // directory with images
            'dir' => '/uploads',
            // image sizes. If 'suffix' not set, than width and height be used for suffix name.
            'sizes' => [
                // in this case height will be calculated automatically
                ['width' => 600, 'height' => null, 'suffix' => ''],

            ],
            // handle directory recursively
            'recursively' => true,
            // enable rewrite thumbs if its already exists
            'enableRewrite' => true,
            // array|string the driver to use. This can be either a single driver name or an array of driver names.
            // If the latter, the first available driver will be used.
            'driver' => ['gmagick', 'imagick', 'gd2'],
            // image creation mode.
            'mode' => 'inset',
            // enable to delete all images, which sizes not in $this->sizes array
            'deleteNonActualSizes' => true,
            // background transparency to use when creating thumbnails in `ImageInterface::THUMBNAIL_INSET`.
            // If "true", background will be transparent, if "false", will be white color.
            // Note, than jpeg images not support transparent background.
            'bgTransparent' => false,
            // want you to get thumbs of a fixed size or not. Has no effect, if $mode set "outbound".
            // If "true" then thumbs will be the exact same size as in the $sizes array.
            // The background will be filled with white color.
            // Background transparency is controlled by the parameter $bgTransparent.
            // If "false", then thumbs will have a proportional size.
            // If the size of the thumbs larger than the original image,
            // the thumbs will be the size of the original image.
            'fixedSize' => false,
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'R2L7oAOzYLURyknxlPNI8nlOG5tKYwm4',
        ],
        'assetManager' => [
            'bundles' => [
                'kartik\form\ActiveFormAsset' => [
                    'bsDependencyEnabled' => false // do not load bootstrap assets for a specific asset bundle
                ],
            ],
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true],
            'authTimeout' => 60 * 60 * 24 * 100
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 7 * 24 * 60 * 60],
        ],
        /* 'errorHandler' => [
             'errorAction' => 'marafon/error',
         ],*/


        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                's/<shortID:[\w_\/-]+>' => 'marafon/signup',
                'login' => 'site/login',
            ],
        ],

    ],
    'params' => $params,
];
return $config;

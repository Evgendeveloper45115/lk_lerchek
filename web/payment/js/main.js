$('.menu__button').on('click touch', function () {
    $('.popup__menu').fadeIn('300', function () {
    });
    $('.popup__menu').css({
        display: 'flex'
    })
    $('body').addClass('menu--open')
    $('.button__close').show();
    $('.menu__button').hide()
})


$('.button__close').on('click touch', hideMenu)

function hideMenu() {
    $('.popup__menu').fadeOut('300', function () {
        $('.popup__menu').css({
            display: 'none'
        })
        $('.button__close').hide();
        $('.menu__button').show()
        $('body').removeClass('menu--open')
    });
}


$('.js-join').on('click touch', function () {
    var body = $("html, body");
    body.stop().animate({
        scrollTop: $('#form').offset().top
    }, 500);
})

$('.js-show-without-cosmetics').on('click touch', function () {

    $('.js-without-cosmetics').show();
    $('.js-without-cosmetics').find('.js-yes').attr('href', $(this).attr('href'));
    return false
})
$('.js-without-cosmetics-close, .js-no-cosmetics').on('click touch', function () {
    $('.js-without-cosmetics').hide();
    return false;
})

$('.faq-block__title').on('click touch', function () {
    $(this).siblings('.faq-block__active').toggleClass('faq-block__active--db');
    $(this).parent().find('.faq-block__title-icon').toggleClass('faq-block__title-icon--active');
})

$('.marafon-button').on('click touch', function () {
    $('.marafon-button--active').removeClass('marafon-button--active');
    $(this).addClass('marafon-button--active');

    $('.marafon-block__img--active').removeClass('marafon-block__img--active');

    $('.marafon-block__img:nth-child(' + ($(this).index() + 1) + ')').addClass('marafon-block__img--active')

    $('.marafon-txts--active').removeClass('marafon-txts--active');

    $('.marafon-txts:nth-child(' + ($(this).index() + 1) + ')').addClass('marafon-txts--active')
})


$('.popup__menu-link').on('click touch', function () {

    hideMenu();

    var body = $("html, body");
    body.stop().animate({
        scrollTop: $($(this).attr('href')).offset().top
    }, 500);

    return false
})


$('.wadein-button').on('click touch', function () {
    $('.wadein-button--active').removeClass('wadein-button--active');
    $(this).addClass('wadein-button--active');

    $('.wadein-info__wrapper').hide();
    $('#' + $(this).data('id')).show();

    return false;
})


$('.results-blocks').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: true,
    appendArrows: $('.results-buttons'),
    prevArrow: '<button class="results-button"><svg viewBox="0 0 49 16" fill="white" xmlns="http://www.w3.org/2000/svg" class="results-button__icon"><path d="M0.292892 7.2929C-0.0976296 7.68342 -0.0976295 8.31658 0.292892 8.70711L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41422 8L8.07107 2.34315C8.46159 1.95262 8.46159 1.31946 8.07107 0.928934C7.68054 0.53841 7.04738 0.53841 6.65685 0.928934L0.292892 7.2929ZM49 7L1 7L1 9L49 9L49 7Z" fill="white"></path></svg></button>',
    nextArrow: '<button class="results-button results-button--right"><svg viewBox="0 0 49 16" fill="white" xmlns="http://www.w3.org/2000/svg" class="results-button__icon"><path d="M0.292892 7.2929C-0.0976296 7.68342 -0.0976295 8.31658 0.292892 8.70711L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41422 8L8.07107 2.34315C8.46159 1.95262 8.46159 1.31946 8.07107 0.928934C7.68054 0.53841 7.04738 0.53841 6.65685 0.928934L0.292892 7.2929ZM49 7L1 7L1 9L49 9L49 7Z" fill="white"></path></svg></button>',
    responsive: [{
        breakpoint: 767,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    },]
})


$(function () {
    $(".results-blocks").on('afterChange', function (event, slick, currentSlide) {
        $("#cp").text(currentSlide + 1);
    });
});


function openModal(event) {
    event.preventDefault();
    let dataVal = $(this).attr('data-modal');
    let $modal = $('.jsModal[data-modal="' + dataVal + '"]');
    $modal.addClass('_active');

    $('body').addClass('modal--opened')
}

function closeItem() {
    $(this).closest('.jsModal').removeClass('_active');
    body.removeClass('modal--opened');
}

$('.jsModalOpen').on('click', openModal);
$('.jsClose').on('click', closeItem);
$('.jsModal').on('click touch', function () {
    $(this).find('.jsClose').click();
})

$('.modalText__wrapper').on('click touch', function (e) {
    e.stopPropagation()
})

$('.press-info__block').on('click touch', function () {
    $('.press-info__txt').text($(this).data('text'));
})

$('.program-info__fits-button').on('click touch', function () {
    $('.program-info__fits-button--active').removeClass('program-info__fits-button--active');
    $(this).addClass('program-info__fits-button--active');
    $('.program-info__img--active').removeClass('program-info__img--active');
    $('.program-info__img:nth-child(' + ($(this).index() + 1) + ')').addClass('program-info__img--active')
})

var currentPrizeSlide = 1;
$('.prizes-info__arrow--right').on('click touch', nextPrize);
$('.prizes-info__arrow--left').on('click touch', prevPrize);

function nextPrize() {
    $('.prize').removeClass('prize--' + currentPrizeSlide);
    if (currentPrizeSlide == 6) {
        currentPrizeSlide = 1;
    } else {
        currentPrizeSlide++;
    }
    $('.prize').addClass('prize--' + currentPrizeSlide);
}

function prevPrize() {
    $('.prize').removeClass('prize--' + currentPrizeSlide);
    if (currentPrizeSlide == 1) {
        currentPrizeSlide = 6;
    } else {
        currentPrizeSlide--;
    }
    $('.prize').addClass('prize--' + currentPrizeSlide);
}

$(document).on('click touch', function () {
    $('.figure-block--active').removeClass('figure-block--active');
})
$('.figure-block').on('click touch', function (e) {
    e.stopPropagation();
    $('.figure-block').removeClass('figure-block--active');
    $(this).addClass('figure-block--active');
})

var min_horizontal_move = 30;
var max_vertical_move = 30;
var within_ms = 1000;

var start_xPos;
var start_yPos;
var start_time;

function touch_start(event) {
    start_xPos = event.touches[0].pageX;
    start_yPos = event.touches[0].pageY;
    start_time = new Date();
}


function touch_end(event) {
    var end_xPos = event.changedTouches[0].pageX;
    var end_yPos = event.changedTouches[0].pageY;
    var end_time = new Date();
    let move_x = end_xPos - start_xPos;
    let move_y = end_yPos - start_yPos;
    let elapsed_time = end_time - start_time;
    if (Math.abs(move_x) > min_horizontal_move && Math.abs(move_y) < max_vertical_move && elapsed_time < within_ms) {
        if (move_x < 0) {
            return 'prev';
        } else {
            return 'next';
        }
    }
}

var prize = document.getElementById("prize");
prize.addEventListener('touchend', function (e) {
    var dir = touch_end(e);

    if (dir === 'prev') {
        prevPrize();
    } else if (dir === 'next') {
        nextPrize();
    }
});
prize.addEventListener('touchstart', touch_start);


var mfBlock = document.querySelector('.marafon-block');
mfBlock.addEventListener('touchend', function (e) {
    var dir = touch_end(e);

    if (dir === 'next') {
        $('.marafon-button--active').prev().click();
        $('.marafon-button--active').prev().focus();
    } else if (dir === 'prev') {
        $('.marafon-button--active').next().click();
        $('.marafon-button--active').next().focus();
    }
});
mfBlock.addEventListener('touchstart', touch_start);
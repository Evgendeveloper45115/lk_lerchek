(function ($) {

    $('.ingredient-items-block').on('click', '.add-new-button', function (e) {
        e.preventDefault();

        var $block = $(this).closest('.ingredient-items-block');
        var $item = $block.find('.ingredient-item:first-child').clone();
        $item.find(':input').each(function () {
            this.value = '';
        });
        $block.find('.ingredient-items-container').append($item);
    }).on('click', '.remove-button', function (e) {
        e.preventDefault();

        if ($(this).closest('.ingredient-items-container').find('.ingredient-item').length > 1) {
            $(this).closest('.ingredient-item').remove();
        }
    });

})(jQuery);
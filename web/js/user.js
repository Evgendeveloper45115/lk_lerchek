(function ($) {
    var cost = $('.sectionPay__input_country').val();
    var degrees = 0;
    if ($('.sectionPay__input_country').val() != 'RU') {
        var allCost = Number(cost) + Number($('.cost-js span').text());
        $('.cost_delivery').val(cost);
        $('.cost_append').text(cost);
        $('.result_text span').text(allCost);
    }
    $(document).on('change', '.sectionPay__input_country', function () {

        var key = $(this).val();
        var cost = deliveryPrice[key];

        var allCost = Number(cost) + Number($('.cost-js span').text());
        $($('.all_cost')).text('');
        $('.cost_delivery').val(cost);
        $('.result_text span').text(allCost);

        $('.cost_append').text(cost);
    });
    var PhotoUpload = $('.progress_load_form_photos').owlCarousel({
        loop: false,
        nav: false,
        items: 1,
        navText: false
    });
    PhotoUpload.on('changed.owl.carousel', function (e) {
        dot.removeClass("active").eq(e.page.index).addClass("active")
    });

    $('.menu_opener').on('click', function () {
        $('.mobile_menu').fadeIn();
        $('html, body').addClass('popup_opened')
    });
    $('.close_custom_popup').on('click', function () {
        $('.mobile_menu').fadeOut();
        $('html, body').removeClass('popup_opened')
    });
    $('.header_user_photo').on('click', function () {
        $(this).parent('.header_user_menu_inner').toggleClass('opened')
        $('.header_user_drop_menu').slideToggle();
    });
    $('.training_access').on('click', function () {
        $('.access_popup_not_time').fadeIn();
        $('html, body').addClass('popup_opened');
    });
    $('.close_popup, .popup_close_bg').on('click', function () {
        if ($(this).next().find('.close_dark').length || $(this).attr('class').indexOf('close_popup') + 1) {
            $('.popup').fadeOut();
            $('html, body').removeClass('popup_opened');
            if (location.href.indexOf('recipes') + 1 || location.href.indexOf('vote') + 1) {
                $('.recipes_popup__inner:not(.recipes_popup_inner--vote)').html('');
            }
        } else {
            return false;
        }
    });
    $(document).on('click', '.recipes_popup_opener', function (e) {
        e.preventDefault();
        if (location.href.indexOf('vote') + 1) {
            $.ajax({
                url: $(this).attr('href'),
                type: 'post',
                success: function (response) {
                    $('.recipes_popup__inner').append(response);
                    $(document).find('.recipes_popup_img').imageZoom({
                        zoom: 200
                    });

                }
            });
        }
    });
    $('.food_list_item').on('click touch', function () {
        $(this).toggleClass('active')
    })

    $('.vote-list__arrow--prev').on('click touch', function (e) {
        e.preventDefault();
        prev();
    })
    $('.vote-list__arrow--next').on('click touch', function (e) {
        e.preventDefault();
        next();
    })

    $('.recipes_popup_opener').on('click', function () {
        $('.recipes_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    });

    $('.faq_item').on('click', function () {
        $(this).toggleClass('opened');
        $(this).children('p').slideToggle();
    });
    $('.food_menu__list').on('click touch', openDropdown);

    function openDropdown() {
        $(this).toggleClass('opened');
        if (!$(this).find('.food_menu_item').hasClass('active')) {
            var href = $(this).find('.food_menu_item a').attr('href');
            $(location).attr('href', href)
        }
    }

    $('.food_menu_item .button').on('click touch', function (e) {
        if ($(window).width() < 1024) {
            if (!$(this).parents('.opened').length) {
                e.preventDefault();
            }
        }

    });

    $('.tab-button').on('click', function () {
        var $this = $(this);
        var $tab_buttons_wrapper = $this.closest('.tab-buttons-wrapper');
        $tab_buttons_wrapper.find('.tab-button').removeClass('tab-button__active');
        $this.addClass('tab-button__active');
        if ($('.tab-answers').hasClass('tab__visible')) {
            $('.tab-answers').removeClass('tab__visible');
        }
        $($tab_buttons_wrapper.data('tabs-wrapper')).find('.tab').removeClass('tab__visible');
        $($this.data('tab')).addClass('tab__visible');
    });
    $('.js_close_popup').on('click', function () {
        $('.popup').fadeOut();
        $('html, body').removeClass('popup_opened');
    });
    $('.close_popup_test, .popup_close_bg').on('click', function () {
        if ($(this).attr('class').indexOf('close_test') + 1 || $(this).attr('class').indexOf('close_popup_test') + 1) {
            var url = $('.view_test_button_js').data('view-test-url');
            $('.popup').fadeOut();
            $('html, body').removeClass('popup_opened');
            if (url) {
                $.ajax({
                    url: url,
                    type: 'get',
                    data: {'test_id': $(this).data('id')}
                });
            }
        } else {
            return false;
        }
    });
    $(document).on('click', '.progress_load_opener', function (e) {
        e.preventDefault();
        $('.progress_load').fadeIn();
        $('html, body').addClass('popup_opened');
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $('.progress_load__inner').html('');
        var week = $(this).data('week');
        var int = 4;
        if ($('.continue').attr('data-value') == 1 && week == 8) {
            int = 8;
        }
        if (week === 0 || week === int) {
            $('body').addClass('progress_popup--file')
        } else {
            $('body').removeClass('progress_popup--file')
        }
        $.ajax({
            url: $(this).attr('href'),
            type: 'post',
            data: {_csrf: csrfToken, week: $(this).attr('data-week')},
            success: function (response) {
                $('.progress_load__inner').append(response);
            }
        });
    });

    $(document).on('click', '.load_collage', function (e) {
        e.preventDefault();
        $('.progress_load').fadeIn();
        $('html, body').addClass('popup_opened');
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $('.progress_load__inner').html('');
        $('body').addClass('progress_popup--file')
        $.ajax({
            url: 'load-collage',
            type: 'post',
            data: {_csrf: csrfToken, week: $(this).attr('data-week')},
            success: function (response) {
                $('.progress_load__inner').append(response);
            }
        });
    });

    var isSupportDragAndDrop = function () {
        var div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }()

    if (isSupportDragAndDrop) {

        $('body').on('drag dragstart dragend dragover dragenter dragleave drop', '.progress_load_form_photo, .profile_photo, .progress_load_form_photo_upload', function (e) {
            e.preventDefault();
            e.stopPropagation();
        })
        $('body').on('drop dragdrop', '.progress_load_form_photo, .profile_photo, .progress_load_form_photo_upload', function (e) {

            var files = e.originalEvent.dataTransfer.files;
            var $fileInput = $('.progress_load_form_photo_upload:visible, .profile_photo').find('input[type="file"]');
            $fileInput.prop('files', files);
            $fileInput.change();

            preview(files[0]);
        })

    }
    $(document).on('change touch', '.progress_load_form_photo_upload input', function () {
        InputFile = $(this);
        var files = this.files;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (!file.type.match(/image\/(jpeg|jpg|png|gif)/)) {
                alert('Фотография должна быть в формате jpg, png или jpeg');
                return false;
            }
            preview(files[i]);
        }
    });
    $(document).on("click", '.send-vote', function (e) {
        var q = this;
        $(q).attr('disabled', 'disabled');
        $.ajax({
            url: '/marafon/send-vote',
            type: 'post',
            data: {
                member_id: $(this).attr('data-member')
            },
            success: function (response) {
                if ($.isNumeric(response)) {
                    $(q).parent().find('.count-vote').text(response);
                    $(q).addClass('vote-success js-remove-vote').removeClass('send-vote').text('Удалить голос')
                } else if (response == 'false') {
                    $('.progress_popup').fadeIn();
                    $('html, body').addClass('popup_opened');
                }
                setTimeout(function () {
                    $(q).attr('disabled', false);
                }, 1000);

            }
        });

    });
    $(document).on("click", '.send-vote-new', function (e) {
        var q = this;
        //$(q).attr('disabled', 'disabled');
        if ($('.visible_false').length) {
            $('.progress_popup').fadeIn();
            $('html, body').addClass('popup_opened');
            return false;
        }
        $.ajax({
            url: '/marafon/send-vote-test',
            type: 'post',
            data: {
                for_id: $(this).attr('data-member'),
                couple_id: $(this).attr('data-couple')
            },
            success: function (response) {
                if (response == 'true') {
                    location.reload();
                }
                return false
            }
        });

    });
    $(".custom-checkbox").change(function () {
        if (this.checked) {
            $('.agree_button').attr('disabled', false)
        } else {
            $('.agree_button').attr('disabled', true)

        }
    });
    $(document).on("click", '.js-remove-vote', function (e) {
        var q = this;
        $(q).attr('disabled', 'disabled');
        $.ajax({
            url: '/marafon/remove-vote',
            type: 'post',
            data: {
                member_id: $(this).attr('data-member')
            },
            success: function (response) {
                if ($.isNumeric(response)) {
                    setTimeout(function () {
                        $(q).attr('disabled', false);
                    }, 1000);

                    $(q).parent().find('.count-vote').text(response);
                    $(q).removeClass('vote-success js-remove-vote').addClass('send-vote').text('Проголосовать')
                }
            }
        });

    });
    $(document).on("click", '.agree_button', function (e) {
        window.location.href = $(this).attr('href')
    });

    $(document).on('click touch', '.delete-progress', function (e) {
        e.preventDefault();
        $(".popup_close_bg").html("<div class='holder'><div class='preloader' style='z-index:999'><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>");
        $.ajax({
            url: '/marafon/delete-progress',
            type: 'post',
            data: {week: $(this).parent().find('#progress-week').val()},
            success: function (response) {
                $('.holder').remove();
            }
        });
    });

    function preview(file) {
        var reader = new FileReader();
        var resize_width = 600;
        reader.readAsDataURL(file);

        reader.name = file.name;//get the image's name
        reader.size = file.size; //get the image's size
        reader.onload = function (event) {
            var img = new Image();//create a image
            img.src = event.target.result;//result is base64-encoded Data URI
            img.name = event.target.name;//set name (optional)
            img.size = event.target.size;//set size (optional)
            img.onload = function (el) {
                var elem = document.createElement('canvas');//create a canvas

                //scale the image to 600 (width) and keep aspect ratio
                var scaleFactor = resize_width / el.target.width;
                elem.width = resize_width;
                elem.height = el.target.height * scaleFactor;

                //draw in canvas
                var ctx = elem.getContext('2d');
                ctx.drawImage(el.target, 0, 0, elem.width, elem.height);

                //get the base64-encoded Data URI from the resize image
                var srcEncoded = ctx.canvas.toDataURL(el.target, 'image/jpeg', 0);

                //assign it to thumb src
                InputFile.parent().parent().css('background-image', 'url(' + srcEncoded + ')');
                InputFile.parent().parent().attr('src', srcEncoded);
                InputFile.parent().parent().css('background-size', 'contain');
                InputFile.parent().parent().css('background-position-y', '0');
                InputFile.parent().parent().find('.text').remove();
                if ($('.img_profile').length) {
                    $('.img_profile').attr('src', '')
                }
                if (InputFile.parent().parent().find('.help-block').length) {
                    InputFile.parent().parent().find('.help-block').remove();
                }
                var resized = srcEncoded;

//note: remember that the image is now base64-encoded Data URI

//sendind the image to the server (php)
                var fd = new FormData();
                fd.append("image", resized);
                fd.append("week", InputFile.closest('#progress_add_form').find('.field-progress-week input').val());
                fd.append("attr", InputFile.parent().parent().attr('id'));
//sending data to the server
                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = function () {
                    $('.progress_load_form_photo_buttons').hide();
                    $(".popup_close_bg").html("<div class='holder'><div class='preloader'><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>");
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        var response = JSON.parse(xhr.responseText);
                        if (response.success == true) {
                            $('.holder').remove();
                            alert('Фото успешно загружено!');
                            $('.progress_load_form_photo_buttons').show();
                        } else {
                            alert('Ошибка, попробуйте выбрать другое фото или загрузите еще раз.');
                        }
                    }
                }
                if ($('body').hasClass('progress_popup--file')) {
                    xhr.open("POST", "/marafon/upload-progress");

                } else {
                    xhr.open("POST", "/marafon/upload-progress-collage");
                }
                xhr.send(fd);

                /*Now you can send "srcEncoded" to the server and
                convert it to a png o jpg. Also can send
                "el.target.name" that is the file's name.*/

            }
        };

    }

    $(document).on('click', '.progress_load_form_photo_buttons .button', function () {
        if ($(this).attr('disabled') === 'disabled') {
            return false;
        }
        if ($('.progress_load_form_photo_buttons .button_green'.length)) {
            $('.progress_load_form_photo_buttons .button_green').removeClass('button_green')
        }
        $('.progress_load_form_photo_buttons .button').each(function () {
            if (!$(this).hasClass('button_green_transparent')) {
                $(this).addClass('button_green_transparent')
            }
        });
        $(this).addClass('button_green');
        if ($(this).hasClass('button_green_transparent')) {
            $(this).removeClass('button_green_transparent')
        }

        $('.progress_load_form_photo .progress_load_form_photo_upload').hide();
        $(document).find('#' + $(this).attr('data-id')).show();

    });
    $('.progress_item_slider').owlCarousel({
        nav: true,
        loop: true,
        dots: false,
        items: 1,
        navText: false
    })
    $('.js-product-slides').owlCarousel({
        items: 1,
        nav: true,
        navText: ['', '']
    });
    $(document).on('click', '#show-table', function (e) {
        e.preventDefault();
        var slider = $(this).closest('.progress_item').find('.progress_item_slider');
        slider.hide();
        $(this).closest('.progress_item').find('.progress_item_tb').css('display', 'table');
        $(this).closest('.mobile_progress_item').css('height', '140vw');
        slider.next().find('#show-button').css('display', 'block');
    });
    $(document).on('click', '#show-button', function (e) {
        e.preventDefault();
        $(this).closest('.progress_item').find('.progress_item_slider').show();
        $(this).closest('.progress_item').find('.progress_item_tb').css('display', 'none');
        $(this).closest('.mobile_progress_item').css('height', '65vw');
        $('#show-button').css('display', 'none');
    });
    $('.progress_popup_opener').on('click', function () {
        $('.progress_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    });
    $('.progress_popup_opener').on('click', function () {
        $('.progress_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    });
    $(document).on('click', '.zamery_link', function (e) {
        e.preventDefault();
        $('.zamery_img').css('display', 'block');
        $('.progress_download').css('display', 'none');
        $('.video_how').css('display', 'none');

    });
    $(document).on('click', '.video_how_link', function (e) {
        e.preventDefault();
        $('.video_how').css('display', 'block');
        $('.progress_download').css('display', 'none');
        $('.zamery_img').css('display', 'none');

    });
    $(document).on('click', '.zamery_download', function (e) {
        e.preventDefault();
        $('.video_how').css('display', 'none');
        $('.zamery_img').css('display', 'none');
        $('.progress_download').css('display', 'block');

    });

    $('.update-question-button').on('click', function () {
        $('.update-question').css('display', 'block');
        $('.hero_post_content_wrap').css('display', 'none');
        $('.update-question-button').css('display', 'none');
        var val = $('.hero_post_content_wrap').text();
        $('.update-question').find('#reporthasuser-user_text').val(val);
    });

    $('body').on('click touch', '.new-photo-area', function (e) {
        e.preventDefault();
    })
    $('.email_send').on('click touch', function (e) {
        e.preventDefault();
        var email = $('.sing_input').val();
        var csrfToken = $('input[name="_csrf"]').val();
        var type = $('.type-recover').val();
        $.ajax({
            url: '/marafon/remember',
            type: 'post',
            data: {_csrf: csrfToken, email: email, type: type},
            success: function (response) {
                $(document).find('.error').html($(response).find('.error'));
            }
        });
    });
    $(document).on('click', '.rotateIn', function (e) {
        degrees += 90;
        var csrfToken = $('input[name="_csrf"]').val();
        $.ajax({
            url: '/marafon/rotate',
            type: 'post',
            data: {_csrf: csrfToken, path: $(this).attr('data-photo')},
            success: function (response) {
                $(document).find('.active').find('#' + response).attr('style', 'background-image: url(/uploads/progress/' + response + '.jpg?v=' + $.now() + ')');
            }
        });
    });
    $(document).on('click', '.button_edit_video_save', function (e) {
        e.preventDefault();
        var csrfToken = $('input[name="_csrf"]').val();
        $.ajax({
            url: '/marafon/save-video-link',
            type: 'post',
            data: {_csrf: csrfToken, path: $('#video_link_progress').val()},
            success: function (response) {
                $(document).find('.button_edit_video_save').text('Успешно!').css('background-color', '#dff0d8').css('color', 'black');
                setTimeout(function () {
                    $(document).find('.button_edit_video_save').text('Сохранить').css('background-color', '#2b343c').css('color', 'white');
                }, 1000);
            }
        });
    });


})(jQuery);

function copytext(el) {
    var $tmp = $("<input>");
    $("body").append($tmp);
    $tmp.val($(el).val()).select();
    document.execCommand("copy");
    $tmp.remove();
    $('.referralProgramForm-btn').text('Скопировано')
}

document.oncontextmenu = cmenu;

function cmenu() {
    var focused = document.activeElement;
    if ($(focused).attr('id') == 'video_link_progress' || $(focused).attr('id') == 'groupmember-video_url') {
        return true
    }
    return false;
}

document.onkeydown = function (e) {
    if (event.keyCode == 123) {
        return false;
    }
    if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
        return false;
    }
    if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
        return false;
    }
    if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
        return false;
    }

};
/** vote section */
var min_horizontal_move = 30;
var max_vertical_move = 30;
var within_ms = 1000;

var start_xPos;
var start_yPos;
var start_time;

function touch_start(event) {
    start_xPos = event.touches[0].pageX;
    start_yPos = event.touches[0].pageY;
    start_time = new Date();
}


function touch_end(event) {
    var end_xPos = event.changedTouches[0].pageX;
    var end_yPos = event.changedTouches[0].pageY;
    var end_time = new Date();
    let move_x = end_xPos - start_xPos;
    let move_y = end_yPos - start_yPos;
    let elapsed_time = end_time - start_time;
    if (Math.abs(move_x) > min_horizontal_move && Math.abs(move_y) < max_vertical_move && elapsed_time < within_ms) {
        if (move_x < 0) {
            return 'prev';
        } else {
            return 'next';
        }
    }
}

var member = 0;

var voteList = document.getElementById("vote-list");
var $member0 = document.getElementById("member-0");
var $member1 = document.getElementById("member-1");
voteList.addEventListener('touchend', function (e) {
    var dir = touch_end(e);

    if (dir === 'prev') {
        next();
    } else if (dir === 'next') {
        prev();
    }

});
voteList.addEventListener('touchstart', touch_start);

function prev() {
    if (member == 0) return
    member--;
    $member1.style.transform = 'translate(0, 0)';
    $member0.style.transform = 'translate(0, 0)'
    $('.vote-list__arrows span:nth-child(3)').removeClass('active');
    $('.vote-list__arrows span:nth-child(2)').addClass('active');
}

function next() {
    if (member == 1) return
    member++;
    $member1.style.transform = 'translate(-100%, 0)';
    $member0.style.transform = 'translate(-100%, 0)'
    $('.vote-list__arrows span:nth-child(2)').removeClass('active');
    $('.vote-list__arrows span:nth-child(3)').addClass('active');
    if ($('.visible_false').length) {
        $('.visible_false').removeClass('visible_false');
    }

}

/** vote section */






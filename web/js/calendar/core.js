var vbox = $('.js-venobox').venobox({
    bgcolor: "rgba(255,255,255,0)",
    cb_post_open: function (obj) {
        if (obj.data('vbtype') == 'inline') {
            $('.vbox-overlay').addClass('inline')
        }
    },
});
$(document).on('click', '.popup-close', function (e) {
    vbox.VBclose();
});
if ($('.js-progress').length) {
    var progress = $('.js-progress').data('progress'),
        $svgPath = $('.js-progress .top-user__svg--2 path'),
        length = $svgPath[0].getTotalLength();


    $svgPath.attr('stroke-dashoffset', Math.round(length * progress / 100));
}

$('.js-get-mark').on('click touch', function () {
    $(this).parent().siblings('.mark-popup').show()
    return false;
})
$('.js-mark-close').on('click touch', function () {
    $(this).parents('.mark-popup').hide();
    return false;
})

if ($(window).width() <= 750) {
    $('.calendar').appendTo('.diary-left');
    $('.calendar-wrapper').appendTo('.diary-left');
    $('.social--calendar').appendTo('.diary-left');
    $('.diary-bages').appendTo('.diary-right')

}
var events = {};

moment.locale('ru')
var $calendar = $('#calendar'),
    $hintDay = $('.hint--day');

var eventsToAdd = [];
var arr = {
    1: 'power',
    2: 'cardio',
    3: 'food',
    4: 'ill',
    5: 'care',
    6: 'blood'
};
var arr_second = {
    'power': '<a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end event event--power"><div class="fc-content"> <span class="fc-title">&nbsp;</span></div></a>',
    'cardio': '<a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end event event--cardio"><div class="fc-content"> <span class="fc-title">&nbsp;</span></div></a>',
    'food': '<a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end event event--food"><div class="fc-content"> <span class="fc-title">&nbsp;</span></div></a>',
    'ill': '<a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end event event--ill"><div class="fc-content"> <span class="fc-title">&nbsp;</span></div></a>',
    'care': '<a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end event event--care"><div class="fc-content"> <span class="fc-title">&nbsp;</span></div></a>',
    'blood': '<a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end event event--blood"><div class="fc-content"> <span class="fc-title">&nbsp;</span></div></a>',
};

$calendar.fullCalendar({
    locale: 'ru',
    viewRender: function (event, element) {
        if (event.type != 'month') return;

        $('.calendar__current').text(event.intervalStart.format('MMMM'))
        $('.calendar__prev').text(event.intervalStart.subtract(1, 'months').format('MMMM'));
        $('.calendar__next').text(event.intervalEnd.format('MMMM'));
    },
    events: function (start, end, timezone, callback) {
        var evs = [];
        setTimeout(function () {

            for (var date in document['markers_group']) {
                if (document['markers_group'].hasOwnProperty(date)) {
                    var types = document['markers_group'][date];
                    var cub = $('.fc-day[data-date="' + date + '"]');
                    cub.html('');
                    var length = Object.keys(document['markers_group'][date]).length;
                    var k = 0;
                    for (var i in types) {
                        if (types.hasOwnProperty(i)) {
                            var image = $(arr_second[arr[types[i]['type']]]);
                            image.addClass('event--pos' + k);
                            image.addClass('event--count' + length);
                            //console.log();
                            cub.append(image);
                            k++;
                        }
                    }
                }
            }
        }, 1);
        for (var day in events) {

            for (var event = 0; event < events[day].length; event++) {

                var className = 'event event--' + events[day][event].type;
                className += ' event--pos' + event,
                    className += ' event--count' + events[day].length;
                evs.push({
                    className: className,
                    start: day,
                    type: events[day][event].type
                })
            }

        }
        callback(evs);
    },
    eventAfterRender: function (event, element, view) {

        var shadowDay = element.parents('.fc-week').find(".fc-bg [data-date='" + event.start["_i"] + "']");
        shadowDay.append(element);
    },
    dayClick: dayClick
})

$('#calendar').fullCalendar('click', '2018-09-25')

function dayClick(date, jsEvent, allDay) {

    clearSelectedDay();

    var dayEvents = $calendar.fullCalendar('clientEvents', function (event) {
        return event.start.isSame(date)
    })

    setDayHit(jsEvent.target, date);
    for (var i = 0; i < dayEvents.length; i++) {
        var event = dayEvents[i];
        console.log(1);
        var $row = $hintDay.find('.hint__row--' + event.type);
        $row.addClass('hint__row--active');
        $row.find('.hint__btn').text('Добавлено');
    }
}

function clearSelectedDay() {
    eventsToAdd = [];
    $('.fc-day--active').removeClass('fc-day--active');
}

function setDayHit(target, date) {
    var target = $(target)
    if (target[0].tagName.toLowerCase() != 'td') {
        target = $(target).parents('td');
    }
    var offsetLeft = target.offset().left - $calendar.offset().left + target.width() + 20,
        offsetTop = 80;

    if ($(window).width() - offsetLeft < 350 * 2) {
        offsetLeft -= $('.hint--day').outerWidth() + target.width() + 20;
    }
    if (target.hasClass('fc-today') || target.hasClass('fc-past')) {
        target.addClass('fc-day--active');
        $hintDay.css({
            left: offsetLeft,
            top: offsetTop,
            display: 'block'
        })
        $hintDay.find('.hint__title').text(date.format('DD MMMM'));
        $hintDay.find('.hint__row--active').removeClass('hint__row--active');
        $hintDay.find('.hint__row .hint__btn').text('Добавить');
        $hintDay.data('date', date.format('YYYY-MM-DD'));
        $.each(document["markers"], function (k, v) {
            if (v.date_change == date.format('DD MMMM')) {
                $hintDay.find('[data-type="' + arr[v.type] + '"]').parent().addClass('hint__row--active');
                $hintDay.find('[data-type="' + arr[v.type] + '"]').text('Добавлено')
            }
        });

    } else {
        $('.access_popup').fadeIn();
        $('html, body').addClass('popup_opened');
    }

}

$(document).on('click', '.btn--popup', function (e) {
    e.preventDefault();
    $('.popup-close').click();
});

$('.menu_opener').on('click', function () {
    $('.menu_popup').fadeIn();
    $('html, body').addClass('popup_opened')
});
$(document).on('click', '#left_col ul li a, #mobile_menu_wrap ul li a', function (e) {
    e.preventDefault();
    var current_index = $(this).parent().index(),
        str,
        folder_list = '',
        counter = 0,
        index;
    if ($(this).parent().hasClass('block_part')) {
        if (current_index < 4) {
            str = '<div class="content">Вы не можете попасть в раздел ' + $(this).children('span').text() + ', пока не будет пройдено задание из раздела ' + $(this).parent().siblings('li').eq(current_index - 1).children('a').children('span').text() + '</div>';
        } else {
            $('.block_part').each(function () {
                if (counter === 0) {
                    index = $(this).index() - 1;
                    folder_list += $('#left_col ul li').eq(index).children('a').text();
                }
                if ($.trim($(this).children('a').text()) === 'Конкурс') {
                    //folder_list += 'Тренировки'
                } else {
                    if (counter >= 0) {
                        folder_list += ',';
                    }
                    folder_list += $(this).children('a').text();
                }
                counter++;
            });
            str = '<div class="content">Вы не можете принимать участие в конкурсе, пока не будут пройдены задания из разделов: ' + folder_list + '</div>';
        }
        creat_lb(str);
        return false;
    } else {
        document.location.href = $(this).attr('href')
    }
});
$('.close_popup, .popup_close_bg').on('click', function () {
    if ($(this).next().find('.close_dark').length || $(this).attr('class').indexOf('close_popup') + 1) {
        $('.popup').fadeOut();
        $('html, body').removeClass('popup_opened');
        if (location.href.indexOf('recipes') + 1) {
            $('.recipes_popup__inner').html('');
        }
    } else {
        return false;
    }
});

$('.hint__btn').on('click touch', function () {
    var type = $(this).attr('data-type');
    var date = $(this).closest('.hint.hint--day').find('.hint__title').text();
    var second_date = $(document).find('.fc-day.fc-day--active').attr('data-date');
    if (!second_date) {
        second_date = $(document).find('.fc-day-top.fc-day--active').attr('data-date');
    }
    if ($(this).parent().hasClass('hint__row--active')) {
        $(this).parent().removeClass('hint__row--active');
        $(this).text('Добавить');
        $(this).parent('.hint--day').find();
        $.ajax({
            type: 'get',
            url: '/marafon/delete-event',
            data: {date: date, type: type, second_date: second_date},
            success: function (msg) {
                document['markers'] = msg['markers'];
                document['markers_group'] = msg['markers_group'];
            }
        });
        eventsToAdd.push({
            type: $(this).data('type')
        })

    } else {
        if ($(this).parent().parent().find('.hint__row--active').length <= 3) {
            $(this).text('Добавлено');
            $(this).parent().addClass('hint__row--active');
        }
    }

    if ($(this).parent().hasClass('hint__row--active')) {
        $.ajax({
            type: 'get',
            url: '/marafon/set-event',
            data: {date: date, type: type, second_date: second_date, user_id: $('.user_id').val()},
            success: function (msg) {
                document['markers'] = msg['markers'];
                document['markers_group'] = msg['markers_group'];
            }
        });
        eventsToAdd.push({
            type: $(this).data('type')
        })
    }

})


$('.hint__btn').on('click touch', function () {

    if (!events[$hintDay.data('date')]) {
        events[$hintDay.data('date')] = [];
    }
    for (var i = 0; i < eventsToAdd.length; i++) {
        events[$hintDay.data('date')].push(eventsToAdd[i])
    }
    var cub = $('.fc-day[data-date="' + $hintDay.data('date') + '"]');
    cub.html('');
    var k = 1;
    var length = $('.hint--day .hint__body .hint__row.hint__row--active').length;

    $('.hint--day .hint__body .hint__row.hint__row--active').each(function () {
        var image = $(arr_second[$(this).find('.hint__btn').data('type')]);
        image.addClass('event--pos' + (k - 1));
        image.addClass('event--count' + length);
        cub.append(image);
        k++;
    });
    // $('#calendar').fullCalendar('refetchEvents');

    return false;
})
$('.js-calendar-addtoday').on('click touch', function (e) {
    e.preventDefault();
    var date = moment(),
        jsEvent = {
            target: $('.fc-day[data-date="' + moment().format('YYYY-MM-DD') + '"]')
        };
    dayClick(date, jsEvent);
});
$('.calendar__next').on('click touch', function () {
    $calendar.fullCalendar('next')
})

$('.calendar__prev').on('click touch', function () {
    $calendar.fullCalendar('prev')
})

$(document).on('click touch', '.hint__close', function () {
    location.reload();
    $(this).parent().hide();
    $('.fc-day--active').removeClass('fc-day--active');
})
(function ($) {
    var parent_tab = $('.first_tab.active a');
    if (parent_tab.length) {
        var middle_tab = $(parent_tab.attr('href')).find('.middle_child_tab.active a');
        if (middle_tab.length) {
            var last_child_tab = $(middle_tab.attr('href')).find('.last_child_tab.active a');
            if (last_child_tab.length) {
                tinymce.init({
                    selector: last_child_tab.attr('href') + ' textarea'
                })
            }
        }
    }

    $(document).on('click', '.last_child_tab a', function (e) {
        if (!tinymce.hidden) {
            tinymce.remove();
        }
        tinymce.init({
            selector: $(this).attr('href') + ' textarea'
        });
    })
})(jQuery);
$(function () {
    if ($('.kv-panel-before').length) {
        getPopover();
        $(document).ajaxComplete(function () {
            getPopover();
        });
    }
});

function getPopover() {
    $('.generate_in_email').popover({
        html: true,
        content: function () {
            return $(
                "<div class=\"form-group form-group_hidden\">\n" +
                "        <input type=\"text\" name=\"email\" class=\"form-control link-text\" placeholder=\"qwerty@mail.ru\"/>\n" +
                "        <button class=\"generate_email btn_my\">Выслать</button>\n" +
                "    </div>"
            );
        }
    });
}

(function ($) {
    $(document).on("click", '.set-rating', function (e) {
        var button = $(this);
        var url = '/admin/user/set-rating';
        var text = 'Добавлено';
        if (button.hasClass('success-response')) {
            url = '/admin/user/remove-rating'
            text = 'Удалено';
        }
        e.preventDefault();
        $.ajax({
            url: url,
            type: 'post',
            data: {
                email: $.trim($('.content-header h1').text()), rating: $(this).attr('name')
            },
            success: function (response) {
                button.text(text);
                button.addClass('success-response');
                $('.content-wrapper').prepend('<div id="w3-success" class="alert-success alert fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    '\n' +
                    '<i class="icon fa fa-check"></i>' + text + '!\n' +
                    '\n' +
                    '</div>')
            }
        });

    });
    $(document).on("change", '.cat_vote', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/admin/vote/set-cat-vote',
            type: 'post',
            data: {
                member_id: $('.cat_vote_id').val(), cat: $(this).val()
            },
            success: function (response) {
                $('.content-wrapper').prepend('<div id="w3-success" class="alert-success alert fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    '\n' +
                    '<i class="icon fa fa-check"></i>Успешно!\n' +
                    '\n' +
                    '</div>')
            }
        });

    });
    $('.ref_button').on("click", function (e) {
        e.preventDefault();
        var keys = $('#kv-grid-demo').yiiGridView('getSelectedRows');
        $.ajax({
            url: '/admin/ref/' + $(this).attr('action'),
            type: 'post',
            data: {
                ids: keys, value: $(this).attr('data-val'),
            },
            success: function (response) {
                //$.pjax({container: '#kv-grid-demo'});
                $("#kv-grid-demo").yiiGridView("applyFilter");
                $('.content-wrapper').prepend('<div id="w3-success" class="alert-success alert fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    '\n' +
                    '<i class="icon fa fa-check"></i>Успешно!\n' +
                    '\n' +
                    '</div>');
                if (response) {
                    $('.content-wrapper').prepend('<a class="www_yandex" style="display: none" href="' + response + '"></a>');
                    document.location.href = $(document).find('.www_yandex').attr('href');
                }
                return false
            }
        });

    });
    $(document).on("rating:clear", function (e) {
        $.ajax({
            url: '/admin/user/remove-rating',
            type: 'post',
            data: {
                email: $.trim($('.content-header h1').text()), rating: $('.rating-stars').attr('title')
            },
            success: function (response) {
                $('.content-wrapper').prepend('<div id="w3-success" class="alert-success alert fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    '\n' +
                    '<i class="icon fa fa-check"></i>Успешно!\n' +
                    '\n' +
                    '</div>')
            }
        });
    });

    $(document).on("click", ".generate_email", function (e) {
        e.preventDefault();
        var email = $('.link-text');
        $(document).find('#w3-error').remove();
        if (email.val() != '') {
            if (!isEmail(email.val())) {
                $('.content').prepend('<div id="w3-error" class="alert-danger alert fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    '\n' +
                    '<i class="icon fa fa-check"></i>Не валидный имейл!\n' +
                    '\n' +
                    '</div>');
                return false
            }
            $.ajax({
                url: '/admin/user/payment-admin',
                type: 'post',
                data: {email: email.val()},
                success: function (response) {
                    $('.content').prepend('<div id="w3-success" class="alert-success alert fade in">\n' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                        '\n' +
                        '<i class="icon fa fa-check"></i>Успешно!\n' +
                        '\n' +
                        '</div>');
                    $.pjax.reload({container: '#kv-grid-demo-pjax', timeout: false});
                    return false;
                }
            });
            return false;
        } else {
            $('.content').prepend('<div id="w3-error" class="alert-danger alert fade in">\n' +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                '\n' +
                '<i class="icon fa fa-check"></i>Не валидный имейл!\n' +
                '\n' +
                '</div>');
            return false;
        }
    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

//training block
    var tpl_training_number;
    var tpl_sort_index;
    var $cached_elements = null;
    if (document.getElementById('tpl_Training_number') !== null) {
        tpl_training_number = document.getElementById('tpl_Training_number').innerHTML;
        tpl_sort_index = document.getElementById('tpl_fields_pair').innerHTML;
    }

    function initWeekBlocks(checkbox, $form) {
        var number = checkbox.value;
        var label = $(checkbox).parent().text().trim();
        var $tpl_container = $form.find('.tpl-training-container');
        var $current_el = $(tpl_training_number.replace(new RegExp('{number}', 'g'), number));

        if ($cached_elements) {
            var $c_el = $cached_elements.find('.week-block[data-number="' + number + '"]');
            if ($c_el.length) {
                $current_el = $c_el;
            }
            console.log(number, $c_el.length);
        }

        var $label_1 = $current_el.find('.Training_number_label');
        var $label_2 = $current_el.find('.Training-week_text-label');

        $label_1.html($label_1.attr('data-label-text') + ' (' + label + ')');
        $label_2.html($label_2.attr('data-label-text') + ' (' + label + ')');

        $current_el.find('[type="checkbox"]').off().on('change', function () {
            var current_sub_el = tpl_sort_index.replace(new RegExp('{number}', 'g'), number);

            //$current_el.find('.tpl-training-sub-container').each();

            if ($cached_elements) {
                $cached_elements
                    .find('.week-block[data-number="' + number + '"] .tpl-training-sub-container')
                    .html($current_el.find('.tpl-training-sub-container').html())
                ;
            }

            $cached_elements = $form.find('.tpl-training-container').clone();
            $current_el.find('.tpl-training-sub-container').empty();

            $current_el.find('.Training-Training_number [type="checkbox"]:checked').each(function () {
                var $current_sub_el = $(current_sub_el.replace(new RegExp('{training_number}', 'g'), this.value));
                var $sub_label = $current_sub_el.find('.sort_index_label');

                $sub_label.html($sub_label.attr('data-label-text') + ' (' + $(this).parent().text().trim() + ')');

                if ($cached_elements) {
                    var $c_sub_el = $cached_elements
                        .find('.week-block[data-number="' + number + '"]')
                        .find('.week-training-block[data-training_number="' + this.value + '"]')
                    ;
                    if ($c_sub_el.length) {
                        $current_sub_el = $c_sub_el;
                    }
                    console.log(number, this.value, $c_sub_el.length);
                }

                $current_el.find('.tpl-training-sub-container').append($current_sub_el);
            });
        });

        $tpl_container.append($current_el);
    }

    var $form = $('#training_form');

    $form.find('.tpl-training-container').empty();
    $form.find('#Training-week :input:checked').each(function () {
        initWeekBlocks(this, $form);
    });

    setTimeout(function () {
        $.each(window['training_group'], function (week_number, wt) {
            $.each(wt, function (t) {
                $('[name="Training[week][' + week_number + '][text]"]').val(wt[t].text);

                $('[name="Training[week][' + week_number + '][' + t + ']"]').each(function () {
                    this.checked = true;
                    $(this).trigger('change');
                    setTimeout(function () {
                        $('[name="Training[week][' + week_number + '][' + t + '][sort_index]"]')
                            .val(wt[t]['sort_index'])
                        ;
                    }, 10);
                });
            });
        });
    }, 100);

    $(document).on('change', '.week', function () {
        $cached_elements = $form.find('.tpl-training-container').clone();
        $form.find('.tpl-training-container').empty();
        $form.find('#Training-week :input:checked').each(function () {
            initWeekBlocks(this, $form);
        });
    });
//training block
    $(document).on("click", ".help_img", function (e) {
        $('#help_button').show();
        $('#popup_wrapper.payment_need.static').show();
        $('.popup_wrapper.payment_need.static .popup_help').show();
    });
    $(document).on('click', '.new_button_signLog', function (e) {
        // e.preventDefault();
        var error = false;
        var error_2 = false;
        $('.required').each(function () {
            if ($(this).find('.col_right .form-control').val() === '') {
                error = true;
            }
        });

        if ($('.sign_email').length) {
            var val = jQuery.trim($('.sign_email').val());
            if (isEmail(val)) {
                $('.sign_email').parent().find('p').css('display', 'none');
                $('.sign_email').closest('.has-error').removeClass('has-error').addClass('success')
            } else {
                $('.sign_email').closest('.required').removeClass('success').addClass('has-error');
                $('.sign_email').parent().find('p').css('display', 'block').text('Введите правильный email.');
                error_2 = true;
            }
        }
        if ($('.sign_password').val() != '') {
            $('.sign_password').parent().find('p').css('display', 'none');
            $('.sign_password').closest('.has-error').removeClass('has-error').addClass('success')
        } else {
            $('.sign_password').closest('.required').removeClass('success').addClass('has-error');
            $('.sign_password').parent().find('p').css('display', 'block').text('Введите пароль');
            error_2 = true;
        }
        if (error) {
            $('.close').click();
        } else if (error_2) {
            return false;
        } else {
            $('.close').click();

            return true;
        }
    });
    $('.close').on('click', function () {
        $('.payment_need_overlay').fadeOut();
        $('.payment_need').fadeOut();
    });

    $(document).on('click', '.new_button_signUp', function (e) {
        if ($('input[name="validateEmail"]').val() == 1) {
            $(document).find('.popover.popover-default').css('display', 'none');
            $('.overlay.not_time.static').show();
            $('.popup_wrapper.not_time.static').show();
            return false;
        } else {
            var error = false;
            $(document).find('.popover-title .close').css('display', 'none');
            $('.required').each(function () {
                if ($(this).find('.col_right .form-control').val() === '') {
                    error = true;
                }
            });
            $(document).find('.new_button_signLog').click();
        }
    });
    $('#qwe').on('afterValidate', function (e) {
        console.log($('.has-error').length);
        if ($('.has-error').length) {
            $("html, body").animate({
                scrollTop: $(".error").closest('.has-error').offset().top
            }, 1000);
        } else {
            $(document).find('.popover-x').css('display', 'block');
        }
    });
    $('#user-email_man').keyup(function () {
        if ($('#user-email').val() === $('#user-email_man').val()) {
            $('#user-email_man').next().text('Введите другой Email')
            return false
        }
    });
    $('#payment').on('afterValidate', function (e) {

        if ($('.has-error').length) {
            $("html, body").animate({
                scrollTop: $('.has-error').offset().top
            }, 1000);
        } else {
            $(document).find('.popover-x').css('display', 'block');
        }
    });
    $(document).on('change', '.pay-form__select', function () {

        var key = $(this).val();
        var cost = deliveryPrice[key];

        var allCost = Number(cost) + Number($('.price__txt--blue span').text());
        $($('.all_cost')).text('');
        $('.cost_delivery').val(cost);
        $('.result_text span').text(allCost);

        $('.cost_append').text(cost);
    });

    $('.js-payment-popup').on('click touch', function () {
        var id = $(this).attr('href').replace('#', '');
        $('[data-modal="' + id + '"]').show();
        $('body').addClass('menu--open')
        return false;
    })

    $('.jsClose').on('click touch', function () {
        $('.jsModal').hide();
        $('body').removeClass('menu--open')
        return false;
    })

    $('#user-email_confirm').on('paste', function (e) {
        e.preventDefault();
    })

})(jQuery);





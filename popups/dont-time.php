<?php
/**
 * @var $dont_time string
 */
?>
<div class="popup access_popup_not_time" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_popup_inner_not_time">
        <span class="close_popup close_dark"></span>
        <div class="progress_popup__inner">
            <div class="progress_popup_text">Всему свое время!</div>
            <div class="progress_popup_text" style="font-weight: 100"> <?= $dont_time ?>, когда настанет
                соответствующая
                неделя
            </div>
        </div>
    </div>
</div>


<?php

?>
<div id="help_button" class="overlay static payment_need_overlay"></div>
<div id="popup_wrapper" class="popup_wrapper static payment_need">
    <div class="popup popup_help static payment_need" id="popup3">
        <span class="close"></span>
        <div class="popup_content exclamation_point">
            <?php
            if (!\app\helpers\My::isGirl($_GET['type'])) {
                ?>
                <div class="sign_wrapper" style="text-align: left; padding: 25px 25px 25px 25px; color: black;">
                    <p>
                        <strong>Малоподвижные люди</strong> - Очень низкая физическая активность (работа дома,
                        преимущественно - сидя у компьютера, менее 5 тыс шагов в день)
                    </p>
                    <p>
                        <strong>Люди с низкой активностью</strong> - Малоподвижный образ жизни (офисная работа, тоже
                        преимущественно сидя у компьютера, 5-8 тысяч шагов в день)
                    </p>
                    <p>
                        <strong>для умеренно активных людей</strong> - Умеренная физическая активность (активная,
                        подвижная работа, 8 и более тысяч шагов в день)
                    </p>
                </div>
                <?php
            } else {
                ?>
                <div class="sign_wrapper" style="text-align: left; padding: 25px 25px 25px 25px; color: black;">
                    <p>
                        <strong>Участие с тренировками</strong> - выберите этот пункт, если планируете выполнять
                        упражнения
                        во время прохождения нашей программы.
                    </p>
                    <p>
                        <strong>Участие без тренировок</strong> - выберите этот пункт, если вы не будете тренироваться
                        на
                        программе
                    </p>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<div class="popup access_popup" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_popup_inner" style="height: auto;">
        <div class="close_popup"></div>
        <div class="progress_popup__inner">
            <div class="progress_popup_img"></div>
            <div class="progress_popup_text">
                Извините, но вы не можете отметить данный день, так как он еще не наступил.
            </div>
        </div>

    </div>

</div>

<?php

use yii\helpers\Url;

?>
<div class="popup access_popup">
    <div class="popup_close_bg close_test"></div>
    <div class="trouble_popup_inner">
        <div class="close_popup_test"></div>
        <div class="progress_popup__inner">
            <div class="progress_popup_text">
                Спасибо за обращение!<br>
                <br>
                <span class="popup_text-normal">Ваш вопрос направлен на рассмотрение специалисту.</span>
                <br>
                <br>
            </div>
            <div class="button button_green button_big js_close_popup">OK</div>
        </div>

    </div>

</div>

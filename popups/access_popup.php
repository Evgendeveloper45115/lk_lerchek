<?php
/**
 * @var $member \app\models\GroupMember
 */

use yii\helpers\Url;

?>
<div class="popup access_popup" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_popup_inner" style="height: auto; padding: 10px 0 10px 0;">
        <div class="progress_popup__inner">
            <div class="progress_popup_img"></div>
            <div class="progress_popup_text">
                <strong><?= $member->first_name ?> Ваш доступ закончился</strong>
                <br>
                <span style="font-weight: 100">вы можете купить продление на 1 месяц всего за 1900 руб: <a
                            style="color: #0000aa;text-decoration: underline"
                            href="/marafon/continue">По ссылке</a> </span>
            </div>
        </div>

    </div>

</div>
<style>
    .popup_foot {
        font-size: 1.5vw;
    }

    @media screen and (max-width: 1154px) {
        .popup_foot {
            font-size: 16px;
        }
    }
</style>
<div class="popup progress_load" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_load_inner">
        <span class="close_popup close_dark"></span>
        <div class="progress_load__inner">
        </div>
    </div>
</div>
<div class="popup progress_popup" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_popup_inner">
        <span class="close_popup close_dark"></span>
        <div class="progress_popup__inner">
            <div class="progress_popup_img"
                 style="background-image: url(/images/project/progress_popup_img.png)"></div>
            <div class="progress_popup_text"><?= $member->first_name ?>, Прежде чем проголосовать посмотрите второй
                результат. Для этого нужно свайпнуть справа-налево.
            </div>
        </div>
    </div>
</div>
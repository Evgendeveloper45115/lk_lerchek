<div class="popup progress_load" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_load_inner">
        <span class="close_popup close_dark"></span>
        <div class="progress_load__inner">
        </div>
    </div>
</div>
<div class="popup progress_popup" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_popup_inner">
        <span class="close_popup close_dark"></span>
        <div class="progress_popup__inner">
            <div class="progress_popup_img"
                 style="background-image: url(/images/project/progress_popup_img.png)"></div>
            <div class="progress_popup_text"><?= $member->first_name ?>, вы использовали все свои голоса<br>
                Если вы хотите переголосовать - удалите голос, который вы отдали за одного или
                нескольких участников и проголосуйте за другого участника.
            </div>
        </div>
    </div>
</div>
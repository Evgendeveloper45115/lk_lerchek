<?php
/**
 * @var $member \app\models\GroupMember
 * @var $popup \app\models\Popups
 */

use yii\helpers\Url;

?>
<div class="popup access_popup" style="display: none;">
    <div class="popup_close_bg"></div>
    <div class="progress_popup_inner">
        <span class="close_popup close_dark"></span>
        <div class="progress_popup_text">
            <strong><?= $member->first_name ?> , привет!</strong>
            <br>
            <div style="font-weight: 100"><?= $popup->description ?></div>
        </div>
    </div>
</div>
<style>
    .popup_foot {
        font-size: 1.5vw;
    }

    @media screen and (max-width: 1154px) {
        .popup_foot {
            font-size: 16px;
        }
    }
</style>
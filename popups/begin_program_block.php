<?php

use yii\helpers\Url;

/**
 * @var $user \app\models\User;
 */
$user = Yii::$app->user->identity;
$css = \app\helpers\My::getCoverUp();

if (!$user->groupMember->group_id && Yii::$app->controller->action->id != 'change-program' && Yii::$app->controller->action->id != 'error' && Yii::$app->controller->action->id != 'bonus'
    || strtotime($user->groupMember->group->datetime_start) > strtotime(date('Y-m-d H:i:s')) && Yii::$app->controller->action->id != 'change-program' && Yii::$app->controller->action->id != 'error' && Yii::$app->controller->action->id != 'bonus') {
    ?>
    <div class="start-program" style="display:block;">
        <div class="start-program__back"
             style="background: url(<?= $css ?>?v=1) no-repeat;background-size: cover;">
            <div class="start-program__title">
                Вы успешно зарегистрированы в личном кабинете!
            </div>
            <div class="start-program__text">
                Вся информация подготавливается для вас и будет доступна в день старта марафона
            </div>
            <div class="start-program__text-2">
                Старт 22 марта в 10:00 по Москве
            </div>
            <?php
            if (count($user->groupMembers) > 1) {
                ?>
                <div style="width: 100%; flex-shrink: 0">
                    <a href="<?= Url::to("/") ?>" class="start-program__link-2 button button_green">Назад</a>
                </div>
                <?php
            }
            ?>

            <a class="start-program__text-2 referal_popup"
               href="<?= Url::to(['bonus']) ?>">Реферальная программа</a>

        </div>
    </div>
    <?php
}

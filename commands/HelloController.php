<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use ApeltDmitry\SmsaeroApiV2\SmsaeroApiV2;
use app\helpers\My;
use app\models\Calories;
use app\models\CaloriesCat;
use app\models\Care;
use app\models\ContentType;
use app\models\DetoxDay;
use app\models\EveryDay;
use app\models\ExpressExercise;
use app\models\Faq;
use app\models\Group;
use app\models\GroupMember;
use app\models\Ingredient;
use app\models\Markers;
use app\models\MyAR;
use app\models\PaymentLer;
use app\models\Potato;
use app\models\Progress;
use app\models\RationComplex;
use app\models\Recipe;
use app\models\Recommendation;
use app\models\Ref;
use app\models\Report;
use app\models\ReportHasUser;
use app\models\Retail;
use app\models\Training;
use app\models\User;
use app\models\Vote;
use app\models\Week;
use app\models\WeekProducts;
use app\models\WeekTraining;
use Hashids\Hashids;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use RetailCrm\ApiClient;
use RetailCrm\Exception\CurlException;
use RetailCrm\Methods\V4\Users;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionGetContent()
    {
        /**
         * @var $payments PaymentLer[]
         * @var $members GroupMember[]
         * @var $member GroupMember
         */
        $path = \Yii::getAlias('@app/');
        foreach (json_decode(file_get_contents($path . 'markers.json'), true) as $email => $markers) {
            $user = User::findOne(['email' => $email]);
            $member = $user->getGroupMemberGuest(My::FOR_MAN)->one();

            if ($member->markers) {
                foreach ($member->markers as $attr => $mark) {
                    foreach ($markers as $marker) {
                        if ($marker->second_date != $mark['second_date']) {
                            $markerNew = Markers::find()->where(['user_id' => $member->id, 'second_date' => $marker['second_date']])->one();
                            if ($markerNew === null) {
                                $markerNew = new Markers();
                                $markerNew->date_change = $marker['date_change'];
                                $markerNew->second_date = $marker['second_date'];
                                $markerNew->user_id = $member->id;
                                $markerNew->type = $marker['type'];
                                $markerNew->save(false);
                                VarDumper::dump($email . '-----markers-----' . PHP_EOL);
                            }
                        }
                    }

                }
            }
        }

        foreach (json_decode(file_get_contents($path . 'progress.json'), true) as $email => $progressess) {
            $user = User::findOne(['email' => $email]);
            if ($user) {
                $member = $user->getGroupMemberGuest(My::FOR_MAN)->one();
                if ($member->progress) {
                    foreach ($member->progress as $progressRel) {
                        foreach ($progressess as $progress) {
                            if ($progress) {
                                $marker = Progress::find()->where(['member_id' => $member->id, 'week' => $progress['week']])->one();
                                if ($marker === null) {
                                    $marker = new Progress();
                                    $marker->member_id = $progressRel->member_id;
                                    $marker->week = $progress['week'];
                                    $marker->weight = $progress['weight'];
                                    $marker->bust = $progress['bust'];
                                    $marker->waist = $progress['waist'];
                                    $marker->hip = $progress['hip'];
                                    $marker->photo = $progress['photo'];
                                    $marker->photo_second = $progress['photo_second'];
                                    $marker->photo_third = $progress['photo_third'];
                                    $marker->save(false);
                                    VarDumper::dump($email . '-----progress-----' . PHP_EOL);
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach (json_decode(file_get_contents($path . 'report.json'), true) as $email => $reports_json) {
            $user = User::findOne(['email' => $email]);
            $member = $user->getGroupMemberGuest(My::FOR_MAN)->one();
            if ($member->reportHasUsers) {
                foreach ($member->reportHasUsers as $RHU) {
                    foreach ($reports_json as $report_json) {
                        $RHU_new = ReportHasUser::find()->where(['member_id' => $member->id, 'report_id' => $report_json['report_id']])->one();
                        if ($RHU_new === null) {
                            $RHU_new = new ReportHasUser();
                            $RHU_new->report_id = $report_json['report_id'];
                            $RHU_new->member_id = $member->id;
                            $RHU_new->user_text = $report_json['user_text'];
                            $RHU_new->admin_text = $report_json['admin_text'];
                            $RHU_new->status = $report_json['status'];
                            $RHU_new->date_added = $report_json['date_added'];
                            $RHU_new->save(false);
                            VarDumper::dump($email . '-----report-----' . PHP_EOL);
                        }
                    }
                }
            }
        }
    }

    public function actionQwe()
    {
        $connection = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname=lerchek_lk',
            'username' => 'root',
            'password' => '',
        ]);
        $connection->open();
        $command = $connection->createCommand('SELECT *,ration_complex.name as rat_name FROM ration_complex LEFT JOIN ingredient ON ingredient.ration_complex_id = ration_complex.id');
        $posts = $command->queryAll();;
        VarDumper::dump($posts);
        exit;
        foreach ($posts as $post) {
        }
    }


    public function actionMarkers()
    {
        /**
         * @var $user User
         */
        $marks = Markers::find()->all();
        foreach ($marks as $mark) {
            $user = User::find()->where(['id' => $mark->user_id])->one();
            if (!$user) {
                VarDumper::dump($mark->user_id . '----' . PHP_EOL);
                $mark->delete();
            } else {
                $member = $user->getGroupMemberGuest(1)->one();
                $mark->user_id = $member->id;
                $mark->save(false);
                VarDumper::dump($member->id . PHP_EOL);
            }
        }
    }

    public function actionGetMoscow()
    {
        /**
         * @var $member GroupMember
         */
        /* $dir = \Yii::getAlias('@app/runtime/1.txt');
         $arr = explode(PHP_EOL, file_get_contents($dir));
         $pr = [];
         foreach ($arr as $phone) {
             $member = GroupMember::find()->where(['phone' => $phone])->one();
             foreach ($member->progressRel as $progress)
                 if ($member->id) {
                     $pr[$member->id][$progress->week] = $progress->attributes;
                 }

         }*/
        $progresses = json_decode(file_get_contents('q.json'), true);
        $dir = \Yii::getAlias('@app/web/uploads/week-products/');
        foreach ($progresses as $member => $progress) {
            $member = GroupMember::findOne($member);
            foreach ($progress as $week => $pr) {
                if (isset($pr['photo']) && !empty($pr['photo'])) {
                    $dir_old = \Yii::getAlias('@app/web/uploads/progress/');
                    if (isset($pr['photo']) && !empty($pr['photo'])) {
                        if ($week == 4) {
                            VarDumper::dump($member->user->email);
                        }
                    }
                }
            }
        }
    }


    public function actionSetGroup()
    {
        //модераторы ж 31 м 10
        /**
         * @var $payment PaymentLer[]
         */
        $members = GroupMember::find()->where(['type_program' => 7])->with(['payment' => function (ActiveQuery $query) {
            $query->andWhere(['not', ['cost' => null]]);
        }])->all();
        VarDumper::dump(count($members));
        exit;
        $payments = array_chunk($members, 500);
        $file = \Yii::getAlias('@runtime') . '/logs/telegram.txt';
        $contentT = file_get_contents($file);
        $k = 1;
        $new_arr = [];
        $arr = explode(PHP_EOL, $contentT);
        foreach ($arr as $tg) {
            if (!stristr($tg, ';')) {
                $new_arr[$k][] = $tg;
            } else {
                $new = str_replace(';', '', $tg);
                $new_arr[$k][] = $new;
                $k++;
            }
        }
        if (!empty($payments)) {
            foreach ($payments as $key => $payment) {
                $group = new Group();
                $group->num = $key + 1;
                $group->datetime_start = date('Y-m-d');
                $group->datetime_create = date('Y-m-d H:i:s');
                $date_end = strtotime($group->datetime_start);
                $group->datetime_end = date('Y-m-d H:i:s', strtotime('+37 days', $date_end));
                $group->date_continue = date('Y-m-d H:i:s', strtotime('+37 days', $date_end));
                if (isset($new_arr[$group->num]) && isset($new_arr[$group->num][0]) && $new_arr[$group->num][0] != '') {
                    $group->link_main = $new_arr[$group->num][1];
                    $group->link_second = $new_arr[$group->num][0];
                }
                $group->save(false);
                $contentType = new ContentType();
                $contentType->class_name = Group::class;
                $contentType->relation_id = $group->id;
                $contentType->type_program = My::FOR_GIRL_30_10;
                $contentType->save(false);
                VarDumper::dump($group->num . PHP_EOL);
                foreach ($payment as $member) {
                    if ($group) {
                        if ($member->type_program == My::FOR_GIRL_30_10 && $member->user && $member->group_id == null && $member->payment) {
                            $member->group_id = $group->id;
                            $member->save(false);
                        }
                    }
                }
            }
        }
    }

    public function actionSetGroupMan()
    {
        /**
         * @var $payment PaymentLer[]
         */
        $count = 0;
        $members = GroupMember::find()->where(['type_program' => 8])->all();
        foreach ($members as $member) {
            if ($member->payment) {
                $count += 1;
            }
        }
        $payments = array_chunk($members, 500);
        $file = \Yii::getAlias('@runtime') . '/logs/telegram-man.txt';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        $contentT = file_get_contents($file);
        $k = 1;
        $new_arr = [];
        foreach (explode(PHP_EOL, $contentT) as $tg) {
            if (!stristr($tg, ';')) {
                $new_arr[$k][] = $tg;
            } else {
                $new = str_replace(';', '', $tg);
                $new_arr[$k][] = $new;
                $k++;
            }
        }
        if (!empty($payments)) {
            foreach ($payments as $key => $payment) {
                $group = new Group();
                $group->num = $key + 1;
                $group->datetime_start = date('Y-m-d');
                $group->datetime_create = date('Y-m-d H:i:s');
                $date_end = strtotime($group->datetime_start);
                $group->datetime_end = date('Y-m-d H:i:s', strtotime('+37 days', $date_end));
                $group->date_continue = date('Y-m-d H:i:s', strtotime('+37 days', $date_end));
                if (isset($new_arr[$group->num]) && isset($new_arr[$group->num][0]) && $new_arr[$group->num][0] != '') {
                    $group->link_main = $new_arr[$group->num][1];
                    $group->link_second = $new_arr[$group->num][0];
                }
                $group->save(false);
                $contentType = new ContentType();
                $contentType->class_name = Group::class;
                $contentType->relation_id = $group->id;
                $contentType->type_program = My::FOR_MAN_30_10;
                $contentType->save(false);
                VarDumper::dump($group->num . PHP_EOL);
                foreach ($payment as $member) {
                    if ($group) {
                        if ($member->type_program == My::FOR_MAN_30_10 && $member->user && $member->group_id == null && $member->payment) {
                            $member->group_id = $group->id;
                            $member->save(false);
                        }
                    }
                }
            }
        }
    }

    public function actionAddClients()
    {
        /**
         * @var $payment PaymentLer[]
         * @var $members GroupMember[]
         */
        $members = GroupMember::find()->where(['group_id' => null])->andWhere(['type_program' => 8])->all();
        foreach ($members as $member) {
            if ($member->payment) {
                $member->group_id = 752;
                $member->save(false);
            }
        }
    }

    public function actionBok()
    {
        /**
         * @var $members GroupMember[]
         */
        $members = GroupMember::find()->where(['group_id' => 73])->all();
        foreach ($members as $member) {
            $member->group_id = 392;
            $member->save(false);
        }
    }


    public function actionGetClientsWoman()
    {
        /**
         * @var $models Ref[]
         * @var $payments PaymentLer[]
         */


        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'woman.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $payments = PaymentLer::find()->all();
        foreach ($payments as $payment) {
            if ($payment->member->group_id && $payment->member && $payment->member->user && $payment->member->type_program != My::FOR_GIRL_16_02_2021) {
                $sheet->setCellValue('A' . $i, $payment->member->phone);
                $sheet->setCellValue('B' . $i, $payment->member->user->email);
                $sheet->setCellValue('C' . $i, $payment->member->getUserFIO());
                $i++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);

        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'woman-current.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $payments = PaymentLer::find()->all();
        foreach ($payments as $payment) {
            if ($payment->member->group_id && $payment->member && $payment->member->user && $payment->member->type_program == My::FOR_GIRL_16_02_2021) {
                $sheet->setCellValue('A' . $i, $payment->member->phone);
                $sheet->setCellValue('B' . $i, $payment->member->user->email);
                $sheet->setCellValue('C' . $i, $payment->member->getUserFIO());
                $i++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
    }

    public function actionGetClientsMan()
    {
        /**
         * @var $models Ref[]
         * @var $payments PaymentLer[]
         */


        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'man.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $payments = PaymentLer::find()->all();
        foreach ($payments as $payment) {
            if ($payment->member && $payment->member->user && $payment->member->type_program != My::FOR_MAN_16_02_2021) {
                $sheet->setCellValue('A' . $i, $payment->member->phone);
                $sheet->setCellValue('B' . $i, $payment->member->user->email);
                $sheet->setCellValue('C' . $i, $payment->member->getUserFIO());
                $i++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);

        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'man-current.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $payments = PaymentLer::find()->all();
        foreach ($payments as $payment) {
            if ($payment->member && $payment->member->user && $payment->member->type_program == My::FOR_MAN_16_02_2021) {
                $sheet->setCellValue('A' . $i, $payment->member->phone);
                $sheet->setCellValue('B' . $i, $payment->member->user->email);
                $sheet->setCellValue('C' . $i, $payment->member->getUserFIO());
                $i++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
    }


    public function actionGetClientsProgress()
    {
        //392
        /**
         * @var $members GroupMember[]
         */
        $members = GroupMember::find()->where(['type_program' => 11])->all();
        $fileName = 'girls.txt';
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . $fileName;
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        foreach ($members as $member) {
            if ($member->progressRelFirstWeek && $member->progressRelFirstWeek->photo && $member->progressRelLastWeek && $member->progressRelLastWeek->photo) {
                VarDumper::dump($member->id . 'girls' . PHP_EOL);
                file_put_contents($file, $member->getUserFIO() . '; ' . $member->user->email . '; ' . $member->phone . PHP_EOL, FILE_APPEND);
            }
        }


        $members = GroupMember::find()->where(['type_program' => 12])->all();
        $fileName = 'man.txt';
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . $fileName;
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        foreach ($members as $member) {
            if ($member->progressRelFirstWeek && $member->progressRelFirstWeek->photo && $member->progressRelLastWeek && $member->progressRelLastWeek->photo) {
                VarDumper::dump($member->id . 'mans' . PHP_EOL);
                file_put_contents($file, $member->getUserFIO() . '; ' . $member->user->email . '; ' . $member->phone . PHP_EOL, FILE_APPEND);
            }
        }
    }

    public function actionGetClientsProgressFirst()
    {
        //392
        /**
         * @var $members GroupMember[]
         */
        $members = GroupMember::find()->where(['type_program' => 9])->all();
        $fileName = 'girls.txt';
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . $fileName;
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        $count_man = 0;
        $count_girl = 0;
        foreach ($members as $member) {
            if ($member->progressRelFirstWeek && $member->progressRelFirstWeek->photo) {
                $count_girl += 1;
                VarDumper::dump($member->id . 'girls' . PHP_EOL);
                file_put_contents($file, $member->getUserFIO() . '; ' . $member->user->email . '; ' . $member->phone . PHP_EOL, FILE_APPEND);
            }
        }
        $members = GroupMember::find()->where(['type_program' => 10])->all();
        $fileName = 'man.txt';
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . $fileName;
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        foreach ($members as $member) {
            if ($member->progressRelFirstWeek && $member->progressRelFirstWeek->photo) {
                $count_man += 1;
                VarDumper::dump($member->id . 'mans' . PHP_EOL);
                file_put_contents($file, $member->getUserFIO() . '; ' . $member->user->email . '; ' . $member->phone . PHP_EOL, FILE_APPEND);
            }
        }
        VarDumper::dump($count_girl . PHP_EOL);
        VarDumper::dump($count_man . PHP_EOL);
    }

    public function actionGetClientsNoGroup()
    {
        //392
        /**
         * @var $members GroupMember[]
         */
        $members = GroupMember::find()->where(['type_program' => 9])->all();
        $fileName = 'girls.txt';
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . $fileName;
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        foreach ($members as $member) {
            if ($member->payment) {
                VarDumper::dump($member->id . 'girls' . PHP_EOL);
                file_put_contents($file, $member->getUserFIO() . '; ' . $member->user->email . '; ' . $member->phone . PHP_EOL, FILE_APPEND);
            }
        }
        $members = GroupMember::find()->where(['type_program' => 10])->all();
        $fileName = 'mans.txt';
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . $fileName;
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        foreach ($members as $member) {
            if ($member->payment) {
                VarDumper::dump($member->id . 'mans' . PHP_EOL);
                file_put_contents($file, $member->getUserFIO() . '; ' . $member->user->email . '; ' . $member->phone . PHP_EOL, FILE_APPEND);
            }
        }
    }

    public function actionDeleteOriginalImages()
    {
        /**
         * @var $progresses Progress[]
         */
        $progresses = Progress::find()->all();
        foreach ($progresses as $progress) {
            if ($progress->week == 0 && $progress->photo) {
                $dir = \Yii::getAlias('@webroot/uploads/progress/');
                $ext = pathinfo($dir . $progress->photo, PATHINFO_EXTENSION);
                $file = basename($dir . $progress->photo, "." . $ext);
                unlink($dir . $file);
                rename($dir . $file . '-' . '.' . $ext, $dir . $file . $ext);
            }
        }
    }

    public function actionDeleteDuplicate()
    {
        $content_types = ContentType::find()->where(['class_name' => 'app\models\RationComplex', 'type_program' => 4])->all();
        $duplicates = [];
        foreach ($content_types as $content_type) {
            $types = ContentType::find()->where(['class_name' => 'app\models\RationComplex', 'type_program' => 1, 'relation_id' => $content_type->relation_id])->all();
            if (!empty($types)) {
                foreach ($types as $type) {
                    VarDumper::dump($type);
                    $type->delete();
                }
            }
        }
    }


    public function actionCloneCalories()
    {
        /**
         * @var $caloriesCats CaloriesCat[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\CaloriesCat'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $caloriesCats = CaloriesCat::find()->where(['id' => $content_ids])->all();
        foreach ($caloriesCats as $caloriesCat) {
            $new_cal_cat = new CaloriesCat();
            $new_cal_cat->name = $caloriesCat->name;
            $new_cal_cat->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = CaloriesCat::class;
            $model->relation_id = $new_cal_cat->id;
            $model->save(false);

            if ($caloriesCat->calories) {
                foreach ($caloriesCat->calories as $calories) {
                    $new_calories = new Calories();
                    $new_calories->protein = $calories->protein;
                    $new_calories->grease = $calories->grease;
                    $new_calories->carbohydrate = $calories->carbohydrate;
                    $new_calories->sugar = $calories->sugar;
                    if ($calories->image) {
                        $this->moveImage($calories, 'calories', 'image');
                        $new_calories->image = 'copy_' . $calories->image;
                    }
                    $new_calories->cat_id = $new_cal_cat->id;
                    $new_calories->name = $calories->name;
                    $new_calories->save(false);
                    $model_rel = new ContentType();
                    $model_rel->type_program = 6;//6
                    $model_rel->class_name = Calories::class;
                    $model_rel->relation_id = $new_calories->id;
                    $model_rel->save(false);
                }
            }

        }
    }

    public function actionCloneRationComplex()
    {
        /**
         * @var $ration_complexes RationComplex[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\RationComplex'])->andWhere(['type_program' => 2])->all(), 'relation_id', 'relation_id');
        $ration_complexes = RationComplex::find()->where(['id' => $content_ids])->all();
        foreach ($ration_complexes as $ration_complex) {
            $new_ration_complex = new RationComplex();
            $new_ration_complex->name = $ration_complex->name;
            $new_ration_complex->ration_type = $ration_complex->ration_type;
            $new_ration_complex->week = $ration_complex->week;
            $new_ration_complex->day = $ration_complex->day;
            $new_ration_complex->day_part = $ration_complex->day_part;
            $new_ration_complex->portion = $ration_complex->portion;
            $new_ration_complex->calories = $ration_complex->calories;
            $new_ration_complex->protein = $ration_complex->protein;
            $new_ration_complex->grease = $ration_complex->grease;
            $new_ration_complex->carbohydrate = $ration_complex->carbohydrate;
            if ($ration_complex->image) {
                $this->moveImage($ration_complex, 'food', 'image');
                $new_ration_complex->image = 'copy_' . $ration_complex->image;
            }
            $new_ration_complex->description = $ration_complex->description;
            $new_ration_complex->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = RationComplex::class;
            $model->relation_id = $new_ration_complex->id;
            $model->save(false);
            if ($ration_complex->ingredients) {
                foreach ($ration_complex->ingredients as $ingredient) {
                    $new_ing = new Ingredient();
                    $new_ing->name = $ingredient->name;
                    $new_ing->value = $ingredient->value;
                    $new_ing->count_symbol = $ingredient->count_symbol;
                    $new_ing->ration_complex_id = $new_ration_complex->id;
                    $new_ing->save(false);
                }
            }

        }
    }

    public function actionCloneTrainings()
    {
        /**
         * @var $trainings Training[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\Training'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $trainings = Training::find()->where(['id' => $content_ids])->all();
        foreach ($trainings as $training) {
            $new_training = new Training();
            $new_training->type = $training->type;
            $new_training->name = $training->name;
            $new_training->video = $training->video;
            $new_training->body_part = $training->body_part;
            $new_training->save(false);
            if ($training->weeks) {
                foreach ($training->weeks as $week) {
                    $new_week = new Week();
                    $new_week->training_id = $new_training->id;
                    $new_week->number = $week->number;
                    $new_week->text = $week->text;
                    if ($new_week->save()) {
                        foreach ($week->weekTrainings as $w_tr) {
                            $weekTraining = new WeekTraining();
                            $weekTraining->week_id = $new_week->id;
                            $weekTraining->training_number = $w_tr->training_number;
                            $weekTraining->sort_index = $w_tr->sort_index;
                            $weekTraining->save();
                        }
                    }
                }
            }
            $model = new ContentType();
            $model->type_program = 5;//6
            $model->class_name = Training::class;
            $model->relation_id = $new_training->id;
            $model->save(false);
        }
    }

    public function actionCloneCare()
    {
        /**
         * @var $cares Care[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\Care'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $cares = Care::find()->where(['id' => $content_ids])->all();
        foreach ($cares as $care) {
            $new_care = new Care();
            $new_care->description = $care->description;
            $new_care->type = $care->type;
            $new_care->video_link = $care->video_link;
            if ($care->image) {
                $this->moveImage($care, 'care', 'image');
                $new_care->image = 'copy_' . $care->image;
            }
            $new_care->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = Care::class;
            $model->relation_id = $new_care->id;
            $model->save(false);
        }
    }

    public function actionCloneDetox()
    {
        /**
         * @var $detoxes DetoxDay[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\DetoxDay'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $detoxes = DetoxDay::find()->where(['id' => $content_ids])->all();
        foreach ($detoxes as $detox) {
            $new_detox = new DetoxDay();
            $new_detox->text = $detox->text;

            if ($detox->image) {
                $new_detox->image = 'copy_' . $detox->image;
                $this->moveImage($detox, 'detox-day', 'image');
            }
            $new_detox->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = DetoxDay::class;
            $model->relation_id = $new_detox->id;
            $model->save(false);
        }
    }

    public function actionCloneEveryDay()
    {
        /**
         * @var $every_days EveryDay[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\EveryDay'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $every_days = EveryDay::find()->where(['id' => $content_ids])->all();
        foreach ($every_days as $every_day) {
            $new_every_day = new EveryDay();
            $new_every_day->text = $every_day->text;
            $new_every_day->day = $every_day->day;
            $new_every_day->save(false);
            $model = new ContentType();
            $model->type_program = 5;//6
            $model->class_name = EveryDay::class;
            $model->relation_id = $new_every_day->id;
            $model->save(false);
        }
    }

    public function actionCloneExpressExercise()
    {
        /**
         * @var $express_exercises ExpressExercise[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\ExpressExercise'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $express_exercises = ExpressExercise::find()->where(['id' => $content_ids])->all();
        foreach ($express_exercises as $express_exercise) {
            $new_express_exercise = new ExpressExercise();
            $new_express_exercise->text = $express_exercise->text;
            $new_express_exercise->name = $express_exercise->name;
            $new_express_exercise->video = $express_exercise->video;
            $new_express_exercise->type = $express_exercise->type;
            $new_express_exercise->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = ExpressExercise::class;
            $model->relation_id = $new_express_exercise->id;
            $model->save(false);
        }
    }

    public function actionCloneFaq()
    {
        /**
         * @var $faqs Faq[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\Faq'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $faqs = Faq::find()->where(['id' => $content_ids])->all();
        foreach ($faqs as $faq) {
            $new_faq = new Faq();
            $new_faq->type = $faq->type;
            $new_faq->title = $faq->title;
            $new_faq->content = $faq->content;
            $new_faq->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = Faq::class;
            $model->relation_id = $new_faq->id;
            $model->save(false);
        }
    }

    public function actionClonePotato()
    {
        /**
         * @var $potatos Potato[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\Potato'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $potatos = Potato::find()->where(['id' => $content_ids])->all();
        foreach ($potatos as $potato) {
            $new_potato = new Potato();
            $new_potato->text = $potato->text;
            $new_potato->video_link = $potato->video_link;
            $new_potato->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = Potato::class;
            $model->relation_id = $new_potato->id;
            $model->save(false);
        }
    }

    public function actionCloneRecipe()
    {
        /**
         * @var $recipes Recipe[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\Recipe'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $recipes = Recipe::find()->where(['id' => $content_ids])->all();
        foreach ($recipes as $recipe) {
            $new_recipe = new Recipe();
            $new_recipe->name = $recipe->name;
            $new_recipe->type = $recipe->type;
            $new_recipe->text = $recipe->text;
            $new_recipe->text_cut = $recipe->text_cut;
            if ($recipe->image) {
                $new_recipe->image = 'copy_' . $recipe->image;
                $this->moveImage($recipe, 'recipe', 'image');
            }
            $new_recipe->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = Recipe::class;
            $model->relation_id = $new_recipe->id;
            $model->save(false);
        }
    }

    public function actionCloneRecommendation()
    {
        /**
         * @var $recomendations Recommendation[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\Recommendation'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $recomendations = Recommendation::find()->where(['id' => $content_ids])->all();
        foreach ($recomendations as $recomendation) {
            $new_recomendation = new Recommendation();
            $new_recomendation->text = $recomendation->text;
            $new_recomendation->link = $recomendation->link;
            $new_recomendation->type = $recomendation->type;
            if ($recomendation->image) {
                $new_recomendation->image = 'copy_' . $recomendation->image;
                $this->moveImage($recomendation, 'recipe', 'image');
            }
            $new_recomendation->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = Recommendation::class;
            $model->relation_id = $new_recomendation->id;
            $model->save(false);
        }
    }

    public function actionCloneReport()
    {
        /**
         * @var $reports Report[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\Report'])->andWhere(['type_program' => 2])->all(), 'relation_id', 'relation_id');
        $reports = Report::find()->where(['id' => $content_ids])->all();
        foreach ($reports as $report) {
            $new_reports = new Report();
            $new_reports->title_adm = $report->title_adm;
            $new_reports->text_adm = $report->text_adm;
            $new_reports->week = $report->week;
            $new_reports->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = Report::class;
            $model->relation_id = $new_reports->id;
            $model->save(false);
        }
    }

    public function actionCloneWeekProducts()
    {
        /**
         * @var $products WeekProducts[]
         */
        $content_ids = ArrayHelper::map(ContentType::find()->where(['=', 'class_name', 'app\models\WeekProducts'])->andWhere(['type_program' => 1])->all(), 'relation_id', 'relation_id');
        $products = WeekProducts::find()->where(['id' => $content_ids])->all();
        foreach ($products as $product) {
            $new_product = new WeekProducts();
            $new_product->text = $product->text;
            $new_product->week = $product->week;
            $new_product->veg = $product->veg;
            if ($product->image) {
                $new_product->image = 'copy_' . $product->image;
                $this->moveImage($product, 'week-products', 'image');
            }
            $new_product->save(false);
            $model = new ContentType();
            $model->type_program = 6;//6
            $model->class_name = WeekProducts::class;
            $model->relation_id = $new_product->id;
            $model->save(false);
        }
    }

    public function moveImage($model, $dir, $attribute)
    {
        $dir = \Yii::getAlias('@app/web/uploads/' . $dir . '/');
        if (is_file($dir . $model->{$attribute})) {
            copy($dir . $model->{$attribute}, $dir . 'copy_' . $model->{$attribute});
        }
    }


    public function actionFrost()
    {
        /**
         * @var $group Group
         * @var $content ContentType
         */
        $group = Group::findOne(805);
        $group->num = 'разморозка';
        $group->save(false);
        $content = ContentType::find()->where(['type_program' => 8, 'class_name' => Group::class, 'relation_id' => $group->id])->one();
        if ($content) {
            $content->type_program = 10;
            $content->save(false);
        }
        foreach ($group->groupMembers as $member) {
            $member->type_program = 10;
            $member->save(false);
            $member->user->session_type = 10;
            $member->user->save(false);
            VarDumper::dump($member->id . PHP_EOL);
        }
    }

    public function actionSendEmail()
    {
        /**
         * @var $members GroupMember[]
         */
        $members = GroupMember::find()->where(['type_program' => 7])->orWhere(['type_program' => 8])->all();
        foreach ($members as $member) {
            if ($member->group_id && $member->payment) {
                $post_value = [
                    'api_key' => '69e8txindd4yyjiiu3h3ftu6rg4oh7y7ptogy3ya',
                    'domain' => 'lerchek.ru',
                    'message' => [
                        'body' => [
                            'html' => \Yii::$app->controller->renderPartial('@app/mail/error', ['member' => $member])
                        ],
                        'subject' => 'Доступ к личному кабинету восстановлен',
                        'from_email' => 'info@lerchek.ru',
                        'from_name' => 'Ler_chek',
                        'recipients' => [
                            [
                                'email' => $member->user->email,
                            ],
                        ],
                    ],
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://eu1.unione.io/ru/transactional/api/v1/email/send.json');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_value));
                $result = curl_exec($ch);
                VarDumper::dump($result);
                curl_close($ch);
                $smsaero_api = new \app\helpers\SmsaeroApiV2('admin@lerchek.ru', '2r0dxVxGZCuxg1SliJwk3fBmcfz7', 'Lerchek'); // api_key из личного кабинета
                $text = 'Дорогой клиент! Почта техподдержки переехала на новый адрес: help@lerchek.ru Если вы писали на почту техподдержки и не получили ответ, напишите, пожалуйста именно на этот имейл.
                Если вы не можете зайти в личный кабинет, то переходите по ссылке: https://lk.lerchek.ru/';
                $result2 = $smsaero_api->send($member->phone, $text, 'DIRECT');
                VarDumper::dump(PHP_EOL);
                VarDumper::dump($result2);

            }
        }
    }

    public function actionRetailErrorsPush()
    {
        /**
         * @var $retails Retail[]
         */
        $client = new ApiClient(
            'https://letique.retailcrm.ru',
            'a599Z4G4PCJEmvFFgxliTd2oVaVkkHuk',
            ApiClient::V5
        );

        $retails = Retail::find()->where(['member_id' => [
            215017,
            215015,
            214741,
            215021,
            215019,
            215018,
            215024,
            215028,
            215029,
            215025,
            215022,
            215026,
            215027,
            212522,
            215032,
            215035,
            215033,
            215036,
            215030,
            211920,
            215037,
            215038,
            215041,
            215031,
            215039,
            215023,
            215042,
            212527,
            215043,
            215048,
            215047,
            215052,
            215050,
            215051,
            215053,
            215049,
            215057,
            215056,
            215055,
            215061,
            215063]])->all();
        foreach ($retails as $retail) {
            $ids = explode('_', $retail->present);
            $response = $client->request->ordersCreate(array(
                'customFields' => [
                    'tempoline_delivery' => is_string(My::getDelivery($retail->city, $retail->country)) ? My::getDelivery($retail->city, $retail->country) : 'Grastin',
                ],
                'firstName' => $retail->groupMember->first_name,
                'countryIso' => $retail->country ? $retail->country : null,
                'orderMethod' => 'gift',
                'lastName' => $retail->groupMember->last_name,
                'phone' => $retail->groupMember->phone,
                'email' => $retail->groupMember->user->email,
                'weight' => 100,
                'length' => 300,
                'width' => 200,
                'height' => 100,
                'status' => 'new',
                'discountManualPercent' => '100%',
                'items' => My::countSet($ids),
                'customerComment' => $retail->comment ? $retail->comment : null,
                'delivery' => array(
                    'code' => 'tempoline-post',
                    'date' => is_array(My::getDelivery($retail->city, $retail->country)) ? My::getDelivery($retail->city, $retail->country)['Grastin'] : null,
                    'address' => [
                        'index' => $retail->index_post ? $retail->index_post : null,
                        'city' => $retail->city ? $retail->city : null,
                        'region' => null,
                        'street' => $retail->street ? $retail->street : null,
                        'building' => $retail->building ? $retail->building : null,
                        'flat' => $retail->flat ? $retail->flat : null,
                    ],
                    'cost' => My::getCountryCost()[$retail->country],
                )
            ), 'letique-ru');

            VarDumper::dump($response);
            $response = $client->request->ordersPaymentCreate(
                [
                    'order' => [
                        'id' => $response->id
                    ],
                    'amount' => $retail->payment->cost,
                    'paidAt' => date('Y-m-d H:i:s'),
                    'status' => 'paid',
                    'type' => 'bank-card',
                ],
                'letique-ru'
            );
            VarDumper::dump($response);
        }
    }

    public function actionGetCountUsers()
    {
        /**
         * @var $members GroupMember[]
         */
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'no-payment-girls-all.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $members = GroupMember::find()->select('*,COUNT(user_id) as cnt')->with('user')->where(['!=', 'type_program', My::FOR_GIRL_16_02_2021])->andWhere(['!=', 'type_program', My::FOR_MAN_16_02_2021])->groupBy('user_id')->all();

        foreach ($members as $member) {
            if (!$member->payment && My::isGirl($member->type_program)) {
                $sheet->setCellValue('A' . $i, $member->user->email);
                $i++;
            }
        }
        $writer = new Xlsx($spreadsheet);
        VarDumper::dump($writer->save($file));

    }

    public function actionGetUsersVotes()
    {
        /**
         * @var $members GroupMember[]
         */
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'votes-girl.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $members = GroupMember::find()->select('*')->with('user')->where(['=', 'type_program', My::FOR_GIRL_16_02_2021])->all();

        foreach ($members as $member) {
            if ($member->payment) {
                $sheet->setCellValue('A' . $i, $member->user->email);
                $i++;
            }
        }
        $writer = new Xlsx($spreadsheet);
        VarDumper::dump($writer->save($file));

    }

    public function actionGetCountUsersPayment()
    {
        /**
         * @var $users User[]
         */
        $payments = PaymentLer::find()->where(['>=', 'date_payment', '01-03-2020'])->all();
        $all_count = 0;
        $new_count = 0;
        foreach ($payments as $payment) {

        }
        VarDumper::dump($new_count);
        VarDumper::dump(PHP_EOL . $all_count);
    }

    public function actionDeleteVotes()
    {
        /**
         * @var $votes Vote[]
         */
        $votes = Vote::find()->all();
        foreach ($votes as $vote) {
            if ($vote->groupMember->type_program == My::FOR_MAN_04_01_2020 || $vote->groupMember->type_program == My::FOR_GIRL_04_01_2020) {
                VarDumper::dump($vote->delete() . PHP_EOL);
            }
        }
    }

    public function actionAddRef()
    {
        $ref_user = User::findOne(['email' => 'kristinochka328@mail.ru']);
        $ref = new Ref();
        $ref->user_id = $ref_user->id;
        $ref->unique_id = md5(mktime());
        $ref->sum = '+' . 279;
        $ref->status = Ref::NEW_PAYMENT;
        $ref->status_export = Ref::NO_EXP;
        $ref->date = date('Y-m-d');
        $ref->member_id = 281861;
        $ref->save(false);
        $ref_user = User::findOne(['email' => 'koneva.ma@yandex.ru']);
        $ref = new Ref();
        $ref->user_id = $ref_user->id;
        $ref->unique_id = md5(mktime());
        $ref->sum = '+' . 279;
        $ref->status = Ref::NEW_PAYMENT;
        $ref->status_export = Ref::NO_EXP;
        $ref->date = date('Y-m-d');
        $ref->member_id = 287762;
        $ref->save(false);

    }

    public function actionPerenos()
    {
        $group = Group::findOne(847);
        $group->num = 'РАЗМОРОЗКА';
        $contentType = ContentType::findOne(['class_name' => 'app\models\Group', 'relation_id' => $group->id]);
        if ($contentType) {
            $contentType->type_program = 11;
            $contentType->save();
            foreach ($group->groupMembers as $member) {
                $member->type_program = 11;
                $member->save(false);
                $member->user->session_type = 11;
                $member->user->save(false);
            }
        }
        $group = Group::findOne(848);
        $group->num = 'РАЗМОРОЗКА';
        $contentType = ContentType::findOne(['class_name' => 'app\models\Group', 'relation_id' => $group->id]);
        if ($contentType) {
            $contentType->type_program = 12;
            $contentType->save();
            foreach ($group->groupMembers as $member) {
                $member->type_program = 12;
                $member->save(false);
                $member->user->session_type = 12;
                $member->user->save(false);
            }
        }
    }

    public function actionGetUsersTelegram()
    {
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'mans-group.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $contentTypes = ContentType::find()->where(['class_name' => 'app\models\Group', 'type_program' => 12])->all();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        foreach ($contentTypes as $contentType) {
            $group = Group::findOne($contentType->relation_id);
            if ($group) {
                foreach ($group->groupMembers as $member) {
                    $sheet->setCellValue('A' . $i, $member->user->email);
                    $sheet->setCellValue('B' . $i, $group->link_main);
                    $sheet->setCellValue('C' . $i, $group->link_second);
                    $i++;

                }
            }
        }
        $writer = new Xlsx($spreadsheet);
        VarDumper::dump($writer->save($file));
    }

    public function actionGetClientsEmpty()
    {
        /**
         * @var $models Ref[]
         * @var $members GroupMember[]
         */


        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'no_group_girls.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        $members = GroupMember::find()->where(['type_program' => 13])->orWhere(['type_program' => 14])->all();
        $count = 0;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;

        foreach ($members as $member) {
            if ($member->payment && $member->group_id === null && $member->user) {
                $member->short_url = 'https://lk.lerchek.ru/marafon/signup?email=' . $member->user->email . '&type=' . $member->type_program;
                $member->save(false);
                $hashids = new Hashids('', 5);
                $hash = $hashids->encode($member->id);
                $url = 's/' . $hash;
                $subject = 'Марафон Artem_chek уже стартовал!';
                $text = 'Марафон Artem_chek уже стартовал! Мы ждем Вас: https://lerchek.ru/' . $url;

                $view = 'send_invite_man';
                if (My::isGirl($member->type_program)) {
                    $view = 'send_invite_girl';
                    $subject = 'Марафон Ler_chek уже стартовал!';
                    $text = 'Марафон Ler_chek уже стартовал! Мы ждем Вас: https://lerchek.ru/' . $url;
                }

                $smsaero_api = new \app\helpers\SmsaeroApiV2('admin@lerchek.ru', '2r0dxVxGZCuxg1SliJwk3fBmcfz7', 'Lerchek'); // api_key из личного кабинета
                $result_sms = $smsaero_api->send($member->phone, $text, 'DIRECT');
                My::log('sms_empty_group', $member->phone);
                My::log('sms_empty_group', $result_sms);
                My::sendEmail($member, $view, $subject);
                $count++;
                $sheet->setCellValue('A' . $i, $member->user->email);
                $sheet->setCellValue('B' . $i, 'https://lerchek.ru/' . $url);
                $i++;

                VarDumper::dump('https://lerchek.ru/' . $url);
                VarDumper::dump($i);
            }
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save($file);

    }

    public function actionGetDuplicateProgram()
    {
        /**
         * @var $users User[]
         */
        $users = User::find()->all();
        $cnt = 0;
        foreach ($users as $user) {
            if (count($user->groupMembers) > 1) {
                foreach ($user->groupMembers as $groupMember) {
                    if ($groupMember->type_program == 11 || $groupMember->type_program == 12) {
                        $cnt += 1;

                    }
                }
                VarDumper::dump($user->email . PHP_EOL);
            }
        }
        VarDumper::dump($cnt, 11, 1);
    }

    public function actionGetUsersNotToDay()
    {
        /**
         * @var $members GroupMember[]
         */
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'users-yes.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $members = GroupMember::find()->select('*')->with('user')->where(['=', 'type_program', 9])->orWhere(['=', 'type_program', 10])->all();

        foreach ($members as $member) {
            $newMemb = GroupMember::find()->where(['user_id' => $member->user_id])->andWhere(['type_program' => 11])->orWhere(['user_id' => $member->user_id])->andWhere(['type_program' => 12])->one();
            if ($member->payment && $newMemb != null) {
                $sheet->setCellValue('A' . $i, $member->user->email);
                $i++;
            }
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save($file);

    }

    public function actionGetUsers890()
    {
        /**
         * @var $payments PaymentLer[]
         */
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '-890.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;
        $payments = PaymentLer::find()->where(['>=', 'date_payment', '2021-02-13'])->andWhere(['<=', 'date_payment', '2021-02-29'])->andWhere(['cost' => 890])->all();

        foreach ($payments as $payment) {
            if ($payment->member && $payment->member->user) {
                $sheet->setCellValue('A' . $i, $payment->member->user->email);
                $i++;
            }
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
    }

    public function actionGetAllUsers()
    {
        /**
         * @var $members GroupMember[]
         */
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'all-users-payment-man.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        /*$file2 = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'all-users-no-payment-girls.xlsx';
        if (!is_file($file2)) {
            fopen($file2, "w", true);
        }*/


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $i = 1;
        $members = GroupMember::find()->groupBy(['user_id'])->all();


        foreach ($members as $member) {
            if ($member->payment && !My::isGirl($member->type_program)) {
                $sheet->setCellValue('A' . $i, $member->user->email);
                $sheet->setCellValue('B' . $i, $member->phone);
                $i++;
                VarDumper::dump($i . PHP_EOL);
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
    }

    public function actionGetMart()
    {
        /**
         * @var $payments PaymentLer[]
         */
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        /* $file2 = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'all-users-man-payment.xlsx';
         if (!is_file($file2)) {
             fopen($file2, "w", true);
         }*/


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $i = 1;
        $payments = PaymentLer::find()->where(['>=', 'date_payment', '2021-03-01'])->all();


        foreach ($payments as $payment) {
            if ($payment->member && $payment->member->user && $payment->member->type_program = My::FOR_GIRL_22_03_2021 || $payment->member && $payment->member->user && $payment->member->type_program = My::FOR_MAN_22_03_2021) {
                $sheet->setCellValue('A' . $i, $payment->member->user->email);
                $sheet->setCellValue('B' . $i, $payment->member->phone);
                $i++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
    }

    public function actionGetUsers()
    {
        /**
         * @var $members GroupMember[]
         */
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '-Girls.xlsx';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        /* $file2 = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'all-users-man-payment.xlsx';
         if (!is_file($file2)) {
             fopen($file2, "w", true);
         }*/


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $i = 1;
        $members = GroupMember::find()->where(['type_program' => 11])->all();

        foreach ($members as $member) {
            $member_new = GroupMember::find()->where(['type_program' => 13, 'user_id' => $member->user_id])->count();
            if ($member->user && $member->payment && $member->group_id && $member_new == 0) {
                $member->experience = \Yii::$app->security->generateRandomString();
                $member->save(false);
                $sheet->setCellValue('A' . $i, $member->user->email);
                $sheet->setCellValue('B' . $i, 'https://lk.lerchekmarafon.ru/marafon/continue?token=' . $member->experience);
                $i++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
    }
}

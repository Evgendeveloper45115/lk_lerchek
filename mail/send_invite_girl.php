<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \app\models\User */
/* @var $input array */
/* @var $link string */

?>
<?php $this->beginPage() ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="ru">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


        <style type="text/css">


            * {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 15px;
            }

            div, p, span, strong, b, em, i, a, li, td {
                -webkit-text-size-adjust: none;
            }

            img {
                max-width: 100%;
            }

            @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
                body {
                    margin: 0 auto;
                    max-width: 320px;
                }

                table {
                    width: 320px !important;
                    margin: 0 auto !important;
                }

                span.size28 {
                    font-size: 28px !important;
                }

                .block {
                    display: block;
                    width: 98% !important;
                    margin: 0 auto !important;
                }

                .margin0 {
                    margin: 0 !important;
                }

                .margin_bl {
                    display: block;
                    margin: 0 auto 28px;
                }

                .button {
                    width: 295px !important;
                    margin-top: 30px !important;
                }

                .padding {
                    padding: 6px 0 0 32px !important;
                }

                .block2 {
                    display: table;
                    width: 145px !important;
                    vertical-align: bottom;
                }

                .block3 {
                    width: 140px !important;
                    display: table;
                }

                .block4 {
                    width: 140px !important;
                    display: table;
                }

                .height0 {
                    height: 0 !important;
                }

                .center {
                    font-size: 14px !important;
                }

                .width155 {
                    width: 147px !important;
                }

                .widht150 {
                    width: 150px !important;
                }

                .height {
                    height: 0 !important;
                }

                .float {
                    float: none !important;
                    margin: 0 auto !important;
                }

                .none {
                    display: none !important;
                }

                .width100 {
                    width: 100% !important;
                }

                .paddi {
                    padding: 6px 0 0 0 !important;
                }
            }
        </style>
        <?php $this->head() ?>
    </head>
    <body style="margin: 0 auto;padding: 0;max-width: 750px; width: 100%;">
    <?php $this->beginBody() ?>
    <table cellpadding="0" cellspacing="0" width="100%" border="0"
           style="min-width:300px;max-width: 740px; border-collapse:collapse;" align="center">
        <tr>
            <td align="center">

                <table cellpadding="0" cellspacing="0" width="100%" border="0"
                       style="min-width:300px;max-width: 740px; border-collapse:collapse;background:#000000 url(http://lerchek.ru/images/email/reg/iyoax5.jpg) no-repeat top center;background-size: cover; "
                       align="center" bgcolor="#55bd9d">
                    <tr>
                        <td align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                                   style="border-collapse:collapse;max-width: 500px;" align="center">
                                <tr>
                                    <td height="249" align="center" valign="top">
                                        <table align="center" style="max-width: 500px; margin-top: 30px;">
                                            <tr>
                                                <td align="center" width="96">
                                                    <a href="#" target="_blank">
                                                        <img src="https://lk.lerchek.ru/images/logo_mail.png"
                                                             alt=""/>
                                                    </a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="1" height="30"></td>
                                            </tr>

                                            <tr>
                                                <td>
                                                <span
                                                        style="display: block; font-family: 'Roboto Slab', serif; font-size: 34px;color: #ffffff;text-align: center;"
                                                        class="size28">Ждем вас на марафоне!</span>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <!-- End header -->


                <table width="100%" align="center" border="0" style="height: 165px;max-width: 505px;">
                    <tr>
                        <td width="1" height="20"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <span style="text-align: center;color: #676767;font-size: 17px;">Привет! Мы заметили, что вы не участвуете в марафоне. Для того, чтобы попасть в личный кабинет нужно заполнить анкету по ссылке ниже. Регистрируйтесь в марафоне и переходите в чат, где вас уже ждет команда единомышленников!</span>
                        </td>
                    </tr>

                </table>


                <table width="100%" align="center" border="0"
                       style="border-collapse:collapse;max-width: 500px;min-width: 300px;">
                    <tr>
                        <td align="center" valign="top">
                            <a href="https://lk.lerchek.ru/marafon/signup?email=<?= $member->user->email ?>&type=<?= $member->type_program ?>"
                               target="_blank"
                               style="background: #f52950;border-radius: 50px;display: block;width: 330px;height: 44px;padding:22px 0 0 0; margin:0 auto;text-align: center;font-weight: bold;font-family: 'Roboto Slab', serif;text-transform: uppercase;font-size: 15px;color: #ffffff;text-decoration: none;"
                               class="button">заполнить анкету</a>
                        </td>
                    </tr>
                    <tr>
                        <td width="1" height="40"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <span style="text-align: center;color: #676767;font-size: 17px;">До встречи на марафоне!</span>
                        </td>
                    </tr>
                    <tr>
                        <td width="1" height="20"></td>
                    </tr>
                </table>

                <table width="100%" align="center" border="0"
                       style="border-collapse:collapse;max-width: 250px;min-width: 300px;">

                </table>


                <table width="100%" align="center" style="border-collapse:collapse;max-width: 740px;" border="0"
                       bgcolor="#232323">
                    <tr>
                        <td width="1" height="40"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <a href="#" target="_blank" style="text-decoration: none;">
                                <img src="http://lerchek.ru/images/email/logo_mail.png" alt=""/>
                            </a>
                        </td>
                    </tr>


                    <tr>
                        <td align="center">
                            <span style="color: #676767;">Все права защищены</span>
                        </td>
                    </tr>

                    <tr>
                        <td width="1" height="20"></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
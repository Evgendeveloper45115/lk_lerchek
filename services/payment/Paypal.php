<?php

namespace app\services\payment;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Request;

class Paypal implements IPaymentGateway
{
    private $paypalEmail;
    private $paypalDesc;
    private $url;

    public function __construct()
    {
        $this->paypalEmail = Yii::$app->params['paypal_email'];
        $this->paypalDesc = Yii::$app->params['paypal_desc'];
        $this->url = Yii::$app->params['paypal_url'];
        $this->debug = Yii::$app->params['paypal_debug'];

    }

    public function renderForm($user, $cost)
    {

        return Yii::$app->controller->renderPartial('_form/auto-submit-form_paypal', [
            'user' => $user,
            'cost' => $cost,
            'url' => $this->url,
            'paypalEmail' => $this->paypalEmail,
            'paypalDesc' => $this->paypalDesc,

        ]);
    }

    public function getPaymentData(Request $request)
    {
        $data = new PaymentData();
        $data->userId = $request->post('item_number');
        $data->invoiceId = $request->post('item_number');
        $data->amount = $request->post('mc_gross');

        return $data;
    }

    public function isValidPayment(Request $request)
    {

        $ipn = new PaypalIPN();

        if ($this->debug) {
            $ipn->useSandbox();
        }
        try {
            $verified = $ipn->verifyIPN();
        } catch (\Exception $e) {
            return false;
        }

        if (!$verified) {
            return false;
        }

        if ($request->post('payment_status') != 'Completed') {
            return false;
        }

        if ($request->post('mc_currency') != 'RUB') {

            return false;
        }

        if ($request->post('receiver_email') != $this->paypalEmail) {
            return false;
        }

        return true;
    }

    public function renderSuccessView()
    {
        return Yii::$app->response->statusCode = 200;
    }

    public function renderFailedView()
    {
        throw new NotFoundHttpException();
    }
}
<?php

namespace app\services\payment;

class PaymentData
{
    public $userId;
    public $invoiceId;
    public $amount;
    public $rebillingOn;
    public $clientOrderId;
    public $cvv;
    public $promo;
    public $type_program;
    public $retail_id;
    public $payment_amount;
    public $email;
    public $email_man;
    public $continue;
    public $manWoman;
    public $halva;
    public $ref;
    public $shop_id;
    public $ga_param;
    public $cid_param;
    public $orderNumber;
}
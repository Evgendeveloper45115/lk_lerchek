<?php

namespace app\services\payment;

interface IPaymentGatewayWithPreCheck extends IPaymentGateway
{

    public function renderSuccessCheckView();

    public function renderFailedCheckView();

}
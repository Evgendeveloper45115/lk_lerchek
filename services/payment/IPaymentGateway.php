<?php

namespace app\services\payment;

use yii\web\Request;

interface IPaymentGateway
{
    public function renderForm($user, $cost);

    public function isValidPayment(Request $request);

    public function renderSuccessView();

    public function renderFailedView();

    public function getPaymentData(Request $request);

}
<?php

namespace app\services\payment;

use yii\web\Request;

class PaymentFactory
{

    /**
     * @param $paymentType
     * @return Paypal|Yandex
     */
    public static function createPaymentGateway($paymentType)
    {
        switch ($paymentType) {
            case 'paypal':
                return new Paypal();
            default:
                return new Yandex();
        }
    }

    public static function getPaymentTypeFromRequest(Request $request)
    {
        if ($request->post('paymentType')) {
            return 'yandex';
        }

        if ($request->post('mc_gross')) {
            return 'paypal';
        }

    }

}
<?php

namespace app\services\payment;

use app\components\MyUrlManager;
use app\helpers\My;
use app\models\User;
use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Request;

class Yandex implements IPaymentGatewayWithPreCheck
{
    private $url;
    private $shopId;
    private $scid;
    private $ga;
    private $cid;
    private $paymentType;

    public function __construct()
    {
        $this->url = Yii::$app->params['url'];
        $this->shopId = Yii::$app->params['shopId'];
        $this->scid = Yii::$app->params['scid'];
        $this->ga = null;

        if (isset($_COOKIE['_ga'])) {
            $this->ga = $_COOKIE['_ga'];
            $this->ga = str_replace('GA', '', $this->ga);
        }
        if (isset($_COOKIE['_ym_d'])) {
            $this->cid = $_COOKIE['_ym_d'];
        }

    }

    public function renderForm($user, $cost, $retail_id = null)
    {
        return Yii::$app->controller->renderPartial('payment_form/auto-submit-form', [
            'user' => $user,
            'cost' => $cost,
            'paymentType' => $this->paymentType,
            'continue' => $cost == 1900 ? 1 : 0,
            'url' => $this->url,
            'shopId' => $this->shopId,
            'scid' => $this->scid,
            'rebillingOn' => Yii::$app->request->post('rebillingOn') !== null ? 1 : 0,
            'halva' => Yii::$app->request->post('halva_hidden') !== null ? 1 : 0,
            'recipe' => $this->getRecipe($user, $cost),
            'retail_id' => $retail_id,
            'payment_amount' => 1980,
            'ga_param' => $this->ga,
            'cid_param' => $this->cid
        ]);
    }

    public function getRecipe($user, $cost)
    {
        /**
         * @var $user User
         */
        return $receipt = Json::encode(
            [
                'customer' => ['email' => trim($user->email)],
                'taxSystem' => '2',
                'items' => [[
                    'quantity' => '1',
                    'price' => [
                        'amount' => $cost
                    ],
                    'tax' => '1',
                    'text' => 'Марафон похудения',
                    "paymentSubjectType" => "service",
                ]]
            ]
        );

    }

    public function isValidPayment(Request $request)
    {
        return $this->checkMD5($request);
    }

    public function renderSuccessCheckView()
    {
        return $this->renderView('check_success', 0);
    }

    public function renderFailedCheckView()
    {
        return $this->renderView('check_success', 200);
    }

    public function renderSuccessView()
    {
        return $this->renderView('aviso_success', 0);
    }

    public function renderFailedView()
    {
        return $this->renderView('aviso_success', 200);
    }

    public function getPaymentData(Request $request)
    {

        $data = new PaymentData();
        $data->userId = $request->post('customerNumber');
        $data->invoiceId = $request->post('invoiceId');
        $data->amount = $request->post('orderSumAmount');
        $data->rebillingOn = $request->post('rebillingOn');
        $data->clientOrderId = $request->post('clientOrderId');
        $data->cvv = $request->post('cvv');
        $data->type_program = $request->post('type_program');
        $data->retail_id = $request->post('retail_id');
        $data->payment_amount = $request->post('payment_amount');
        $data->email = $request->post('email');
        $data->email_man = $request->post('email_man');
        $data->continue = $request->post('continue');
        $data->manWoman = $request->post('manWoman');
        $data->halva = $request->post('halva');
        $data->ref = base64_decode($request->post('ref'));
        $data->shop_id = $request->post('shopId');
        $data->ga_param = $request->post('ga_param');
        $data->cid_param = $request->post('cid_param');
        $data->orderNumber = $request->post('orderNumber');
        return $data;
    }

    private function renderView($view, $code)
    {
        return Yii::$app->controller->renderPartial($view, [
            'date' => Yii::$app->request->post('requestDatetime'),
            'code' => $code,
            'invoiceId' => Yii::$app->request->post('invoiceId'),
            'shopId' => Yii::$app->params['shopId'],
        ]);
    }

    private function checkMD5(Request $request)
    {

        $str = $request->post('action') . ";"
            . $request->post('orderSumAmount') . ";"
            . $request->post('orderSumCurrencyPaycash') . ";"
            . $request->post('orderSumBankPaycash') . ";"
            . $this->shopId . ";"
            . $request->post('invoiceId') . ";"
            . $request->post('customerNumber') . ";"
            . Yii::$app->params['shopPassword'];


        $md5 = strtoupper(md5($str));

        if ($md5 != strtoupper($request->post('md5'))) {
            return false;
        }
        return true;
    }
}
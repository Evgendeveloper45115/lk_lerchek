<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAssetPayment extends AssetBundle
{

    public $import = false;
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/payment/style.css?v=9.99',
    ];
    public $js = [
        'js/my.js?v=3.0',
    ];
    public $depends = [
        //  'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
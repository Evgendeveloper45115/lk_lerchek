<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetCalendar extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/main.css?v=1',
        'css/media.css?v=0.8',
        'css/owl.carousel.css',
        'css/fullcalendar.min.css',
        'css/venobox.css',
        'css/calendar.css?v=0.3',
    ];
    public $js = [
        'js/calendar/jquery.js',
        'js/owl.carousel.min.js',
        'js/user.js',
        'js/calendar/slick/slick.js',
        'js/calendar/venobox/venobox.js',
        'js/calendar/mask/jquery.mask.min.js',
        'js/calendar/fullcalendar/moment.js',
        'js/calendar/fullcalendar/fullcalendar.min.js',
        'js/calendar/fullcalendar/locale-all.js',
        'js/calendar/core.js?v=3.3',
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];

    public $depends = [
        /*'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',*/
    ];
}

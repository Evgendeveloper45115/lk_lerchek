<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/all.css?v=5.3',
        'css/media.css?v=4.9',
        'css/owl.carousel.css',
        'css/cropper.css?v=1.4',
        'js/click-tap-image/css/image-zoom.css',

        'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
    ];
    public $js = [
        'js/owl.carousel.min.js',
        'js/click-tap-image/dist/js/image-zoom.min.js',
        'js/user.js?v=3.32',
        'js/cropper.js?v=1.9',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}

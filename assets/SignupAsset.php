<?php

namespace app\assets;

use yii\web\AssetBundle;

class SignupAsset extends AssetBundle
{

    public $import = false;
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/signup/site.css?v=1.3',
        'css/signup/mobile.css?v=1.2',
        'css/glyphicons.css',
    ];
    public $js = [
        'js/my.js',
    ];
    public $depends = [
        //  'yii\web\YiiAsset',
        //   'yii\bootstrap\BootstrapAsset',
    ];
}
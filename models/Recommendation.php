<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "recommendation".
 *
 * @property int $id
 * @property int|null $type
 * @property string|null $text
 * @property string|null $image
 * @property string|null $link
 */
class Recommendation extends MyAR
{
    const RECOMMENDATION_FOOD = 0;
    const RECOMMENDATION_CARDIO = 1;

    static $types = [
        '0' => 'По питанию',
        '1' => 'По кардио',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recommendation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['text'], 'string'],
            [['image', 'link', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'text' => 'Описание',
            'image' => 'Картинка',
            'link' => 'Ютуб',
        ];
    }
}

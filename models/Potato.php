<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "potato".
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $video_link
 */
class Potato extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'potato';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['video_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Описание',
            'video_link' => 'Ссылка ютуб',
        ];
    }
}

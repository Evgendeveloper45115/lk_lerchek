<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property string $num
 * @property string|null $datetime_create
 * @property string|null $datetime_start
 * @property string|null $datetime_end
 * @property string|null $date_continue
 * @property string|null $link_main
 * @property string|null $link_second
 *
 * @property GroupMember[] $groupMembers
 */
class Group extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num'], 'required'],
            [['datetime_create', 'datetime_start', 'datetime_end', 'date_continue'], 'safe'],
            [['num', 'link_main', 'link_second'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'num' => 'Номер',
            'datetime_create' => 'Дата создания',
            'datetime_start' => 'Дата старта',
            'datetime_end' => 'Конец 1.0',
            'date_continue' => 'Конец потока',
            'link_main' => 'ссылка ТГ 1.0',
            'link_second' => 'ссылка на ТГ канал',
            'is_clone' => 'Такой же контент?',
        ];
    }

    /**
     * Gets query for [[GroupMembers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGroupMembers()
    {
        return $this->hasMany(GroupMember::className(), ['group_id' => 'id']);
    }

    public function getMembersCount()
    {
        return $this->hasMany(GroupMember::className(), ['group_id' => 'id'])->where(['type_program' => Yii::$app->user->identity->session_type])->leftJoin(User::tableName(), 'user.id=group_member.user_id')->andWhere(['role' => User::ROLE_USER])->count();
    }

}

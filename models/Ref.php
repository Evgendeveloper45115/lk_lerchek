<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "ref".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $unique_id
 * @property int|null $sum
 * @property int $status
 * @property int $status_export
 * @property int $phone
 * @property int $member_id
 * @property date $date
 * @property User $user
 * @property GroupMember $member
 */
class Ref extends \yii\db\ActiveRecord
{
    const NEW_PAYMENT = 0;
    const PAYMENT_REQUEST = 1;
    const SUCCESS = 2;
    const DECLINED = 3;
    const ERROR = 4;
    const NO_EXP = 0;
    const EXP = 1;

    public $money;
    public $oferta;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ref';
    }


    public static $statuses = [
        self::NEW_PAYMENT => 'Начислено',
        self::PAYMENT_REQUEST => 'Запрос на выплату',
        self::SUCCESS => 'Выполнено',
        self::DECLINED => 'Отклонено',
        self::ERROR => 'Ошибка',
    ];
    public static $statuses_export = [
        self::NO_EXP => 'Не выгружено',
        self::EXP => 'Выгружено',
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        /**
         * @var $user User
         */
        $user = Yii::$app->user->identity;
        $ref = Ref::find()->where(['user_id' => $user->id])->all();
        $query = (new \yii\db\Query())->from('ref');
        $query->where(['user_id' => $user->id]);
        $query->andWhere(['!=', 'status', Ref::DECLINED]);
        $sum = $query->sum('sum');
        if ($sum === null) {
            $sum = 0;
        }
        return [
            [['user_id', 'status', 'status_export'], 'integer'],
            [['phone'], 'safe'],
            [['date', 'member_id', 'sum', 'unique_id'], 'safe'],
            [['sum'], 'integer', 'min' => 1000, 'max' => $sum, 'tooSmall' => 'Сумма должна быть не меньше 1000р', 'tooBig' => 'Введенная сумма больше, чем ' . $sum . ' р.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Клиент',
            'unique_id' => 'Уникальный номер выплаты',
            'sum' => 'Сумма',
            'status' => 'Статус',
            'status_export' => 'Статус выгрузки',
            'phone' => 'Номер телефона',
            'date' => 'Дата'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getMember()
    {
        return $this->hasOne(GroupMember::class, ['id' => 'member_id']);
    }

}

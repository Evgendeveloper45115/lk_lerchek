<?php

namespace app\models;

use app\helpers\My;
use app\models\base\MyAR;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "ration_complex".
 *
 * @property int $id
 * @property int|null $ration_type
 * @property int|null $week
 * @property int|null $day
 * @property int|null $day_part
 * @property string|null $name
 * @property float|null $portion
 * @property float|null $calories
 * @property float|null $protein
 * @property float|null $grease
 * @property float|null $carbohydrate
 * @property string|null $image
 * @property string|null $description
 * @property Ingredient[] $ingredients
 */
class RationComplex extends MyAR
{

    public static $rationTypes = [
        1 => 'Вегетарианский (меню без продуктов животного происхождения)',
        2 => 'Особенный (меню с продуктами средней и высокой ценовой категории)',
        3 => 'Парный (меню для тех, кто участвует в марафоне с парой)',
        5 => 'Базовый (меню с базовым недорогим набором продуктов)',
        7 => 'Альтернативное (меню с 4 приемами пищи)',
        8 => 'AntiAge (ваш секрет молодости)',
        9 => 'Базовое меню NEW (меню с продуктами низкой и средней ценовой категории)',
        10 => 'Постное меню'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ration_complex';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ration_type', 'week', 'day', 'day_part', 'type_field'], 'integer'],
            [['portion', 'calories', 'protein', 'grease', 'carbohydrate'], 'number'],
            [['description'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'week' => 'Неделя',
            'day' => 'День',
            'ration_type' => 'Тип питания',
            'day_part' => 'Время приема пищи',
            'name' => 'Название',
            'portion' => 'Порция',
            'calories' => 'Калории',
            'protein' => 'Белки',
            'grease' => 'Жиры',
            'carbohydrate' => 'Углеводы',
            'image' => 'Картинка',
            'img' => 'Картинка',
            'description' => 'Описание',
        ];
    }

    public static function getWeekRation($week = null)
    {
        $arr = [
            1 => '1 неделя',
            2 => '2 неделя',
            3 => '3 неделя',
            4 => '4 неделя',
        ];
        return $week ? (isset($arr[$week]) ? $arr[$week] : null) : $arr;
    }

    public function getDaysRation($day = null)
    {
        $arr = [
            1 => '1 день',
            2 => '2 день',
            3 => '3 день',
            4 => '4 день',
            5 => '5 день',
            6 => '6 день',
            7 => '7 день',
        ];
        return $day ? (isset($arr[$day]) ? $arr[$day] : null) : $arr;
    }

    public static function getDayParts()
    {
        /**
         * @var $user User
         */
        $user = Yii::$app->user->identity;
        if ($user->role == User::ROLE_ADMIN) {
            if ($_GET['ration_type'] != 3 && $user->session_type == My::FOR_MAN || $_GET['ration_type'] == 7) {
                return [
                    1 => 'Завтрак',
                    2 => 'Перекус',
                    3 => 'Обед',
                    4 => 'Ужин',
                    5 => 'Перекус2',
                ];
            }
        }
        if ($user->role == User::ROLE_USER) {
            if ($user->groupMember->veg != 3 && $user->groupMember->type_program == My::FOR_MAN || $user->groupMember->veg == 7) {
                return [
                    1 => 'Завтрак',
                    2 => 'Перекус',
                    3 => 'Обед',
                    4 => 'Ужин',
                ];
            }
        }
        return [
            1 => 'Завтрак',
            2 => 'Перекус 1',
            3 => 'Обед',
            4 => 'Перекус 2',
            5 => 'Ужин',
        ];
    }

    public function calculateCalories($member)
    {
        /**
         * @var $member GroupMember
         */
        $gram = (655 + (9.6 * $member->weight) + (1.8 * $member->height) - (4.7 * $member->age)) * $member->physical_activity;
        switch ($member->ration_type) {
            case 1 :
                $gram *= 0.8;
                break;
        }
        if ($member->gv_info == 2) {
            $gram += 500;
        } elseif ($member->gv_info == 4) {
            $gram += 300;
        }
        $gram = $gram / 1320;

        $this->calories = $gram * $this->calories;
        $this->protein = $gram * $this->protein;
        $this->grease = $gram * $this->grease;
        $this->carbohydrate = $gram * $this->carbohydrate;
        $this->portion = $gram * $this->portion;
        if ($this->calories <= 0.5) {
            $this->calories = 0.5;
        } else {
            $this->calories = round($this->calories, 0, PHP_ROUND_HALF_EVEN);
        }
        if ($this->protein <= 0.5) {
            $this->protein = 0.5;
        } else {
            $this->protein = round($this->protein, 0, PHP_ROUND_HALF_EVEN);
        }
        if ($this->grease <= 0.5) {
            $this->grease = 0.5;
        } else {
            $this->grease = round($this->grease, 0, PHP_ROUND_HALF_EVEN);
        }
        if ($this->carbohydrate <= 0.5) {
            $this->carbohydrate = 0.5;
        } else {
            $this->carbohydrate = round($this->carbohydrate, 0, PHP_ROUND_HALF_EVEN);
        }
        if ($this->portion <= 0.5) {
            $this->portion = 0.5;
        } else {
            $this->portion = round($this->portion, 0, PHP_ROUND_HALF_EVEN);
        }
        if ($this->ingredients) {
            foreach ($this->ingredients as $ingredient) {
                $ingredient->value = $gram * $ingredient->value;
                if ($ingredient->value && $ingredient->value <= 0.5) {
                    $ingredient->value = 0.5;
                } elseif ($ingredient->value) {
                    $ingredient->value = round($ingredient->value, 0, PHP_ROUND_HALF_EVEN);
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['ration_complex_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentType()
    {
        return $this->hasMany(ContentType::className(), ['relation_id' => 'id'])->andWhere(['class_name' => 'app\models\RationComplex']);
    }

}

<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "markers".
 *
 * @property int $id
 * @property int $type
 * @property string $date_change
 * @property int|null $user_id
 * @property string|null $second_date
 */
class Markers extends ActiveRecord
{
    static $types = [
        'power' => 1,
        'cardio' => 2,
        'food' => 3,
        'ill' => 4,
        'care' => 5,
        'blood' => 6,
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'markers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'date_change'], 'required'],
            [['type', 'user_id'], 'integer'],
            [['second_date'], 'safe'],
            [['date_change'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'date_change' => 'Date Change',
            'user_id' => 'User ID',
            'second_date' => 'Second Date',
        ];
    }
}

<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "recipe".
 *
 * @property int $id
 * @property int $type
 * @property string $name
 * @property string|null $text
 * @property string|null $text_cut
 * @property string $image
 */
class Recipe extends MyAR
{

    public static function getTypes()
    {
        return [
            0 => 'завтрак',
            1 => 'обед',
            2 => 'ужин',
            3 => 'перекус',
            4 => 'десерты',
            5 => 'хлеб/лаваш',
            6 => 'вегетарианское',
            7 => 'смузи',
            8 => 'напитки',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recipe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['text', 'text_cut'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип рецепта',
            'name' => 'Название',
            'text' => 'Текст',
            'text_cut' => 'Краткое описание рецепта',
            'image' => 'Картинка',
        ];
    }
}

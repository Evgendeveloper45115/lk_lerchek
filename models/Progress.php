<?php

namespace app\models;

use app\helpers\My;
use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "progress".
 *
 * @property int $id
 * @property int $member_id
 * @property string $week
 * @property string $weight
 * @property string $bust
 * @property string $waist
 * @property string $hip
 * @property string|null $photo
 * @property string|null $stomach
 * @property string|null $butt
 * @property string|null $biceps
 * @property string|null $1_hip
 * @property string|null $caviar
 * @property string|null $photo_second
 * @property string|null $photo_third
 * @property date $date_create
 */
class Progress extends MyAR
{

    public $photo_form;
    public $photo_form_second;
    public $photo_form_third;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'progress';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_create'], 'safe'],
            [['member_id'], 'required'],
            [['member_id'], 'integer'],
            [['week', 'weight', 'bust', 'waist', 'hip', 'photo', 'stomach', 'butt', 'biceps', '1_hip', 'caviar', 'photo_second', 'photo_third'], 'string', 'max' => 255],
            [['photo_form', 'photo_form_second', 'photo_form_third'], 'required', 'message' => 'Загрузите 3 фотографии: спереди/сбоку/сзади', 'on' => 'week'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'week' => 'Неделя',
            'weight' => 'Вес (кг)',
            'bust' => 'Объем груди (см)',
            'waist' => 'Объем талии (см)',
            'hip' => 'Объем бедра (см)',
            'photo' => 'Photo',
            'stomach' => 'Stomach',
            'butt' => 'Объем ягодиц (см)',
            'biceps' => 'Biceps',
            '1_hip' => '1 Hip',
            'caviar' => 'Caviar',
            'photo_second' => 'Photo Second',
            'photo_third' => 'Photo Third',
        ];
    }
}

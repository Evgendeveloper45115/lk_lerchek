<?php

namespace app\models;

use app\helpers\My;
use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "payment_ler".
 *
 * @property int $id
 * @property string|null $date_payment
 * @property int|null $member_id
 * @property int|null $continue_program
 * @property int|null $cost
 * @property int $doplata
 * @property int $is_admin
 * @property int $shop_id
 *
 * @property GroupMember $member
 */
class PaymentLer extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_ler';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_payment', 'continue_program', 'shop_id'], 'safe'],
            [['member_id', 'cost', 'doplata', 'is_admin'], 'integer'],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => GroupMember::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_payment' => 'Date Payment',
            'member_id' => 'Member ID',
            'cost' => 'Cost',
            'doplata' => 'Doplata',
            'is_admin' => 'Is Admin',
        ];
    }

    /**
     * Gets query for [[Member]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(GroupMember::className(), ['id' => 'member_id']);
    }

    /**
     * @param $member_id
     */
    public function setAttributesAdmin($member_id)
    {
        $this->member_id = $member_id;
        $this->date_payment = date('Y-m-d H:i:s');
        $this->cost = Yii::$app->params['programs'][Yii::$app->user->identity->session_type];
        $this->is_admin = true;

    }
}

<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "care".
 *
 * @property int $id
 * @property string|null $description
 * @property int|null $type
 * @property string|null $image
 * @property string|null $video_link
 */
class Care extends MyAR
{

    static $types = [
        'Введение',
        'Скраб',
        'Обертывание',
        'Массаж сухой щёткой',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'care';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['type'], 'integer'],
            [['image', 'video_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Описание',
            'type' => 'Тип',
            'image' => 'Картинка',
            'video_link' => 'Ссылка Ютуб',
        ];
    }
}

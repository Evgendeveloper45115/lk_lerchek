<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "couple_vote".
 *
 * @property int $id
 * @property int|null $couple_id
 * @property int|null $member_id
 * @property int|null $for_member_id
 * @property int|null $is_send
 * @property CoupleMembers $coupleMember
 * @property GroupMember $member
 */
class CoupleVote extends \yii\db\ActiveRecord
{
    public $cnt;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'couple_vote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['couple_id', 'member_id', 'is_send', 'for_member_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'couple_id' => 'Couple ID',
            'member_id' => 'Member ID',
            'is_send' => 'Is Send',
            'for_member_id' => 'for_member_id',
        ];
    }

    public function getCoupleMember()
    {
        return $this->hasOne(CoupleMembers::className(), ['id' => 'couple_id']);
    }

    public function getMember()
    {
        return $this->hasOne(GroupMember::className(), ['id' => 'for_member_id']);
    }

}

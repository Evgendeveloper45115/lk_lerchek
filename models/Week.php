<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "week".
 *
 * @property int $id
 * @property int|null $training_id
 * @property int|null $number
 * @property string|null $text
 *
 * @property Training $training
 * @property WeekTraining[] $weekTrainings
 */
class Week extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'week';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['training_id', 'number'], 'integer'],
            [['text'], 'string'],
            [['training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['training_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'training_id' => 'Training ID',
            'number' => 'Number',
            'text' => 'Text',
        ];
    }

    /**
     * Gets query for [[Training]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'training_id']);
    }

    /**
     * Gets query for [[WeekTrainings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWeekTrainings()
    {
        return $this->hasMany(WeekTraining::className(), ['week_id' => 'id']);
    }
}

<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "report_has_user".
 *
 * @property int $id
 * @property int|null $report_id
 * @property string|null $user_text
 * @property string|null $admin_text
 * @property int|null $member_id
 * @property int|null $status
 * @property int $user_status
 * @property string|null $date_added
 * @property GroupMember $groupMember
 * @property Report $report
 */
class ReportHasUser extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_has_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_id', 'member_id', 'status', 'user_status'], 'integer'],
            [['user_text', 'admin_text'], 'string'],
            [['date_added'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_id' => 'Отчет',
            'user_text' => 'Отчет клиента',
            'admin_text' => 'Ответ куратора',
            'member_id' => 'Клиент',
            'status' => 'Статус',
            'user_status' => 'User Status',
            'date_added' => 'Дата создания',
        ];
    }

    static function getReportCount()
    {
        return self::find()->getContent()
            ->leftJoin('group_member', 'group_member.id = report_has_user.member_id')
            ->leftJoin('payment_ler', 'payment_ler.member_id = group_member.id')
            ->where(['=', 'status', 0])
            ->andWhere('payment_ler.continue_program = 1')
            ->count();

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupMember()
    {
        return $this->hasOne(GroupMember::className(), ['id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(Report::className(), ['id' => 'report_id']);
    }

}

<?php

namespace app\models\forms;

use app\components\MyUrlManager;
use app\helpers\My;
use app\models\ContentType;
use app\models\Group;
use app\models\GroupMember;
use app\models\User;
use yii\base\Model;
use yii\db\Expression;
use yii\helpers\VarDumper;

class SignupForm extends Model
{
    public $last_name;
    public $first_name;
    public $patronymic;
    public $email;
    public $phone;
    public $age;
    public $height;
    public $weight;
    public $bust;
    public $waist;
    public $hip;
    public $ration_type;
    public $gv_info;
    public $food_last_3days;
    public $unwanted_food;
    public $training_type;
    public $veg;
    public $smoke;
    public $alcohol;
    public $diabetes;
    public $pregnant;
    public $health_constraints;
    public $extra_info;
    public $experience;
    public $password;
    public $token;
    public $type_program;
    public $physical_activity;
    public $training_status;
    public $city;
    public $family;
    public $childs;
    public $telegram;
    public $parthner;
    public $vk_nick;


    public static $physical_activities = [
        '1.38' => 'Активный - с тренировками',
        '1.2' => 'Сидячий - участвую без тренировок',
    ];
    public static $physical_activities_man = [
        '1.2' => 'Малоподвижные люди',
        '1.375' => 'Люди с низкой активностью (1-3 тренировки в неделю)',
        '1.55' => 'для умеренно активных людей (тренировки 3-5 раз в неделю)',
    ];

    public function rules()
    {
        return [
            ['family', 'required', 'message' => 'Укажите семейное положение.'],
            ['childs', 'required', 'message' => 'Укажите сколько у вас детей.'],
            ['telegram', 'safe'],
            ['vk_nick', 'required', 'message' => 'Укажите инстаграм.'],
            ['parthner', 'required', 'message' => 'Укажите партнёра'],
            ['last_name', 'required', 'message' => 'Введите свою фамилию.'],
            ['first_name', 'required', 'message' => 'Введите свое имя.'],
            ['patronymic', 'safe', 'message' => 'Введите свое отчество.'],

            [['last_name', 'first_name', 'patronymic', 'phone', 'age', 'height', 'weight', 'bust', 'waist', 'hip'], 'trim'],
            ['email', 'required', 'message' => 'Введите адрес электронной почты.'],
            [['email'], 'validateUniqueEmail', 'message' => 'Пользователь с таким email уже зарегистрирован.'],

            // ['phone', 'required', 'message' => 'Введите номер своего телефона.'],
            //['phone', 'integer', 'message' => 'Введите номер своего телефона.'],

            ['age', 'required', 'message' => 'Укажите свой возраст.'],
            ['city', 'required', 'message' => 'Введите свой город'],
            ['height', 'required', 'message' => 'Укажите свой рост.'],
            ['weight', 'required', 'message' => 'Укажите свой вес.'],
//            ['bust', 'required', 'message' => 'заполните поле.'],
//            ['waist', 'required', 'message' => 'заполните поле.'],
//            ['hip', 'required', 'message' => 'заполните поле.'],

            [['age', 'height', 'weight', 'bust', 'waist', 'hip'], 'integer', 'message' => 'Введите число'],

            ['ration_type', 'required', 'message' => 'Уточните, для какой цели нужен рацион'],
            ['veg', 'required', 'message' => 'Уточните, какой рацион вам нужен.'],
            ['gv_info', 'required', 'message' => 'Уточните, это важно.'],
            ['food_last_3days', 'required', 'message' => 'заполните поле.'],
            ['training_type', 'required', 'message' => 'заполните поле.'],

            [['physical_activity'], 'required', 'message' => 'заполните поле.'],
            ['smoke', 'required', 'message' => 'заполните поле.'],
            ['alcohol', 'required', 'message' => 'заполните поле.'],
            ['diabetes', 'safe'],
            ['pregnant', 'required', 'message' => 'заполните поле.'],

            [['experience', 'extra_info', 'type_program', 'physical_activity', 'health_constraints'], 'safe'],

            ['password', 'required', 'message' => 'Введите пароль'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['landing'] = [
            'experience',
            'extra_info',
            'type_program',
            'health_constraints',
            'pregnant',
            'diabetes',
            'alcohol',
            'smoke',
            'veg',
            'training_type',
            'unwanted_food',
            'food_last_3days',
            'gv_info',
            'ration_type',
            'last_name',
            'first_name',
            'patronymic',
            'phone',
            'age',
            'height',
            'weight',
            'bust',
            'waist',
            'hip',
            'password',
            'physical_activity',
            'type_program',
            'city',
            'family',
            'childs',
            'telegram',
            'parthner',
            'vk_nick',
        ];//Scenario Values Only Accepted
        return $scenarios;
    }

    public function attributeLabels()
    {
        return [
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'patronymic' => 'Отчество',
            'email' => 'E-mail (ваш логин)',
            'phone' => 'Номер телефона:',
            'age' => 'Возраст',
            'height' => 'Рост (в см)',
            'weight' => 'Вес (кг)',
            'bust' => 'Объем груди (в см)',
            'waist' => 'Объем талии (в см)',
            'hip' => 'Объем ягодиц (в см)',
            'ration_type' => 'Для какой цели нужен рацион?',
            'gv_info' => 'Вы сейчас кормите грудью?',
            'food_last_3days' => 'Что вы ели последние три дня? Перечислите продукты',
            'unwanted_food' => 'Какие продукты вы не едите? Если что-то едите но редко, то укажите, сколько раз в неделю.',
            'training_type' => 'Программу какого типа тренировок вы хотели бы получить?',
            'veg' => 'Какой рацион вам нужен?',
            'smoke' => 'Вы курите?',
            'alcohol' => 'Как часто вы употребляете алкоголь?',
            'diabetes' => 'Есть ли у вас сахарный диабет?',
            'pregnant' => 'Беременность',
            'health_constraints' => 'Есть ли у вас ограничения по здоровью для физических нагрузок?',
            'extra_info' => 'Есть ли что-то, что вам нужно сообщить кураторам до начала занятий?',
            'experience' => 'Есть ли у вас опыт тренировок в тренажерном зале? Если да, опишите его.',
            'password' => 'Пароль',
            'type_program' => 'Тип программы',
            'physical_activity' => 'Дневная активность',
            'city' => 'Город проживания',
            'family' => 'Семейное положение',
            'childs' => 'Количество детей',
            'telegram' => 'Имя пользователя в телеграм',
            'parthner' => 'Если вы участвуете парой, укажите в поле ниже имейл вашего партнёра',
            'vk_nick' => 'Ник в инстаграм',

        ];
    }

    public function save()
    {

        /**
         * @var $user User
         * @var $groupPrev Group
         */
        $user = User::find()->where(['email' => ($this->email ? trim($this->email) : trim($_GET['email']))])->one();
        if (empty($user)) {
            $user = new User();
        }
        $user->groupMemberGuest->last_name = $this->last_name;
        $user->groupMemberGuest->first_name = $this->first_name;
        $user->groupMemberGuest->patronymic = $this->patronymic;
        if ($this->email) {
            $user->email = trim($this->email);
        } else {
            $user->email = trim($_GET['email']);
        }
        $user->groupMemberGuest->password = $this->password;
        $user->role = User::ROLE_USER;
        $user->datetime_signup = date('Y-m-d H:i:s');
        $user->datetime_last_login = new Expression('now()');
        $user->session_type = $_GET['type'];
        $user->save(false);
        $group_member = $user->groupMemberGuest;
        if ($group_member === null) {
            $group_member = new GroupMember();
        }
        $group_member->user_id = $user->id;
        $group_member->datetime_signup = date('Y-m-d H:i:s');
        // $group_member->phone = $this->phone;
        $group_member->age = $this->age;
        $group_member->height = $this->height;
        $group_member->weight = $this->weight;
        $group_member->training_type = $this->training_type;
        $group_member->physical_activity = $this->physical_activity;

        if (!empty($this->bust)) {
            $group_member->bust = $this->bust;
        }
        if (!empty($this->waist)) {
            $group_member->waist = $this->waist;
        }
        if (!empty($this->hip)) {
            $group_member->hip = $this->hip;
        }
        $group_member->ration_type = $this->ration_type;
        $group_member->gv_info = $this->gv_info;
        $group_member->food_last_3days = $this->food_last_3days;
        $group_member->health_constraints = null;
        $group_member->extra_info = null;
        $group_member->type_program = $user->session_type;
        $group_member->veg = $this->veg;
        $group_member->smoke = $this->smoke;
        $group_member->alcohol = $this->alcohol;
        $group_member->diabetes = 1;
        $group_member->pregnant = $this->pregnant;
        $group_member->city = $this->city;
        $group_member->family = $this->family;
        $group_member->childs = $this->childs;
        $group_member->telegram = $this->telegram;
        $group_member->parthner = $this->parthner;
        $group_member->vk_nick = $this->vk_nick;

        if ($group_member->type_program == My::FOR_GIRL_22_03_2021 && $group_member->group_id == null || $group_member->type_program == My::FOR_MAN_22_03_2021 && $group_member->group_id == null) {
            $num = 0;
            $groupPrevs = Group::find()->getContent()->where(['!=', 'num', 'test'])->andWhere(['!=', 'num', 'продолжение'])->andWhere(['!=', 'num', 'заморозка'])->andWhere(['!=', 'num', 'разморозка'])->orderBy('id DESC')->all();
            if (!empty($groupPrevs)) {
                foreach ($groupPrevs as $k => $groupPrev) {
                    if ($k == 0) {
                        $num = $groupPrev->num;
                    }
                    if (count($groupPrev->groupMembers) < 500) {
                        $group_member->group_id = $groupPrev->id;
                    } else {
                        continue;
                    }
                }
            }
            if ($group_member->group_id == null) {
                $group = new  Group();
                $group->num = $num + 1;
                $group->datetime_start = date('Y-m-d H:i:s', strtotime('2021-03-22'));
                $group->datetime_create = date('Y-m-d H:i:s');
                $group->datetime_end = date('Y-m-d H:i:s', strtotime('2021-04-25'));
                $group->date_continue = date('Y-m-d H:i:s', strtotime('2021-04-25'));
                $group->save(false);
                $group_member->group_id = $group->id;
                $contentType = new ContentType();
                $contentType->class_name = Group::class;
                $contentType->relation_id = $group->id;
                $contentType->type_program = $group_member->type_program;
                $contentType->save(false);
                $text = 'Создана новая группа ' . $group->num . ' Программа ' . My::$type_program_rus[$group_member->type_program];
                $smsaero_api = new \app\helpers\SmsaeroApiV2('admin@lerchek.ru', '2r0dxVxGZCuxg1SliJwk3fBmcfz7', 'Lerchek'); // api_key из личного кабинета
                $smsaero_api->send('+79096591390', $text, 'DIRECT');
            }
            if ($group_member->group_id == null) {
                $group = Group::find()->where(['num' => 'баганулось-' . date('Y-m-d')])->one();
                if ($group === null) {
                    $group = new Group();
                    $group->num = 'баганулось-' . date('Y-m-d');
                    $group->datetime_start = date('Y-m-d H:i:s', strtotime('2021-03-22'));
                    $group->datetime_create = date('Y-m-d H:i:s');
                    $group->datetime_end = date('Y-m-d H:i:s', strtotime('2021-04-25'));
                    $group->date_continue = date('Y-m-d H:i:s', strtotime('2021-04-25'));
                    $group->save(false);
                    $contentType = new ContentType();
                    $contentType->class_name = Group::class;
                    $contentType->relation_id = $group->id;
                    $contentType->type_program = $group_member->type_program;
                    $contentType->save(false);

                    $text = 'Что то пошло не так при создании анкеты клиента' . My::$type_program_rus[$group_member->type_program] . " Клиент:" . $group_member->user->email;
                    $smsaero_api = new \app\helpers\SmsaeroApiV2('admin@lerchek.ru', '2r0dxVxGZCuxg1SliJwk3fBmcfz7', 'Lerchek'); // api_key из личного кабинета
                    $smsaero_api->send('+79096591390', $text, 'DIRECT');

                }
                $group_member->group_id = $group->id;
            }
        }
        My::log('signup_post', json_encode($_POST));
        My::log('signup_post', json_encode($group_member->attributes));

        if ($group_member->save(false)) {
            $user->login();
            return true;
        } else {
            exit;
        }

    }

    function remove_emoji($string)
    {

        // Match Emoticons
        $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clear_string = preg_replace($regex_emoticons, '', $string);

        // Match Miscellaneous Symbols and Pictographs
        $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clear_string = preg_replace($regex_symbols, '', $clear_string);

        // Match Transport And Map Symbols
        $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clear_string = preg_replace($regex_transport, '', $clear_string);

        // Match Miscellaneous Symbols
        $regex_misc = '/[\x{2600}-\x{26FF}]/u';
        $clear_string = preg_replace($regex_misc, '', $clear_string);

        // Match Dingbats
        $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
        $clear_string = preg_replace($regex_dingbats, '', $clear_string);

        return $clear_string;
    }

    public function validateUniqueEmail($attribute, $params)
    {
        $user = User::findByEmail($this->$attribute);
        if (!empty($user) && empty($this->token)) {
            $this->addError($attribute, 'Пользователь с таким email уже существует.');
        }
    }
}

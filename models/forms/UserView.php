<?php

namespace app\models\forms;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\VarDumper;

/**
 * LoginForm is the model behind the login form.
 */
class UserView extends User
{
    public $email_view;
    public $phone_view;
    public $first_name_view;
    public $last_name_view;
    public $patronymic_view;
    public $avatar_view;
    public $group_view;
    public $photo_one;
    public $photo_second;
    public $photo_third;
    public $photo_one_last;
    public $photo_second_last;
    public $photo_third_last;

    /* public function __construct()
     {
     }*/
    public function __construct($config = [])
    {
        $user = User::findOne($_GET['id']);
        $this->email_view = $user->email;
        $this->phone_view = $user->groupMember->phone;
        $this->first_name_view = $user->groupMember->first_name;
        $this->last_name_view = $user->groupMember->last_name;
        $this->patronymic_view = $user->groupMember->patronymic;
        $this->avatar_view = $user->groupMember->avatar;
        $this->group_view = $user->groupMember->group->num;

        parent::__construct($config);
    }

    public function attributeLabels()
    {

        return [
            'email_view' => 'Email',
            'phone_view' => 'Номер телефона',
            'first_name_view' => 'Имя',
            'patronymic_view' => 'Отчество',
            'last_name_view' => 'Фамилия',
            'avatar_view' => 'фото',
        ]; // TODO: Change the autogenerated stub
    }
}

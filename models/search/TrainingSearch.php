<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Training;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * TrainingSearch represents the model behind the search form of `app\models\Training`.
 */
class TrainingSearch extends Training
{
    public $number;
    public $text;
    public $trainingNumber;
    public $sort_index;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'body_part', 'sort_index', 'trainingNumber', 'type', 'video', 'number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     * @throws \Throwable
     */
    public function search($params)
    {
        $query = Training::find()->joinWith(['weeks' => function (ActiveQuery $query) {
            $query->joinWith(['weekTrainings']);
        }]);
        $query->getContent();
        if ($params['sort'] === null) {
            $query->orderBy('id DESC');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $dataProvider->sort->attributes['number'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['week.number' => SORT_ASC],
            'desc' => ['week.number' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['trainingNumber'] = [
            'asc' => ['week_training.training_number' => SORT_ASC],
            'desc' => ['week_training.training_number' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['sort_index'] = [
            'asc' => ['week_training.sort_index' => SORT_ASC],
            'desc' => ['week_training.sort_index' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'video' => $this->video,
            'body_part' => $this->body_part,
            'week.number' => $this->number,
            'week_training.training_number' => $this->trainingNumber,
            'week_training.sort_index' => $this->sort_index,
        ]);

        $query->andFilterWhere(['like', 'name', $_GET['ExerciseSearch']['name']]);

        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Questions;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * QuestionsSearch represents the model behind the search form of `app\models\Questions`.
 */
class CuratorQuestionsSearch extends Questions
{
    public $count;
    public $group_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'view'], 'integer'],
            [['question', 'date_added', 'answer', 'image', 'group_id', 'member_id', 'curator_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions::find()->joinWith(['userCurator']);
        $query->joinWith(['groupMember as gm' => function (ActiveQuery $query) use ($params) {
            $query->andWhere(['type_program' => \Yii::$app->user->getIdentity()->session_type]);
            $query->joinWith(['user as u' => function (ActiveQuery $query) use ($params) {
                if (isset ($params['CuratorQuestionsSearch']['member_id']) && $params['CuratorQuestionsSearch']['member_id'] != '') {
                    $query->andFilterWhere(['LIKE', 'u.email', $params['CuratorQuestionsSearch']['member_id']]);
                }
            }]);
        }]);

        if ($params['sort'] === null) {
            $query->orderBy('id ASC');
        }
        $query->andWhere(['status' => 1]);
        $query->andWhere(['not', ['curator_id' => null]]);
        // add conditions that should always apply here
        // grid filtering conditions
        $query->andFilterWhere(['=', 'user.id', $params['CuratorQuestionsSearch']['curator_id']]);

        if (isset ($params['CuratorQuestionsSearch']['date_added']) && $params['CuratorQuestionsSearch']['date_added'] != '') { //you dont need the if function if yourse sure you have a not null date
            $date_explode = explode(" - ", $params['CuratorQuestionsSearch']['date_added']);
            $date1 = trim($date_explode[0]);
            $date2 = trim($date_explode[1]);
            // VarDumper::dump(date('Y-m-d', strtotime($date1)), 11, 1);
            $query->andFilterWhere(['between', 'date_added', date('Y-m-d', strtotime($date1)), date('Y-m-d', strtotime('+1 day', strtotime($date2)))]);
        }
        if (isset ($params['CuratorQuestionsSearch']['question']) && $params['CuratorQuestionsSearch']['question'] != '') {
            $query->andFilterWhere(['LIKE', 'question', $params['CuratorQuestionsSearch']['question']]);
        }

        $count = $query->count();
        if ($params['CuratorQuestionsSearch']['date_added'] == '') {
            $query->groupBy('curator_id');
            if (isset ($params['CuratorQuestionsSearch']['member_id']) && $params['CuratorQuestionsSearch']['member_id'] != '') {
                $query->groupBy('member_id');
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}

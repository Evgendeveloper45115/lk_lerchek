<?php

namespace app\models\search;

use app\models\CoupleMembers;
use app\models\CoupleVote;
use app\models\Vote;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Group;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * GroupSearch represents the model behind the search form of `app\models\Group`.
 */
class CoupleMembersSearch extends CoupleMembers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['members_id', 'group_id'], 'safe'],
            [['counter'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoupleMembers::find();
        $query->getContent();
        if ($params['sort'] === null) {
            $query->orderBy('counter DESC');
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use app\models\Report;
use app\models\ReportHasUser;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Questions;
use yii\helpers\VarDumper;

/**
 * QuestionsSearch represents the model behind the search form of `app\models\Questions`.
 */
class ReportHasUserSearch extends ReportHasUser
{
    public $count;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'member_id', 'status'], 'integer'],
            [['question', 'date_added', 'answer', 'image', 'user_text'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportHasUser::find();
        $query->leftJoin('group_member', 'group_member.id = report_has_user.member_id');
        $query->leftJoin('payment_ler', 'payment_ler.member_id = group_member.id');
        $query->orderBy('date_added ASC');
        $query->andWhere(['status' => 0]);
        $query->andWhere('payment_ler.continue_program = 1');
        $query->getContent();
        // add conditions that should always apply here

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'member_id' => $this->member_id,
            'status' => $this->status,
            'date_added' => $this->date_added,
            'user_text' => $this->user_text,
        ]);

        $query->groupBy('member_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}

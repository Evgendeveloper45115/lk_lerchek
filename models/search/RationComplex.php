<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RationComplex as RationComplexModel;

/**
 * RationComplex represents the model behind the search form of `app\models\RationComplex`.
 */
class RationComplex extends RationComplexModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ration_type', 'week', 'day', 'day_part'], 'integer'],
            [['name', 'image', 'description'], 'safe'],
            [['portion', 'calories', 'protein', 'grease', 'carbohydrate'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RationComplexModel::find();
        $query->andWhere(['type_program' => \Yii::$app->getUser()->getIdentity()->session_type]);
        if ($params['sort'] === null) {
            $query->orderBy('id DESC');
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ration_type' => $this->ration_type,
            'week' => $this->week,
            'day' => $this->day,
            'day_part' => $this->day_part,
            'portion' => $this->portion,
            'calories' => $this->calories,
            'protein' => $this->protein,
            'grease' => $this->grease,
            'carbohydrate' => $this->carbohydrate,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}

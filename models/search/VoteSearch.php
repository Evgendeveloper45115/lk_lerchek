<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vote;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * VoteSearch represents the model behind the search form of `app\models\Vote`.
 */
class VoteSearch extends Vote
{
    /**
     * {@inheritdoc}
     */
    public $rating;

    public function rules()
    {
        return [
            [['id', 'vote_id', 'member_id', 'cat', 'rating'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vote::find()->joinWith(['groupMemberVote' => function (ActiveQuery $query) {
            $query->where(['type_program' => \Yii::$app->user->identity->session_type]);
            $query->joinWith('user');
        }]);

        $query->groupBy(['vote_id']);
        $query->orderBy('COUNT(vote_id) DESC');
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cat' => $this->cat,
        ]);
        $query->filterWhere(['like', 'email', $this->vote_id]);
        $query->filterWhere(['=', 'group_member.rating', $this->rating]);

        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use app\helpers\My;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    public $first_name;
    public $last_name;
    public $telegram;
    public $patronymic;
    public $avatar;
    public $rating;
    public $visible_curator;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role', 'visible_curator'], 'integer'],
            [['email', 'datetime_signup', 'datetime_signup', 'datetime_last_login', 'first_name', 'last_name', 'patronymic', 'avatar', 'complete', 'rating', 'read', 'telegram'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        if (\Yii::$app->controller->action->id == 'view-users') {
            $query->getMembersGroup();
            $query->andWhere(['role' => User::ROLE_USER]);
        } else {
            $query->getNewMembers();
            if ($params['sort'] === null) {
                $query->orderBy('group_member.datetime_signup DESC');
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->sort->attributes['visible_curator'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['group_member.visible_curator' => SORT_ASC],
            'desc' => ['group_member.visible_curator' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['first_name'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['group_member.first_name' => SORT_ASC],
            'desc' => ['group_member.first_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['last_name'] = [
            'asc' => ['group_member.last_name' => SORT_ASC],
            'desc' => ['group_member.last_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['telegram'] = [
            'asc' => ['group_member.telegram' => SORT_ASC],
            'desc' => ['group_member.telegram' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['complete'] = [
            'asc' => ['group_member.password' => SORT_ASC],
            'desc' => ['group_member.password' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['datetime_signup'] = [
            'asc' => ['datetime_signup' => SORT_ASC],
            'desc' => ['datetime_signup' => SORT_DESC],
        ];

        $query->andFilterWhere([
            '<=', 'group_member.datetime_signup', $this->datetime_signup
        ]);

        if ($this->complete == 1) {
            $query->andWhere(['password' => null]);
        }
        if ($this->complete == 2) {
            $query->andWhere(['is not', 'password', null])->andWhere(['is not', 'physical_activity', null]);
        }
        if ($this->rating) {
            $query->andFilterWhere(['=', 'rating', $this->rating]);
        }
        if ($this->visible_curator || $this->visible_curator == 0) {
            $query->andFilterWhere(['=', 'group_member.visible_curator', $this->visible_curator]);
        }
        $query->filterWhere(['like', 'group_member.first_name', $this->first_name]);
        $query->filterWhere(['like', 'group_member.last_name', $this->last_name]);
        $query->filterWhere(['like', 'email', $this->email]);
        $query->filterWhere(['like', 'group_member.telegram', $this->telegram]);

        return $dataProvider;
    }

    public function searchFinal($params)
    {
        $query = User::find()->joinWith(['groupMember' => function (ActiveQuery $query) {
            $query->andWhere(['rating' => My::FINAL_VOTE]);
        }]);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->sort->attributes['first_name'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['group_member.first_name' => SORT_ASC],
            'desc' => ['group_member.first_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['last_name'] = [
            'asc' => ['group_member.last_name' => SORT_ASC],
            'desc' => ['group_member.last_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['telegram'] = [
            'asc' => ['group_member.telegram' => SORT_ASC],
            'desc' => ['group_member.telegram' => SORT_DESC],
        ];

        $query->filterWhere(['like', 'group_member.first_name', $this->first_name]);
        $query->filterWhere(['like', 'group_member.last_name', $this->last_name]);
        $query->filterWhere(['like', 'email', $this->email]);
        $query->filterWhere(['like', 'group_member.telegram', $this->telegram]);

        return $dataProvider;
    }
}

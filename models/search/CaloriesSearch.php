<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calories;
use yii\helpers\VarDumper;

/**
 * CaloriesSearch represents the model behind the search form of `app\models\Calories`.
 */
class CaloriesSearch extends Calories
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cat_id'], 'integer'],
            [['protein', 'grease', 'carbohydrate', 'sugar', 'image', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calories::find()->joinWith('cat');
        $query->getContent();
        if ($params['sort'] === null) {
            $query->orderBy('id DESC');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cat_id' => $this->cat_id,
        ]);

        $query->andFilterWhere(['like', 'protein', $this->protein])
            ->andFilterWhere(['like', 'grease', $this->grease])
            ->andFilterWhere(['like', 'carbohydrate', $this->carbohydrate])
            ->andFilterWhere(['like', 'sugar', $this->sugar])
            ->andFilterWhere(['like', 'calories.name', $this->name]);

        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Questions;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * QuestionsSearch represents the model behind the search form of `app\models\Questions`.
 */
class QuestionsSearch extends Questions
{
    public $count;
    public $group_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'view'], 'integer'],
            [['question', 'date_added', 'answer', 'image', 'group_id', 'member_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions::find()->joinWith(['groupMember' => function (ActiveQuery $query) {
            $query->andWhere(['type_program' => \Yii::$app->user->getIdentity()->session_type]);
            $query->joinWith(['user']);
        }]);
        if ($params['sort'] === null) {
            $query->orderBy('date_added ASC');
        }
        $query->andWhere(['status' => 0]);
        // add conditions that should always apply here
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'view' => $this->view,
        ]);
        if ($params['QuestionsSearch']['date_added'] != '') {
            $query->andFilterWhere(['LIKE', 'date_added', date('Y-m-d', strtotime($params['QuestionsSearch']['date_added'])),]);
        }
        $query->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'answer', $this->answer])
            ->andFilterWhere(['in', 'group_member.group_id', $params['QuestionsSearch']['group_id']])
            ->andFilterWhere(['LIKE', 'email', $params['QuestionsSearch']['member_id']])
            ->andFilterWhere(['like', 'image', $this->image]);
        $count = $query->count();
        $query->groupBy('member_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}

<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "every_day".
 *
 * @property int $id
 * @property string|null $text
 * @property int|null $day
 */
class EveryDay extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'every_day';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['day'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Описание',
            'day' => 'День',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "popups".
 *
 * @property int $id
 * @property int|null $member_id
 * @property int|null $curator_id
 * @property int|null $popup_id
 * @property int|null $status
 * @property string $description
 * @property GroupMember $groupMemberProgress
 * @property User $curator
 */
class Popups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'popups';
    }

    public static $statuses = [
        0 => 'Отправлено',
        1 => 'Прочитано',
        3 => 'Исправлено(ждёт ответ)',
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'popup_id', 'status', 'curator_id'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'popup_id' => 'Popup ID',
            'status' => 'Status',
            'description' => 'Описание проблемы',
            'curator_id' => 'Куратор',
        ];
    }

    public function getGroupMemberProgress()
    {
        return $this->hasOne(GroupMember::class, ['member_id' => 'id'])->andWhere(['popup_id' => 1])->andWhere(['status' => 0]);
    }

    public function getCurator()
    {
        return $this->hasOne(User::class, ['id' => 'curator_id']);
    }

    public function getGroupMemberProgressStart()
    {
        return $this->hasOne(GroupMember::class, ['member_id' => 'id'])->andWhere(['popup_id' => 2])->andWhere(['status' => 0]);
    }

}

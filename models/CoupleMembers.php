<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "couple_members".
 *
 * @property int $id
 * @property string $members_id
 * @property int|null $counter
 * @property int|null $group_id
 * @property CoupleVote $coupleVote
 * @property Group $group
 * @property CoupleVote $coupleVoteNotSend
 */
class CoupleMembers extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'couple_members';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id'], 'number'],
            [['members_id'], 'string'],
            [['counter'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'members_id' => 'Members ID',
            'counter' => 'Counter',
            'group_id' => 'Группа',
        ];
    }

    public function getCoupleVote()
    {
        return $this->hasOne(CoupleVote::className(), ['couple_id' => 'id'])->andWhere(['member_id' => Yii::$app->user->identity->groupMember->id, 'is_send' => true]);
    }

    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    public function getCoupleVotes()
    {
        return $this->hasMany(CoupleVote::className(), ['couple_id' => 'id']);
    }

    public function getCoupleVoteNotSend()
    {
        return $this->hasOne(CoupleVote::className(), ['couple_id' => 'id'])->andWhere(['member_id' => Yii::$app->user->identity->groupMember->id, 'is_send' => false]);
    }

}

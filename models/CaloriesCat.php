<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "calories_cat".
 *
 * @property int $id
 * @property string|null $name
 *
 * @property Calories[] $calories
 */
class CaloriesCat extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calories_cat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[Calories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalories()
    {
        return $this->hasMany(Calories::className(), ['cat_id' => 'id']);
    }
}

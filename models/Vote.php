<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vote".
 *
 * @property int $id
 * @property int|null $vote_id
 * @property int|null $member_id
 * @property int|null $cat
 * @property GroupMember groupMemberVote
 * @property GroupMember groupMember
 */
class Vote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vote_id', 'member_id', 'cat'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vote_id' => 'За кого',
            'member_id' => 'Кто',
            'cat' => 'Категория',
        ];
    }

    public function getGroupMemberVote()
    {
        return $this->hasOne(GroupMember::className(), ['id' => 'vote_id']);
    }

    public function getGroupMember()
    {
        return $this->hasOne(GroupMember::className(), ['id' => 'member_id']);
    }
}

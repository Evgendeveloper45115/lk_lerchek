<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property int $type
 * @property string $title
 * @property string $content
 */
class Faq extends MyAR
{
    const TYPE_FOOD = 1;
    const TYPE_TRAINING = 2;
    const TYPE_PROGRESS = 3;
    const TYPE_CARE = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['title', 'content', 'type'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'title' => 'Заголовок',
            'content' => 'Описание',
        ];
    }

    public static function getFaqType()
    {
        return [
            1 => 'FAQ питание',
            2 => 'FAQ тренировки',
            3 => 'FAQ прогресс',
            4 => 'FAQ уход',
        ];
    }

}

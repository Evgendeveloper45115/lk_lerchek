<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "detox_day".
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $image
 */
class DetoxDay extends MyAR
{
    public $img;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detox_day';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'image' => 'Image',
        ];
    }
}

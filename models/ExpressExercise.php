<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "express_exercise".
 *
 * @property int $id
 * @property string $name
 * @property string|null $text
 * @property string $video
 * @property int|null $type
 */
class ExpressExercise extends MyAR
{

    static $types = [
        'vacuum',
        'planck',
        'correction'
    ];
    static $typesRus = [
        'Упражнение вакуум',
        'Планка',
        'Коррекция осанки'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'express_exercise';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['type'], 'integer'],
            [['name', 'video'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Описание',
            'video' => 'Ссылка ютуб',
            'type' => 'Тип',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content_type".
 *
 * @property int $id
 * @property int|null $type_program
 * @property string|null $class_name
 * @property int|null $relation_id
 */
class ContentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type_program', 'relation_id'], 'integer'],
            [['class_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_program' => 'Type Program',
            'class_name' => 'Class Name',
            'relation_id' => 'Relation ID',
        ];
    }
}

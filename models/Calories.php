<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "calories".
 *
 * @property int $id
 * @property string|null $protein
 * @property string|null $grease
 * @property string|null $carbohydrate
 * @property string|null $sugar
 * @property string|null $image
 * @property int|null $cat_id
 * @property string|null $name
 *
 * @property CaloriesCat $cat
 */
class Calories extends MyAR
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id'], 'integer'],
            [['protein', 'grease', 'carbohydrate', 'sugar', 'image'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 244],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => CaloriesCat::className(), 'targetAttribute' => ['cat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'protein' => 'Белок',
            'grease' => 'Жиры',
            'carbohydrate' => 'Углеводы',
            'sugar' => 'Сахар',
            'image' => 'Картинка',
            'cat_id' => 'Категория',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[Cat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(CaloriesCat::className(), ['id' => 'cat_id']);
    }

    public static function getTypesCalories()
    {
        $caloriesCat = CaloriesCat::find()->getContent()->all();
        return ArrayHelper::map($caloriesCat, 'id', 'name');
    }

}

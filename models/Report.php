<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "report".
 *
 * @property int $id
 * @property string|null $title_adm
 * @property string|null $text_adm
 * @property int $week
 */
class Report extends MyAR
{

    public $rhu_user_text;
    public $rhu_admin_text;
    public $rhu_status;
    public $rhu_member_id;
    public $rhu_date_added;
    public $rhu_food_mark;
    public $rhu_training_mark;
    public $rhu_motivation_mark;
    public $rhu_cardio;
    public $group_status = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_adm'], 'string'],
            [['week'], 'required'],
            [['week'], 'integer'],
            [['title_adm'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_adm' => 'Название',
            'text_adm' => 'Описания задания',
            'week' => 'Неделя',
        ];
    }
}

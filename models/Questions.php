<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property int $member_id
 * @property int $curator_id
 * @property string|null $question
 * @property int $status
 * @property string|null $date_added
 * @property string|null $answer
 * @property int|null $view
 * @property string|null $image
 * @property GroupMember $groupMember
 * @property User $userCurator
 */
class Questions extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id', 'status', 'view'], 'integer'],
            [['question', 'answer'], 'string'],
            [['date_added', 'curator_id'], 'safe'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Клиент',
            'curator_id' => 'Куратор',
            'question' => 'Вопрос',
            'status' => 'Статус',
            'date_added' => 'Дата вопроса',
            'answer' => 'Ответ',
            'view' => 'Чек',
            'image' => 'Картинка',
        ];
    }

    public function getGroupMember()
    {
        return $this->hasOne(GroupMember::className(), ['id' => 'member_id']);
    }

    public function getUserCurator()
    {
        return $this->hasOne(User::className(), ['id' => 'curator_id']);
    }

}

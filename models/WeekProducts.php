<?php

namespace app\models;

use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "week_products".
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $image
 * @property int|null $week
 * @property int|null $veg
 */
class WeekProducts extends MyAR
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'week_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['week', 'veg'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Описание',
            'image' => 'Картинка',
            'week' => 'На какой неделе',
            'veg' => 'Тип питания',
        ];
    }
}

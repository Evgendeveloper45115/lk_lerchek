<?php

namespace app\models;

use app\components\MyUrlManager;
use app\helpers\My;
use app\models\base\MyAR;
use Yii;

/**
 * This is the model class for table "training".
 *
 * @property int $id
 * @property int|null $type
 * @property string|null $name
 * @property string|null $video
 * @property string|null $body_part
 *
 * @property Week[] $weeks
 */
class Training extends MyAR
{

    public static function getWeekName($n)
    {
        $arr = [
            1 => '1-я неделя',
            2 => '2-я неделя',
            3 => '3-я неделя',
            4 => '4-я неделя',
            5 => '5-я неделя',
            6 => '6-я неделя',
            7 => '7-я неделя',
            8 => '8-я неделя',
            9 => '9-я неделя',
            10 => '10-я неделя',
            11 => '11-я неделя',
            12 => '12-я неделя',
        ];

        return isset($arr[$n]) ? $arr[$n] : null;
    }

    public static function getWeeksTraining()
    {
        return [
            1 => '1-я неделя',
            2 => '2-я неделя',
            3 => '3-я неделя',
            4 => '4-я неделя',
            5 => '5-я неделя',
            6 => '6-я неделя',
            7 => '7-я неделя',
            8 => '8-я неделя',
        ];
    }

    public static function getTrainingTypeAdmin()
    {
        return [
            1 => 'Для зала (полностью здоров)',
            2 => 'Для дома (полностью здоров)',
            3 => 'Для зала (Новичок)',
            4 => 'Для зала (Продвинутый)',
            5 => 'Для дома 1 уровень (Новичок)',
            6 => 'Для дома 1 уровень (Продвинутый)',
            7 => 'C резинками',
            8 => 'Для Дома',
            9 => 'Для дома 2 уровень (Новичок)',
            10 => 'Для дома 2 уровень (Продвинутый)',
            11 => 'С акцентом на пресс (Новичок)',
            12 => 'С акцентом на пресс (Продвинутый)',
            13 => 'Функциональная тренировка (Новичок)',
            14 => 'Функциональная тренировка (Продвинутый)',
        ];
    }

    public static function getTrainingType()
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            if (My::isGirl($_GET['type'])) {
                return [
                    5 => 'Для дома 1 уровень (Новичок)',
                    9 => 'Для дома 2 уровень (Новичок)',
                    6 => 'Для дома 1 уровень (Продвинутый)',
                    10 => 'Для дома 2 уровень (Продвинутый)',
                    3 => 'Для зала (Новичок)',
                    4 => 'Для зала (Продвинутый)',
                    13 => 'Функциональная тренировка (Новичок)',
                    14 => 'Функциональная тренировка (Продвинутый)',
                ];
            }
            return [
                5 => 'Для дома (Новичок)',
                6 => 'Для дома (Продвинутый)',
            ];

        }
        if (My::isGirl()) {
            if (My::isCloneProgram()) {
                return [
                    5 => 'Для дома 1 уровень (Новичок)',
                    9 => 'Для дома 2 уровень (Новичок)',
                    6 => 'Для дома 1 уровень (Продвинутый)',
                    10 => 'Для дома 2 уровень (Продвинутый)',
                    3 => 'Для зала (Новичок)',
                    4 => 'Для зала (Продвинутый)',
                    13 => 'Функциональная тренировка (Новичок)',
                    14 => 'Функциональная тренировка (Продвинутый)',
                ];
            }
            return [
                5 => 'Для дома (Новичок)',
                6 => 'Для дома (Продвинутый)',
                3 => 'Для зала (Новичок)',
                4 => 'Для зала (Продвинутый)',
            ];
        }

        return [
            6 => 'Для дома (Продвинутый)', //1
            5 => 'Для дома (Новичок)',//2
            13 => 'Функциональная тренировка (Новичок)',
            14 => 'Функциональная тренировка (Продвинутый)',
        ];
    }

    public function getTrainingTypePress()
    {
        return [
            11 => 'С акцентом на пресс (Новичок)',
            12 => 'С акцентом на пресс (Продвинутый)',
        ];
    }

    public static function getFilledWeeksAdmin(array $checked = [])
    {
        $arr = [
            1 => '1-я неделя',
            2 => '2-я неделя',
            3 => '3-я неделя',
            4 => '4-я неделя',
            5 => '5-я неделя',
            6 => '6-я неделя',
            7 => '7-я неделя',
            8 => '8-я неделя',
        ];

        $clean = [];
        foreach (array_keys($arr) as $n) {
            $clean[$n] = isset($checked[$n]);
        }
        return $clean;
    }

    public static function getExerciseNumber()
    {

        return [
            1 => 'Тренировка №1',
            2 => 'Тренировка №2',
            3 => 'Тренировка №3',
            4 => 'Тренировка №4',
            5 => 'Тренировка №5',
            6 => 'Тренировка №6',
        ];
    }


    /**
     * @param null $key
     * @return array
     */
    public static function getBodyParts($key = null)
    {
        $array = [
            1 => 'Грудь',
            2 => 'Ноги',
            3 => 'Плечи',
            4 => 'Пресс',
            5 => 'Разминка',
            6 => 'Растяжка',
            7 => 'Руки',
            8 => 'Спина',
            9 => 'Ягодицы'
        ];
        if ($key !== null && isset($array[$key])) {
            return $array[$key];
        }
        return $array;
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'training';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['name', 'video', 'body_part'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип тренировки',
            'name' => 'Название',
            'video' => 'Ссылка Ютуба',
            'body_part' => 'Часть тела',
        ];
    }

    /**
     * Gets query for [[Weeks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWeeks()
    {
        return $this->hasMany(Week::className(), ['training_id' => 'id']);
    }
}

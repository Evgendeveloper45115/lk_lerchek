<?php

namespace app\models\base;

use app\components\MyUrlManager;
use app\helpers\My;
use app\models\CaloriesCat;
use app\models\ContentType;
use app\models\query\GlobalQuery;
use Yii;
use \yii\db\ActiveRecord;
use yii\db\Connection;
use yii\helpers\VarDumper;

/**
 * Class MyAR
 * @package app\models
 *
 */
class MyAR extends ActiveRecord
{
    public $is_clone;
    public $img;

    public static function find()
    {
        return new GlobalQuery(get_called_class());
    }

    public function getRelationModel()
    {
        return $this->hasOne(ContentType::class, ['relation_id' => 'id'])->andWhere(['class_name' => $this::className()])->andWhere(['content_type.type_program' => My::getTypeContentClone($this)]);
    }

}

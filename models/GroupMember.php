<?php

namespace app\models;

use app\components\MyUrlManager;
use app\helpers\My;
use app\models\base\MyAR;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "group_member".
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $group_id
 * @property string|null $datetime_signup
 * @property string $phone
 * @property string|null $vk_nick
 * @property int|null $age
 * @property int|null $height
 * @property int|null $weight
 * @property int|null $bust
 * @property int|null $waist
 * @property int|null $hip
 * @property int|null $ration_type
 * @property int|null $gv_info
 * @property int|null $rating
 * @property string|null $food_last_3days
 * @property string|null $unwanted_food
 * @property int|null $training_type
 * @property int|null $veg
 * @property int|null $smoke
 * @property int|null $alcohol
 * @property int|null $diabetes
 * @property int|null $continue_program
 * @property int|null $pregnant
 * @property string|null $health_constraints
 * @property string|null $extra_info
 * @property string|null $experience
 * @property int|null $type_program
 * @property string|null $invoiceId
 * @property string|null $mail
 * @property bool $visible_curator
 * @property float|null $physical_activity
 * @property string $password
 * @property string $first_name
 * @property string $avatar
 * @property string $last_name
 * @property string $patronymic
 * @property string $vote_category
 * @property string $short_url
 * @property string $video_url
 * @property integer $ref_user
 * @property User $user
 * @property Group $group
 * @property PaymentLer $payment
 * @property Retail $retail
 * @property Progress $progress
 * @property Progress[] $progressRel
 * @property Progress $progressRelLastWeek
 * @property Progress $progressRelFirstWeek
 * @property Vote[] $votes
 * @property Markers[] $markers
 * @property ReportHasUser[] $reportHasUsers
 * @property User $refUser
 * @property Popups $progressPopup
 * @property int $coupleVoteCount
 */
class GroupMember extends MyAR
{

    const SCENARIO_USER_CREATE = 'user_create';
    public $cnt;
    public static $cats = [
        1 => '1 группа',
        2 => '2 группа',
        3 => '3 группа',
    ];

    public function getCurrentDay()
    {
        /**
         * @var $member GroupMember
         */
        $member = GroupMember::find()->getMemberProgram()->one();
        $start = My::getTimeToStart();
        $now = new \DateTime('now');
        if ($start == 'Группа ожидает старта') {
            return $start;
        } else {
            $interval = $start->diff($now);
            if ($interval->invert) {
                return 'Группа ожидает старта';
            }
            return $interval->days + 1;
        }
    }

    /**
     * @return int
     */
    public function getCurrentWeek()
    {
        $cur_week = 0;
        $cur_day = $this->getCurrentDay();
        if ($cur_day <= 5) {
            $cur_week = 1;
        } elseif ($cur_day > 5 && $cur_day < 13) {
            $cur_week = 2;
        } elseif ($cur_day >= 13 && $cur_day < 20) {
            $cur_week = 3;
        } elseif ($cur_day >= 20) {
            $cur_week = 4;
        }
        return $cur_week;
    }

    public function getCurrentWeekReportProgress()
    {
        $cur_week = 0;
        $cur_day = $this->getCurrentDay();
        if ($cur_day >= 7) {
            $cur_week = 1;
        }
        if ($cur_day >= 14) {
            $cur_week = 2;
        }
        if ($cur_day >= 21) {
            $cur_week = 3;
        }
        if ($cur_day >= 26) {
            $cur_week = 4;
        }
        return $cur_week;
    }

    public function getAccess()
    {
        /**
         * @var $payment PaymentLer
         */
        if ($this->group->num == 'продолжение') {
            return true;
        }
        if (date('Y-m-d H:i:s') >= $this->group->date_continue && !$this->payment->doplata) {
            return false;
        }
        $days = 30;
        if ($this->payment->doplata && $this->payment->cost == '890') {
            $days = 90;
        }
        if ($this->payment->doplata && !in_array($this->type_program, My::$unlimaccess) && strtotime('+' . $days . ' days', strtotime($this->payment->date_payment)) < time()) {
            return false;
        }
        return true;
    }

    public function getAccessToWrite()
    {
        /**
         * @var $payment PaymentLer
         */

        if ($this->getCurrentDay() >= 30) {
            if (!stristr($this->progressPopup->description, 'замечаний у него нет') && $this->progressPopup->status != 3 && Yii::$app->controller->action->id == 'progress') {
                return true;
            }
            return false;
        }
        return true;
    }

    public function getUserFIO()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    /**
     * @return array
     */
    public function getPhysicalActivities()
    {
        if (My::isGirl()) {
            return [
                '1.38' => 'Участвую с тренировками',
                '1.2' => 'Участвую без тренировок',
            ];
        }
        return [
            '1.2' => 'Малоподвижные люди',
            '1.375' => 'Люди с низкой активностью (1-3 тренировки в неделю)',
            '1.55' => 'для умеренно активных людей (тренировки 3-5 раз в неделю)',
        ];
    }

    /**
     * @return array
     */
    public function getTrainingType()
    {
        if (My::isGirl()) {
            return [
                3 => 'Для зала (Новичок)',
                4 => 'Для зала (Продвинутый)',
                5 => 'Для дома (Новичок)',
                6 => 'Для дома (Продвинутый)',
            ];
        }
        return [
            5 => 'Для дома (Новичок)',//2
            6 => 'Для дома (Продвинутый)', //1
        ];
    }

    /**
     * @return array
     */
    public function getVeg()
    {
        if (My::isGirl()) {
            $arr = [
                1 => 'Веганский (меню без продуктов животного происхождения)',
                2 => 'Особенный (меню с продуктами средней и высокой ценовой категории)',
                3 => 'Парный (меню для тех, кто участвует в марафоне с парой)',
                5 => 'Базовый (меню с базовым недорогим набором продуктов)',
                7 => 'Альтернативное (меню с 4 приемами пищи)',
                8 => 'AntiAge (ваш секрет молодости)',
                9 => 'Базовое меню NEW (меню с продуктами низкой и средней ценовой категории)',
            ];
        } else {
            $arr = [
                1 => 'Веганский (без продуктов животного происхождения)',
                2 => 'Особенный (меню с продуктами средней и высокой ценовой категории)',
                3 => 'Парный (меню для тех, кто участвует в марафоне с парой)',
                5 => 'Базовый (меню с базовым недорогим набором продуктов)',
            ];
        }
        if ($this->type_program == My::FOR_GIRL_22_03_2021 || $this->type_program == My::FOR_MAN_22_03_2021) {
            $arr += [10 => 'Постное меню'];
        }
        return $arr;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            1 => 'На похудение',
            6 => 'Худеть не нужно, нужен рельеф',
        ];
    }

    /**
     * @return array
     */
    public static function getGV()
    {
        return [
            1 => 'Нет',
            2 => 'да, полноценное ГВ (ребенок преимущественно на груди)',
            4 => 'да, эпизодическое (2-3 раза в день недолгое прикладывание)',
        ];
    }

    /**
     * @return array
     */
    public static function getSmoke()
    {
        return [
            1 => 'Да',
            2 => 'Нет',
        ];
    }

    public static function getAlcoholType()
    {
        return [
            1 => 'Регулярно и часто',
            2 => 'Очень редко и несистематически',
            3 => 'Не употребляю вообще',
        ];
    }

    public static function getPregnantType()
    {
        return [
            1 => 'я точно НЕ беременна сейчас',
            3 => 'я беременна (1 триместр)',
            4 => 'я беременна (2 триместр)',
            5 => 'я беременна (3 триместр)',
            6 => 'я уже не беременна, но рожала в последние три месяца ',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group_member';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_USER_CREATE] = ['first_name', 'last_name', 'phone'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visible_curator', 'short_url', 'video_url'], 'safe'],
            [['user_id'], 'required'],
            [['user_id', 'group_id', 'age', 'height', 'weight', 'bust', 'waist', 'hip', 'ration_type', 'gv_info', 'training_type', 'veg', 'smoke', 'alcohol', 'diabetes', 'pregnant', 'type_program'], 'integer'],
            [['datetime_signup', 'continue_program', 'password', 'first_name', 'last_name', 'patronymic', 'avatar', 'rating', 'city', 'family', 'childs', 'telegram', 'parthner', 'vk_nick'], 'safe'],
            [['food_last_3days', 'unwanted_food', 'health_constraints', 'experience'], 'string'],
            [['physical_activity', 'vote_category'], 'number'],
            [['phone', 'vk_nick', 'invoiceId'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['first_name', 'last_name'], 'required', 'on' => self::SCENARIO_USER_CREATE, 'message' => 'Заполните {attribute}'],
            [['phone'], 'required', 'on' => self::SCENARIO_USER_CREATE, 'message' => 'Введите номер телефона'],
            [['phone'], 'match', 'pattern' => '^[0-9]+', 'on' => self::SCENARIO_USER_CREATE, 'message' => 'Допустисные символы: цифры'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'patronymic' => 'Отчество',
            'phone' => 'Номер телефона',
            'age' => 'Возраст',
            'height' => 'Рост (в см)',
            'weight' => 'Вес (кг)',
            'bust' => 'Объем груди (в см)',
            'waist' => 'Объем талии (в см)',
            'hip' => 'Объем ягодиц (в см)',
            'ration_type' => 'Для какой цели нужен рацион?',
            'gv_info' => 'Вы сейчас кормите грудью?',
            'food_last_3days' => 'Что вы ели последние три дня? Перечислите продукты',
            'unwanted_food' => 'Какие продукты вы не едите? Если что-то едите но редко, то укажите, сколько раз в неделю.',
            'training_type' => 'Программу какого типа тренировок вы хотели бы получить?',
            'veg' => 'Какой рацион вам нужен?',
            'smoke' => 'Вы курите?',
            'alcohol' => 'Как часто вы употребляете алкоголь?',
            'diabetes' => 'Есть ли у вас сахарный диабет?',
            'pregnant' => 'Беременность',
            'health_constraints' => 'Есть ли у вас ограничения по здоровью для физических нагрузок?',
            'extra_info' => 'Есть ли что-то, что вам нужно сообщить кураторам до начала занятий?',
            'experience' => 'Есть ли у вас опыт тренировок в тренажерном зале? Если да, опишите его.',
            'password' => 'Пароль',
            'type_program' => 'Тип программы',
            'physical_activity' => 'Дневная активность',
            'group_id' => 'Номер группы',
            'datetime_signup' => 'Дата заполнения анкеты',
            'city' => 'Город проживания',
            'family' => 'Семейное положение',
            'childs' => 'Количество детей (если у вас нет детей, поставьте в этом пункте - 0)',
            'telegram' => 'Имя пользователя в телеграм',
            'parthner' => 'Если вы участвуете парой, укажите в поле ниже имейл вашего партнёра',
            'vk_nick' => 'Ник в инстаграм',
            'vote_category' => 'Категория голосования',
            'video_url' => 'Ссылка на видео',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCoupleVoteCount()
    {
        return $this->hasMany(CoupleVote::className(), ['for_member_id' => 'id'])->andWhere(['is_send' => true])->count();
    }

    public function getRefUser()
    {
        return $this->hasOne(User::className(), ['id' => 'ref_user']);
    }

    /**
     * Gets query for [[Group]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * Gets query for [[Payment_ler]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentLer::className(), ['member_id' => 'id']);
    }

    public function getProgressRel()
    {
        return $this->hasMany(Progress::className(), ['member_id' => 'id']);

    }

    public function getProgressPopup()
    {
        return $this->hasOne(Popups::class, ['member_id' => 'id'])->andWhere(['popup_id' => 2]);
    }

    public function getVotes()
    {
        return $this->hasMany(Vote::className(), ['vote_id' => 'id']);
    }

    public function getProgressRelLastWeek()
    {
        return $this->hasOne(Progress::className(), ['member_id' => 'id'])->andWhere(['week' => 4]);
    }

    public function getProgressRelFirstWeek()
    {
        return $this->hasOne(Progress::className(), ['member_id' => 'id'])->andWhere(['week' => 0]);
    }

    public function getProgress()
    {
        return Progress::find()->where(['member_id' => $this->id])->indexBy('week')->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getRetail()
    {
        return $this->hasOne(Retail::class, ['member_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMarkers()
    {
        return $this->hasMany(Markers::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getReportHasUsers()
    {
        return $this->hasMany(ReportHasUser::class, ['member_id' => 'id']);
    }

    /**
     * @param $week
     * @param $training_number
     * @return RationComplex[]|array|\yii\db\ActiveRecord[]
     */
    public function getRationComplex($week, $training_number)
    {
        $veg = $this->veg;
        $models = RationComplex::find()
            ->joinWith([
                'ingredients' => function (ActiveQuery $query) use ($week, $training_number) {
                    //  $query->onCondition(['number' => $week]);
                },
            ], true, 'LEFT JOIN')
            ->select('ration_complex.*')
            ->where(['ration_complex.ration_type' => $veg])
            ->andWhere(['ration_complex.week' => $week])
            ->andWhere(['ration_complex.day' => $training_number])
            ->orderBy('ration_complex.day_part ASC')
            ->getContent()
            ->all();
        return $models;
    }

    /**
     * @param $user_id
     */
    public function setAttributesAdmin($user_id)
    {
        $this->phone = '+7';
        $this->datetime_signup = date('Y-m-d H:i:s');
        $this->user_id = $user_id;
        $this->group_id = null;
        $this->first_name = 'Имя';
        $this->last_name = 'Фамилия';
        $this->patronymic = "Отчество";
        $this->type_program = Yii::$app->user->identity->session_type;
    }

    public function minMaxWeekDay($week, $day)
    {
        if ($week < 0 && Yii::$app->controller->action->id == 'report' || $week < 1 && Yii::$app->controller->action->id != 'report') {
            return true;
        }
        if ($week > 4) {
            return true;
        }
        if ($day > (Yii::$app->controller->action->id == 'trainings' ? $this->getMaxDayTraining() : $this->getMaxDay()) || $day < 1) {
            return true;
        }
        return false;
    }

    public function getMaxDay()
    {
        return 7;
    }

    public function getMaxDayTraining()
    {
        $training_type = $this->training_type;
        $week = $_GET['week'];
        if (isset($_GET['training'])) {
            $training_type = $_GET['training'];
        }
        if (My::isCloneProgram() && $training_type >= 13 && $week == 1) {
            return 4;
        }
        if (My::isCloneProgram() && $training_type >= 13 && $week == 2) {
            return 5;
        }
        if (My::isCloneProgram() && $training_type >= 13 && $week >= 3) {
            return 6;
        }
        if (My::isGirl() && $training_type == 12) {
            return 6;
        }
        if (My::isCloneProgram() && $training_type == 11) {
            return 4;
        }
        if (My::isCloneProgram() && $training_type >= 9) {
            return 5;
        }
        return 4;
    }

    /**
     * @return mixed|string
     */
    public function getRationText()
    {
        return Yii::$app->user->identity->groupMember->getVeg()[$this->veg];
    }

    public function getWeeks()
    {

        $week = [1 => 1, 2, 3, 4];
        if (Yii::$app->controller->action->id == 'report') {
            $week = [0 => 'Начало'] + $week;
        }
        return $week;
    }

    public function accessButtonNext($select_week, $cur_week, $url)
    {
        if ($select_week + 1 > $cur_week) {
            return '<span class="motivation_week_nextweek access_popup training_access"></span>';
        }
        if (Yii::$app->controller->action->id == 'trainings' || Yii::$app->controller->action->id == 'perfect-abs') {
            $training_type = $this->training_type;
            if (isset($_GET['training'])) {
                $training_type = $_GET['training'];
            }
            return '<a class="motivation_week_nextweek" href="' . Url::to([$url, 'week' => $select_week + 1, 'training' => $training_type]) . '"></a>';
        }
        return '<a class="motivation_week_nextweek" href="' . Url::to([$url, 'week' => $select_week + 1]) . '"></a>';
    }

    public function accessButtonPrev($select_week, $url)
    {
        if (Yii::$app->controller->action->id == 'trainings' || Yii::$app->controller->action->id == 'perfect-abs') {
            $training_type = $this->training_type;
            if (isset($_GET['training'])) {
                $training_type = $_GET['training'];
            }
            return '<a class="motivation_week_prevweek" href="' . Url::to([$url, 'week' => $select_week - 1, 'training' => $training_type]) . '"></a>';
        }
        return '<a class="motivation_week_prevweek" href="' . Url::to([$url, 'week' => $select_week - 1]) . '"></a>';
    }

    public function getExercises($week, $training_number)
    {
        $type = $this->training_type;
        $get = Yii::$app->request->get('training');
        if (isset($get)) {
            $type = Yii::$app->request->get('training');
        }
        $models = Training::find()
            ->joinWith([
                'weeks' => function (ActiveQuery $query) use ($week, $training_number) {
                    $query->onCondition(['number' => $week]);
                },
                'weeks.weekTrainings' => function (ActiveQuery $query) use ($week, $training_number) {
                    $query->onCondition([
                        'training_number' => $training_number,
                    ]);
                },
            ], true, 'INNER JOIN')
            ->select('training.*')
            ->where(['training.type' => $type])
            ->orderBy('week_training.sort_index')
            ->asArray()
            ->getContent()
            ->all();
        return $models;
    }

    public function updatePassword($new_password)
    {
        $this->password = $new_password;
    }

    public function is30Day()
    {
        if (Yii::$app->user->identity->email == 'vdovenkova2@gmail.com' || Yii::$app->user->identity->email == 'test_marafon@mail.ru') {
            return true;
        }

        if ($this->getCurrentDay() == 32) {
            return true;
        }
        return false;
    }

    public function is34Day()
    {
        if (Yii::$app->user->identity->email == 'vdovenkova2@gmail.com' || Yii::$app->user->identity->email == 'test_marafon@mail.ru') {
            return true;
        }
        if ($this->getCurrentDay() == 34) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getPressTrainings()
    {
        return $this->payment->date_payment > '2021-03-01' && $this->payment->date_payment < '2021-03-17';
    }

}

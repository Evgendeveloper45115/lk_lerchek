<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "week_training".
 *
 * @property int $id
 * @property int|null $week_id
 * @property int|null $training_number
 * @property int|null $sort_index
 *
 * @property Week $week
 */
class WeekTraining extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'week_training';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['week_id', 'training_number', 'sort_index'], 'integer'],
            [['week_id'], 'exist', 'skipOnError' => true, 'targetClass' => Week::className(), 'targetAttribute' => ['week_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'week_id' => 'Week ID',
            'training_number' => 'Training Number',
            'sort_index' => 'Sort Index',
        ];
    }

    /**
     * Gets query for [[Week]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWeek()
    {
        return $this->hasOne(Week::className(), ['id' => 'week_id']);
    }
}

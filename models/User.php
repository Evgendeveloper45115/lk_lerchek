<?php

namespace app\models;

use app\components\MyUrlManager;
use app\helpers\My;
use app\models\base\MyAR;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $email
 * @property string $datetime_signup
 * @property string $datetime_last_login
 * @property intege $role
 * @property integer $session_type
 * @property integer $point
 * @property GroupMember[] $groupMembers
 * @property GroupMember $groupMember
 * @property GroupMember $groupMemberGuest
 * @property GroupMember $groupMemberLogin
 * @property GroupMember $groupMembersLastPotok
 * @property Ref $refUser
 */
class User extends MyAR implements IdentityInterface
{
    public $orig_password;
    public $new_password;
    public $read;
    public $password1;
    public $email_confirm;
    public $email_man;
    public $complete;

    const ROLE_USER = 0;
    const ROLE_MODERATOR = 1;
    const ROLE_ADMIN = 2;
    const SCENARIO_CHANGE_AVATAR = 'change_user_avatar';
    const SCENARIO_CHANGE_PASSWORD_USER = 'change_password_user';
    const SCENARIO_USER_CREATE = 'user_create';
    const SCENARIO_USER_CREATE_COUPLE = 'user_create_couple';


    static $roles = [
        self::ROLE_USER => 'Клиент',
        self::ROLE_MODERATOR => 'Модератор',
        self::ROLE_ADMIN => 'Куратор',
    ];
    static $visible_curators = [
        0 => 'Не просмотрено',
        1 => 'Младший куратор',
        2 => 'Старший куратор',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHANGE_AVATAR] = ['email'];
        $scenarios[self::SCENARIO_CHANGE_PASSWORD_USER] = ['orig_password', 'new_password', 'password1',];
        $scenarios[self::SCENARIO_USER_CREATE] = ['email', 'email_confirm', 'email_man', 'point'];
        $scenarios[self::SCENARIO_USER_CREATE_COUPLE] = ['email', 'email_confirm', 'email_man', 'point'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'email_confirm'], 'required', 'on' => self::SCENARIO_USER_CREATE, 'message' => 'Введите email'],
            [['email', 'email_confirm'], 'required', 'on' => self::SCENARIO_USER_CREATE_COUPLE, 'message' => 'Введите email'],
            [['email_man'], 'required', 'on' => self::SCENARIO_USER_CREATE_COUPLE, 'message' => 'Заполните {attribute}'],
            [['datetime_signup', 'datetime_last_login', 'session_type'], 'safe'],
            [['role'], 'integer'],
            [['email', 'complete', 'point'], 'safe'],
            [['email', 'email_confirm', 'email_man'], 'filter', 'filter' => 'trim'],
            [['email'], 'email', 'message' => 'Не валидный Email!'],
            // [['email'], 'email', 'message' => 'Не валидный Email!'],
            [['email_confirm'], 'email', 'message' => 'Не валидный Email!'],
            [['email_man'], 'email', 'message' => 'Не валидный Email!'],
            ['email_man', function ($attribute, $params) {
                if ($this->email_man == $this->email) {
                    $this->addError($attribute, 'Введите другой Email');
                }
            }
            ],
            [['last_name'], 'required', 'on' => self::SCENARIO_USER_CREATE, 'message' => 'Заполните {attribute}'],
            ['email_confirm', 'compare',
                'compareAttribute' => 'email',
                'on' => self::SCENARIO_USER_CREATE,
                'message' => 'Email не совпадает'
            ],
            ['email_confirm', 'compare',
                'compareAttribute' => 'email',
                'on' => self::SCENARIO_USER_CREATE_COUPLE,
                'message' => 'Email не совпадает'
            ],

            ['orig_password', 'myCompare', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER],
            ['password1', 'compare',
                'compareAttribute' => 'new_password',
                'on' => self::SCENARIO_CHANGE_PASSWORD_USER,
                'message' => 'Пароли не совпадают'
            ],

        ];
    }

    public function myCompare($attribute, $params)
    {
        if (!password_verify($this->$attribute, $this->groupMember->password) && $this->$attribute != $this->groupMember->password) {
            $this->addError($attribute, 'Старый пароль не совпадает');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'datetime_signup' => 'Дата оплаты/заполнения анкеты',
            'datetime_last_login' => 'Datetime Last Login',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'role' => 'Role',
            'session_type' => 'session_type',
            'point' => 'Баллы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupMembers()
    {
        return $this->hasMany(GroupMember::className(), ['user_id' => 'id']);
    }

    public function getGroupMembersLastPotok()
    {
        return $this->hasMany(GroupMember::className(), ['user_id' => 'id'])->andWhere(['type_program' => 11])->orWhere(['type_program' => 12])->with(['payment' => function (ActiveQuery $query) {
            $query->andWhere(['not', ['cost' => null]]);
        }])->all();
    }

    public function getGroupMember()
    {
        return $this->hasOne(GroupMember::className(), ['user_id' => 'id'])->andWhere(['=', 'group_member.type_program', Yii::$app->getUser()->getIdentity()->session_type]);
    }

    /**
     * @param null $type
     * @return \yii\db\ActiveQuery
     */
    public function getGroupMemberGuest($type = null)
    {
        if ($type === null) {
            $type = $_GET['type'];
        }
        return $this->hasOne(GroupMember::className(), ['user_id' => 'id'])->andWhere(['=', 'group_member.type_program', $type]);
    }

    public function getGroupMemberLogin()
    {
        return $this->hasOne(GroupMember::className(), ['user_id' => 'id'])->andWhere(['LIKE binary', 'group_member.password', $_POST['LoginForm']['password']]);
    }

    public function setAttributesAdmin()
    {
        $this->email = Yii::$app->request->post()['email'];
        $this->datetime_signup = date('Y-m-d H:i:s');
        $this->datetime_last_login = date('Y-m-d H:i:s');
        $this->session_type = Yii::$app->user->identity->session_type;
    }

    /**
     * @param int|string $id
     * @return User|IdentityInterface|null
     */
    public static function findIdentity($id)
    {
        $session = Yii::$app->session;
        if ($session->get('avatar')) {
            $id = $session->get('avatar');
        }
        return static::findOne($id);
    }

    /**
     * @param $token
     * @param null $type
     * @return |null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function getRefUser()
    {
        return $this->hasOne(Ref::className(), ['user_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAuthKey()
    {
        return $this->authKey;

    }

    /**
     * @param $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function sendMail($password)
    {

        /* @var \app\models\User $user */
        $user = Yii::$app->getUser()->identity;
        $text = 'registration-html-' . My::$type_program[$user->session_type];
        $subject = Yii::$app->params['email-subject-registration-' . My::$type_program[$user->session_type]];

        My::sendEmail($this->groupMember, $text, $subject, $password);
    }

    public function login()
    {
        return Yii::$app->user->login($this);
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "retail".
 *
 * @property int $id
 * @property int $member_id
 * @property string $index_post
 * @property string $city
 * @property string $street
 * @property string $building
 * @property string|null $flat
 * @property string $cost
 * @property string|null $comment
 * @property string|null $present
 * @property string|null $country
 * @property int $status
 * @property string|null $error_text
 * @property string|null $date_payment
 * @property string|null $payment_status
 * @property GroupMember $groupMember
 * @property PaymentLer $payment
 */
class Retail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'retail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'index_post', 'city', 'street', 'building', 'cost'], 'required'],
            [['member_id', 'status'], 'integer'],
            [['comment', 'error_text'], 'string'],
            [['index_post', 'city', 'street', 'building', 'flat', 'cost', 'present', 'country'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'index_post' => 'Index Post',
            'city' => 'City',
            'street' => 'Street',
            'building' => 'Building',
            'flat' => 'Flat',
            'cost' => 'Cost',
            'comment' => 'Comment',
            'present' => 'Present',
            'country' => 'Country',
            'status' => 'Status',
            'error_text' => 'Error Text',
        ];
    }

    public function getGroupMember()
    {
        return $this->hasOne(GroupMember::className(), ['id' => 'member_id']);
    }

    public function getPayment()
    {
        return $this->hasOne(PaymentLer::className(), ['member_id' => 'member_id']);
    }

}

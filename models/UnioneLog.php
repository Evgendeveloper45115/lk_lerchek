<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unione_log".
 *
 * @property int $id
 * @property string|null $email
 * @property int|null $status
 * @property int|null $url
 */
class UnioneLog extends \yii\db\ActiveRecord
{

    public static $statuses = [
        'sent' => 1,
        'delivered' => 2,
        'opened' => 3,
        'hard_bounced' => 4,
        'soft_bounced' => 5,
        'spam' => 6,
        'clicked' => 7,
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unione_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['email', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }
}

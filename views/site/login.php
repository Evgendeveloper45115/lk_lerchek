<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Вход</h1>
<div class="login_form">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'validateOnChange' => false,
        'enableClientValidation' => false,

        'options' => ['class' => 'form-horizontal', 'name' => 'sign_in'],
        'fieldConfig' => [
            'options' => ['class' => 'form_group clearfix'],
            'template' => "{label}\n<div class=\"\">{input}\n<div class=\"error\">{error}</div></div>",
        ],
    ]); ?>

    <?= $form->field($model, 'email')->textInput(['class' => 'sing_input', 'placeholder' => 'Логин'])->label(false) ?>

    <?= $form->field($model, 'password')->passwordInput(['class' => 'sing_input', 'placeholder' => 'Пароль'])->label(false) ?>

    <div class="clearfix" style="margin-top: 15px;">
        <button class="button button_green">Войти</button>
        <a href="/marafon/remember" class="remind_password">Забыли пароль?</a>
    </div>

    <?php ActiveForm::end(); ?>
</div>
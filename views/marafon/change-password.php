<?php

/* @var $this yii\web\View */

/* @var $success bool */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Смена пароля';
$active_menu = 'change_password';
?>
<h1>Смена пароля</h1>
<div class="profile_changepassword">
    <?php

    $form = ActiveForm::begin([
        'fieldConfig' => [
            'options' => ['class' => 'form_group clearfix'],
            'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
        ],
    ]) ?>
    <?= $form->field($user, 'orig_password')->passwordInput()->label('Старый пароль') ?>
    <?= $form->field($user, 'new_password')->passwordInput()->label('Новый пароль') ?>
    <?= $form->field($user, 'password1')->passwordInput()->label('Повтор пароля') ?>
    <?php
    if ($success) {
        echo '<div style="color: red;margin-bottom: 10px;">Пароль успешно изменен</div>';
    }
    ?>
    <?= Html::submitButton('Сменить пароль', ['class' => 'button button_green button_big']) ?>
    <?php ActiveForm::end(); ?>
</div>

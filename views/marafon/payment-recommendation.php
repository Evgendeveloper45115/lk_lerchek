<div class="jsModal body__modalText" data-modal="recommendation">
    <div class="modalText">
        <div class="modalText__wrapper">
            <img src="/images/project/cancel-black.png" alt="" class="jsClose modalText__close">
            <p class="modalText__title">Агентский договор-оферта</p>
            <p class="modalText__text">
                Индивидуальный предприниматель Феопентова Эльвира Викторовна, действующая на основании Свидетельства о
                государственной регистрации физического лица в качестве индивидуального предпринимателя №
                320774600440342 от 23.10.2020, именуемая в дальнейшем «Оператор», с одной стороны, и физическое лицо,
                согласившееся с условиями настоящей Публичной оферты, размещенной на сайте Оператора по адресу:
                https://<?= Yii::$app->request->hostName ?>, называемое в дальнейшем «Партнер», с другой стороны, далее
                по тексту именуемые Стороны, заключили договор на условиях настоящей Публичной оферты:
            </p>
            <p class="modalText__title">1. Термины и определения</p>
            <p class="modalText__text">
                1.1 Услуга – услуги Оператора по предоставлению информационных материалов, направленных на достижение
                поставленных целей (похудение, набор мышечной массы и т.д.). Перечень таких Услуг устанавливается
                Оператором и отражается в Личном кабинете Партнера.
            </p>
            <p class="modalText__text">
                1.2. Личный кабинет — программный интерфейс взаимодействия Партнера с Оператором, который содержит
                информацию об Партнере, историю операций и иную информацию в отношении услуг Оператора, а также
                предоставляет возможность удаленного взаимодействия Сторон в рамках Договора, доступный Партнеру после
                авторизации на сайте https://<?= Yii::$app->request->hostName ?> с использованием логина и пароля
                Партнера.
            </p>
            <p class="modalText__text">
                1.3. Реферал – физическое лицо, зарегистрировавшееся на сайте:
                https://<?= Yii::$app->request->hostName ?>, заключившее договор с Оператором на использование Сайта и
                оплатившее услуги Оператора по Реферальной ссылке.
            </p>
            <p class="modalText__text">
                1.4. Реферальная ссылка — часть электронного (гипертекстового) документа, ведущая на интернет-сайт
                Оператора, расположенный по адресу: https://<?= Yii::$app->request->hostName ?>, расположенный в сети
                Интернет, имеющая специальный формат с уникальным кодом Партнера.
            </p>
            <p class="modalText__text">
                1.5. История операций — данные систем автоматизированного учета информации Оператора, которые содержат
                сведения о Рефералах Партнера, информацию об операциях, которые совершили Рефералы и сведения об
                изменениях баланса на счету Партнера.
            </p>
            <p class="modalText__text">
                1.6. Сайт – интернет-сайт Оператора, расположенный по адресу в сети Интернет:
                https://<?= Yii::$app->request->hostName ?>.
            </p>
            <p class="modalText__title">2. Предмет Договора</p>
            <p class="modalText__text">
                2.1. По условиям настоящего Договора (далее в тексте Договор), Партнер оказывает Оператору услуги по
                привлечению Рефералов на Сайт Оператора, а Оператор оплачивает Партнеру оказываемые услуги в размере, в
                порядке и в сроки, указанные в настоящем Договоре.
            </p>
            <p class="modalText__text">
                2.2. Договоры на оказание Услуг и иные соглашения заключаются непосредственно между Оператором и
                Рефералами. Партнер не уполномочен на заключение каких-либо сделок в интересах Оператора, равно как от
                своего имени, так и от имени Оператора.
            </p>
            <p class="modalText__title">3. Порядок привлечения Реферала</p>
            <p class="modalText__text">
                3.1. Партнер привлекает Реферала посредством личной рекомендации через отправку Реферальной ссылки на
                оплату продукта. Реферал признается привлеченным, если он совершил все перечисленные ниже действия:
            </p>
            <p class="modalText__text">
                3.1.1. Осуществил переход на интернет-сайт Оператора по Реферальной ссылке Партнера;
            </p>
            <p class="modalText__text">
                3.1.2. Зарегистрировался на Сайте Оператора и заключил с Оператором договор на основании публичной
                оферты.
            </p>
            <p class="modalText__text">
                3.1.3. Оплатил Услугу Оператора.
            </p>
            <p class="modalText__text">
                3.2. Рефералом считается только новый Новый пользователь, который ранее никогда не был
                зарегистрирован в личном кабинете марафона Валерии и Артема Чекалиных.</p>

            <p class="modalText__title">4. Права и обязанности сторон</p>
            <p class="modalText__text">
                <b>4.1. Оператор обязуется:</b>
            </p>
            <p class="modalText__text">
                4.1.1. Выполнять свои обязательства перед Партнером в соответствии с условиями Договора.
            </p>
            <p class="modalText__text">
                4.1.2. Обеспечить возможность ознакомления Партнера с данными статистики через Личный кабинет, при этом
                Оператор не несет ответственности в случае невозможности ознакомления Партнера с данными статистики по
                причинам, не зависящим от Оператора.
            </p>
            <p class="modalText__text">
                4.1.3. Выплачивать Партнеру вознаграждение на условиях настоящего Договора.
            </p>
            <p class="modalText__text">
                4.1.4. В случае отказа Партнера от исполнения Договора по основаниям, предусмотренным Договором,
                выплатить Партнеру по его требованию сумму не выплаченных ранее вознаграждений.
            </p>
            <p class="modalText__text">
                4.1.5. Оберегать персональные данные Партнера, в соответствии с ФЗ-152 «О персональных данных».
            </p>
            <p class="modalText__text">
                <b>4.2. Оператор имеет право:</b>
            </p>
            <p class="modalText__text">
                4.2.1. Временно приостановить работу Сервиса и Реферальной ссылки по техническим, технологическим
                причинам, на время устранения таких причин.
            </p>
            <p class="modalText__text">
                4.2.2. Осуществлять любые действия (контрольные мероприятия) по проверке содержания и качества
                размещенных Партнером Реферальных ссылок и их соответствия требованиям Договора. Данные, полученные в
                результате осуществления контрольных мероприятий, являются достаточным, но не единственным основанием
                для установления несоответствия Реферальных ссылок требованиям Договора. При этом Оператор
                самостоятельно определяет способы, периодичность и основания проведения контрольных мероприятий.
            </p>
            <p class="modalText__text">
                <b>4.3. Партнер обязуется:</b>
            </p>
            <p class="modalText__text">
                4.3.1. При продвижении и рекламе услуг Оператора, в том числе при распространении Реферальных ссылок,
                соблюдать все требования Оператора, указанные в Договоре, а также все применимые нормы и требования
                действующего законодательства, в том числе Федерального Закона «О рекламе», законодательства об
                интеллектуальной собственности, но не ограничиваясь перечисленным.
            </p>
            <p class="modalText__text">
                4.3.2. Использовать Личный кабинет только в соответствии с целями, в которых он создан.
            </p>
            <p class="modalText__text">
                4.3.3. При обработке персональных данных потенциального Реферала Партнер руководствуется Федеральным
                законом РФ «О персональных данных».
            </p>
            <p class="modalText__text">
                <b>4.4. Партнер имеет право:</b>
            </p>
            <p class="modalText__text">
                4.4.1. На доступ к данным истории операций и перечню Рефералов в установленном порядке.
            </p>
            <p class="modalText__text">
                4.4.2. Приостановить и/или прекратить размещение Реферальных ссылок в любое время
            </p>
            <p class="modalText__text">
                <b>4.5. Партнеру запрещается:</b>
            </p>
            <p class="modalText__text">
                4.5.1. Производить принудительную переадресацию пользователей на сайт Оператора через рекламную ссылку
                или баннер.
            </p>
            <p class="modalText__text">
                4.5.2. Размещать в поисковых системах (google, yandex, rambler и т.д.) контекстную рекламу услуг
                Оператора, содержащую имя домена lerchek.ru и его производные как на русском, так и английском языках.
            </p>
            <p class="modalText__text">
                4.5.3. Производить рекламную рассылку (спам) с распространением партнерской Реферальной ссылки или кода
                Партнера.
            </p>
            <p class="modalText__text">
                4.5.4 Применять любые технические манипуляции в Личном кабинете (не заявленные Оператором) для
                извлечения личной выгоды. Если Партнер обнаружит уязвимость в Личном кабинете, он обязан сообщить об
                этом на электронный адрес info@lerchek.ru
            </p>
            <p class="modalText__title">5. Расчет и выплата вознаграждения</p>
            <p class="modalText__text">
                5.1. Учет Рефералов, зашедших на Сайт Оператора по рекомендации Партнера осуществляется по уникальному
                коду Партнера, выданному Оператором, который Реферал сообщает Оператору. Реферал, выполнивший все
                обязательные условия (пп. 3.1.1, 3.1.2, 3.1.3 Договора), закрепляется за Партнером. За каждую оплату
                Реферала Партнер получает вознаграждение.
            </p>
            <p class="modalText__text">
                5.2. Сумма вознаграждения составляет 10% от покупок Реферала, но не более 3 000 ₽ за одного Реферала.
                Отслеживать покупки привлеченных Рефералов можно в Личном кабинете на странице
                https://<?= Yii::$app->request->hostName ?>/marafon/bonus.
            </p>
            <p class="modalText__text">
                5.3. Вознаграждение за Реферала выплачивается однократно после оплаты по ссылке Партнера.
            </p>
            <p class="modalText__text">
                5.4. Начисление вознаграждения в личном кабинете происходит в течение 1 часа после того, как проходит
                оплата Реферала. Если Реферал не оплатил Услугу, вознаграждение не начисляется.
            </p>
            <p class="modalText__text">
                5.5. Вознаграждение может быть использовано для выведения на баланс мобильного телефона от суммы 1000
                рублей. Вывести вознаграждение можно только <b>на российский номер телефона</b>. Для выведения наличных
                средств на свой баланс необходимо заполнить в Личном Кабинете заявку на выведение средств с указанием
                суммы. После заполнения заявки на выведение средств заявка приобретает статус «В обработке». После
                обработки денежные средства поступают на указанный клиентом номер телефона в течение 1-5 рабочих дней.
            </p>
            <p class="modalText__text">
                5.6. Выплаты вознаграждения производятся в российских рублях.
            </p>
            <p class="modalText__text">
                5.7. Обязанность по оплате вознаграждения Партнера считается исполненной с момента списания денежных
                средств с корреспондентского счета банка Оператора.
            </p>
            <p class="modalText__text">
                5.8. Выплата вознаграждения осуществляется с использованием сервиса «Быстрый платеж через ЮMoney» <a
                        href="https://yoomoney.ru/pay/page?id=526623">https://yoomoney.ru/pay/page?id=526623</a>
            </p>
            <p class="modalText__text">
                <b>5.9. Партнер самостоятельно уведомляет налоговые органы и уплачивает налоги на доходы физических лиц
                    в связи с получением вознаграждения в порядке и сроки, предусмотренные действующим
                    законодательством.</b>
            </p>
            <p class="modalText__title">6. Срок действия и изменение условий договора</p>
            <p class="modalText__text">
                6.1. Договор, между Оператором и Партнером, считается заключенным с момента акцепта агентского
                договора-оферты и оплаты Услуги на сайте https://<?= Yii::$app->request->hostName ?>.
            </p>
            <p class="modalText__text">
                6.2. Договор действует до момента его расторжения, либо до момента когда по решению Оператора
                Реферальная программа будет закрыта, либо до момента когда по решению Оператора будет приостановлено
                функционирование Сервиса.
            </p>
            <p class="modalText__text">
                6.3. Оператор и Партнер вправе досрочно в одностороннем внесудебном порядке расторгнуть Договор по
                письменному уведомлению, направленному другой Стороне не менее чем за 30 календарных дней до дня
                расторжения Договора.
            </p>
            <p class="modalText__text">
                6.4. Договор может быть расторгнут по соглашению Сторон в порядке и сроки, указанные в соглашении сторон
                о растяжении договора.
            </p>
            <p class="modalText__text">
                6.5. Прекращение Договора в связи с закрытием Реферальной программы, либо в связи приостановлением
                функционирования Сервиса осуществляется в одностороннем порядке по решению Оператора, о чем Оператор
                обязуется уведомить Партнера, разместив сообщение на Сайте. Оператор вправе направить личное уведомление
                Партнеру с использованием любых доступных средств связи на адреса, указанные Партнером при регистрации.
            </p>
            <p class="modalText__text">
                6.6 Оператор оставляет за собой право аннулировать частично или полностью все бонусные рубли на счету
                Партнера.
            </p>

            <p class="modalText__title">7. Гарантии</p>
            <p class="modalText__text">
                7.1. В течение срока действия Договора Оператор предпримет все усилия для устранения каких-либо сбоев и
                ошибок в работе Сайта, в случае их возникновения. При этом Оператор не гарантирует отсутствия ошибок и
                сбоев при работе Сайта, в том числе в отношении работы программного обеспечения Личного кабинета.
            </p>
            <p class="modalText__text">
                7.2. За исключением гарантий, прямо указанных в тексте настоящего Договора, Оператор не предоставляет
                никаких иных прямых или подразумеваемых гарантий по Договору.
            </p>
            <p class="modalText__text">
                7.3. Заключая Договор, Партнер подтверждает и гарантирует Оператору, что:
            </p>
            <p class="modalText__text">
                7.3.1. Партнер является гражданином Российской Федерации (физическим лицом, постоянно проживающим на
                территории РФ). Оператор не заключает договоры с физическими лицами, являющимися гражданами иностранных
                государств.
            </p>
            <p class="modalText__text">
                7.3.2. Партнер обязуется указать достоверные данные при регистрации в Личном кабинете, в том числе
                достоверный номер телефона.
            </p>
            <p class="modalText__text">
                7.3.3. Размещение (воспроизведение, показ), иное использование Партнером Реферальных ссылок по Договору,
                а также ссылки (их содержание), к которым осуществляется переадресация потенциальных Рефералов, не
                нарушает и не влечет за собой нарушение каких-либо прав третьих лиц и действующего законодательства.
            </p>
            <p class="modalText__title">8. Ответственность и ограничение ответственности</p>
            <p class="modalText__text">
                8.1. За нарушение условий Договора Стороны несут ответственность, установленную Договором и/или
                действующим законодательством РФ.
            </p>
            <p class="modalText__text">
                8.2. Оператор ни при каких обстоятельствах не несет никакой ответственности по Договору за:
            </p>
            <p class="modalText__text">
                8.2.1. Какие-либо косвенные убытки и/или упущенную выгоду Партнера и/или третьих сторон вне зависимости
                от того, мог ли Оператор предвидеть возможность таких убытков или нет.
            </p>
            <p class="modalText__text">
                8.2.2. Использование/невозможность использования Партнером и/или третьими лицами любых средств и/или
                способов передачи/получения Реферальных ссылок и/или информации.
            </p>
            <p class="modalText__text">
                8.3. Совокупный размер ответственности Оператора по Договору, включая размер штрафных санкций (пеней,
                неустоек) и/или возмещаемых убытков, по любому иску или претензии в отношении Договора или его
                исполнения, ограничивается 10% от стоимости всех вознаграждений по Договору, на дату предъявления
                претензии или иска.
            </p>
            <p class="modalText__text">
                8.4. Стороны освобождаются от ответственности за частичное или полное неисполнение обязательств по
                настоящему Договору, если это неисполнение явилось следствием обстоятельств непреодолимой силы, которые
                возникли после заключения Договора, либо если неисполнение обязательств Сторонами по Договору явилось
                следствием событий чрезвычайного характера, которые Стороны не могли ни предвидеть, ни предотвратить
                разумными мерами.
            </p>
            <p class="modalText__text">
                8.5. Партнер несет ответственность в полном объеме за:
            </p>
            <p class="modalText__text">
                8.5.1. Соблюдение всех требований законодательства, в том числе законодательства о рекламе, об
                интеллектуальной собственности, о конкуренции, но не ограничиваясь перечисленным, в отношении содержания
                и формы Реферальных ссылок, иные действия, осуществляемые им в качестве рекламодателя и/или
                рекламопроизводителя.
            </p>
            <p class="modalText__text">
                8.5.2. Достоверность сведений, указанных им при регистрации в Личном кабинете.
            </p>
            <p class="modalText__text">
                8.6. В случае нарушения п. 7.3. настоящего Договора Партнер обязуется компенсировать убытки и упущенную
                выгоду вызванные таким нарушением.
            </p>
            <p class="modalText__title">9. Прочие условия</p>
            <p class="modalText__text">
                9.1. Договор, его заключение и исполнение регулируется действующим законодательством Российской
                Федерации. Все вопросы, не урегулированные настоящим Договором или урегулированные не полностью,
                регулируются в соответствии с материальным правом Российской Федерации. Если споры между Оператором и
                Партнером в отношении Договора не разрешены путем переговоров Сторон, они подлежат рассмотрению в
                порядке, предусмотренном действующим законодательством.
            </p>
            <p class="modalText__text">
                9.2. Любые уведомления в рамках отношений Сторон по Договору могут направляться одной Стороной другой
                Стороне:
            </p>
            <p class="modalText__text">
                9.2.1. По электронной почте – с использованием адреса электронной почты Партнера, указанного им при
                регистрации в Личном кабинете, и адреса электронной почты Оператора, указанного в разделе 10 Договора.
            </p>
            <p class="modalText__text">
                9.2.2. Почтой с уведомлением о вручении или курьерской службой.
            </p>
            <p class="modalText__text">
                9.3. В случае если одно или более из положений Договора являются по какой-либо причине недействительным,
                не имеющим юридической силы, такая недействительность не оказывает влияния на действительность любого
                другого положения Договора, которые остаются в силе.
            </p>
            <p class="modalText__title">10. Наименование и банковские реквизиты Оператора</p>
            <p class="modalText__text">
                ИП Феопентова Эльвира Викторовна<br>
                ИНН 632132854350<br>
                ОГРНИП 320774600440342 от 23.10.2020<br>
                АО “АЛЬФА-БАНК”<br>
                БИК 044525593<br>
                р/с 40802810402860005874<br>
                к/с 30101810200000000593
            </p>
        </div>
    </div>
</div>

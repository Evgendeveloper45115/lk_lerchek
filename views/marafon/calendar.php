<?php
/**
 * @var $this \yii\web\View
 * @var $view integer
 * @var $user \app\models\User
 * @var $markers array
 */
$this->registerJs('
document["markers"] = ' . new \yii\web\JsExpression(\yii\helpers\Json::encode($markers)) . ';
document["markers_group"] = ' . new \yii\web\JsExpression(\yii\helpers\Json::encode(\yii\helpers\ArrayHelper::index($markers, 'id', 'second_date'))) . ';
', \yii\web\View::POS_HEAD);
\app\helpers\My::getAccess();
?>
    <input type="hidden" class="user_id" value="<?= $user->id ?>">
    <div class="diary">
        <h3 class="diary__title" style="text-align: center; margin-bottom: 20px;">Календарь активности</h3>
        <div class="calendar">
            <div class="calendar__prev"></div>
            <div class="calendar__current"></div>
            <div class="calendar__next"></div>
            <div><span class="btn">Отметьте как прошел день</span>
            </div>
            <div class="hint hint_message">
                <div class="hint__close"></div>
                <div class="hint__title">Подсказка</div>
                <div class="hint__text">кликните на число, чтобы отметить, что вы делали в этот день</div>
            </div>
        </div>
        <div class="calendar-wrapper">
            <div id="calendar"></div>
            <div class="hint hint--day">
                <div class="hint__close"></div>
                <div class="hint__title"></div>
                <div class="hint__text">Как прошел ваш день?</div>
                <div class="hint__body">
                    <div class="hint__row hint__row--power"><span class="hint__btn" data-type="power">Добавить</span>
                        <img class="hint__ico" src="../images/calendar/event_power.png" alt=""><span
                                class="hint__training">Тренировка</span>
                    </div>
                    <div class="hint__row hint__row--cardio"><span class="hint__btn" data-type="cardio">Добавить</span>
                        <img class="hint__ico" src="../images/calendar/event_cardio.png" alt=""><span
                                class="hint__training">Кардио</span>
                    </div>
                    <div class="hint__row hint__row--food"><span class="hint__btn" data-type="food">Добавить</span>
                        <img class="hint__ico" src="../images/calendar/event_food.png" alt=""><span
                                class="hint__training">Нарушение</span>
                    </div>
                    <div class="hint__row hint__row--ill"><span class="hint__btn" data-type="ill">Добавить</span>
                        <img class="hint__ico" src="../images/calendar/event_ill.png" alt=""><span
                                class="hint__training">Плохое самочувствие</span>
                    </div>
                    <?php
                    if (\app\helpers\My::isGirl()) {
                        ?>
                        <div class="hint__row hint__row--care"><span class="hint__btn" data-type="care">Добавить</span>
                            <img class="hint__ico" src="../images/calendar/event_cream.png" alt=""><span
                                    class="hint__training">Уход</span>
                        </div>
                        <div class="hint__row hint__row--blood"><span class="hint__btn"
                                                                      data-type="blood">Добавить</span>
                            <img class="hint__ico" src="../images/calendar/event_blood.png" alt=""><span
                                    class="hint__training">Критические дни</span>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix">
                <div class="calendar-wrapper__left"><a class="btn btn--addtoday js-calendar-addtoday"
                                                       href="#">Добавить</a>
                </div>
            </div>
        </div>
    </div>
<?= $this->render('../../popups/calendar') ?>
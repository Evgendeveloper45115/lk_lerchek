<?php

/* @var $this yii\web\View */

/* @var $member \app\models\GroupMember */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Прогресс';
$active_menu = 'progress';

?>
<div class="progress_load_tittle"><?= $week == 0 ? 'Начало' : $week . '-я неделя' ?></div>
<div class="progress_load_tittle_after">Загрузите свои результаты</div>
<div class="load_form">
    <?php
    $form = ActiveForm::begin([
        'validateOnChange' => false,
        'validateOnBlur' => false,
        'options' => ['class' => 'progress_add_form', 'id' => 'progress_add_form',
            'enctype' => false,
        ],
    ]);
    ?>
    <?= $form->field($model, 'member_id')->hiddenInput(['value' => Yii::$app->user->identity->groupMember->id])->label(false) ?>
    <?= $form->field($model, 'week')->hiddenInput(['value' => $week])->label(false) ?>
    <?php
    if ($week == 0 || $week == 4) {
        ?>
        <div class="progress_load_form_photo">
            <img src="">
            <label id="first_image" class="progress_load_form_photo_upload">
                                <span class="text"><span class="desktop">Перетащите фотографию в эту область или кликните, чтобы загрузить вручную</span><span
                                            class="mobile">Загрузите фотографию</span></span>
                <?= $form->field($model, 'photo_form')->fileInput()
                    ->label(false) ?>
            </label>
            <label id="second_image" class="progress_load_form_photo_upload" style="display: none">
                                <span class="text"><span class="desktop">Перетащите фотографию в эту область или кликните, чтобы загрузить вручную</span><span
                                            class="mobile">Загрузите фотографию</span></span>
                <?= $form->field($model, 'photo_form_second')->fileInput()
                    ->label(false) ?>
            </label>
            <label id="third-image" class="progress_load_form_photo_upload" style="display: none">
                                <span class="text"><span class="desktop">Перетащите фотографию в эту область или кликните, чтобы загрузить вручную</span><span
                                            class="mobile">Загрузите фотографию</span></span>
                <?= $form->field($model, 'photo_form_third')->fileInput()
                    ->label(false) ?>
            </label>
            <div class="progress_load_form_photo_buttons">
                <span href="#" data-id="first_image" class="button button_green">Спереди</span>
                <span href="#" data-id="second_image" class="button button_green_transparent">Сзади</span>
                <span href="#" data-id="third-image" class="button button_green_transparent">Сбоку</span>
            </div>
            <div>
                <?= $form->field(new \app\models\GroupMember(), 'video_url')->textInput(['placeholder' => 'ссылка на видео'])->label(false) ?>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="progress_load_form_table">
        <table>
            <tbody>
            <tr>
                <td><label>Вес (кг)</label></td>
                <td> <?= $form->field($model, 'weight')->textInput()->label(false) ?></td>
            </tr>
            <tr>
                <td><label>Объем груди (см)</label></td>
                <td><?= $form->field($model, 'bust')->textInput()->label(false) ?></td>
            </tr>
            <tr>
                <td><label>Объем талии (см)</label></td>
                <td><?= $form->field($model, 'waist')->textInput()->label(false) ?></td>
            </tr>
            <tr>
                <td><label>Объем ягодиц (см)</label></td>
                <td><?= $form->field($model, 'butt')->textInput()->label(false) ?></td>
            </tr>
            <tr>
                <td><label>Объем бедра (см)</label></td>
                <td> <?= $form->field($model, 'hip')->textInput()->label(false) ?></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="load_form_button">
        <?= Html::submitButton('Сохранить', ['class' => 'button button_green button_big']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
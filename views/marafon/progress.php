<?php

/* @var $this yii\web\View */

/* @var $member \app\models\GroupMember */
/* @var $display bool */
/* @var $popup \app\models\Popups */

/* @var $progress \app\models\Progress[] */

use yii\helpers\Url;

$this->title = 'Прогресс';
\app\helpers\My::getAccess();
?>
<h1>Прогресс</h1>


<?php
if (isset($_GET['success'])) {
    ?>
    <div id="w3-error" class="alert-success alert fade in">
        <i class="icon fa fa-check"></i> Успешно сохранено!
    </div>
    <?php
}
?>
<?php
if ($display) {
    if ($popup) {
        echo '<h3 style="margin-bottom:20px">' . $member->getUserFIO() . ' ' . $popup->description . '</h3>';
    }
    ?>
    <div class="tittle_afterh1 tittle_afterh1_green_bottom zamery_download">Загрузите свои результаты по неделям</div>

    <div class="tittle_afterh1 tittle_afterh1_green_bottom zamery_link pulse">Как делать замеры</div>
    <!-- <div class="tittle_afterh1 tittle_afterh1_green_bottom video_how_link pulse" style="text-decoration: underline;">Что
         нужно для участия в конкурсе
     </div>-->

    <a href="<?= Url::to(['faq', 'type' => 3]) ?>"
       class="tittle_afterh1 tittle_afterh1_green_bottom pulse pulse_custom">FAQ</a>
    <div>
        <div class="continue" data-value="<?= $member->payment->continue_program ?>"></div>
    </div>
    <div class="progress_list clearfix">
        <div style="text-align: center;display: none" class="zamery_img">
            <div class="training_item">
                <div class="training_item_video training_item_video_custom">
                    <?= \yii\helpers\Html::img('/images/project/zamery.png') /*Новая картинка zamery.png*/ ?>
                </div>
                <div class="training_item_tittle">Как правильно делать замеры</div>
            </div>
        </div>
        <div style="text-align: center;display: none" class="video_how">
            <div class="training_item training_item_video_instruction">
                <div class="training_item_tittle">Инструкция по оформлению фото и видео для участия в голосовании за
                    призовые места
                </div>
                <div class="training_item_video training_item_video_custom description_winner">
                    <strong>Правильно заполненный отчет по фото и видео – это необходимое условие для борьбы за призы
                        марафона. Поэтому будьте внимательны и занесите все необходимые данные.</strong>
                    <br>1. У вас должны быть загружены фото в раздел НАЧАЛО и 4я неделя. Соблюдайте правила фото (Дата.
                    Кодовое слово. Одинаковое белье, фон, свет. Без позирования. Одинаковый ракурс.)
                    <br>2. В раздел коллаж вам необходимо сохранить коллаж из ваших ДО и ПОСЛЕ. Сделать его можно
                    самостоятельно в любой программе. Collageable, Photogrid, Layout. Любая программа на вашем
                    устройстве. Удалите водяные знаки программы.
                    <br>3. Вам необходимо сохранить свои видео ДО и После с открытым лицом В любом хранилище (ГуглДиск,
                    ЯндексДиск или любое другое) и прикрепить ссылку на него в окошке – ЗАГРУЗИТЬ ССЫЛКУ НА ВИДЕО.
                    Проверьте, чтобы был открыт доступ к папке с вашими данными.
                    <br><strong>ВНИМАНИЕ! ПРОВЕРЬТЕ СЕБЯ</strong>
                    <br>1.       ФОТО "ДО" (3 ШТ.)
                    <br>2.       ФОТО "ПОСЛЕ" (3 ШТ.)
                    <br>3.       КОЛЛАЖ (1 ШТ.)
                    <br>4.       ССЫЛКА НА ВИДЕО.
                </div>
            </div>
        </div>
        <?php
        $arr = $member->getWeeks();
        array_unshift($arr, 0);
        ?>
        <div class="progress_download">
            <?php
            foreach ($arr as $week) {
                ?>
                <div class="progress_item <?= Yii::$app->devicedetect->isMobile() ? 'mobile_progress_item' : null ?><?= Yii::$app->devicedetect->isMobile() && $week != 0 && isset($progress[$week]) && $week != 4 && isset($progress[$week]) ? ' mobile_progress_item_no_photo' : null ?>">
                    <?php
                    if ($progress[$week]) {
                        ?>

                        <a onclick="return confirm('Вы уверены что хотите удалить прогресс за неделю?');"
                           href="<?= Url::to(['marafon/progress-delete', 'id' => $progress[$week]->id]) ?>"
                           class="close_popup close_progress" data-id="<?= $progress[$week]->id ?>"></a>
                        <?php
                    }
                    ?>
                    <?php
                    if (isset($progress[$week]) && $week == 0 || isset($progress[$week]) && $week == end($member->getWeeks()) || isset($progress[$week]) && $week == 4 && $member->payment->continue_program) {
                        ?>
                        <div class="progress_item_slider owl-carousel">
                            <div class="progress_item_slider_item"
                                 id="<?= pathinfo($progress[$week]->photo, PATHINFO_FILENAME) ?>"
                                 style="background-image: url(/uploads/progress/<?= $progress[$week]->photo ?>?v=<?= time() ?>)">
                                <div class="progress_item_slider_item_buttons">
                                    <?php
                                    if ($week == 0 || $week == 4) {
                                        ?>
                                        <div class="button button_green rotateIn"
                                             data-photo="<?= $progress[$week]->photo ?>">Повернуть
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <a id="show-table" href="#" class="button button_green">Подробнее</a>
                                </div>
                            </div>
                            <div class="progress_item_slider_item"
                                 id="<?= pathinfo($progress[$week]->photo_second, PATHINFO_FILENAME) ?>"
                                 style="background-image: url(/uploads/progress/<?= $progress[$week]->photo_second ?>?v=<?= time() ?>)">
                                <div class="progress_item_slider_item_buttons">
                                    <?php
                                    if ($week == 0 || $week == 4) {
                                        ?>
                                        <div class="button button_green rotateIn"
                                             data-photo="<?= $progress[$week]->photo_second ?>">Повернуть
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <a id="show-table" href="#" class="button button_green">Подробнее</a>
                                </div>
                            </div>
                            <div class="progress_item_slider_item"
                                 id="<?= pathinfo($progress[$week]->photo_third, PATHINFO_FILENAME) ?>"
                                 style="background-image: url(/uploads/progress/<?= $progress[$week]->photo_third ?>?v=<?= time() ?>)">
                                <div class="progress_item_slider_item_buttons">
                                    <?php
                                    if ($week == 0 || $week == 4) {
                                        ?>
                                        <div class="button button_green rotateIn"
                                             data-photo="<?= $progress[$week]->photo_third ?>">Повернуть
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <a id="show-table" href="#" class="button button_green">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        <div class="progress_item_tb" id="zero-week">
                            <div class="progress_item_td without_button with_result" style="padding-top: 0">
                                <div class="progress_item_result">
                                    <div class="load_form">
                                        <?php
                                        $form = \yii\bootstrap\ActiveForm::begin([
                                            'validateOnChange' => true,
                                            'validateOnBlur' => true,
                                            'options' => ['class' => 'progress_add_form',
                                                'enctype' => false,
                                            ],
                                        ]);
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($progress[$week], 'weight')->textInput() ?>
                                                <?= $form->field($progress[$week], 'bust')->textInput() ?>
                                                <?= $form->field($progress[$week], 'waist')->textInput() ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($progress[$week], 'butt')->textInput() ?>
                                                <?= $form->field($progress[$week], 'hip')->textInput() ?>
                                                <div class="form-group field-progress-hip">
                                                    <label style="opacity: 0" class="control-label button_edit_label"
                                                           for="progress-hip">label</label>
                                                    <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'button_edit']) ?>

                                                    <p class="help-block help-block-error"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?= $form->field($progress[$week], 'member_id')->hiddenInput()->label(false) ?>
                                        <?= $form->field($progress[$week], 'week')->hiddenInput()->label(false) ?>


                                        <?php \yii\bootstrap\ActiveForm::end(); ?>
                                    </div>
                                </div>
                                <a id="show-button" href="#" class="button button_green">Назад</a>
                            </div>
                        </div>

                    <?php } elseif (isset($progress[$week]) && $week > 0) {
                        ?>
                        <div class="progress_item_tb">
                            <div class="progress_item_td without_button with_result">
                                <div class="progress_item_tittle"><?= $week ?>-я неделя</div>
                                <div class="progress_item_result">
                                    <div class="load_form load_form_no_photo">
                                        <?php
                                        $form = \yii\bootstrap\ActiveForm::begin([
                                            'validateOnChange' => true,
                                            'validateOnBlur' => true,
                                            'options' => ['class' => 'progress_add_form',
                                                'enctype' => false,
                                            ],
                                        ]);
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($progress[$week], 'weight')->textInput() ?>
                                                <?= $form->field($progress[$week], 'bust')->textInput() ?>
                                                <?= $form->field($progress[$week], 'waist')->textInput() ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($progress[$week], 'butt')->textInput() ?>
                                                <?= $form->field($progress[$week], 'hip')->textInput() ?>
                                                <div class="form-group field-progress-hip">
                                                    <label style="opacity: 0" class="control-label button_edit_label"
                                                           for="progress-hip">label</label>
                                                    <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'button_edit']) ?>

                                                    <p class="help-block help-block-error"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?= $form->field($progress[$week], 'member_id')->hiddenInput()->label(false) ?>
                                        <?= $form->field($progress[$week], 'week')->hiddenInput()->label(false) ?>


                                        <?php \yii\bootstrap\ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    } elseif (!isset($progress[$week]) && $week === 0) {
                        ?>
                        <div class="progress_item_tb">
                            <div class="progress_item_td">
                                <div class="progress_item_tittle">Начало</div>
                                <a href="#" data-week="<?= $week ?>"
                                   class="button button_green progress_load_opener">Загрузить замеры</a>
                            </div>
                        </div>
                        <?php
                    } elseif (!isset($progress[$week]) && $week > 0) {
                        ?>
                        <div class="progress_item_tb">
                            <div class="progress_item_td">
                                <div class="progress_item_tittle"><?= $week ?>-я неделя</div>
                                <?php
                                if ($member->getCurrentWeekReportProgress() >= $week) {
                                    echo '<a data-week=' . $week . ' href="#" class="button button_green progress_load_opener">Загрузить замеры</a>';
                                } else {
                                    echo '<a data-week=' . $week . ' href="#" class="button button_green progress_popup_opener">Загрузить замеры</a>';
                                }
                                ?>

                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            if ($last_progress) {
                ?>
                <div class="progress_item" style="border: 0;">
                    <?php
                    if ($member->avatar) {
                        ?>
                        <a onclick="return confirm('Вы уверены что хотите удалить коллаж');"
                           href="/marafon/collage-delete" class="close_popup close_progress"></a>
                        <?php
                    }
                    ?>
                    <div class="progress_item_tb">
                        <?php
                        $form = \yii\widgets\ActiveForm::begin([
                            'validateOnChange' => false,
                            'validateOnBlur' => false,
                            'options' => ['class' => 'progress_add_form', 'id' => 'progress_add_form',
                                'enctype' => false,
                            ],
                        ]);
                        ?>
                        <div class="progress_load_form_photo_custom">
                            <label class="progress_load_form_photo_upload progress_load_form_photo_upload_custom"
                                   style="<?= $member->avatar ? "background-image: url('/uploads/progress/$member->avatar'); background-size: contain" : null ?>">
                                <span style="color:<?= !$member->avatar ? '#2b343c' : 'white' ?>" class="text"><span
                                            class="desktop"><?= $member->avatar ? 'Редактировать' : 'Перетащите коллаж в эту область или кликните, чтобы загрузить вручную' ?></span><span
                                            class="mobile"><?= $member->avatar ? 'Редактировать' : 'Загрузите коллаж' ?></span></span>

                                <?= $form->field(new \app\models\Progress(), 'photo_form')->fileInput()
                                    ->label(false) ?>
                            </label>
                        </div>
                        <?php \yii\widgets\ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="form-group video_link_progress">
                    <label class="control-label" for="progress-butt">Ссылка на видео участника</label>
                    <input type="text" id="video_link_progress" class="form-control" name="Progress[butt]"
                           placeholder="Видео для участия за призы"
                           value="<?= $member->video_url ?>">
                    <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'button_edit button_edit_video_save']) ?>

                </div>


                <?php
            }
            ?>
        </div>
    </div>
    <?= Yii::$app->controller->renderPartial('../../popups/progress', ['member' => $member]) ?>
    <?php

} else {
    if ($popup) {
        echo '<h3 style="margin-bottom:20px">' . $member->getUserFIO() . ' ' . $popup->description . '</h3>';
    }
    echo "<br><div class=tittle_afterh1>Дни замеров подошли к концу=(</div>";
} ?>

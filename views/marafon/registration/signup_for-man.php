<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\SignupForm */

/* @var $detect \alexandernst\devicedetect\DeviceDetect */

use app\models\Exercise;
use app\models\GroupMember;
use app\models\Ration;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use \kartik\popover\PopoverX;

$this->params['page_type'] = 'signup';
$this->title = 'Анкета';
echo Yii::$app->controller->renderPartial(('@app/popups/help_button'));
?>
<div class="page_signup page_with_menu page_purple page_content clearfix">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    if (!$validateEmail) {
        ?>
        <ul style="font-weight: bold">
            <li>
                Старт марафона - 22 марта
            </li>
            <li>Доступ к личному кабинету откроется за 2 дня до старта марафона.</li>
            <li>Также за 2 дня до старта вам на почту и по смс придет ссылка на телеграм-канал марафона.</li>
            <li>Зайти в личный кабинет после регистрации (заполнения анкеты) можно по этой ссылке:
                lk.lerchek.ru
            </li>
            <li>Логин - имейл, который указывали при оплате.</li>
            <li>Пароль - создаете самостоятельно в анкете.</li>
            <li>ВАЖНО! Сохраните сразу же эти данные для входа</li>
        </ul>

        <?php
    }
    ?>

    <?php if ($is_exist) { ?>
        <p>Пользователь с таким email существует, пожалуйста, войдите через <a href="/">форму входа</a></p>
    <?php } ?>
    <?php if ($validateEmail) { ?>
        <p>Пользователь с таким email не оплачен, перейдите на <a style="color: #f52950;"
                                                                  href="/marafon/payment?type=<?= $_GET['type'] ?>">форму
                оплаты</a> и
            заполните анкету</p>
        <?php
    }
    ?>
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
        'id' => 'qwe',
        //  'validateOnBlur' => false,
        'validateOnType' => true,
        'fieldConfig' => [
            'options' => ['class' => 'form_group clearfix'],
            'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
        ],
    ]); ?>

    <?= $form->field($model, 'last_name')->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'first_name')->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'patronymic')->textInput(['autocomplete' => 'off']) ?>

    <?= $form->field($model, 'age')->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'height')->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'weight')->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'bust', [
        'template' => "<div class='left_col'>{label}\n<div class='hint'>заполняем по желанию</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
    ])->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'waist', [
        'template' => "<div class='left_col'>{label}\n<div class='hint'>заполняем по желанию</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
    ])->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'hip', [
        'template' => "<div class='left_col'>{label}\n<div class='hint'>заполняем по желанию</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
    ])->textInput(['autocomplete' => 'off']) ?>

    <?= $form->field($model, 'city')->textInput() ?>
    <?= $form->field($model, 'family')->radioList([
        1 => 'Женат',
        2 => 'Не женат',
    ]); ?>
    <?= $form->field($model, 'childs', [
        'template' => "<div class='left_col'>{label}\n<div class='hint'>если у вас нет детей, поставьте в этом пункте - 0</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
    ])->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'telegram')->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'vk_nick', [
        'template' => "<div class='left_col'>{label}\n<div class='hint'>если у вас нет профиля в инстаграм, напишите – нет</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
    ])->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'parthner', [
        'template' => "<div class='left_col'>{label}\n<div class='hint'>если вы участвуете самостоятельно, напишите в поле ниже – нет</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
    ])->textInput(['autocomplete' => 'off']) ?>

    <?= $form->field($model, 'physical_activity')->radioList($model::$physical_activities_man, ['class' => 'physical_activity'])->label('Ваша физическая активность ' . Html::img('/images/project/help-button.png', ['class' => 'help_img'])) ?>
    <?= $form->field($model, 'training_type')->radioList(\app\models\Training::getTrainingType()) ?>
    <?php
    ?>
    <?= $form->field($model, 'veg')->radioList([
        1 => 'Веганский (без продуктов животного происхождения)',
        2 => 'Особенный (меню с продуктами средней и высокой ценовой категории)',
        3 => 'Парный (меню для тех, кто участвует в марафоне с парой)',
        5 => 'Базовый (меню с базовым недорогим набором продуктов)',
        10 => 'Постное меню',
    ]);
    ?>
    <?php
    ?>
    <?= $form->field($model, 'ration_type')->radioList(GroupMember::getTypes()) ?>
    <?= $form->field($model, 'gv_info')->hiddenInput(['value' => 1])->label(false) ?>

    <?= $form->field($model, 'smoke')->radioList([
        1 => 'Да',
        2 => 'Нет',
    ]) ?>
    <?= $form->field($model, 'alcohol')->radioList(GroupMember::getAlcoholType()) ?>
    <?= $form->field($model, 'pregnant')->hiddenInput(['value' => 1])->label(false) ?>
    <?= $form->field($model, 'food_last_3days')->textarea()->label('Какие цели ставите на ближайшие 4 недели?') ?>

    <?= Html::hiddenInput('validateEmail', $validateEmail) ?>
    <div class="form_group clearfix">
        <div class="overlay not_time static" style="opacity: 0.7"></div>
        <label>&nbsp;</label>

        <div class="col_right">
            <div class="form_group">
                <label class="control-label" for="signupform-login">E-mail (ваш логин)</label>
                <div class="col_right"><?= Html::textInput('email', Yii::$app->request->get('email'), ['autocomplete' => 'off', 'disabled' => 'disabled', 'id' => 'signupform-login']); ?></div>
            </div>
            <?php
            echo $form->field($model, 'password')->passwordInput(['class' => 'sign_password', 'placeholder' => 'Это ваш пароль для входа в ЛК', 'style' => 'margin-bottom:15px', 'autocomplete' => 'off']);
            if (!$validateEmail) {
                echo Html::submitButton('Подтвердить', ['class' => 'new_button_signLog']);
            }
            ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>
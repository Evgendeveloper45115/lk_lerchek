<?php

/* @var $this yii\web\View */
/* @var $displayTrouble bool */

/* @var $question \app\models\Questions */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Вопросы?';

\app\helpers\My::getAccess();
\app\helpers\My::getTYFlash();
?>

<h1><?= $this->title ?></h1>

<div class="tab-buttons-wrapper" data-tabs-wrapper=".tabs-wrapper">
    <div class="tab-button tab-button__active" data-tab=".tab-question">Вопросы</div>
    <div class="tab-button" data-tab=".tab-answers">Ответы</div>
</div>
<div class="tabs-wrapper">
    <div class="tab tab__visible tab-question">
        <div class="tittle_afterh1_light">
            У вас есть вопросы по питанию или программе тренировок? Задайте их в поле ниже.
            Ваш куратор ответит Вам в течение дня, в рабочее время.
        </div>
        <div class="question_form">
            <?php
            if ($displayTrouble) {
                $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'options' => ['class' => 'form_group clearfix'],
                        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
                    ],
                ]);
                ?>
                <?= $form->field($question, 'question')->textarea()->label(false) ?>
                <div style="margin: 10px">
                    <?= $form->field($question, 'img')->fileInput() ?>
                </div>
                <?= $form->field($question, 'member_id')->hiddenInput()->label(false) ?>
                <div class="form_group clearfix">
                    <div>
                        <?= Html::submitButton('Отправить', ['class' => 'button button_green button_big disabled']) ?>
                    </div>
                </div>

                <?php ActiveForm::end();
            } else {
                echo '<span style="font-weight: bold">Вопросы больше не доступны=(</span>';
            }
            ?>

        </div>
    </div>
    <div class="tab tab-answers">
        <?php if (!empty($questionsTwo)) {
            foreach ($questionsTwo as $question) {
                ?>
                <div class="questions">
                    <div class="questions_q">
                        <div><b>Вопрос:</b> <?= $question->question ?></div>
                    </div>
                    <div class="questions_a">
                        <b>Ответ от куратора <?= $question->userCurator->groupMembers[0]->first_name ?>:</b>
                        <?php
                        echo $question->answer;
                        ?>
                    </div>
                </div>
                <?php
            }
        } else { ?>
            <div class="tittle_afterh1_light">Пока у вас нет ответов.</div>
        <?php }
        ?>
    </div>
</div>
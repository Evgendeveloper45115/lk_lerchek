<?php

/* @var $this \yii\web\View */

/* @var $member \app\models\GroupMember */

/* @var $instruction \app\models\EveryDay */


use yii\helpers\Url;

$this->title = 'Инструкция';
$access = \app\helpers\My::getAccess();
$name = $member->getUserFIO();
?>
<h1>Инструкция</h1>
<?php
if ($access) {
    ?>
    <div class="tittle_afterh1 tittle_afterh1_blue_bottom">Здравствуйте, <?= $name ?></div>
    <div class="content_text">
        <div class="text_group text_group_withimage">
            <div style="margin-top: 10px">Поздравляем с началом нового пути!</div>
            <div style="margin-top: 10px">
                Ближайший месяц нам нужно усердно потрудиться, чтобы достичь тех целей, что Вы перед собой поставили! Мы
                советуем начинать действовать уже сейчас! Не завтра или послезавтра, а прямо сегодня, если вы хотите
                побороться за главные призы. В конце инструкции вас ждут первые задания.
            </div>

            <div style="margin-top: 10px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['instruction']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/instruction.png)"></span>
                        </div>
                    </a>
                </div>

                В разделе "<a
                        href="<?= Url::to(['instruction']) ?>">Инструкция</a>",
                в котором сейчас находитесь, вы найдете не только краткий обзор каждого раздела личного кабинета, но и
                задачи на текущий день. Если вы перелистнёте страницу до конца, то увидите, какие именно задачи вам
                нужно
                выполнить СЕГОДНЯ! Не забывайте их просматривать каждый день и планировать свои действия заранее.
            </div>
            <div style="margin-top: 10px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['food']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/food.png)"></span>
                        </div>
                    </a>
                </div>

                В разделе "<a
                        href="<?= Url::to(['food']) ?>">Питание</a>"
                для вас составлено меню, посчитанное в граммах.
                <?php
                if (\app\helpers\My::isGirl()) {
                    ?>
                    Каждые 2 дня в Базовом, Базовом NEW, Особенном, Вегетарианском, Альтернативном меню,
                    а также AntiAge - повторяются, это сделано специально, чтобы максимально сэкономить
                    Вам время и силы на приготовление еды. В меню для пар представлено 2 варианта меню,
                    которые тоже чередуются каждые два дня, это меню менее разнообразно по продуктам,
                    но очень действенно. Вы можете выбрать любой вид меню! Это опция доступна у вас в настройках профиля.
                    Доступ к меню следующей недели открывается в ночь на 6-ой день программы
                    (те, кто приступает в понедельник, - это ночь с пятницы на субботу), так что время будет,
                    чтобы закупить продукты на следующую неделю. Также в этом разделе вы найдете список продуктов на неделю,
                    список рекомендуемых продуктов, ответы на частые вопросы по питанию, информация о детокс-дне,
                    а также дополнительные рецепты.
                    <?php
                } else {
                    ?>
                    У вас будет 4 варианта меню, которые будут повторяться в течение недели. Доступ к новому меню следующей
                    недели открывается в ночь на 6-ой день программы (те, кто приступает в понедельник, - это ночь с пятницы на субботу),
                    так что время будет, чтобы закупить продукты на следующую неделю. Также в этом разделе вы найдете список продуктов на неделю,
                    список рекомендуемых продуктов, ответы на частые вопросы по питанию.
                    <?php
                }
                ?>
            </div>
            <div style="margin-top: 10px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['trainings']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/treni.png)"></span>
                        </div>
                    </a>
                </div>
                В разделе "<a
                        href="<?= Url::to(['trainings']) ?>">Тренировки</a>"
                для вас подготовлена программа тренировок. Каждую неделю вам будет доступна новая программа тренировок.
                Тренировки у нас поделены на уровни «Начальный и «Продвинутый». Выбирайте тот, что подходит именно вам,
                все
                варианты открыты. Всего за неделю нужно сделать 4 тренировки.
                Также в этом разделе вы найдете информацию о кардио, вакууме и ответы на частые вопросы о тренировках.
            </div>
            <div style="margin-top: 10px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['communication']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/chat.png)"></span>
                        </div>
                    </a>

                </div>

                Вопросы, которые у вас будут возникать по ходу марафона, вы можете задавать в разделе "Вопросы".
                Кураторы
                отвечают на вопросы ежедневно в рабочее время. Первое время может быть задержка в ответах, но это только
                первые 1-5 дней. Дальше - будьте уверены, ответ на свой вопрос вы будете получать почти мгновенно.
            </div>
            <div style="margin-top: 50px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['progress']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/progress.png)"></span>
                        </div>
                    </a>

                </div>
                Не забывайте вовремя заносить свой прогресс в табличку в разделе "Прогресс". Изучите там ответы на часто
                задаваемые вопросы, а также видео-инструкции. <a
                        href="<?= Url::to(['progress']) ?>"></a>
            </div>
            <div style="margin-top: 50px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['report']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/rezult.png)"></span>
                        </div>
                    </a>

                </div>

                Раз в неделю по воскресеньям обязательно заглядывайте в раздел<a
                        href="<?= Url::to(['report']) ?>">
                    «Отчеты»!</a> Подведем итоги прошедшей недели, а
                также вас будет ждать задание на следующую неделю.
            </div>
            <?php
            if (\app\helpers\My::isGirl()) {
                ?>
                <div style="margin-top: 50px">
                    <div class="icon_heigh">
                        <a href="<?= Url::to(['care-instruction']) ?>">
                            <div class="login_page_programm"><span class="icon"
                                                                   style="background-image: url(/images/index-icons/care.png)"></span>
                            </div>
                        </a>
                    </div>
                    В разделе «Уход» вас ждет подробная информация об уходе за телом: когда и как делать скрабы,
                    обертывания
                    и
                    т.д. Схемы применения.
                </div>
                <?php
            }
            ?>
            <div style="margin-top: 50px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['training-calendar']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/calendar.png)"></span>
                        </div>
                    </a>

                </div>
                Очень рекомендуем вести «Календарь активности»! Он будет вас дисциплинировать, помогать анализировать
                результат и не забывать вовремя проводить тренировки.
            </div>
            <div style="margin-top: 50px">
                <div class="icon_heigh">
                    <a href="<?= Url::to(['whats-app']) ?>">
                        <div class="login_page_programm"><span class="icon"
                                                               style="background-image: url(/images/index-icons/chat.png)"></span>
                        </div>
                    </a>

                </div>
                В этом разделе вы найдете ссылку на телеграм-чат и канал марафона.
                В телеграм-канале будет ежедневно публиковаться полезная информация
                от <?= \app\helpers\My::isGirl() ? 'Леры' : 'Артёма' ?>, а в чатах вы можете общаться,
                обмениваться опытом и просто дружить!
            </div>
            <div>&nbsp;</div>
        </div>
        <div class="text_group">
            <div class="text_group_tittle">Ваши задачи на сегодняшний день</div>
            <?php
            if ($instruction) {
                echo $instruction->text;
            }
            ?>
        </div>
    </div>
    <?php
}
?>
<style>
    .login_page_programm {
        width: auto !important;
        margin-bottom: 0px;
    }

    @media (max-width: 1024px) {
        .icon_heigh {
            height: 20vw;
        }

        .icon_heigh .icon {
            left: 30vw;
            top: 8vw;
        }
    }

</style>
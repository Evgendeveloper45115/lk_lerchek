<?php

use app\helpers\My;

/* @var $this yii\web\View */

/* @var $groupMember \app\models\GroupMember */

/* @var $user \app\models\User */

/* @var $every_day \app\models\EveryDay */

use yii\helpers\Url;

$this->title = 'Личный кабинет LerChek';
$text = explode('!', $every_day->text);
?>
    <div class="login_page__main">
        <div class="login_page_main_screen_content">
            <div class="main_screen_content_tittle2"><?= My::getMainText() ?></div>
            <?php
            if (is_string($user->groupMember->getCurrentDay())) {
                ?>
                <div class="main_screen_content_text" style="margin-bottom: 5px; font-size: 30px">
                    <strong><?= $user->groupMember->getCurrentDay() ?></strong>
                </div>
                <?php
            } else {
                ?>
                <div class="main_screen_content_text" style="margin-bottom: 5px; font-size: 30px"> Идет
                    <strong><?= $user->groupMember->getCurrentDay() ?>-й</strong>
                    день программы
                </div>
                <?php
            }
            ?>
            <div class="main_screen_content_text"
                 style="margin-bottom: 1px"><?= isset($text[0]) && $text[0] != '' ? $text[0] . '!' : null ?>
            </div>
            <div class="main_screen_content_text2">Весь список задач на сегодня читайте в разделе: <a
                        href="<?= Url::to(['instruction']) ?>"
                        class="main_screen_content_a">Инструкция</a>
            </div>
        </div>
    </div>
<?= Yii::$app->controller->renderPartial('index/' . My::$type_program[$user->session_type]) ?>
<?php

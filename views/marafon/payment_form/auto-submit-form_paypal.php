<form method="POST" id="auto_submit_form" class="clearfix payment_form" action="<?= $url ?>">
    <input name="cmd" value="_xclick" type="hidden"/>
    <input name="business" value="<?= $paypalEmail ?>" type="hidden"/>
    <input name="receiver_email" value="<?= $paypalEmail ?>" type="hidden"/>
    <input name="item_number" value="<?= $user->id ?>" type="hidden">
    <input name="amount" value="<?= $cost ?>" type="hidden">
    <input name="item_name" value="<?= $paypalDesc ?>" type="hidden"/>
    <input name="no_shipping" value="1" type="hidden">
    <input name="return" value="https://lk.lerchek.ru/payment/success?user_id=<?= $user->id ?>" type="hidden">
    <input name="currency_code" value="RUB" type="hidden">
</form>
<script type="text/javascript">
    document.forms[0].submit();
</script>
<?php
/**
 * @var $this \yii\web\View
 * @var $user \app\models\User
 * @var $url string
 * @var $shopId string
 * @var $scid string
 * @var $cost string
 * @var $continue string
 * @var $paymentType string
 * @var $payment_amount string
 * @var $recipe string
 * @var $retail_id string
 */
?>
<form method="POST" id="auto_submit_form" class="clearfix payment_form" action="<?= $url ?>">
    <input name="shopId" value="<?= $shopId ?>" type="hidden"/>
    <input name="scid" value="<?= $scid ?>" type="hidden"/>
    <input name="sum"
           value="<?= $cost ?>"
           type="hidden">
    <input name="orderNumber" value="<?= $user->id ?>-<?= time() ?>" type="hidden"/>
    <input name="customerNumber" value="<?= trim($user->email) ?>" type="hidden"/>
    <input name="email" value="<?= $user->email ?>" type="hidden"/>
    <input name="email_man" value="<?= $_POST['User']['email_man'] ?>" type="hidden"/>
    <input name="ref" value="<?= isset($_GET['ref']) ? $_GET['ref'] : null ?>" type="hidden"/>
    <input name="continue" value="<?= Yii::$app->user->isGuest ? 0 : 1 ?>" type="hidden"/>
    <input name="paymentType" value="" type="hidden">
    <input name="ga_param" value="<?= $ga_param ?>" type="hidden">
    <input name="cid_param" value="<?= $cid_param ?>" type="hidden">
    <input name="manWoman" value="<?= Yii::$app->user->isGuest ? $_GET['type'] : $user->groupMember->type_program ?>"
           type="hidden">
    <input name="cps_email" value="<?= $user->email ?>" type="hidden">
    <input name="cps_phone" value="<?= $user->groupMember->phone ?>" type="hidden">
    <input name="type_program" value="<?= $user->groupMember->type_program ?>" type="hidden">
    <input name="retail_id" value="<?= $retail_id ?>" type="hidden">
    <input name="shopSuccessURL"
           value="https://<?= Yii::$app->request->hostName ?>/payment/success?user_id=<?= $user->id ?>"
           type="hidden">
    <input name="shopFailURL" value="https://<?= Yii::$app->request->hostName ?>/payment/fail?user_id=<?= $user->id ?>"
           type="hidden">
    <input name="ym_merchant_receipt" value='<?= $recipe ?>' type="hidden"/>
    <input name="payment_amount" value='<?= $payment_amount ?>' type="hidden"/>
</form>
<script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'RUB',
            'pay': {
                'actionField': {
                    'id': '502',
                    'revenue': '<?=$cost?>',
                },
                'products': [{
                    'name': '<?=Yii::$app->user->isGuest ? \app\helpers\My::$type_program_rus[$_GET['type']] : \app\helpers\My::$type_program_rus[$user->groupMember->type_program]?>',
                    'price': '<?=$cost?>',
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'pay',
        'gtm-ee-event-non-interaction': 'False',
    });

    document.forms[0].submit();
</script>
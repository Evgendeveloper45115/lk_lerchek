<?php

use yii\helpers\Url;

/**
 * @var $member \app\models\GroupMember
 * @var $week int
 * @var $cur_week int
 * @var $url string
 * @var $class string
 */
?>
<div class="motivation_week_item <?= $class ?>">
    <?php

    if ($week <= $cur_week) { ?>
        <a href="<?= Url::to([$url, 'week' => $week]) ?>"
           class="button button_transparent_green2 "><?= $week == 0 ? 'Начало' : $week . '-я неделя' ?></a>
    <?php } elseif ($week >= $cur_week) { ?>
        <span class="button button_transparent_green2 access_popup training_access"><?= $week ?>-я неделя</span>
    <?php } ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $recipes Recipe[] */
/* @var $recipe Recipe */

/* @var $pages Pagination */

/* @var $cur_type string */
/* @var $member \app\models\GroupMember */

/* @var $types [] */

use app\models\Recipe;
use yii\data\Pagination;
use yii\helpers\Url;

$this->title = 'Рецепты';
$access = \app\helpers\My::getAccess();
?>
<h1><?= $member->getRationText() ?></h1>
<?= Yii::$app->controller->renderPartial('food/header_food', ['active' => 'recipes']) ?>
<div class="food_menu_list first_noactive">
    <div class="food_menu__list">
        <div class="food_menu_item <?= $cur_type === null ? 'active' : null ?> "><a
                    href="<?= Url::to(['marafon/recipes']) ?>"
                    class="button button_transparent">Все</a>
        </div>
        <?php foreach ($types as $key => $type) {
            if (isset($types_if[$key])) {
                ?>
                <div class="food_menu_item <?= ($cur_type == $key && $cur_type !== null) ? 'active' : '' ?>"><a
                            href="<?= Url::to(['marafon/recipes', 'type' => $key]) ?>"
                            class="button button_transparent"><?= $type ?></a></div>
            <?php }
        }
        ?>
    </div>
</div>
<?php
if ($access) {
    ?>
    <div class="recipes clearfix">
        <?php
        foreach ($recipes as $recipe) { ?>
            <div class="recipe clearfix" id="<?= $recipe->id ?>">
                <div class="recipe">
                    <a href="<?= Url::to(['marafon/recipe', 'recipe' => $recipe->id]) ?>" class="recipes_popup_opener">
                        <div class="recipe_img"
                             style="background-image: url(/uploads/recipe/<?= $recipe->image ?>)"></div>
                    </a>
                    <div class="recipe_name"><?= $recipe->name ?></div>
                    <a href="<?= Url::to(['marafon/recipe', 'recipe' => $recipe->id]) ?>" class="recipes_popup_opener">Читать
                        целиком →</a>
                </div>
            </div>
        <?php }
        ?>
    </div>
    <?php
}
?>
<?= Yii::$app->controller->renderPartial('../../popups/recipe-json') ?>


<?php

/* @var $this yii\web\View */
/* @var $recipes Recipe[] */
/* @var $recipe Recipe */

/* @var $pages Pagination */

use app\models\Recipe;
use yii\data\Pagination;
use yii\helpers\Url;

?>
<div class="box_shadow"></div>
<div class="recipes_popup_img" style="background-image: url(/uploads/recipe/<?= $recipe->image ?>)"></div>
<div class="recipes_popup_tittle"><?= $recipe->name ?></div>
<p class="small">
    <?= $recipe->text ?>
</p>


<?php

/* @var $this yii\web\View */

/* @var $rationsCustom \app\models\RationComplex[] */

/* @var $member \app\models\GroupMember */

/* @var $detox \app\models\DetoxDay */

use yii\helpers\Url;

$this->title = 'Детокс-день';
$access = \app\helpers\My::getAccess();
?>
<h1><?= $member->getRationText() ?></h1>
<?= Yii::$app->controller->renderPartial('food/header_food', ['active' => 'detox']) ?>
<?php

if ($detox[0] && $access) {
?>
<div class="progress_list clearfix">
    <div style="text-align: center; display: block;" class="zamery_img">
        <div class="training_item">
            <div class="training_item_video">
                <img src="/uploads/detox-day/<?= $detox[0]->image ?>" alt="" style="">
            </div>
            <div class="training_list">
                <?= $detox[0]->text ?>
            </div>
        </div>
        <?php
        }
        ?>

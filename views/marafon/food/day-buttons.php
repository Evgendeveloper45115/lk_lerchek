<?php

use yii\helpers\Url;

/**
 * @var $member \app\models\GroupMember
 * @var $select_number int
 * @var $select_week int
 * @var $url string
 * @var $training string
 */
?>
<div class="food_menu__list">
    <?php
    foreach (range(1, $member->getMaxDay()) as $number) {
        $class = \app\helpers\My::getActiveClassNumber($number, $select_number);
        ?>
        <div class="food_menu_item <?= $class ?>"><a
                    href="<?= Url::to([$url, 'week' => $select_week, 'number' => $number, 'training' => $training]) ?>"
                    class="button button_transparent">День <?= $number ?></a></div>
    <?php }
    ?>
</div>

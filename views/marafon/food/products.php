<?php

/* @var $this yii\web\View */
/* @var $rationsCustom \app\models\RationComplex[] */
/* @var $member \app\models\GroupMember */
/* @var $product \app\models\WeekProducts */
/* @var $select_week string */

/* @var $cur_week string */

use yii\helpers\Url;

$this->title = 'Продукты на неделю';
$access = \app\helpers\My::getAccess();
?>
    <h1><?= $member->getRationText() ?></h1>
<?= Yii::$app->controller->renderPartial('food/header_food', ['active' => 'food-week']) ?>
    <div class="motivation_week">
        <?php
        echo $member->accessButtonPrev($select_week, 'products');
        foreach ($member->getWeeks() as $week) {
            $class = \app\helpers\My::getActiveClassWeek($week, $select_week);
            echo Yii::$app->controller->renderPartial('food/week-buttons', ['url' => 'marafon/products', 'week' => $week, 'class' => $class, 'cur_week' => $cur_week]);
        }
        echo $member->accessButtonNext($select_week, $cur_week, 'products')
        ?>
    </div>
<?php
if ($product && $access) {
    ?>
    <div class="progress_list clearfix">
    <div style="text-align: center; display: block;" class="zamery_img">
        <?php
        if ($product->image) {
            ?>
            <div class="training_item">
                <div class="training_item_video">
                    <img src="/uploads/week-products/<?= $product->image ?>" alt="" style="">
                </div>
            </div>
            <?php
        }
        ?>
        <div class="training_list">
            <?= $product->text ?>
        </div>
    </div>
    <?php
}
?>
<?= Yii::$app->controller->renderPartial('../../popups/dont-time', ['dont_time' => 'Список продуктов будет доступен']) ?>
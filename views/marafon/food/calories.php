<?php
/**
 * @var $img []|string
 * @var $cur_type integer
 * @var $calories \app\models\Calories[]
 * @var $member \app\models\GroupMember
 */

use \yii\helpers\Url;

$access = \app\helpers\My::getAccess();
?>
    <h1><?= $member->getRationText() ?></h1>
<?= Yii::$app->controller->renderPartial('food/header_food', ['active' => 'calories']) ?>
    <div class="food_menu_list">
        <div class="food_menu__list">
            <div class="food_menu_item <?= ($cur_type === null) ? 'active' : '' ?>"><a
                        href="<?= Url::to(['marafon/calories']) ?>" class="button button_transparent">Все</a></div>
            <?php foreach (\app\models\CaloriesCat::find()->getContent()->asArray()->all() as $key => $type) { ?>
                <div class="food_menu_item <?= ($cur_type == $type['id'] && $cur_type !== null) ? 'active' : '' ?>"><a
                            href="<?= Url::to(['marafon/calories', 'cat_id' => $type['id']]) ?>"
                            class="button button_transparent"><?= $type['name'] ?></a></div>
            <?php } ?>
        </div>
    </div>
    <p style="margin-bottom: 25px">
        Правильное сбалансированное питание должно приносить удовольствие, поэтому мы рекомендуем заранее определиться с
        гастрономическими предпочтениями. Ниже мы подготовили список всех продуктов, которые подходят под понятие
        «Здоровое
        питание».
    </p>
<?php
if ($access) {
    ?>
    <div class="food_list clearfix">
        <?php
        if (!empty($calories)) {
            foreach ($calories as $caloriy) {
                ?>
                <div class="food_list_item" style="background-image: url(/uploads/calories/<?= $caloriy->image ?>)">
                    <div class="food_list_item_more">
                        <div class="food_list_item__more">
                            <span style="line-height: 20px;"><?= $caloriy->name ?></span>
                            <span>Ккал: <?= $caloriy->sugar ?></span>
                            <span>Белки: <?= $caloriy->protein ?> г.</span>
                            <span>Жиры: <?= $caloriy->grease ?> г.</span>
                            <span>Углеводы: <?= $caloriy->carbohydrate ?> г.</span>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <?php
}
?>
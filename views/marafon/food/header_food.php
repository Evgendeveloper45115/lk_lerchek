<?php

use \yii\helpers\Url;

/**
 * @var $active string
 * @var $user \app\models\User
 */
?>
    <div class="food_menu_list">
        <div class="food_menu__list">
            <div class="food_menu_item <?= $active == 'food-custom' ? 'active' : null ?>"><a
                        href="<?= Url::to(['marafon/food']) ?>"
                        class="button button_transparent <?= $active == Yii::$app->controller->action->id ? 'active' : null ?>">Детальный
                    рацион</a></div>
            <div class="food_menu_item <?= $active == 'food-week' ? 'active' : null ?>"><a
                        href="<?= Url::to(['marafon/products']) ?>"
                        class="button button_transparent">Список продуктов на неделю</a></div>
            <div class="food_menu_item <?= $active == 'calories' ? 'active' : null ?>"><a
                        href="<?= Url::to(['marafon/calories']) ?>"
                        class="button button_transparent">Список рекомендуемых продуктов</a></div>
            <?php
            if (\app\helpers\My::isGirl()) {
                ?>
                <div class="food_menu_item <?= $active == 'detox' ? 'active' : null ?>"><a
                            href="<?= Url::to(['marafon/detox-day']) ?>"
                            class="button button_transparent">Детокс-день</a></div>
                <div class="food_menu_item <?= $active == 'recipes' ? 'active' : null ?>"><a
                            href="<?= Url::to(['marafon/recipes']) ?>"
                            class="button button_transparent">Доп. рецепты</a>
                </div>
                <div class="food_menu_item <?= $active == 'potato' ? 'active' : null ?>"><a
                            href="<?= Url::to(['marafon/potato']) ?>"
                            class="button button_transparent">Это вам не батат</a>
                </div>
                <?php
            }
            ?>
            <div class="food_menu_item <?= $active == 'faq' ? 'active' : null ?>"><a
                        href="<?= Url::to(['/marafon/faq/', 'type' => 1]) ?>"
                        class="button button_transparent">FAQ по питанию</a></div>


        </div>
    </div>
<?php

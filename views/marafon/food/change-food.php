<?php

/* @var $this yii\web\View */

/* @var $rations [] */

use yii\helpers\Url;

$this->title = 'Изменить рацион';
\app\helpers\My::getAccess();
?>
<h2 style="margin: 30px 0;">Выберите необходимый рацион</h2>
<?php
foreach ($rations as $type => $ration) {
    ?>
    <div style="margin: 0 0 20px;">
        <a href="/marafon/change-food?veg=<?= $type ?>" class="button button_green"><?= $ration ?></a>
    </div>
    <?php
}
?>
<style>
    a.button_green {
        height: auto;
        padding-top: 10px;
        padding-bottom: 8px;
        height: auto;
        line-height: 1.3;
        white-space: normal;
    }

    a.button_green:hover {
        color: #fff;
    }
</style>

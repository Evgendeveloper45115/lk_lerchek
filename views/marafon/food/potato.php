<?php

/* @var $this yii\web\View */

/* @var $rationsCustom \app\models\RationComplex[] */

/* @var $member \app\models\GroupMember */

/* @var $models \app\models\Potato[] */

use yii\helpers\Url;

$this->title = 'Детокс-день';
$access = \app\helpers\My::getAccess();
?>
<h1><?= $member->getRationText() ?></h1>
<?= Yii::$app->controller->renderPartial('food/header_food', ['active' => 'potato']) ?>
<?php
if ($access) {
    ?>
    <div class="training">
        <div class="training_list">
            <?php
            foreach ($models as $model) {
                if ($model->video_link) {
                    ?>
                    <div class="training_item">
                        <div class="training_item_video">
                            <iframe src="https://www.youtube.com/embed/<?= $model->video_link ?>?rel=0&amp;showinfo=0"
                                    frameborder="0"
                                    allow="encrypted-media" allowfullscreen=""></iframe>
                        </div>
                        <div class="training_item_text"><p><?= $model->text ?></p></div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <?php
}
?>

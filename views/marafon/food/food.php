<?php

/* @var $this yii\web\View */

/* @var $rationsCustom \app\models\RationComplex[] */

/* @var $member \app\models\GroupMember */

/* @var $select_week integer */
/* @var $select_number integer */

/* @var $cur_week integer */

use yii\helpers\Url;

$this->title = 'Рационы';
$access = \app\helpers\My::getAccess();
?>

<h1><?= $member->getRationText() ?>
    <a href="/marafon/change-food" class="button button_green button--change-food">Выбрать меню</a>
</h1>
<div class="training">
    <?= Yii::$app->controller->renderPartial('food/header_food', ['active' => 'food-custom']) ?>
    <div class="motivation_week">
        <?php
        echo $member->accessButtonPrev($select_week, 'food');
        foreach ($member->getWeeks() as $week) {
            $class = \app\helpers\My::getActiveClassWeek($week, $select_week);
            echo Yii::$app->controller->renderPartial('food/week-buttons', ['url' => 'marafon/food', 'week' => $week, 'class' => $class, 'cur_week' => $cur_week]);
        }
        echo $member->accessButtonNext($select_week, $cur_week, 'food')
        ?>
    </div>
    <div class="food_menu_list">
        <?= Yii::$app->controller->renderPartial('food/day-buttons', ['url' => 'marafon/food', 'member' => $member, 'select_number' => $select_number, 'select_week' => $select_week]) ?>
    </div>
    <div class="food_list">
        <div class="food_item">
            <div class="food_item_name">
                Завтрак должен быть в течение часа после пробуждения.<br>
                Следующие приемы пищи - по мере возникновения физиологического голода.<br>
                Между приёмами пищи в среднем выдерживаем интервал в 2-4 часа.<br>
                Последний приём пищи должен быть минимум за 1-2 часа до сна, а лучше - за 3.
            </div>

        </div>
        <?php
        if ($access) {
            if (!empty($rationsCustom)) {
                foreach ($rationsCustom as $ration) {
                    ?>
                    <div class="food_item">
                        <div class="food_tittle"><?= \app\models\RationComplex::getDayParts()[$ration['day_part']] ?></div>
                        <div class="food_item_name"><?= $ration['name'] ?></div>
                        <div class="food_item_inner">
                            <div class="food_item_inner_top">
                                <div class="food_item_inner__top clearfix">
                                    <?php
                                    if ($ration['image'] && is_file(Yii::getAlias('@webroot') . "/uploads/food/" . $ration['image'])) {
                                        ?>
                                        <div class="food_item_img"
                                             style='background-image: url(<?= "/uploads/food/" . $ration['image'] ?>)'></div>
                                        <?php
                                    }
                                    ?>
                                    <table>
                                        <tbody>
                                        <?php
                                        foreach ($ration['ingredients'] as $val) {
                                            ?>
                                            <tr>
                                                <td class="ingredients"><?= $val['name'] ?> <?= $val['value'] ? $val['value'] : null ?> <?= $val['count_symbol'] ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Ккал: <?= $ration['calories'] ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Белки: <?= $ration['protein'] ?>г.</td>
                                        <td>Жиры:<?= $ration['grease'] ?>г.</td>
                                        <td>Углеводы: <?= $ration['carbohydrate'] ?>г.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                            if ($ration['description']) {
                                ?>
                                <div class="cooking">
                                    <div class="cooking_tittle">Способ приготовления:</div>
                                    <?= $ration['description'] ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo '<h1><strong>Контент появится в новом потоке, измените меню по кнопке "Выбрать меню"</strong></h1>';
            }

        }
        ?>
    </div>

</div>
<?= Yii::$app->controller->renderPartial('../../popups/dont-time', ['dont_time' => 'Рацион будет доступен']) ?>

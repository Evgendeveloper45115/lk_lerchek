<?php
/**
 * @var $member \app\models\GroupMember
 * @var $user \app\models\User
 */
$this->title = 'Выбор программы';
$user = Yii::$app->user->identity;
?>
    <h1><?= $this->title ?></h1>
<?php
foreach (Yii::$app->session->getAllFlashes() as $key => $flash) {
    echo '<div class="alert-' . $key . '>">' . $flash . '</div>';
}
if (count($user->groupMembers) > 1) {
    ?>
    <div class="program-list">
        <?php
        foreach (array_reverse($user->groupMembers) as $member) {
            ?>
            <a href="<?= \yii\helpers\Url::to(['change-program', 'type' => $member->type_program]) ?>"
               class="program-list__link">
                <img src="/images/marathon/program<?= $member->type_program ?>.png?v=1.0" alt="">
                <?php
                if ($member->group_id && date('Y-m-d H:i:s') >= $member->group->datetime_start) {
                    ?>
                    <span class="program-list__link-txt program-list__link-txt--<?= $member->type_program ?>"><?= implode('<br>(', explode('(', \app\helpers\My::$type_program_rus[$member->type_program])) ?></span>
                    <?php
                } else {
                    ?>
                    <span class="program-list__link-txt program-list__link-txt--<?= $member->type_program ?>">
                        <p style="font-weight: bold">(Ожидайте старта)</p>
                        <?= implode('<br>(', explode('(', \app\helpers\My::$type_program_rus[$member->type_program])) ?>
                    </span>
                    <?php
                }
                ?>
                <span class="program-list__link-hover program-list__link-hover--<?= $member->type_program ?>">Перейти</span>
            </a>
            <?php
        }
        ?>
    </div>
    <?php
}

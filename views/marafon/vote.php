<?php
/**
 * @var $img []|string
 * @var $cur_type integer
 * @var $calories \app\models\Calories[]
 * @var $member \app\models\GroupMember
 * @var $user \app\models\User
 */

use \yii\helpers\Url;

$user = Yii::$app->user->identity;
//\app\helpers\My::getAccess();
?>
<h1>Выбор участников</h1>
<div class="food_menu_list">
    <div class="food_menu__list">
        <div class="food_menu_item <?= ($cat === null) ? 'active' : '' ?>"><a
                    href="<?= Url::to(['marafon/vote']) ?>" class="button button_transparent">Голосование</a></div>

        <div class="food_menu_item <?= $cat == 1 ? 'active' : null ?>"><a
                    href="<?= Url::to(['marafon/vote', 'cat' => 1]) ?>"
                    class="button button_transparent <?= $cat == 1 ? 'active' : null ?>">Инструкция</a>
        </div>
    </div>
</div>
<?php
if ($cat) {
    ?>
    <p style="margin-bottom: 25px">
        <?= Yii::$app->controller->renderPartial('vote-instruction') ?>
    </p>
    <?php
}
?>

<div class="food_list clearfix vote-list">
    <?php
    if (!empty($members) && !$cat) {
        shuffle($members);
        foreach ($members as $member) {
            ?>
            <div class="vote-item">
                <div> Голосов:
                    <span class="count-vote"><?= \app\models\Vote::find()->where(['vote_id' => $member->id])->count() ?></span>
                </div>

                <a href="<?= Url::to(['marafon/vote-img', 'member_id' => $member->id]) ?>" class="recipes_popup_opener">
                    <div class="food_list_item">
                        <img src="/uploads/progress/<?= $member->avatar ?>" alt="">
                    </div>
                </a>
                <?php
                if ((\app\models\Vote::findOne(['vote_id' => $member->id, 'member_id' => Yii::$app->user->identity->groupMember->id])) === null) {
                    ?>
                    <button class="send-vote"
                            data-member="<?= $member->id ?>">Проголосовать
                    </button>
                    <?php
                } else {

                    ?>
                    <button class="vote-success js-remove-vote"
                            data-member="<?= $member->id ?>">Удалить голос
                    </button>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>

        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
        <?php
    }
    ?>

    <?= Yii::$app->controller->renderPartial('../../popups/vote-json') ?>
    <?= Yii::$app->controller->renderPartial('../../popups/vote-block', ['member' => $user->groupMember]) ?>
</div>
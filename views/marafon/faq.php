<?php

/* @var $this yii\web\View */

/* @var $member \app\models\GroupMember */

/* @var $items array */

/* @var $type int */

use yii\jui\Accordion;

$this->title = 'Вопрос-ответ';
\app\helpers\My::getAccess()

?>
    <h1><?= \app\models\Faq::getFaqType()[$type] ?></h1>
<?php
if ($type == \app\models\Faq::TYPE_FOOD) {
    echo Yii::$app->controller->renderPartial('food/header_food', ['active' => 'faq']);
}
if ($type == \app\models\Faq::TYPE_TRAINING) {
    echo Yii::$app->controller->renderPartial('training/training_buttons', ['active' => 'faq']);
}
if ($type == \app\models\Faq::TYPE_CARE) {
    echo Yii::$app->controller->renderPartial('care/care_buttons', ['active' => 'faq']);
}
?>
    <div class="faq_list">
        <?php
        foreach ($items as $item) {
            ?>
            <div class="faq_item">
                <span class="faq_item_icon"></span>
                <div class="faq_item_tittle"><?= $item['header'] ?></div>
                <p class="fz17" style="display: none;"><?= $item['content'] ?></p>
            </div>
            <?php
        }
        ?>
    </div>
<?php

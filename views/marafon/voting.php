<?php
/**
 * @var $couple \app\models\CoupleMembers
 * @var $members \app\models\GroupMember[]
 * @var $count \app\models\CoupleVote[]
 */
?>
<div class="food_list clearfix vote-list" style="position: relative;">
    <?php
    if (!empty($members)) {
        ?>
        <h2 class="title text-center">Проголосуй за один из двух показанных результатов. <br>У вас
            осталось: <strong><?= 20 - count($count) ?></strong> голосов
        </h2>
        <div class="vote__list" id="vote-list">

            <?php
            foreach ($members as $index => $member) {

                ?>
                <div class="vote-item" id="member-<?= $index ?>">


                    <a href="<?= \yii\helpers\Url::to(['marafon/vote-img', 'member_id' => $member->id]) ?>"
                       class="recipes_popup_opener">
                        <div class="food_list_item">
                            <img src="/uploads/progress/<?= $member->avatar ?>" alt="">
                            <div class="food_list_item_more">
                                <div class="food_list_item__more">
                                    <span style="line-height: 20px;"><?= $member->first_name ?></span>
                                    <span>Возвраст: <?= $member->age ?></span>
                                    <span>Вес: <?= $member->progressRelFirstWeek->weight ?> - <?= $member->progressRelLastWeek->weight ?></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <button class="send-vote-new button <?= Yii::$app->devicedetect->isMobile() && $index == 1 ? 'visible_false' : null ?>"
                            data-member="<?= $member->id ?>" data-couple="<?= $couple->id ?>">Проголосовать
                    </button>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="vote-list__arrows">
            <a href="#" class="vote-list__arrow vote-list__arrow--prev">
                &larr;
            </a>
            <span class="active"></span>
            <span></span>
            <a href="#" class="vote-list__arrow vote-list__arrow--next">
                &rarr;
            </a>
        </div>
        <?php
    } else {
        ?>
        <strong style="font-size: 22px">Спасибо, ваши голоса учтены!<br>
            2 этап голосования откроется, как только все голоса будут посчитаны.<br>
            Ожидайте уведомление об открытии 2 этапа на почту и в телеграм-канале. Не пропустите!</strong>
        <?php
    }
    ?>
</div>
<?= Yii::$app->controller->renderPartial('../../popups/vote-block-mobile', ['member' => Yii::$app->user->identity->groupMember]) ?>

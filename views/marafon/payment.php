<?php
/**
 * @var $user \app\models\User
 * @var $type int
 * @var $cost int
 * @var $set string
 * @var $member \app\models\GroupMember
 */

?>
<script>
    var deliveryPrice = <?=json_encode(\app\helpers\My::getCountryCost())?>;
</script>
<?= Yii::$app->controller->renderPartial('payment-oferta') ?>
<?= Yii::$app->controller->renderPartial('payment-policy') ?>
<?= Yii::$app->controller->renderPartial('payment-fortuna') ?>
<?= Yii::$app->controller->renderPartial('payment-recommendation') ?>
<div class="container__pay">
    <div class="header__menu header__menu--pay">
        <a href="<?= \yii\helpers\Url::to('/') ?>" class="logo__link">
            <img src="/icons/lerchek.png" srcset="/icons/lerchek@2x.png 2x" alt="" class="logo">
        </a>
    </div>
    <div class="container">
        <div class="pay__block">
            <div class="pay">
                <h3 class="pay__title">
                    оплата
                </h3>
                <p class="pay__text">
                    <?= $set ? Yii::$app->params['programs-label'][$_GET['type']] . '<span> + комплекс Letique</span>' : Yii::$app->params['programs-label'][$_GET['type']] ?>
                </p>
                <div class="price__pay">
                    <p class="price__txt">
                        <?php
                        if ($set) {
                            ?>
                            <?= Yii::$app->params['programs-old'][$_GET['type']] ?>₽

                            <?php
                        } else {
                            ?>
                            <?= Yii::$app->params['programs-old-no-set'][$_GET['type']] ?>₽
                            <?php
                        }
                        ?>
                    </p>

                    <p class="price__txt--blue">
                        <span><?= $cost ?></span> ₽
                    </p>
                </div
                <form></form>
                <?php $form = \kartik\form\ActiveForm::begin([
                    'id' => 'payment'
                ]) ?>
                <?= $form->field($user, 'datetime_signup')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false) ?>
                <?= $form->field($member, 'datetime_signup')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false) ?>
                <?= $form->field($user, 'datetime_last_login')->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false) ?>
                <input type="hidden" name="continue" value="0">
                <div class="container__form">
                    <div class="pay-form__inputs">
                        <?= $form->field($member, 'first_name')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Имя'])->label(false) ?>
                        <?= $form->field($member, 'last_name')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Фамилия'])->label(false) ?>
                    </div>
                    <div class="pay-form__inputs">
                        <?= $form->field($user, 'email', [
                            'template' => '{input}<div class="help-error" style="color: white">Не рекомендуем использовать почту icloud, письма могут быть не доставлены</div>{error}'
                        ])->textInput([
                            'autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'e-mail', 'onPaste' => 'return false;'
                        ])->label(false) ?>

                        <?= $form->field($user, 'email_confirm')->textInput(['autocomplete' => 'off', 'class' => 'input', 'onPaste' => 'return false;', 'placeholder' => 'Подтвердите e-mail'])->label(false) ?>
                    </div>
                    <div class="pay-form__inputs">
                        <?php
                        if (is_array(\app\helpers\My::getType($_GET['type']))) {
                            echo $form->field($user, 'email_man')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Мужской Email'])->label(false);
                        }
                        ?>
                        <?= $form->field($member, 'phone')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Телефон'])->label(false) ?>
                    </div>

                    <?php
                    if (\app\helpers\My::isGirl($_GET['type']) && $_GET['type'] != \app\helpers\My::PRESS_FOR_GIRL && $_GET['type'] != \app\helpers\My::PRESS_FOR_MAN && isset($_GET['set'])) {
                        ?>
                        <input type="hidden" name="cost_delivery" class="sectionPay__input cost_delivery">
                        <input type="hidden" name="set" value="<?= $_GET['set'] ?>"
                               class="sectionPay__input cost_delivery">
                        <p class="pay-form__desc">
                            Адрес доставки:
                        </p>
                        <div class="pay-form__inputs">
                            <div class="pagination__number">
                                <?= $form->field(new \app\models\Retail(), 'country')->dropDownList(\app\helpers\My::getCountryNames(), ['class' => 'input pay-form__select'])->label(false) ?>
                            </div>
                            <?= $form->field(new \app\models\Retail(), 'index_post')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Индекс'])->label(false) ?>
                        </div>
                        <div class="pay-form__inputs">
                            <?= $form->field(new \app\models\Retail(), 'city')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Населенный пункт'])->label(false) ?>
                            <?= $form->field(new \app\models\Retail(), 'street')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Улица'])->label(false) ?>
                        </div>
                        <div class="pay-form__inputs">
                            <?= $form->field(new \app\models\Retail(), 'building')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Дом'])->label(false) ?>
                            <?= $form->field(new \app\models\Retail(), 'flat')->textInput(['autocomplete' => 'off', 'class' => 'input', 'placeholder' => 'Квартира'])->label(false) ?>
                        </div>
                        <div class="pay-form__inputs">
                            <?= $form->field(new \app\models\Retail(), 'comment')->textarea(['autocomplete' => 'off', 'class' => 'textarea', 'placeholder' => 'Комментарий', 'rows' => 15])->label(false) ?>
                        </div>
                        <input type="hidden" class="region" name="region">
                        <div class="pay-form__txts">
                            <p class="pay-form__txt">
                                Вы выбрали: <b><?= \app\helpers\My::getComplexes()[$_GET['set']] ?></b>
                            </p>
                            <p class="pay-form__txt">
                                <b>Стоимость</b> доставки: <span class="cost cost_append">0</span> руб
                            </p>
                            <p class="pay-form__txt result_text">
                                Итого: <span
                                        class="cost"><?= $cost ?></span> ₽
                            </p>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="pay-form__agree">
                        <label class="pay-form__label"> <input type="checkbox" class="pay-form__checkbox" checked="">
                            <div class="pay-form__label-txt">я согласен с условиями:
                                <a href="#contract" class="pay-form__label-link js-payment-popup"> договора оферты</a>,
                                <a href="#fortuna" class="pay-form__label-link js-payment-popup"> правилами проведения
                                    розыгрыша</a>,
                                <a href="#recommendation" class="pay-form__label-link js-payment-popup"> агентский
                                    договор-оферта </a>
                            </div>
                        </label>
                    </div>

                    <div class="pay-form__payments">
                        <div class="pay-form__payment">
                            <label for="card" class="pay-form__label">
                                <input name="payment" type="radio" class="pay-form__radio" checked="" id="card"
                                       value="AC">
                                <div class="pay-form__label-icon"></div>
                            </label>
                        </div>
                        <div class="pay-form__payment">
                            <label for="yandex" class="pay-form__label">
                                <input name="payment" type="radio" class="pay-form__radio" id="yandex" value="PC">
                                <div class="pay-form__label-icon pay-form__label-icon--2"></div>
                            </label>
                        </div>
                        <div class="pay-form__payment">
                            <label for="qiwi" class="pay-form__label">
                                <input name="payment" type="radio" class="pay-form__radio" id="qiwi" value="3">
                                <div class="pay-form__label-icon pay-form__label-icon--3"></div>
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="button button__pay">
                        Оплатить
                    </button>
                </div>
                <?php
                \kartik\form\ActiveForm::end()
                ?>
                <p class="pay-form__bot-txt">
                    Нажимая кнопку "Оплатить", вы даете своё согласие на <a href="#policy"
                                                                            class="js-payment-popup pay-form__bot-link">обработку
                        персональных данных</a>
                </p>
            </div>
            <img src="/images/payment/paybot.png" srcset="/images/payment/paybot@2x.png 2x" alt="" class="pay__bot">
            <img src="/images/payment/paytop.png" srcset="/images/payment/paytop@2x.png 2x" alt="" class="pay__top">
            <img src="/images/payment/weight.png" srcset="/images/payment/weight@2x.png" alt="" class="pay__weight">
        </div>
    </div>
</div>
<script>
    var token = "01cee4bed2318c523327a1a28e953f02b479678b";
    var $city = $("#retail-city");

    // удаляет районы города и всё с 65 уровня
    function removeNonCity(suggestions) {
        return suggestions.filter(function (suggestion) {
            return suggestion.data.fias_level !== "5" && suggestion.data.fias_level !== "65";
        });
    }

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }

    function cityToString(address) {
        return join([
            join([address.city_type, address.city], " "),
            join([address.settlement_type, address.settlement], " ")
        ]);
    }

    // Ограничиваем область поиска от города до населенного пункта
    var suggestOption = {
        token: token,
        type: "ADDRESS",
        constraints: {
            locations: {country_iso_code: 'RU'}
        },
        count: 20,
        hint: false,
        geoLocation: false,
        bounds: "city-settlement",
        onSuggestionsFetch: removeNonCity,
        onSelect: function (suggestion) {
            $('.region').val(suggestion.data.region_with_type)
        }
    };
    var suggest = $city.suggestions(suggestOption).suggestions();

    $('#retail-country').on('change', function () {
        // suggestOption.constraints.locations.country_iso_code =
        if ($('#retail-country').val() != 'RU') {
            suggest.disable();
        } else {
            suggest.enable();

        }
        suggest.setOptions(suggestOption)

    });

</script>
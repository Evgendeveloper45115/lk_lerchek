<?php

/* @var $this yii\web\View */

/* @var $user \app\models\User */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Профиль';
?>
<h1>Профиль</h1>
<div class="profile_changepassword">
    <div class="profile_photo custom_photo">
        <div class="col-md-1"></div>
        <?php
        $form = ActiveForm::begin([
            'fieldConfig' => [
                'options' => ['class' => 'form_group clearfix'],
                'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"help-block\">{error}</div></div>",
            ],
        ]) ?>
        <?= $form->field($user, 'email')->textInput(['disabled' => true]) ?>
        <?= $form->field($user->groupMember, 'last_name') ?>
        <?= $form->field($user->groupMember, 'first_name') ?>
        <?= $form->field($user->groupMember, 'patronymic') ?>
        <?= $form->field($user->groupMember, 'phone') ?>
        <?= $form->field($user->groupMember, 'height') ?>
        <?= $form->field($user->groupMember, 'weight') ?>
        <?= $form->field($user->groupMember, 'bust') ?>
        <?= $form->field($user->groupMember, 'waist') ?>
        <?= $form->field($user->groupMember, 'hip') ?>
        <?= $form->field($user->groupMember, 'city')->textInput() ?>
        <?php
        if (\app\helpers\My::isGirl()) {
            echo $form->field($user->groupMember, 'family')->radioList([
                1 => 'Замужем',
                2 => 'Не замужем',
            ]);
        } else {
            echo $form->field($user->groupMember, 'family')->radioList([
                1 => 'Женат',
                2 => 'Не женат',
            ]);
        }
        ?>
        <?= $form->field($user->groupMember, 'childs', [
            'template' => "<div class='left_col'>{label}\n<div class='hint'>если у вас нет детей, поставьте в этом пункте - 0</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
        ])->textInput(['autocomplete' => 'off']) ?>
        <?= $form->field($user->groupMember, 'telegram')->textInput(['autocomplete' => 'off']) ?>
        <?= $form->field($user->groupMember, 'vk_nick', [
            'template' => "<div class='left_col'>{label}\n<div class='hint'>если у вас нет профиля в инстаграм, напишите – нет</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
        ])->textInput(['autocomplete' => 'off']) ?>
        <?= $form->field($user->groupMember, 'parthner', [
            'template' => "<div class='left_col'>{label}\n<div class='hint'>если вы участвуете самостоятельно, напишите в поле ниже – нет</div></div><div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
        ])->textInput(['autocomplete' => 'off']) ?>
        <?= $form->field($user->groupMember, 'physical_activity')->radioList(\app\helpers\My::isGirl() ? \app\models\forms\SignupForm::$physical_activities : \app\models\forms\SignupForm::$physical_activities_man, ['class' => 'physical_activity'])->label('Дневная активность ' . Html::img('/images/project/help-button.png', ['class' => 'help_img'])) ?>


        <?= $form->field($user->groupMember, 'training_type')->radioList(\app\models\Training::getTrainingType()) ?>
        <?= $form->field($user->groupMember, 'veg')->radioList($user->groupMember->getVeg()) ?>
        <?= $form->field($user->groupMember, 'ration_type')->radioList($user->groupMember->getTypes()) ?>

        <?= $form->field($user->groupMember, 'smoke')->radioList([
            1 => 'Да',
            2 => 'Нет',
        ]) ?>
        <?= $form->field($user->groupMember, 'alcohol')->radioList(\app\models\GroupMember::getAlcoholType()) ?>
        <?php
        if (\app\helpers\My::isGirl()) {
            ?>
            <?= $form->field($user->groupMember, 'gv_info')->radioList(\app\models\GroupMember::getGV()) ?>
            <?= $form->field($user->groupMember, 'pregnant')->radioList(\app\models\GroupMember::getPregnantType()) ?>
            <?php
        }
        ?>
        <?= $form->field($user->groupMember, 'food_last_3days')->label('Какие цели ставите на ближайшие 4 недели?') ?>

        <?= Html::submitButton('Сохранить', ['class' => 'button button_green button_big']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<style>
    input {
        -webkit-appearance: checkbox;
    }

    .radio input {
        display: inline !important;
    }

    #groupmember-veg {
        text-align: start;
    }

    #groupmember-ration_type {
        text-align: start;
    }

    #groupmember-smoke, #groupmember-alcohol, #groupmember-gv_info, #groupmember-pregnant {
        text-align: start;
    }

    @media (max-width: 1024px) {
        .profile_changepassword form input {
            height: auto;
        }

        .profile_changepassword {
            padding: 4.6875vw 8.59375vw 0vw;
        }

        .button {
            margin-top: 5vw;
        }
    }
</style>
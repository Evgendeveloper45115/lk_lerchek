<?php

use yii\helpers\Url;

?>
<div class="login_page_main">
    <div class="login_page_programms clearfix">
        <a href="<?= Url::to(['instruction']) ?>">
            <div class="login_page_programm">
                <span class="icon" style="background-image: url(/images/index-icons/instruction.png)"></span>
                Инструкция
                <span class="mobiletext"><p>Посмотреть инструкцию<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['food']) ?>">
            <div class="login_page_programm"><span class="icon"
                                                   style="background-image: url(/images/index-icons/food.png)"></span>Питание
                <span class="mobiletext"><p>Посмотреть рацион<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['trainings']) ?>">
            <div class="login_page_programm"><span class="icon"
                                                   style="background-image: url(/images/index-icons/treni.png)"></span>Тренировки
                <span class="mobiletext"><p>Посмотреть упражнения<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['progress']) ?>">
            <div class="login_page_programm"><span class="icon"
                                                   style="background-image: url(/images/index-icons/progress.png)"></span>Прогресс
                <span class="mobiletext"><p>Оценить свой прогресс<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['communication']) ?>">
            <div class="login_page_programm"><span class="icon"
                                                   style="background-image: url(/images/index-icons/chat.png)"></span>Вопросы?
                <span class="mobiletext"><p>Задать вопрос куратору<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['report']) ?>">
            <div class="login_page_programm">
                <span class="icon" style="background-image: url(/images/index-icons/rezult.png)"></span>
                Отчеты
                <span class="mobiletext"><p>Посмотреть отчеты<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['whats-app']) ?>">
            <div class="login_page_programm"><span class="icon"
                                                   style="background-image: url(/images/index-icons/chat.png)"></span>Общение
                <span class="mobiletext"><p>Пообщаться и зарядиться энергией<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['care-instruction']) ?>">
            <div class="login_page_programm"><span class="icon"
                                                   style="background-image: url(/images/index-icons/care.png)"></span>Уход
                за кожей
                <span class="mobiletext"><p>Посмотреть<span class="arrow"></span></p></span>
            </div>
        </a>
        <a href="<?= Url::to(['training-calendar']) ?>">
            <div class="login_page_programm"><span class="icon"
                                                   style="background-image: url(/images/index-icons/calendar.png)"></span>Календарь
                <span class="mobiletext"><p>Посмотреть<span class="arrow"></span></p></span>
            </div>
        </a>
    </div>
</div>

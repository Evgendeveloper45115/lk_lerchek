<?php

use yii\helpers\Url;
use app\models\Care;

/* @var $this yii\web\View */
/* @var $instructions Care[] */
/* @var $type int */

/* @var $member \app\models\GroupMember */

$this->title = 'Питание';
$access = \app\helpers\My::getAccess()
?>
<h1><?= Care::$types[$type] ?></h1>
<?= Yii::$app->controller->renderPartial('care/care_buttons') ?>
<?php
if ($access) {
    ?>
    <div class="training">
        <div class="training_list">
            <?php
            foreach ($instructions as $instruction) {
                if (!$instruction->video_link) {
                    ?>
                    <div class="training_item">
                        <?php
                        if ($instruction->image) {
                            ?>
                            <div class="training_item_video img_preview"
                                 style="background: url(/uploads/care/<?= $instruction->image ?>) no-repeat center top;background-size: contain; width: 100%;
                                         "></div>
                            <?php
                        }
                        ?>

                        <?= $instruction->description ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="training_item">
                        <div class="training_item_video">
                            <iframe src="https://www.youtube.com/embed/<?= $instruction->video_link ?>?rel=0&amp;showinfo=0"
                                    frameborder="0"
                                    allow="encrypted-media" allowfullscreen=""></iframe>
                        </div>
                        <div class="training_item_text"><p><?= $instruction->description ?></p></div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <?php
}
?>

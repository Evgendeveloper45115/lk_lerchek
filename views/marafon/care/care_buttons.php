<?php

use \yii\helpers\Url;

/**
 * @var $active string
 * @var $user \app\models\User
 */
?>
    <div class="food_menu_list">
        <div class="food_menu__list">
            <?php
            foreach (\app\models\Care::$types as $key => $type) {
                ?>
                <div class="food_menu_item <?= $_GET['type'] == $key ? 'active' : null ?>">
                    <a href="<?= Url::to(['care-instruction', 'type' => $key]) ?>"
                       class="button button_transparent"><?= $type ?></a>
                </div>
                <?php
            }
            ?>
            <div class="food_menu_item <?= $active == 'faq' ? 'active' : null ?>"><a
                        href="<?= Url::to(['faq', 'type' => 4]) ?>"
                        class="button button_transparent">FAQ уход</a>
            </div>
        </div>
    </div>
<?php

<?php
/**
 * @var $members array
 * @var $form array
 * @var $user \app\models\User
 */
$this->title = 'Забыли пароль?';
?>
<?php
?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <link rel="stylesheet" href="/css/all.css?v=2.4">
    <link rel="stylesheet" href="/css/media.css?v=2.3">
    <title><?= $this->title ?></title>
</head>
<body>
<div class="recovery-password">
    <h1><?= $this->title ?></h1>
    <div class="login_form " style="text-align: center">
        <?php
        ?>
        <div class="success" style="font-size: 22px">Для восстановления пароля введите
            вашу почту, которую вы указали при регистрации. На указанный почтовый ящик будет выслан новый пароль.
        </div>
        <?php $form = \kartik\form\ActiveForm::begin([
            'id' => 'login-form',
            'validateOnChange' => false,
            'enableClientValidation' => false,

            'options' => ['class' => 'form-horizontal', 'name' => 'sign_in'],
            'fieldConfig' => [
                'options' => ['class' => 'form_group clearfix'],
                'template' => "{label}{input}\n<div class=\"error\">{error}</div>",
            ],
        ]); ?>
        <div style="margin: 10px 0"></div>
        <?= $form->field(new \app\models\User(), 'email')->textInput(['class' => 'sing_input', 'placeholder' => 'Email'])->label(false) ?>
        <div class="clearfix" style="margin-top: 15px;">
            <button type="submit" class="button button_green email_send">Отправить</button>
            <?php
            if (isset($error) && $error != null) {
                ?>
                <div class="error"><?= $error ?></div>
                <?php
            }
            if (isset($success) && $success != null) {
                ?>
                <div class="error"><?= $success ?></div>
                <?php
            }
            ?>
        </div>

        <?php \kartik\form\ActiveForm::end(); ?>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="/js/owl.carousel.min.js?v=2.2"></script>
    <script src="/js/user.js?v=2.2"></script>
</body>

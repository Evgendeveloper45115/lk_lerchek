<?php

use \yii\helpers\Url;

/**
 * @var $active string
 * @var $member \app\models\GroupMember
 */
?>
    <div class="food_menu_list">
        <div class="food_menu__list">
            <?php

            foreach (\app\models\Training::getTrainingTypePress() as $type => $name) {
                ?>
                <div class="food_menu_item <?= $training_type == $type ? 'active' : null ?>">
                    <a href="<?= Url::to(['marafon/perfect-abs', 'week' => $select_week, 'number' => $select_number, 'training' => $type]) ?>"
                       class="button button_transparent active"><?= $name ?></a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
<?php

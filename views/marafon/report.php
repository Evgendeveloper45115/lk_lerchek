<?php

/* @var $this yii\web\View */

/* @var $report \app\models\Report */
/* @var $select_week string */
/* @var $cur_week string */
/* @var $display bool */

/* @var $member \app\models\GroupMember */

use yii\helpers\Url;

$this->title = 'Отчеты';
\app\helpers\My::getAccess();
?>
<h1><?= $this->title ?></h1>
<?php
if ($display) {
    ?>
    <div class="motivation_week">
        <?= $member->accessButtonPrev($select_week, 'report') ?>
        <?php
        foreach ($member->getWeeks() as $week => $val) {
            $class = \app\helpers\My::getActiveClassWeek($week, $select_week);
            echo Yii::$app->controller->renderPartial('food/week-buttons', ['url' => 'marafon/report', 'week' => $week, 'class' => $class, 'cur_week' => $cur_week]);
        }
        echo $member->accessButtonNext($select_week, $cur_week, 'report')
        ?>
    </div>
    <div class="tab-buttons-wrapper" data-tabs-wrapper=".tabs-wrapper">
        <div class="tab-button tab-button__active" data-tab=".tab-question">Задание</div>
        <?php
        if ($member->payment->continue_program && $select_week > 4) {
            ?>
            <div class="tab-button" data-tab=".tab-answers">Проверка</div>
            <?php
        }
        ?>
    </div>
    <?php
    if ($select_week == $report->week && $cur_week >= $select_week) {
        ?>

        <div class="tabs-wrapper">
            <div class="tab tab__visible tab-question">
                <div class="food_list">
                    <div style="font-weight: 700;" class="tittle_afterh1_light"><?= $report->title_adm ?></div>
                    <br>
                    <div class="tittle_afterh1_light"><?= $report->text_adm ?></div>

                    <div class="question_form" style="display: inline-flex; width: 100%;">
                        <?php
                        if (!$report->rhu_member_id) {
                            $form = \yii\widgets\ActiveForm::begin([
                                'enableClientValidation' => true,
                                'validateOnType' => true,
                                'options' => [
                                    'style' => 'width:100%'
                                ],
                                'fieldConfig' => [
                                    'options' => ['class' => 'form_group clearfix'],
                                    'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
                                ],
                            ]);
                            ?>
                            <?= $form->field(new \app\models\ReportHasUser(), 'user_text')->textarea()->label(false) ?>
                            <?= $form->field(new \app\models\ReportHasUser(), 'member_id')->hiddenInput(['value' => $member->id])->label(false) ?>
                            <div class="form_group clearfix">
                                <div>
                                    <?= \yii\helpers\Html::submitButton('Отправить отчет', ['class' => 'button button_green button_big button_custom']) ?>
                                </div>
                            </div>

                            <?php \yii\widgets\ActiveForm::end();
                        } else {
                            ?>
                            <div class="update-question" style="width: 100%; display: none">
                                <?php
                                $form = \yii\widgets\ActiveForm::begin([
                                    'options' => [
                                        'style' => 'width:100%'
                                    ],
                                    'fieldConfig' => [
                                        'options' => ['class' => 'form_group clearfix'],
                                        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
                                    ],
                                ]);
                                ?>
                                <?= $form->field(new \app\models\ReportHasUser(), 'user_text')->textarea()->label(false) ?>
                                <?= $form->field(new \app\models\ReportHasUser(), 'member_id')->hiddenInput(['value' => $member->id])->label(false) ?>
                                <div class="form_group clearfix">
                                    <div>
                                        <?= \yii\helpers\Html::submitButton('Отправить отчет', ['class' => 'button button_green button_big button_custom']) ?>
                                    </div>
                                </div>

                                <?php \yii\widgets\ActiveForm::end();

                                ?>
                            </div>

                            <div class="hero_post_content_wrap"><?= $report->rhu_user_text ?></div>

                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if ($report->rhu_member_id) {
                        ?>
                        <div class="progress_item_tb" style="height: 0">
                            <div class="">
                                <?php
                                echo '<a data-week=' . $report->week . ' href="javascript://" class="button button_green update-question-button">Изменить отчет</a>';
                                ?>

                            </div>
                        </div>
                        <?php
                    }
                    ?>


                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="food_list">
        <?php
        if ($select_week > $cur_week) {
            $this->registerJs("
           $('.access_popup_not_time').fadeIn();
        $('html, body').addClass('popup_opened');
    ");
        }
        ?>
    </div>
    <?= Yii::$app->controller->renderPartial('../../popups/progress', ['member' => $member]) ?>
    <?= Yii::$app->controller->renderPartial('../../popups/dont-time', ['dont_time' => 'Отчет будет доступен']) ?>
    <?php
} else {
    echo "<br><div class=tittle_afterh1>Все отчеты приняты, дни отправки отчетов подошли к концу =(</div>";
}
?>

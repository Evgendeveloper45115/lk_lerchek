<?php
/**
 * @var $members array
 * @var $user \app\models\User
 */
$this->title = 'Ошибка';
$user = Yii::$app->user->identity;
?>
<h1><?= $this->title ?></h1>

<div class="program-list">
    <a href="<?= \yii\helpers\Url::to('/') ?>"
       class="program-list__link">
        Что-то пошло не так...
        Нажмите на кнопку ниже, чтобы вернуться на "главную страницу"
        <br>
        <span>Главная</span>
    </a>
</div>



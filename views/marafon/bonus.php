<?php

/* @var $this yii\web\View */

/* @var $user \app\models\User */
/* @var $referrals \app\models\Ref[] */

/* @var $userRef \app\models\User */

use yii\helpers\Url;

$this->title = 'Бонусы';
$url = str_replace('lk.', '', Yii::$app->request->hostInfo);
?>
<div class="referralProgram">
    <h1>Реферальная программа</h1>

    <div class="referralProgram-introtext">
        Ваша ссылка для рекомендации (отправьте ее тем, кто интересуется марафоном):
    </div>

    <div class="referralProgramForm">
        <input type="text" class="referralProgramForm-input"
               value="https://lerchek.ru?ref=<?= base64_encode($user->email) ?>">
        <button class="referralProgramForm-btn" onclick="copytext('.referralProgramForm-input')">Cкопировать ссылку
        </button>
    </div>

    <div class="referralProgramForm-pol">
        Копируя и передавая ссылку, вы соглашаетесь с условиями <a href="#" class="training_access access_popup ref">агентского
            договора-оферты</a>
    </div>
    <div class="referralProgramConclusion">
        <div class="referralProgramConclusion-name">
            Вывести средства на телефон
        </div>
        <?php
        ?>
        <div class="referralProgramConclusion-box">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'options' => [
                    'class' => 'referralProgramConclusion-grid'
                ]

            ]); ?>

            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '8 (999) 999-99-99',
                'options' => [
                    'required' => 'required',
                    'class' => 'referralProgramConclusion-phone',
                    'id' => 'phone2',
                    'placeholder' => ('Телефон')
                ],

            ])->label(false) ?>


            <?= $form->field($model, 'sum')->textInput(['class' => 'referralProgramConclusion-sum', 'placeholder' => 'Сумма', 'required' => 'required', 'min' => 1000])->label(false) ?>
            <?php
            if ($result) {
                ?>
                <div class="referralProgramConclusion-consideration">
                    <span>в обработке</span>
                    <i></i>
                </div>
                <?php
            } else {
                ?>
                <?= \yii\helpers\Html::submitButton('Вывести', ['class' => 'referralProgramConclusion-btn', 'disabled' => $all_sum >= 1000 ? null : 'disabled']) ?>
                <?php
            }
            \yii\widgets\ActiveForm::end(); ?>


            <div class="referralProgramConclusion-ball">
                <div class="referralProgramConclusion-ball__item">
                    На счету: <strong><?= $all_sum ? $all_sum : 0 ?> </strong>р.
                </div>

                <div class="referralProgramConclusion-ball__item">
                    Вывод средств доступен от <strong>1000</strong> р.
                </div>

            </div>
            <div class="referralProgramConclusion-ball">
                <div class="referralProgramConclusion-ball__item custom">
                    Внимание: Выводить средства можно только на российский номер телефона
                </div>
            </div>

        </div>
    </div>


    <div class="referralProgramHistory">
        <div class="referralProgramHistory-name">
            История транзакций:
        </div>

        <table>
            <thead>
            <tr>
                <th width="200">
                    Дата
                </th>
                <th>
                    Программа/номер телефона
                </th>
                <th width="200">
                    Сумма
                </th>
                <th width="120">
                    Статус
                </th>
            </tr>
            </thead>

            <tbody>
            <?php
            foreach ($referrals as $referral) {
                ?>
                <tr>
                    <td>
                        <?= date('d.m.Y', strtotime($referral->date)) ?>
                    </td>
                    <td>
                        <?= stristr($referral->sum, '+') ? \app\helpers\My::$type_program_rus[$referral->member->type_program] : $referral->phone ?>
                    </td>
                    <td>
                        <?= $referral->sum ?>
                    </td>
                    <td>
                        <?= \app\models\Ref::$statuses[$referral->status] ?>
                    </td>
                </tr>

                <?php
            }
            ?>
            </tbody>
        </table>
    </div>

</div>
<?= Yii::$app->controller->renderPartial('../../popups/ref') ?>

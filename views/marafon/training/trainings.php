<?php

/* @var $this yii\web\View */

/* @var $exercises \app\models\Training[] */

/* @var $member \app\models\GroupMember */

/* @var $select_week integer */
/* @var $select_number integer */
/* @var $training_type integer */

/* @var $cur_week integer */

use yii\helpers\Url;

$this->title = 'Тренировки';
$access = \app\helpers\My::getAccess();
?>

<h1><?= $this->title ?></h1>
<div class="training">
    <?= Yii::$app->controller->renderPartial('training/training_buttons', ['select_week' => $select_week, 'member' => $member, 'select_number' => $select_number, 'training_type' => $training_type]) ?>
    <div class="motivation_week">
        <?php
        echo $member->accessButtonPrev($select_week, 'trainings');
        foreach ($member->getWeeks() as $week) {
            $class = \app\helpers\My::getActiveClassWeek($week, $select_week);
            echo Yii::$app->controller->renderPartial('training/week-buttons', ['url' => 'marafon/trainings', 'week' => $week, 'class' => $class, 'cur_week' => $cur_week, 'training' => $training_type]);
        }
        echo $member->accessButtonNext($select_week, $cur_week, 'trainings')
        ?>
    </div>
    <div class="food_menu_list">
        <?= Yii::$app->controller->renderPartial('training/day-buttons', ['url' => 'marafon/trainings', 'member' => $member, 'select_number' => $select_number, 'training' => $training_type, 'select_week' => $select_week]) ?>
    </div>
    <?php
    if ($access) {
        ?>
        <div class="food_list">
            <?php
            if (!empty($exercises)) {
                foreach ($exercises as $exercise) {
                    ?>
                    <div class="training_item">
                        <div class="training_item_video">
                            <iframe src="https://www.youtube.com/embed/<?= $exercise['video'] ?>?rel=0&amp;showinfo=0"
                                    frameborder="0"
                                    allow="encrypted-media" allowfullscreen=""></iframe>
                        </div>
                        <div class="training_item_tittle"><?= $exercise['name'] ?></div>
                        <div class="training_item_text"><?= $exercise['weeks'][0]['text'] ?></div>
                    </div>
                    <?php
                }
            } else {
                echo 'Новые тренировки скоро появятся!';
            }
            ?>
        </div>
        <?php
    }
    ?>

</div>
<?= Yii::$app->controller->renderPartial('../../popups/dont-time', ['dont_time' => 'Тренировки будут доступны']) ?>

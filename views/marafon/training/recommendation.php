<?php

/* @var $this yii\web\View */
/* @var $member \app\models\GroupMember */

/* @var $model \app\models\Recommendation */

use yii\helpers\Url;

$access = \app\helpers\My::getAccess();
if ($access) {
    ?>
    <div class="training">
        <div class="training_list">
            <?php
            if (!$model->link) {
                ?>
                <div class="training_item">
                    <?php
                    if ($model->image) {
                        ?>
                        <div class="training_item_video img_preview"
                             style="background: url(/uploads/recommendation/<?= $model->image ?>) no-repeat center top;background-size: contain; width: 100%;
                                     "></div>
                        <?php
                    }
                    ?>
                    <?= $model->text ?>
                </div>
                <?php
            } else {
                ?>
                <div class="training_item">
                    <div class="training_item_video">
                        <iframe src="https://www.youtube.com/embed/<?= $model->link ?>?rel=0&amp;showinfo=0"
                                frameborder="0"
                                allow="encrypted-media" allowfullscreen=""></iframe>
                    </div>
                    <div class="training_item_text"><p><?= $model->text ?></p></div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}

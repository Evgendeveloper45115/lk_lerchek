<?php

/* @var $this yii\web\View */
/* @var $type integer */
/* @var $exercises \app\models\ExpressExercise */

/* @var $member \app\models\GroupMember */

$this->title = 'Вакуум';
$access = \app\helpers\My::getAccess();

?>
    <h1><?= $this->title ?></h1>
<?php
if ($access) {
    ?>
    <div class="training">
        <?php
        foreach ($exercises as $exercise) {
            ?>
            <div class="training_item">
                <div class="training_item_video">
                    <iframe src="https://www.youtube.com/embed/<?= $exercise->video ?>?rel=0&amp;showinfo=0"
                            frameborder="0"
                            allow="encrypted-media" allowfullscreen="1"></iframe>
                </div>
                <div class="training_item_tittle"><?= $exercise->name ?></div>
                <div class="training_item_text"><?= $exercise->text ?></div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}

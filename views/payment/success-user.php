<?php

/* @var $this yii\web\View */

/* @var $doplata bool */

/* @var $user \app\models\User */

use yii\jui\Accordion;

$this->title = 'Успешная оплата'; ?>
<script>
    fbq('track', 'Purchase', {
        value: 1900,
        currency: 'RUB',
    });
</script>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/payment/success.css">
    <title>Lerchek</title>
</head>
<body>
<div class="container">
</div>
<div class="container__mob">
    <div class="content">
        <h1 class="title">
            ПОЗДРАВЛЯЕМ!
        </h1>
        <p class="text">
            Вы успешно продлили марафон за 1900 рублей. Ваш доступ в личный кабинет продлен еще на 1 месяц.
        </p>
        <div class="info">
            <h2 class="title title__small">
                Важная информация для участников марафона
            </h2>
            <div class="info-block">
                <div class="info-block__left">
                    <img src="/images/project/message.png" alt="" class="img__message">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="grey-block">
    <div class="container__mob">
        <div class="next-block">
            <h2 class="title title__small">
                Что делать дальше?
            </h2>
            <div class="next-block__items">
                <div class="next-block__item">
                    <div class="next-block__item-left next-block__item-left--1 next-block__item-left--mt">

                    </div>
                    <div class="next-block__item-right">
                        <p class="grey-block__txt grey-block__txt--bold">
                            1. Перейдите в личный кабинет!
                        </p>
                        <div class="buttons__quest">
                            <a target="_blank"
                               href="/marafon"
                               class="button__quest link__lk">
                                Перейти
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container__mob">
    <div class="footer">
        <h2 class="title title__small">
            Желаем вам удачи и отличных результатов и с нетерпением
            ждем новых встреч!
        </h2>
    </div>
</div>
</body>
</html>

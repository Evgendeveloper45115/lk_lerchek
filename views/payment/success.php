<?php

/* @var $this yii\web\View */

/* @var $doplata bool */

/* @var $member \app\models\GroupMember */

use yii\jui\Accordion;

$this->title = 'Успешная оплата';
$type = \app\helpers\My::getType($_GET['manWoman']);
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (m, e, t, r, i, k, a) {
            m[i] = m[i] || function () {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(47993900, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true,
            trackHash: true,
            ecommerce: "dataLayer"
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/47993900" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/payment/style/style.css?v=1.0">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <title>Lerchek Marafon</title>


    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TSGNVLK');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '297273701305854');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=297273701305854&ev=PageView&noscript=1"
    /></noscript>

<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSGNVLK"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script>
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'RUB',
            'purchase': {
                'actionField': {
                    'id': '502',
                    'revenue': '<?=$_GET["sum"]?>',
                },
                'products': [{
                    'name': '<?=\app\helpers\My::$type_program_rus[$member->type_program]?>',
                    'price': '<?=$_GET["sum"]?>',
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'purchase',
        'gtm-ee-event-non-interaction': 'False',
    });
    fbq('track', 'Purchase');
    fbq('track', 'pay');
</script>
<div class="container">
    <h3 class="payment__tit">
        Поздравляем!
        <img src="icons/payment_emoji.png" alt="" class="payment__tit-emoji">
    </h3>
    <p class="payment__txt">
        Вы в числе участников марафона Валерии и Артема Чекалиных, а значит – у вас есть реальный шанс не только
        кардинально преобразиться и изменить свой образ жизни, но и выиграть супер-призы.
    </p>
    <div class="prize__imgs">

        <img src="images/360.png" srcset="images/360@2x.png 2x" alt="" class="prize__img-payment">
        <img src="images/361.png" srcset="images/361@2x.png 2x" alt="" class="prize__img-payment">
        <img src="images/watch_payment.png" srcset="images/watch_payment@2x.png 2x" alt="" class="prize__img-payment">


        <img src="images/cash_payment.png" srcset="images/cash_payment@2x.png 2x" alt="" class="prize__img-payment">
        <img src="images/box_payment.png" srcset="images/box_payment@2x.png 2x" alt="" class="prize__img-payment">
        <img src="images/phone_payment.png" srcset="images/phone_payment@2x.png 2x" alt="" class="prize__img-payment">


    </div>
    <div class="info-blocks">
        <h4 class="info-blocks__tit">
            Важная информация
        </h4>
        <div class="info-block">
            <div class="info-block__left">
                <p class="info-block__num">
                    1
                </p>
                <img src="icons/message_emoji.png" alt="" class="info-block__emoji">
            </div>
            <p class="info-block__txt">
                Сразу после оплаты вам на почту придет письмо со ссылкой на регистрацию в личном кабинете. Проверьте
                папку «Спам» или «Промокакции», письмо могло попасть туда.
            </p>
        </div>
        <div class="info-block">
            <div class="info-block__left">
                <p class="info-block__num">
                    2
                </p>
                <img src="icons/phone_emoji.png" alt="" class="info-block__emoji">
            </div>
            <p class="info-block__txt">
                Также ссылка на регистрацию будет продублирована по смс на тот номер, который вы указывали при оплате.
            </p>
        </div>
        <div class="info-block">
            <div class="info-block__left">
                <p class="info-block__num">
                    3
                </p>
                <img src="icons/calendar_emoji.png" alt="" class="info-block__emoji">
            </div>
            <p class="info-block__txt">
                Выберите любой способ регистрации: вам нужно заполнить анкету либо по ссылке на почте, либо по ссылке из
                смс.
            </p>
        </div>
        <div class="info-block">
            <div class="info-block__left">
                <p class="info-block__num">
                    4
                </p>
                <img src="icons/girl_emoji2.png" alt="" class="info-block__emoji">
            </div>
            <p class="info-block__txt">
                Если письмо не пришло в течение дня, наша техподдержка поможет вам зарегистрироваться. Пишите на почту
                <a href="info@lerchek.ru" class="info-block__txt-link">info@lerchek.ru</a>
            </p>
        </div>
        <div class="info-block">
            <div class="info-block__left">
                <p class="info-block__num">
                    5
                </p>
                <img src="icons/double_emoji.png" alt="" class="info-block__emoji">
            </div>
            <p class="info-block__txt">
                Если при оплате вы указали почту @icloud, то письмо может быть не доставлено из-за настроек этой почты.
                Зарегистрируйтесь по ссылке из смс.
            </p>
        </div>
    </div>
    <h4 class="next__tit">
        Что делать дальше?
    </h4>
    <div class="next-blocks">
        <div class="next-block">
            <div class="next-block__top">
                <span class="next-block__icon"></span>
                <h4 class="next-block__desc">
                    1. Заполните анкету из письма до старта марафона!
                </h4>
            </div>
            <p class="next-block__txt">
                Подготовьте заранее сантиметровую ленту и весы, чтобы заполнить свои данные максимально точно!
                Взвешивание и замеры рекомендуем делать утром натощак.
            </p>
        </div>
        <div class="next-block">
            <div class="next-block__top">
                <span class="next-block__icon next-block__icon--2"></span>
                <h4 class="next-block__desc">
                    2. Ждите старта марафона - 22 марта!
                </h4>
            </div>
            <p class="next-block__txt">
                Доступ к личному кабинету будет открыт за 2 дня до старта марафона.
            </p>
        </div>
    </div>
    <a style="text-decoration: none" target="_blank"
       href="/marafon/signup?email=<?= $user->email ?>&type=<?= is_array($type) ? $type[0] : $type ?>"
       class="button button__mob button__payment">
        Заполнить анкету
    </a>
</div>
<div class="access">
    <div class="container">
        <img src="images/training.png" srcset="images/training@2x.png 2x" alt="" class="access__img">
        <h4 class="access__desc">
            Доступ в Ваш личный кабинет
        </h4>
        <a href="https://<?= Yii::$app->request->hostName ?>" class="access__link">
            https://<?= Yii::$app->request->hostName ?>
        </a>
        <p class="access__txt">
            Логин: ваш e-mail
        </p>
        <p class="access__txt">
            Пароль – вы создаете при заполнении анкеты
        </p>
    </div>
</div>
<div class="luck__block">
    <div class="container">
        <div class="luck">
            <img src="images/lerchek.png" srcset="images/lerchek@2x.png 2x" alt="" class="luck__img">
            <p class="luck__txt">
                Желаем Вам удачи, отличных результатов и с нетерпением ждем Вас на марафоне! До встречи!
            </p>
        </div>
    </div>
</div>

</body>
</html>

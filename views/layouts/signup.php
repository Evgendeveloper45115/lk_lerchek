<?php

use app\assets\LoginAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

\app\assets\SignupAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="general body__<?= \app\helpers\My::$type_program[$_GET['type']] ?>">

<?php $this->beginBody() ?>

<div class="content">
    <div class="wrapper">
        <header>
            <a href="/" class="logo"></a>
        </header>
        <div class="page_width">
            <?= $content ?>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

use \app\helpers\My;
use app\assets\UserAsset;

/* @var $this \yii\web\View */

/* @var $content string */

/* @var $user User */

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use app\models\Faq;
use yii\helpers\VarDumper;

$action_id = Yii::$app->controller->action->id;

if ($action_id == 'training-calendar') {
    \app\assets\AppAssetCalendar::register($this);

} else {
    UserAsset::register($this);
}

$user = Yii::$app->getUser()->identity;
$cntQ = My::getCountQuestion();
$this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <?= $this->render('marafon-header') ?>
    <body class="<?= 'general body__' . My::getTextProgram() ?>">
    <?php
    $this->beginBody() ?>
    <?php
    My::getProgressStatus();
    ?>

    <?= $this->render('../../popups/begin_program_block'); ?>
    <?= $this->render('menus/header_menu', ['cntQ' => $cntQ,]) ?>
    <?= $this->render('menus/mobile_menu') ?>
    <div class="content <?= $action_id ?>_action">
        <div class="wrapper">
            <?php
            $class = null;
            if ($action_id !== 'index') {
                $class = 'content_inner';
                ?>
                <div class="top_menu">
                    <?= $this->render('menus/pc_menu', ['user' => $user]) ?>
                </div>
                <?php
            }
            ?>
            <div class="<?= $class . ' ' . $action_id ?>__action">
                <?= $content ?>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
<?php

use yii\helpers\Url;

/* @var $user \app\models\User */

$user = Yii::$app->user->identity;
?>
<div class="popup mobile_menu" style="display: none; z-index: 30000000">
    <span class="close_popup close_custom_popup"></span>
    <div class="mobile_menu_inner">
        <div class="mobile_menu_tb">
            <div class="mobile_menu_td">
                <ul>
                    <li class=""><a href="<?= Url::to(['bonus']) ?>">Реферальная программа</a></li>
                    <?php
                    if ($user->groupMember->is30Day()) {
                        ?>
                        <li class="<?= $action_id == 'voting' ? "active" : null ?>"><a
                                    href="<?= Url::to(['voting', 'instruction' => 'on']) ?>">
                                Голосование</a></li>
                        <?php
                    }
                    ?>
                    <?php
                    if ($user->groupMember->is34Day()) {
                        ?>
                        <li class="<?= $action_id == 'vote' ? "active" : null ?>"><a
                                    href="<?= Url::to(['vote']) ?>">
                                Голосование(Финал)</a></li>
                        <?php
                    }
                    ?>

                    <?php
                    if (count($user->groupMembers) > 1) {
                        ?>
                        <li class="<?= Yii::$app->controller->action->id == 'change-program' ? "active" : null ?>"><a
                                    href="<?= Url::to(['change-program']) ?>">
                                Выбор программы</a></li>
                        <?php
                    }
                    ?>
                    <li><a href="<?= Url::to(['instruction']) ?>">Инструкция</a></li>
                    <li><a href="<?= Url::to(['food']) ?>">Питание</a></li>
                    <li><a href="<?= Url::to(['trainings']) ?>">Тренировки</a></li>
                    <?php
                    if ($user->groupMember->getPressTrainings()) {
                        ?>
                        <li class="<?= Yii::$app->controller->action->id == 'perfect-abs' ? "active" : null ?>"><a
                                    href="<?= Url::to(['perfect-abs', 'training' => 11, 'week' => $user->groupMember->getCurrentWeek()]) ?>">
                                Идеальный пресс</a></li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="<?= Url::to(['recommendation', 'type' => \app\models\Recommendation::RECOMMENDATION_CARDIO]) ?>">Кардио</a>
                    </li>
                    <li><a href="<?= Url::to(['express-exercise']) ?>">Вакуум</a></li>
                    <li><a href="<?= Url::to(['communication']) ?>">Задать вопрос</a></li>
                    <li class=""><a href="<?= Url::to(['care-instruction']) ?>">Уход за кожей</a></li>
                    <li><a href="<?= Url::to(['progress']) ?>">Прогресс</a></li>
                    <li><a href="<?= Url::to(['trouble']) ?>">Тех. поддержка</a></li>
                    <li class=""><a href="<?= Url::to(['report']) ?>">Отчеты</a></li>
                    <li class=""><a href="<?= Url::to(['whats-app']) ?>">Telegram</a></li>
                    <li class=""><a href="<?= Url::to(['training-calendar']) ?>">Календарь активности</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
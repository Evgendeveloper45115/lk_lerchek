<?php

use app\models\CoupleVote;
use \yii\helpers\Url;

/** @var $user \app\models\User */


$user = Yii::$app->user->identity;
?>
<header>
    <div class="header_wrapper">
            <span class="menu_opener">
                <span></span>
                <span></span>
                <span></span>
            </span>
        <a href="/marafon" class="header_logo"></a>
        <div class="header_main_menu">
            <ul>
                <?php
                if (Yii::$app->session->get('avatar')) {
                    ?>
                    <li class=""><a href="<?= Url::to(Yii::$app->session->get('url')) ?>">Админка</a>
                    </li>
                    <?php
                }
                if ($user->groupMember->is30Day()) {
                    ?>
                    <li class="<?= $action_id == 'vote' ? "active" : null ?>"><a
                                href="<?= Url::to(['voting', 'instruction' => 'on']) ?>">
                            Голосование</a></li>
                    <?php
                }
                if ($user->groupMember->is34Day()) {
                    ?>
                    <li class="<?= $action_id == 'vote' ? "active" : null ?>"><a
                                href="<?= Url::to(['vote']) ?>">
                            Голосование(Финал)</a></li>
                    <?php
                }

                if (count($user->groupMembers) > 1) {
                    ?>
                    <li class="<?= $action_id == 'change-program' ? "active" : null ?>"><a
                                href="<?= Url::to(['change-program']) ?>">
                            Выбор программы</a></li>
                    <?php
                }
                ?>
                <li class=""><a href="<?= Url::to(['bonus']) ?>">Реферальная программа</a></li>
                <li class=""><a href="<?= Url::to(['communication']) ?>">Задать вопрос</a>
                    <?php
                    if ($cntQ != 0) {
                        ?>
                        <span style="padding-right: 5px; position: relative;bottom: 10px;right: 3px;"
                              class="notice_count <?= 'notice_active' ?>"><?= $cntQ ?></span>
                        <?php
                    }
                    ?>
                </li>
                <li class=""><a href="<?= Url::to(['whats-app']) ?>">Telegram</a></li>
                <li class=""><a href="<?= Url::to(['trouble']) ?>">Тех. поддержка</a></li>
            </ul>
        </div>
        <?php
        ?>
        <div class="header_user_menu">
            <div class="header_user_menu_inner slow">
                    <span class="header_user_photo"
                          style="background-image: url(<?= '/images/project/no-avatar2.png' ?>)"></span>
                <div class="header_user_drop_menu">
                    <ul>
                        <li class=""><a href="<?= Url::to(['profile']) ?>">Профиль</a></li>
                        <li class=""><a href="<?= Url::to(['change-password']) ?>">Сменить пароль</a></li>
                        <li class=""><a href="<?= Url::to(['bonus']) ?>">Реферальная программа</a></li>
                        <li><a href="<?= Url::to(['site/logout']) ?>">Выход</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<?php
/**
 * @var $user \app\models\User
 */

use yii\helpers\Url;

$action_id = Yii::$app->controller->action->id;
$user = Yii::$app->user->identity;
?>
<ul>
    <li class="<?= $action_id == 'instruction' ? "active" : null ?>"><a href="<?= Url::to(['instruction']) ?>">Инструкция</a>
    </li>
    <li class="<?= stristr($action_id, 'food') || $action_id == 'recipes' || $action_id == 'products' || $action_id == 'calories' || $action_id == 'detox-day' || $action_id == 'faq' && $_GET['type'] == \app\models\Faq::TYPE_FOOD ? "active" : null ?>">
        <a href="<?= Url::to(['food']) ?>">Питание</a>
    </li>
    <li class="<?= $action_id == 'trainings' || $action_id == 'express-exercise' || $action_id == 'recommendation' ? "active" : null ?>">
        <a href="<?= Url::to(['trainings']) ?>">Тренировки</a>
        <ul class="submenu">
            <li>
                <a href="<?= Url::to(['recommendation', 'type' => \app\models\Recommendation::RECOMMENDATION_CARDIO]) ?>">Кардио</a>
            </li>
            <li><a href="<?= Url::to(['express-exercise']) ?>">Вакуум</a></li>
        </ul>
    </li>
    <?php
    if ($user->groupMember->getPressTrainings()) {
        ?>
        <li class="<?= Yii::$app->controller->action->id == 'perfect-abs' ? "active" : null ?>"><a
                    href="<?= Url::to(['perfect-abs', 'training' => 11, 'week' => $user->groupMember->getCurrentWeek()]) ?>">
                Идеальный пресс</a></li>
        <?php
    }
    ?>
    <?php
    if (\app\helpers\My::isGirl()) {
        ?>
        <li class="<?= stristr($action_id, 'care') ? "active" : null ?>"><a
                    href="<?= Url::to(['care-instruction']) ?>">Уход
                за кожей</a></li>
        <?php
    }
    ?>
    <li class="<?= $action_id == 'progress' ? "active" : null ?>"><a href="<?= Url::to(['progress']) ?>">Прогресс</a>
    </li>
    <li class="<?= $action_id == 'report' ? "active" : null ?>"><a href="<?= Url::to(['report']) ?>">Отчеты</a>
    </li>
    <li class="<?= $action_id == 'training-calendar' ? "active" : null ?>"><a
                href="<?= Url::to(['training-calendar']) ?>">
            Календарь активности</a></li>
</ul>
